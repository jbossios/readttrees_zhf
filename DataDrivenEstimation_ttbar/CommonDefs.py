Options = {
  #'Version'     : '04102021', # using v9 TTrees and 27092021 reader outputs
  #'Version'     : '15102021', # using v9 TTrees and 13102021 reader outputs (new: CombQuantile)
  #'Version'     : '25102021', # using v9 TTrees and 20102021 reader outputs (new: XF dists for lead/sublead btagged jets)
  #'Version'     : '27102021', # using v9 TTrees and 20102021 (SR) and 27102021 (CR/AltCR) reader outputs (new: fixed trigger matching for ELMU)
  #'Version'     : '01112021', # using v9 TTrees and 29102021 (SR/CR/AltCR) reader outputs (new: new extra jet pt bin)
  #'Version'     : '09112021', # same as 01112021 but now producing files for two cases depending on signal generator used (MG or Sherpa 2211)
  #'Version'     : '15112021',  # same as 09112021 but fixed propagation of uncertainties
  #'Version'     : '10122021', # using 07122021 reader's outputs which were produced using v10 TTrees
  #'Version'     : '14022021', # using 07122021 nominal (04022022 syst) reader's outputs which were produced using v10 TTrees
  #'Version'     : '23052022', # using 19052022 nominal (20052022 syst) reader's outputs which were produced using v11 TTrees
  #'Version'     : '01052022', # using 22052022 nominal (23052022 syst) reader's outputs which were produced using v11 TTrees
  #'Version'     : '04062022', # using 22052022 nominal (23052022 syst) reader's outputs which were produced using v11 TTrees (now using all campaigns)
  #'Version'     : '13062022', # same as 04062022 but with fixed PRW uncertainty
  #'Version'     : '06072022', # fixed list of observables and fixed xF calculation (ttbar now includes dilepton)
  #'Version'     : '12072022', # removing smoothing of some components and investigating weird uncertainties (issue understood, I need to run systs on both dilepton+nonallhad)
  #'Version'     : '19072022', # Fixed uncertainties
  #'Version'     : '20072022', # smoothing uncertainties (kernel = 0.1) [terminated due to crash, not finalized]
  #'Version'     : '21072022', # fixed list of observables (systematics not smoothed), fixed systematic names for CxAOD
  #'Version'     : '22072022', # smoothing uncertainties (kernel = 0.1) [equivalent to 21072022 except for smoothing for plots, but w/o AltCR and FxFx missing to speed up things]
  #'Version'     : '23072022', # smoothing uncertainties (kernel = 0.5) [equivalent to 21072022/22072022 except for smoothing for plots, but w/o AltCR and FxFx missing to speed up things]
  #'Version'     : '16082022', # smoothing uncertainties (kernel = 0.5) [equivalent to 22072022 but using 26072022 and 27072022 inputs]
  'Version'     : '19082022', # same as 16082022 but CxAODs inputs now using SumCRs
  #'Version'     : '09092022', # same as 19082022 but TFs now depend on the PCBT bin (instead of being constant vs PCBT bins)
  'Campaign'    : 'MC16all',
  'Tagger'      : 'DL1r',
  'Channels'    : ['EL', 'MU'],
  'Campaigns'   : ['MC16a', 'MC16d', 'MC16e'],
  'FinalStates' : ['Ti1', 'Ti2'],
  'Backgrounds' : ['Zjets', 'SingleTop', 'Diboson', 'VH'],  # Zjets = Zee+Zmumu+Ztautau
  'Observables': {
    'Zpt': ['Ti1'],
    'deltaRZjHF': ['Ti1'],
    'HFjet0_pt': ['Ti1'],
    'HFjet0_XF': ['Ti1'],
    'mjjHF': ['Ti2'],
    'deltaPhijjHF': ['Ti2'],
  }
}
