##########################################################################
#                                                                         #
# Purpose: Compare nominal vs variated TFs to derive uncertainty          #
# Author: Jona Bossio (jbossios@cern.ch)                                  #
#                                                                         #
##########################################################################

###########################################################################
## DO NOT MODIFY (below this line)
###########################################################################

# Imports
import ROOT
import os
import sys
import math
import argparse

# Read common options
from CommonDefs import Options as opt

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Systematics import *
from SystematicTTrees import *

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles             import *
from PlotterHelperFunctions import *
from Luminosities           import *

def main(campaign, channel):

  Debug = False

  ControlRegion = 'CR'

  Systematics = 'AllSysts'  # options: SFonly, AllSysts

  DataYearsOptions = {
    'a': [['15', '16']],
    'd': [['17']],
    'e': [['18']],
    'all': [['15', '16', '17', '18']],
  }[campaign]

  # Loop over data year sets
  for DataYears in DataYearsOptions:

    print('Running for {} data year set... this might take long'.format(DataYears))

    Tagger = opt['Tagger']
    Version = opt['Version']
    FinalStates = opt['FinalStates']
    Observables = opt['Observables']

    ######################################################
    # Find out needed campaigns
    ######################################################
    Campaigns = []
    for year in DataYears:
      if year == '15':
        Campaigns.append('a')
      if year == '16':
        if 'a' not in Campaigns: Campaigns.append('a')
      if year == '17':
        Campaigns.append('d')
      if year == '18':
        Campaigns.append('e')

    ######################################################
    # Retrieve SFs
    ######################################################
    SFHists = dict()
    # Open input file
    if 'a' in Campaigns and 'd' in Campaigns and 'e' in Campaigns:
      cpg = ['all']
    elif len(Campaigns) == 1:
      cpg = Campaigns
    else:
      FATAL('ERROR: The provided combination of campaigns is not supported, exiting')
    extraCR = '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
    sfFileName = 'Outputs/ttbarSFs_{}_{}_MC16{}{}.root'.format(Version, channel, cpg[0], extraCR)
    sfFile = ROOT.TFile.Open(sfFileName)
    CHECKObj(sfFile,'{} not found, exiting'.format(sfFileName))
    # Loop over campaigns
    for campaign in cpg:
      SFHists[campaign] = dict()
      # Loop over channels
      SFHists[campaign][channel] = dict()
      # Loop over observables
      for obs, cases in Observables.items():
        SFHists[campaign][channel][obs] = dict()
        # Loop over event types
        for case in FinalStates:
          #if case == 'Ti1' and 'Te1' not in cases: continue # skip nonsense obs+case combination
          if case not in cases: continue
          SFHists[campaign][channel][obs][case] = dict()
          # Loop over nominal+NPs
          for source, nNPs in SFsysts.items():
            for iNP in range(1,nNPs+1):
              # Observable distribution
              extra = '_{}_{}'.format(source,iNP) if source != 'nominal' else ''
              histname = obs+'_'+case+extra+'_MC16{}_{}SF'.format(campaign, channel)
              hist     = sfFile.Get(histname)
              CHECKObj(hist,'{} not found, exiting in {}'.format(histname, sfFileName))
              hist.SetDirectory(0)
              SFHists[campaign][channel][obs][case][extra] = hist
    sfFile.Close()
    if Systematics == 'AllSysts':
      sfFileName = 'Outputs/ttbarSFs_{}_{}_MC16{}{}_FullSysts.root'.format(Version, channel, cpg[0], extraCR)
      sfFile = ROOT.TFile.Open(sfFileName)
      CHECKObj(sfFile,'{} not found, exiting'.format(sfFileName))
      # Loop over campaigns
      for campaign in cpg:
        # Loop over observables
        for obs, cases in Observables.items():
          # Loop over event types
          for case in cases:
            # Loop over systematic TTrees
            for ttree_name in SystTTrees:
              # Observable distribution
              histname = obs + '_' + case + '_MC16{}_{}SF'.format(campaign, channel)
              hist = sfFile.Get(ttree_name + '/' + histname)
              CHECKObj(hist,'{}/{} not found, exiting in {}'.format(ttree_name, histname, sfFileName))
              hist.SetDirectory(0)
              SFHists[campaign][channel][obs][case][ttree_name] = hist
      sfFile.Close()

    ########################################################################
    # Calculate relative uncertainty components (variation-nominal)/nominal
    ########################################################################
    Uncertainties = dict()
    for campaign in SFHists:
      Uncertainties[campaign] = dict()
      for channel in SFHists[campaign]:
        Uncertainties[campaign][channel] = dict()
        for obs in SFHists[campaign][channel]:
          Uncertainties[campaign][channel][obs] = dict()
          for case in SFHists[campaign][channel][obs]:
            Uncertainties[campaign][channel][obs][case] = dict()
            for np in SFHists[campaign][channel][obs][case]:
              if np == '': continue # skip nominal
              Hist = SFHists[campaign][channel][obs][case][np].Clone('MC16{}_{}_{}_{}_{}'.format(campaign,channel,obs,case,np))
              Hist.Add(SFHists[campaign][channel][obs][case][''], -1)  # subtract nominal SF
              Hist.Divide(SFHists[campaign][channel][obs][case][''])  # divide by nominal SF
              Uncertainties[campaign][channel][obs][case][np] = Hist

    #######################################################
    # Sum in quadrature all components (for plotting only)
    #######################################################
    TotalUncertainty = dict()
    for campaign in Uncertainties:
      TotalUncertainty[campaign] = dict()
      for channel in Uncertainties[campaign]:
        TotalUncertainty[campaign][channel] = dict()
        for obs in Uncertainties[campaign][channel]:
          TotalUncertainty[campaign][channel][obs] = dict()
          for case in Uncertainties[campaign][channel][obs]:
            TotalUncertainty[campaign][channel][obs][case] = dict()
            for sign in ['UP', 'DOWN']:
              Hist = SFHists[campaign][channel][obs][case][''].Clone('MC16{}_{}_{}_{}_Total{}SFuncertainty'.format(campaign,channel,obs,case,sign))
              # Loop over bins
              for ibin in range(1,Hist.GetNbinsX()+1):
                sum2 = 0
                # Loop over NPs
                for np in Uncertainties[campaign][channel][obs][case]:
                  uncert = Uncertainties[campaign][channel][obs][case][np].GetBinContent(ibin)
                  if (uncert >= 0 and sign == 'UP') or (uncert < 0 and sign == 'DOWN'):
                    sum2 += uncert * uncert
                if sign == 'UP':
                  Hist.SetBinContent(ibin, math.sqrt(sum2))
                else: # DOWN
                  Hist.SetBinContent(ibin, -1*math.sqrt(sum2))
                Hist.SetBinError(ibin,0)
              TotalUncertainty[campaign][channel][obs][case][sign] = Hist

    # Create output TFile
    OutFileName = "Outputs/SFuncertainties_{}_{}_MC16{}{}{}.root".format(Version, channel, cpg[0], extraCR, '_' + Systematics)
    OutFile = ROOT.TFile(OutFileName,"RECREATE")
    for campaign in Uncertainties:
      for channel in Uncertainties[campaign]:
        for obs in Uncertainties[campaign][channel]:
          for case in Uncertainties[campaign][channel][obs]:
            for sign in ['UP', 'DOWN']:
              TotalUncertainty[campaign][channel][obs][case][sign].Write()
            for np in Uncertainties[campaign][channel][obs][case]:
              Uncertainties[campaign][channel][obs][case][np].Write()
    OutFile.Close()

    print(">>> ALL DONE <<<")

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--ch', action='store', default='', help='options: EL, MU')
  args = parser.parse_args()
  if not args.ch:
    print('ERROR: channel (--ch) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  main('all', args.ch)
