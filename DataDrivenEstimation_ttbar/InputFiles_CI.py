# All possible input files

InputFiles = dict()  # key should match with one of the types in Samples (see below)

path_by_var = {
  'nominal' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/CI_inputs/',
  'systs' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/CI_inputs/',
  'VR' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/CI_inputs/',
}

date_by_var = {
  'nominal': '26072022',  # nominal + SF variations
  'systs': '27072022',  # systematic TTrees
  'VR' : '09112022',  # nominal + reco + VR only (all except Top) (improved VR defs)
}

###########################################################################################
# DO NOT MODIFY (below this line)
###########################################################################################

# SR for DL1r
tagger = 'DL1r'
PRW = 'usePRW'

samples_by_channel = {
  'Common' : [
    'Signal_data15161718',
    'Signal_data1516',
    'Signal_data15',
    'Signal_data16',
    'Signal_data17',
    'Signal_data18',
    'TTbarNonAllHad_MC16a',
    'TTbarNonAllHad_MC16d',
    'TTbarNonAllHad_MC16e',
    'TTbarDilepton_MC16a',
    'TTbarDilepton_MC16d',
    'TTbarDilepton_MC16e',
    'Ztautau_MC16a',
    'Ztautau_MC16d',
    'Ztautau_MC16e',
    'Diboson_MC16a',
    'Diboson_MC16d',
    'Diboson_MC16e',
    'VH_MC16a',
    'VH_MC16d',
    'VH_MC16e',
    'Wtaunu_MC16a',
    'Wtaunu_MC16d',
    'Wtaunu_MC16e',
  ],
  'MU' : [
    'SignalSH2211_MC16all',
    'SignalSH2211_MC16a',
    'SignalSH2211_MC16d',
    'SignalSH2211_MC16e',
    'SignalMG_MC16a',
    'SignalMG_MC16d',
    'SignalMG_MC16e',
    'SignalFxFx_MC16all',
    'SignalFxFx_MC16a',
    'SignalFxFx_MC16d',
    'SignalFxFx_MC16e',
    'Top_MC16a',
    'Top_MC16d',
    'Top_MC16e',
    'Wmunu_MC16a',
    'Wmunu_MC16d',
    'Wmunu_MC16e',
  ],
  'EL' : [
    'SignalSH2211_MC16all',
    'SignalSH2211_MC16a',
    'SignalSH2211_MC16d',
    'SignalSH2211_MC16e',
    'SignalMG_MC16a',
    'SignalMG_MC16d',
    'SignalMG_MC16e',
    'SignalFxFx_MC16all',
    'SignalFxFx_MC16a',
    'SignalFxFx_MC16d',
    'SignalFxFx_MC16e',
    'Top_MC16a',
    'Top_MC16d',
    'Top_MC16e',
    'Wenu_MC16a',
    'Wenu_MC16d',
    'Wenu_MC16e',
  ],
  'ELMU' : [
    'SingleTop_MC16a',
    'SingleTop_MC16d',
    'SingleTop_MC16e',
    'ZmumuSH2211_MC16a',
    'ZmumuSH2211_MC16d',
    'ZmumuSH2211_MC16e',
    'ZmumuMG_MC16a',
    'ZmumuMG_MC16d',
    'ZmumuMG_MC16e',
    'ZmumuFxFx_MC16a',
    'ZmumuFxFx_MC16d',
    'ZmumuFxFx_MC16e',
    'ZeeSH2211_MC16a',
    'ZeeSH2211_MC16d',
    'ZeeSH2211_MC16e',
    'ZeeMG_MC16a',
    'ZeeMG_MC16d',
    'ZeeMG_MC16e',
    'ZeeFxFx_MC16a',
    'ZeeFxFx_MC16d',
    'ZeeFxFx_MC16e',
    'Wmunu_MC16a',
    'Wmunu_MC16d',
    'Wmunu_MC16e',
    'Wenu_MC16a',
    'Wenu_MC16d',
    'Wenu_MC16e',
  ],
}

def get_campaign(sample):
  if 'MC16all' in sample:
    return 'MC16all'
  elif 'MC16a' in sample:
    return 'MC16a'
  elif 'MC16d' in sample:
    return 'MC16d'
  elif 'MC16e' in sample:
    return 'MC16e'

def translate_sample(sample, channel):
  if 'data' in sample:
    return {'data1516': 'Data1516', 'data15161718': 'Data15161718', 'data15': 'Data15', 'data16': 'Data16', 'data17': 'Data17', 'data18': 'Data18'}[sample.split('_')[1]]
  elif 'Signal' in sample:
    if channel == 'MU':
      if 'SH2211' in sample:
        return 'Zmumu{}Sherpa2211'.format(get_campaign(sample))
      elif 'MG' in sample:
        return 'Zmumu{}MG'.format(get_campaign(sample))
      elif 'FxFx' in sample:
        return 'Zmumu{}FxFx'.format(get_campaign(sample))
    elif channel == 'EL':
      if 'SH2211' in sample:
        return 'Zee{}Sherpa2211'.format(get_campaign(sample))
      elif 'MG' in sample:
        return 'Zee{}MG'.format(get_campaign(sample))
      elif 'FxFx' in sample:
        return 'Zee{}FxFx'.format(get_campaign(sample))
  elif channel == 'ELMU' and ('Zee' in sample or 'Zmumu' in sample):
    if 'SH2211' in sample:
      if 'Zee' in sample:
        return 'Zee{}Sherpa2211'.format(get_campaign(sample))
      elif 'Zmumu' in sample:
        return 'Zmumu{}Sherpa2211'.format(get_campaign(sample))
    elif 'MG' in sample:
      if 'Zee' in sample:
        return 'Zee{}MG'.format(get_campaign(sample))
      elif 'Zmumu' in sample:
        return 'Zmumu{}MG'.format(get_campaign(sample))
    elif 'FxFx' in sample:
      if 'Zee' in sample:
        return 'Zee{}FxFx'.format(get_campaign(sample))
      elif 'Zmumu' in sample:
        return 'Zmumu{}FxFx'.format(get_campaign(sample))
  else:
    sample_type = sample.split('_')[0]
    return '{}{}'.format(get_campaign(sample), sample_type)
  return 'NotImplemented'

configurations = ['MU_SR', 'EL_SR', 'ELMU_CR', 'ELMU_AltCR', 'EL_SRa1jet', 'MU_SRa1jet', 'EL_VR', 'MU_VR', 'EL_SR2', 'MU_SR2']

for var in date_by_var:
  # extra string for dict key
  extra = ''
  if var == 'systs':
    extra = '_FullSysts'
  elif var == 'truth':
    extra = '_truth'
  elif var == 'mcsysts':
    extra = '_mcsysts'
  for conf in configurations:
    if var == 'VR' and 'VR' not in conf:
      continue
    elif var != 'VR' and 'VR' in conf:
      continue
    conf_channel = conf.split('_')[0]
    conf_selection = conf.split('_')[1]
    if conf_selection != 'SR2':
      for sample in samples_by_channel['Common']:
        if 'data' in sample and var != 'nominal' and var != 'truth' and var != 'VR': continue  # skip data for systematic variations (truth for data actually only have flavour dists)
        if 'data' in sample:
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, date_by_var[var])
        else: # MC
          extra_file_name = 'FullSysts' if var == 'systs' else 'nominalOnly'
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_useSFs_useSampleWeights_{}_{}_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, PRW, extra_file_name, date_by_var[var])
      extra_samples = samples_by_channel[conf_channel]
      if var == 'VR':
        extra_samples += [
          'SingleTop_MC16a',
          'SingleTop_MC16d',
          'SingleTop_MC16e',
        ]
      for sample in extra_samples:
        if 'data' in sample and var != 'nominal' and var != 'truth': continue  # skip data for systematic variations (truth for data actually only have flavour dists)
        if 'data' in sample:
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, date_by_var[var])
        else: # MC
          extra_file_name = 'FullSysts' if var == 'systs' else 'nominalOnly'
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_useSFs_useSampleWeights_{}_{}_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, PRW, extra_file_name, date_by_var[var])
    else:  # SR2
      extra_samples += [
        'TTbarNonAllHad_MC16a',
        'TTbarNonAllHad_MC16d',
        'TTbarNonAllHad_MC16e',
      ]
      for sample in extra_samples:
        extra_file_name = 'nominalOnly'
        InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_useSFs_useSampleWeights_{}_{}_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, PRW, extra_file_name, date_by_var[var])


if __name__ == '__main__':
  for key in InputFiles:
    print('InputFiles[{}] = {}'.format(key, InputFiles[key]))
