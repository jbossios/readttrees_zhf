import ROOT

def smooth_hist(hist, log, use_error = False):
  kernel = 0.5
  nbins = hist.GetNbinsX()
  xs = [hist.GetBinCenter(ibin) for ibin in range(1, nbins + 1)]
  ys = [hist.GetBinContent(ibin) for ibin in range(1, nbins + 1)]
  yes = [hist.GetBinError(ibin) for ibin in range(1, nbins + 1)]
  # Loop over bins to be smoothed
  for ibin in range(1, nbins + 1):
    x = xs[ibin - 1]
    y = ys[ibin - 1]
    sumw = 0
    sumwy = 0
    for xi, yi, yei in zip(xs, ys, yes):
      if yi == 0:
        continue
      dx = (x - xi) / kernel
      if log:
        dx /= x
      wi = ROOT.TMath.Gaus(dx)
      if use_error:
        wi *= 1.0/(yei*yei) if yei != 0 else 1.0
      sumw += wi
      sumwy += wi * yi
    hist.SetBinContent(ibin, sumwy / sumw if sumw else 0.0)
    hist.SetBinError(ibin, 0.0)  # error is set to zero
  return hist
