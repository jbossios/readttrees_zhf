def getJVTSFvarNames() -> tuple[list, str]:
  """Get JVT SF variation names"""
  # Import things from Reader
  import os,sys,ROOT
  sys.path.insert(1, '../') # insert at 1, 0 is the script path
  from Systematics import SFsysts
  cases = {
    1: 'up',
    2: 'down',
  }
  return ['SysJET_JvtEfficiency__1{}'.format(cases[inp]) for inp in range(1, SFsysts['JVT']+1)], 'OK'


def getPRWvarNames() -> tuple[list, str]:
  """Get PRW variation names"""
  # Import things from Reader
  import os,sys,ROOT
  sys.path.insert(1, '../') # insert at 1, 0 is the script path
  from Systematics import SFsysts
  cases = {
    1: 'up',
    2: 'down',
  }
  return ['SysPRW_DATASF__1{}'.format(cases[inp]) for inp in range(1, SFsysts['weight_pileup']+1)], 'OK'


def getBTagSFvarNames() -> tuple[list, str]:
  """Get b-tagging SF variation names"""
  # Import things from Reader
  import os,sys,ROOT
  sys.path.insert(1, '../') # insert at 1, 0 is the script path
  from InputLists      import Inputs
  from HelperFunctions import getDSID
  # Loop over samples
  for sample in Inputs['PFlow']:
    if 'Data' in sample: continue # skip metadata from data
    if 'MC16a' not in sample: continue # skip other MC16 campaigns
    PATHs = Inputs['PFlow'][sample] # get list of paths where folders with metadata files are located
    for path in PATHs: # loop over paths
      for folder in os.listdir(path): # loop over folders from each path
        if '_metadata.root' not in folder: continue # skip cutflows/trees
        # open metadata file
        key              = int(getDSID(folder))
        MetadataFileName = '{}{}/{}_metadata.root'.format(path,folder,key)
        MetadataFile     = ROOT.TFile.Open(MetadataFileName)
        if not MetadataFile:
          return [],'{} file not found, exiting'.format(MetadataFileName)
        # get systematics directory
        sysDir = MetadataFile.Get('systematics')
        # get histogram
        FTAGSystNamesHist = sysDir.Get("BJetEfficiency_Algo_DL1r_Continuous")
        FTAGSystNamesHist.SetDirectory(0)
        MetadataFile.Close()
        # return variation names
        return ['Sys'+FTAGSystNamesHist.GetXaxis().GetBinLabel(ibin) for ibin in range(2,FTAGSystNamesHist.GetNbinsX()+1)], 'OK'


def getLeptonSFvarNames() -> tuple[list, str]:
  """Get lepton SF variation names"""
  DirNames = [
    'MuonEfficiencyCorrector_RecoSyst_RecoMedium',
    'MuonEfficiencyCorrector_IsoSyst_IsoFCTight_FixedRad',
    'MuonEfficiencyCorrector_TrigSyst_RecoMedium',
    'MuonEfficiencyCorrector_TTVASyst_TTVA',
    'EleEffCorr_PIDSyst_Tight',
    'EleEffCorr_IsoSyst_Tight_isolTight',
    'EleEffCorr_RecoSyst_Reconstruction',
    'EleEffCorr_TrigSyst_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolTight',
  ]
  # Import things from Reader
  import os,sys,ROOT
  sys.path.insert(1, '../') # insert at 1, 0 is the script path
  from InputLists      import Inputs
  from HelperFunctions import getDSID
  SFsystNames = []
  # Loop over samples
  for sample in Inputs['PFlow']:
    if 'Data' in sample: continue # skip metadata from data
    if 'MC16a' not in sample: continue # skip other MC16 campaigns
    PATHs = Inputs['PFlow'][sample] # get list of paths where folders with metadata files are located
    for path in PATHs: # loop over paths
      for folder in os.listdir(path): # loop over folders from each path
        if '_metadata.root' not in folder: continue # skip cutflows/trees
        # open metadata file
        key              = int(getDSID(folder))
        MetadataFileName = '{}{}/{}_metadata.root'.format(path,folder,key)
        MetadataFile     = ROOT.TFile.Open(MetadataFileName)
        if not MetadataFile:
          return [],'{} file not found, exiting'.format(MetadataFileName)
        # get systematics directory
        sysDir = MetadataFile.Get('systematics')
        if not sysDir:
          print('ERROR: systematics not found on {}'.format(MetadataFileName))
        # Loop over histograms (sources for electrons and muons)
        for DirName in DirNames:
          FTAGSystNamesHist = sysDir.Get(DirName)
          SFsystNames += ['Sys'+FTAGSystNamesHist.GetXaxis().GetBinLabel(ibin) for ibin in range(2,FTAGSystNamesHist.GetNbinsX()+1)]
        MetadataFile.Close()
        # return variation names
        return SFsystNames, 'OK'

if __name__ == '__main__':
  print('Names of b-tagging SF variations:')
  print(getBTagSFvarNames()[0])
  print('\nNames of lepton SF variations:')
  print(getLeptonSFvarNames()[0])
  print('\nNames of JVT SF variations:')
  print(getJVTSFvarNames()[0])
  print('\nNames of PRW variations:')
  print(getPRWvarNames()[0])
