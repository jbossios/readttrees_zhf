##################################################################################
#                                                                                 #
# Purpose: Run scripts used to obtain a data-driven ttbar estimation for a CI job #
# Author:  Jona Bossio (jbossios@cern.ch)                                         #
#                                                                                 #
##################################################################################

# Import Python modules
import ROOT
import os
import sys
import copy
import argparse
import math
import logging
import time
from multiprocessing import Pool

# Import common options
from CommonDefs_CI import Options as opt

# Import functions to get SF-based variation names
from getSFvarNames import getBTagSFvarNames, getLeptonSFvarNames, getJVTSFvarNames, getPRWvarNames

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Systematics import *
from SystematicTTrees import *
from Dicts import Binning
from InputLists import Inputs
from HelperFunctions import *

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles             import *
from PlotterHelperFunctions import *
from Luminosities           import *

from InputFiles_CI import InputFiles as custom_input_files

if __name__ == '__main__':

    # Check if user asked to update references
    parser = argparse.ArgumentParser()
    parser.add_argument('--updateRefs', dest = 'update_refs', action = 'store_true', default = False)
    args = parser.parse_args()
    if args.update_refs:
        opt['Version'] = 'CIref'

    ###########
    # Settings

    # Chose steps to run
    steps = [
        1,
        2,
        3,
        4,
        5,
        6,
    ]

    campaign = opt['Campaign'].replace('MC16', '')
    crs = [
        'CR',
    ]
    channels = ['EL']
    propagate_systematics = True
    ttrees_options = ['nominal', 'FullSysts']
    smooth_uncertainties = True
    tf_vs_pcbt = False  # Use TFs vs PCBT (instead of constant TFs vs PCBT when preparing data-driven ttbar predictions on PCBT distributions)
    
    generators = ('SH2211',)  # signal generators (first is considered nominal)
    use_mc16all_sfs = False
    nominal_cr = 'CR'
    debug = False
    ttbar_samples = 'All'  # options: All (NonAllHad+Dilepton), NonAllHad, Dilepton

    # validation
    target_regions = {'VR': ['EL']}  # CR is like SR but dominated by ttbar
    val_crs = {'CR': 'ELMU'}

    ###########

    # Start timer
    timer = {0: time.perf_counter()}

    # Create 'Outputs' folder
    if not args.update_refs and not os.path.exists('Outputs'):
        os.makedirs('Outputs')

    # 1. Run GetSFs.py
    if 1 in steps:
        from GetSFs import main as get_sfs
        for gen in generators:
            for cr in crs:
                for ch in channels:
                    for ttrees in  ttrees_options:
                        if ttrees == 'FullSysts' and cr != nominal_cr:
                            continue
                        print(f'INFO: Running GetSFs.py w/ cr={cr}, ch={ch}, campaign={campaign} and ttrees={ttrees}...')
                        get_sfs(
                            ControlRegion = cr,
                            channel = ch,
                            MC16Campaigns = campaign,
                            Debug = debug,
                            PropagateSystematics = propagate_systematics,
                            SignalGenerator = gen,
                            TTrees = ttrees,
                            ttbar_samples = ttbar_samples,
                            custom_options = opt,
                            CustomInputFiles = custom_input_files,
                            output_folder = 'Outputs' if not args.update_refs else opt['PathToRefs'],
                        )
    timer[1] = time.perf_counter()

    # 2. Run GetTTbarContribution.py
    if 2 in steps:
        from GetTTbarContribution import main as get_ttbar_contribution
        for gen in generators:
            for cr in crs:
                for ch in channels:
                    for ttrees in  ttrees_options:
                        if ttrees == 'FullSysts' and cr != nominal_cr:
                            continue
                        print(f'INFO: Running GetTTbarContribution.py w/ cr={cr}, ch={ch}, campaign={campaign} and ttrees={ttrees}...')
                        get_ttbar_contribution(
                            Campaign = campaign,
                            ControlRegion = cr,
                            channel = ch,
                            TTrees = ttrees,
                            PropagateSFuncertainties = propagate_systematics,
                            UseAlwaysMC16all = use_mc16all_sfs,
                            SignalGenerator = gen,
                            tf_vs_pcbt = tf_vs_pcbt,
                            Debug = debug,
                            custom_options = opt,
                            output_folder = 'Outputs' if not args.update_refs else opt['PathToRefs'],
                        )
    timer[2] = time.perf_counter()

    # 3. Run DeriveTTbarContributionSystematics.py
    if 3 in steps:
        from DeriveTTbarContributionSystematics import main as derive_systs
        from CommonDefs import Options
        gen = generators[0]  # uncertainties derived for nominal generator only
        version_to_use = opt['Version']
        for ch in channels:
            output_file_name = f'TTbarUncertainties_Signal{gen}_{version_to_use}_{ch}_MC16{campaign}_AllSysts{"_smoothed" if smooth_uncertainties else ""}.root'
            print(f'INFO: Running DeriveTTbarContributionSystematics w/ ch={ch} and campaign={campaign}')
            derive_systs(
                campaign = campaign,
                channel = ch,
                SignalGenerator = gen,
                ControlRegion = nominal_cr,
                smooth = smooth_uncertainties,
                Debug = False,
                custom_options = opt,
                output_folder = 'Outputs' if not args.update_refs else opt['PathToRefs'],
            )
    timer[3] = time.perf_counter()

    # 4. Run GetValTFs.py
    if 4 in steps:
        from GetValTFs import main as get_tfs
        for gen in generators:
            for cr, channel in val_crs.items():
                for target_region, target_channels in target_regions.items():
                    for target_channel in target_channels:
                        print(f'INFO: Running GetValTFs.py w/ base_cr={cr} in channel={channel}, target_region={target_region} in channel={target_channel}, campaign={campaign} and ttree="nominal"...')
                        get_tfs(
                            base_cr = cr,
                            cr_channel = channel,
                            target_region = target_region,
                            target_channel = target_channel,
                            MC16Campaigns = campaign,
                            Debug = debug,
                            SignalGenerator = gen,
                            TTree = "nominal",
                            ttbar_samples = ttbar_samples,
                            custom_options = opt,
                            CustomInputFiles = custom_input_files,
                            output_folder = 'Outputs' if not args.update_refs else opt['PathToRefs'],
                        )
    timer[4] = time.perf_counter()

    ## 5. Run GetTTbarContribution.py
    if 5 in steps:
        from GetValTTbarContribution import main as get_ttbar_contribution
        for gen in generators:
            for cr, channel in val_crs.items():
                for target_region, target_channels in target_regions.items():
                    for target_channel in target_channels:
                      print(f'INFO: Running GetValTTbarContribution.py w/ cr={cr} in channel={channel}, target_region={target_region} in channel={target_channel} and campaign={campaign}...')
                      get_ttbar_contribution(
                          Campaign = campaign,
                          CR = cr,
                          CR_Channel = channel,
                          TargetRegion = target_region,
                          TargetChannel = target_channel,
                          UseAlwaysMC16all = use_mc16all_sfs,
                          SignalGenerator = gen,
                          Debug = debug,
                          custom_options = opt,
                          CustomInputFiles = custom_input_files,
                          output_folder = 'Outputs' if not args.update_refs else opt['PathToRefs'],
                      )
    timer[5] = time.perf_counter()

    # 6. Run tester
    if 6 in steps and not args.update_refs:
        from checker_ddt import compare_to_ref
        for gen in generators:
            for channel in channels:
                compare_to_ref(
                    campaign = campaign,
                    channel = channel,
                    signal_generator = gen,
                    smooth_uncertainties = smooth_uncertainties,
                )
    timer[6] = time.perf_counter()

    print('>>> run_all_ci.py: ALL DONE <<<')
    for step in steps:
        print('Time spent on step {}: {} seconds'.format(step, timer[step]-timer[step - 1]))
    print('Total time: {} seconds'.format(timer[6]-timer[0]))
