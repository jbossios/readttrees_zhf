##########################################################################
#                                                                         #
# Purpose: Data-driven method to estimate ttbar contribution              #
# Author: Jona Bossio (jbossios@cern.ch)                                  #
#                                                                         #
# Procedure: subtract non-ttbar backgrounds from data in CR and apply TF  #
#                                                                         #
##########################################################################

# Imports
import ROOT
import os
import sys
import argparse
import copy
from CommonDefs import Options as opt

# Import things from Reader
sys.path.insert(1, '../')  # insert at 1, 0 is the script path
from HelperFunctions import get_xrootd_prefix

# Import things from Plotter
sys.path.insert(1, '../Plotter/')  # insert at 1, 0 is the script path
from InputFiles import *
from PlotterHelperFunctions import *
from Luminosities import *


def main(
    Campaign,
    CR,
    CR_Channel,
    TargetRegion,
    TargetChannel,
    UseAlwaysMC16all,
    SignalGenerator,
    Debug,
    custom_options = None,
    CustomInputFiles = None,
    output_folder = 'Outputs',
  ):

  DataYearsOptions = {
    'a': [['15', '16']],
    'd': [['17']],
    'e': [['18']],
    'all': [['15', '16', '17', '18']],
  }[Campaign]

  # Protections
  if SignalGenerator not in ['SH2211', 'MG', 'FxFx']:
    FATAL('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))
  if CR not in ['CR', 'AltCR']:
    FATAL('CR ({}) not recognized, exiting'.format(CR))
  if TargetRegion not in ['CR', 'AltCR', 'SR', 'VR']:
    FATAL('TargetRegion ({}) not recognized, exiting'.format(TargetRegion))
  if CR == TargetRegion:
    FATAL('CR ({}) should not match TargetRegion ({}), exiting'.format(CR, TargetRegion))

  # Get dict with input ROOT files
  InputFiles = InputFiles if not CustomInputFiles else CustomInputFiles

  # Read common options
  Tagger = opt['Tagger'] if not custom_options else custom_options['Tagger']
  Version = opt['Version'] if not custom_options else custom_options['Version']
  FinalStates = opt['FinalStates'] if not custom_options else custom_options['FinalStates']
  Backgrounds = opt['Backgrounds'] if not custom_options else custom_options['Backgrounds']
  Observables = opt['Observables'] if not custom_options else custom_options['Observables']

  # Replace Zjets from Backgrounds list by Zee/Zmumu/Ztautau
  if 'Zjets' in Backgrounds:
    Backgrounds.remove('Zjets')
    Backgrounds.append('Zee{}'.format(SignalGenerator))
    Backgrounds.append('Zmumu{}'.format(SignalGenerator))
    Backgrounds.append('Ztautau')

  def get_input_file(input_files_dict, key):
    """ Get already opened input file or open it if not opened already """
    # protection
    if key not in InputFiles:
      msg = f"{key} not found in InputFiles dictionary, exiting"
      FATAL(msg)
    if key not in input_files_dict:  # open file
      input_file_name = InputFiles[key]
      input_file_name = get_xrootd_prefix(input_file_name) + input_file_name
      input_file = ROOT.TFile.Open(input_file_name)
      CHECKObj(input_file, f'{input_file_name} not found, exiting')
      input_files_dict[key] = input_file
    return input_files_dict, input_files_dict[key]

  # Loop over data year sets
  for DataYears in DataYearsOptions:

    print('Running for {} data year set... this might take long'.format(DataYears))

    ######################################################
    # Find out needed campaigns
    ######################################################
    Campaigns = []
    if not UseAlwaysMC16all:
      for year in DataYears:
        if year == '15':
          Campaigns.append('a')
        if year == '16':
          if 'a' not in Campaigns: Campaigns.append('a')
        if year == '17':
          Campaigns.append('d')
        if year == '18':
          Campaigns.append('e')
    else:
      Campaigns = ['a', 'd', 'e']

    ######################################################
    # Retrieve TFs
    ######################################################
    TFHists = dict()
    # Open input file
    if 'a' in Campaigns and 'd' in Campaigns and 'e' in Campaigns:
      cpg = ['all']
    elif len(Campaigns) == 1:
      cpg = Campaigns
    else:
      FATAL('ERROR: The provided combination of campaigns is not supported, exiting')
    extraOutname = '{}_{}_to_{}_{}'.format(CR, CR_Channel, TargetRegion, TargetChannel)
    extraGen = '' if SignalGenerator == 'SH2211' else '_{}'.format(SignalGenerator)
    inputFileName = '{}/ttbarTFs_{}_{}_MC16{}_{}{}.root'.format(output_folder, Version, TargetChannel, cpg[0], extraOutname, extraGen)
    tfFile = ROOT.TFile.Open(inputFileName)
    if not tfFile:
      FATAL('ERROR: {} not found, exiting'.format(inputFileName, cpg[0]))
    # Loop over campaigns
    for campaign in cpg:
      TFHists[campaign] = dict()
      # Loop over observables
      for obs, cases in Observables.items():
        TFHists[campaign][obs] = dict()
        # Loop over event types
        for case in cases:
          # Observable distribution
          histname = obs + '_' + case + '_MC16{}_{}{}TF'.format(campaign, TargetRegion, TargetChannel)
          hist = tfFile.Get(histname)
          CHECKObj(hist, '{} not found in {}, exiting'.format(histname, inputFileName))
          hist.SetDirectory(0)
          TFHists[campaign][obs][case] = hist
    tfFile.Close()

    # Add b-tagging quantile vs obs (Temporary, I need to get TH2 TF histograms to be able to do this) [FIXME/TODO]
    FinalObservables = copy.deepcopy(Observables)
    #for obs, cases in Observables.items():
    #  if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = cases
    #  if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

    #########################################################################################
    # Prepare ttbar contribution in TargetRegion separately for data15+16, data17 and data18
    #########################################################################################
    DataDict = dict()
    if len(Campaigns) == 1:
      if '15' in DataYears and '16' in DataYears: DataDict['1516'] = ['15','16']
      elif '15' in DataYears:                     DataDict['15']   = ['15']
      elif '16' in DataYears:                     DataDict['16']   = ['16']
      if '17' in DataYears:                       DataDict['17']   = ['17']
      if '18' in DataYears:                       DataDict['18']   = ['18']
    else:
      if   '15' in DataYears and '16' in DataYears and '17' in DataYears and '18' in DataYears: DataDict['15161718'] = ['15','16','17','18']
      elif '15' in DataYears and '16' in DataYears:                                             DataDict['1516']     = ['15','16']
      elif '17' in DataYears:                                                                   DataDict['17']       = ['17']
      elif '18' in DataYears:                                                                   DataDict['18']       = ['18']
    for key, DataPeriods in DataDict.items():

      if Debug: print('DEBUG: Prepare inputs for '+key+' period')

      ############################
      # Prepare data distribution
      ############################

      campaigns = []
      Luminosities = dict()
      Luminosities['a'] = 0
      Luminosity = 0
      for period in DataPeriods:
        if period == '15':
          campaigns.append('a')
          Luminosity += Luminosity_2015
          Luminosities['a'] += Luminosity_2015
        elif period == '16':
          Luminosity += Luminosity_2016
          if 'a' not in campaigns: campaigns.append('a')
          Luminosities['a'] += Luminosity_2016
        elif period == '17':
          Luminosity += Luminosity_2017
          campaigns.append('d')
          Luminosities['d'] = Luminosity_2017
        elif period == '18':
          Luminosity += Luminosity_2018
          campaigns.append('e')
          Luminosities['e'] = Luminosity_2018

      # Protection
      if 'a' in campaigns and 'd' in campaigns and 'e' in campaigns:
        cpg = 'all'
      elif len(campaigns) == 1:
        cpg = campaigns[0]
      else:
        FATAL('ERROR: The provided combination of campaigns is not supported, exiting')

      if Debug: print('DEBUG: Total luminosity: '+str(Luminosity))
      if Debug: print('DEBUG: Luminosities dict: ')
      if Debug: print(Luminosities)

      # Create output TFile
      OutFileName = output_folder + "/ttbarContribution_Signal"+SignalGenerator+"_"+Version+"_data"+key+'_'+extraOutname+'.root'
      OutFile = ROOT.TFile(OutFileName, "RECREATE")
      print('Writing outputs to {}'.format(OutFileName))

      # Loop over distributions
      if Debug: print("DEBUG: Loop over distributions")

      # Collect opened files
      input_files = {}

      # Loop over observables
      for obs, cases in FinalObservables.items():
        # Loop over event types
        for case in cases:
          histname = obs + '_' + case
          #################################
          # Get subtracted data
          hDataDict = dict()

          # Loop over data-taking periods
          if Debug: print("DEBUG: Loop over data-taking periods")
          DataHists = []
          for period in DataPeriods:
            if Debug: 'ERROR: Get histogram('+histname+') for data'+period
            # Open input file
            Key = f"Signal_data{period}_{CR_Channel}_{CR}_{Tagger}"
            input_files, File = get_input_file(input_files, Key)
            # Get histogram
            Hist = File.Get(histname)
            CHECKObj(Hist, histname+" not found on {}, exiting".format(InputFiles[Key]))
            Hist.SetDirectory(0)
            DataHists.append(Hist)

          if Debug: 'DEBUG: DatHists dict:'
          if Debug: print(DataHists)

          # Get total data histogram for this CR
          if Debug: print("DEBUG: Get total data histogram")
          hData = DataHists[0].Clone("Data_" + histname + '_' + CR)
          hData.SetDirectory(0)
          for ihist in range(1, len(DataHists)):
            hData.Add(DataHists[ihist])

          # Get background estimations
          if Debug: print("DEBUG: Get background estimations")
          hBkgs = []
          for campaign in campaigns:
            for bkg in Backgrounds:
              # Open File
              Key = f"{bkg}_MC16{campaign}_{CR_Channel}_{CR}_{Tagger}"
              input_files, File = get_input_file(input_files, Key)
              # Get histogram
              Hist = File.Get(histname)
              CHECKObj(Hist,histname+" not found on {}, exiting".format(InputFiles[Key]))
              Hist.SetDirectory(0)
              Hist.Scale(Luminosities[campaign])
              hBkgs.append(Hist)

          # Subtract non-ttbar samples to the total data
          if Debug: print("DEBUG: Subtract non-Z backgrounds to the total data")
          for hist in hBkgs:
            hData.Add(hist, -1)
          
          # Apply TF to data
          if Debug: print("DEBUG: Apply TF to get prediction for {} {}".format(TargetRegion, TargetChannel))
          hFinalData = hData.Clone('ttbar_' + TargetRegion + '_' + TargetChannel + '_' + histname)
          #var = obs
          #if 'Quantile' in obs:
          #  var = obs.split('_vs_')[1]
          #print(f'TFhist[{var}] = {TFHists[cpg if not UseAlwaysMC16all else "all"][var][case]}')
          hFinalData.Multiply(TFHists[cpg if not UseAlwaysMC16all else 'all'][obs][case])

          # Write ttbar predictions to file
          OutFile.cd()
          hFinalData.Write()

      # Close output file
      OutFile.Close()

      # Close input files
      for _, input_file in input_files.items():
        input_file.Close()

  print(">>> ALL DONE <<<")


if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--cr', action='store', default='', help='options: CR and AltCR')
  parser.add_argument('--channelCR', action='store', default='', help='options: ELMU')
  parser.add_argument('--targetRegion', action='store', default='', help='options: CR, AltCR, SR and VR')
  parser.add_argument('--targetChannel', action='store', default='', help='options: ELMU, EL or MU (depending on targetRegion)')
  parser.add_argument('--campaign', action='store', default='', help='options: a, d, e, all')
  parser.add_argument('--debug', action='store_true', default=False, help='Enable debugging')
  args = parser.parse_args()

  if not args.cr:
    print('ERROR: ControlRegion was not provided (--cr is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.channelCR:
    print('ERROR: Channel for ControlRegion was not provided (--channelCR is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.targetRegion:
    print('ERROR: Target region was not provided (--targetRegion is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.targetChannel:
    print('ERROR: Channel for target region was not provided (--targetChannel is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.campaign:
    print('ERROR: campaign not provided, exiting')
    parser.print_help()
    sys.exit(1)

  Campaign = args.campaign
  Debug = args.debug
  ControlRegion = args.cr
  CR_Channel = args.channelCR
  TargetRegion = args.targetRegion
  TargetChannel = args.targetChannel
  TTrees = args.ttrees
  tf_vs_pcbt = args.vsPCBT
  if ControlRegion == 'AltCR':
    UseAlwaysMC16all = True  # used when not using full Run 2 data (Temporary?)
  else:  # Temporary
    #UseAlwaysMC16all = False  # used when not using full Run 2 data (Temporary?)
    UseAlwaysMC16all = True  # used when not using full Run 2 data (Temporary?)

  # Generator to be used for signal
  SignalGenerator = 'SH2211' # options: MG, SH2211 and FxFx

  print('INFO: Running GetValTTbarContribution.py with CR={} in {CR_Channel}, TargetRegion={TargetRecion} in {TargetChannel} and campaign={}'.format(ControlRegion, CR_Channel, TargetRegion, TargetChannel, Campaign))

  # NOTE: Other options are set in CommonDefs.py
  main(Campaign, ControlRegion, CR_Channel, TargetRegion, TargetChannel, UseAlwaysMC16all, SignalGenerator, Debug)
