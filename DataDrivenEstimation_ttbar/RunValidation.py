#####################################################################
#                                                                    #
# Purpose: Run scripts used to validate data-driven ttbar estimation #
# Author:  Jona Bossio (jbossios@cern.ch)                            #
#                                                                    #
#####################################################################

# Import Python modules
import ROOT
import os
import sys
import copy
import argparse
import math
import logging
import time
from multiprocessing import Pool

# Import common options
from CommonDefs import Options as opt

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import Binning
from InputLists import Inputs
from HelperFunctions import *

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles             import *
from PlotterHelperFunctions import *
from Luminosities           import *


if __name__ == '__main__':

    ###########
    # Settings

    # Chose steps to run
    steps = [
    #    1,
    #    2,
        3,
    #    4,
    #    5,
    ]

    campaigns = ['all', 'a', 'd', 'e']
    crs = {
        'CR': 'ELMU',
    }
    target_regions = {
        'VR': ['MU', 'EL']  # VR is like SR but dominated by ttbar
    }
    ttree = 'nominal'
    generators = ('SH2211',)  # signal generators (first is considered nominal)
    use_mc16all_sfs = True
    debug = False
    ttbar_samples = 'All'  # options: All (NonAllHad+Dilepton), NonAllHad, Dilepton

    ###########

    # Start timer
    timer = {0: time.perf_counter()}

    # 1. Run GetValTFs.py
    if 1 in steps:
        from GetValTFs import main as get_tfs
        for gen in generators:
            for campaign in campaigns:
                for cr, channel in crs.items():
                    for target_region, target_channels in target_regions.items():
                        for target_channel in target_channels:
                            print(f'INFO: Running GetValTFs.py w/ base_cr={cr} in channel={channel}, target_region={target_region} in channel={target_channel}, campaign={campaign} and ttree={ttree}...')
                            get_tfs(cr, channel, target_region, target_channel, campaign, debug, gen, ttree, ttbar_samples)
    timer[1] = time.perf_counter()

    # 2. Run GetTTbarContribution.py
    if 2 in steps:
        from GetValTTbarContribution import main as get_ttbar_contribution
        for gen in generators:
            for campaign in campaigns:
                for cr, channel in crs.items():
                    for target_region, target_channels in target_regions.items():
                        for target_channel in target_channels:
                          print(f'INFO: Running GetValTTbarContribution.py w/ cr={cr} in channel={channel}, target_region={target_region} in channel={target_channel} and campaign={campaign}...')
                          get_ttbar_contribution(campaign, cr, channel, target_region, target_channel, use_mc16all_sfs, gen, debug)
    timer[2] = time.perf_counter()

    # 3. Make validation plots and derive extrapolation uncertainty
    if 3 in steps:
        # Import things from Plotter
        sys.path.insert(1, 'Plotter/')  # insert at 1, 0 is the script path
        from MakeValidationPlots import main as make_plots
        for gen in generators:
            for campaign in campaigns:
                for cr, channel in crs.items():
                    for target_region, target_channels in target_regions.items():
                        for target_channel in target_channels:
                          print(f'INFO: Running MakeValidationPlots.py w/ cr={cr} in {channel}, target_region={target_region} in {target_channel} and campaign={campaign}...')
                          make_plots(campaign, cr, channel, target_region, target_channel, gen, debug)
    timer[3] = time.perf_counter()

    # 4. Run GetDataBasedTFs.py
    #if 4 in steps:
    #    from GetDataBasedTFs import main as get_datadriven_tfs
    #    for gen in generators:
    #        for campaign in campaigns:
    #            for cr, channel in crs.items():
    #                for target_region, target_channels in target_regions.items():
    #                    if target_region != 'VR':
    #                        continue
    #                    for target_channel in target_channels:
    #                        print(f'INFO: Running GetValTFs.py w/ base_cr={cr} in channel={channel}, target_region={target_region} in channel={target_channel}, campaign={campaign} and ttree={ttree}...')
    #                        get_datadriven_tfs(cr, channel, target_region, target_channel, campaign, debug, gen, ttree)
    timer[4] = time.perf_counter()

    # 5. Make TF validation plots
    #if 5 in steps:
    #    # Import things from Plotter
    #    sys.path.insert(1, 'Plotter/')  # insert at 1, 0 is the script path
    #    from CompareTFs_data_vs_MC import main as make_plots
    #    for gen in generators:
    #        for campaign in campaigns:
    #            for cr, channel in crs.items():
    #                for target_region, target_channels in target_regions.items():
    #                    if target_region != 'VR':
    #                        continue
    #                    for target_channel in target_channels:
    #                      print(f'INFO: Running CompareTFs_data_vs_MC.py w/ cr={cr} in {channel}, target_region={target_region} in {target_channel} and campaign={campaign}...')
    #                      make_plots(campaign, cr, channel, target_region, target_channel)
    timer[5] = time.perf_counter()
    
    print('>>> RunValidation.py: ALL DONE <<<')
    for step in steps:
        print('Time spent on step {}: {} seconds'.format(step, timer[step]-timer[step - 1]))
    print('Total time: {} seconds'.format(timer[5]-timer[0]))
