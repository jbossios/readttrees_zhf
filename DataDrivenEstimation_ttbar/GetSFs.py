##################################################################
#                                                                #
# Purpose: Derive CR->SR TFs (transfer factors) for ttbar        #
# Method:  SF = EL/ELMU (MU/ELMU) from ttbar MC                  #
# Author:  Jona Bossio (jbossios@cern.ch)                        #
#                                                                #
##################################################################

# Import modules
import ROOT
import os
import sys
import copy
import argparse
from multiprocessing import Pool
from CommonDefs import Options as opt

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Systematics import *
from SystematicTTrees import *
from HelperFunctions import get_xrootd_prefix

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles import *
from PlotterHelperFunctions import *
from Luminosities import *

def main(
    ControlRegion,
    channel,
    MC16Campaigns,
    Debug,
    PropagateSystematics,
    SignalGenerator,
    TTrees,
    ttbar_samples = 'All',
    custom_options = None,
    CustomInputFiles = None,
    output_folder = 'Outputs',
  ):

  # Protections
  if ControlRegion not in ['CR', 'AltCR', 'SumCRs']:
    print('ControlRegion ({}) not recognized, exiting'.format(ControlRegion))
    sys.exit(1)
  if TTrees not in ['nominal', 'FullSysts']:
    print('{} not recognized, exiting'.format(TTrees))
    sys.exit(1)

  # Get dict with input ROOT files
  InputFiles = InputFiles if not CustomInputFiles else CustomInputFiles

  # Prepare list of CRs
  CRs = [ControlRegion] if ControlRegion != 'SumCRs' else ['CR', 'AltCR']

  # Read common options
  Tagger = opt['Tagger'] if not custom_options else custom_options['Tagger']
  Version = opt['Version'] if not custom_options else custom_options['Version']
  all_campaigns_extended = opt['Campaigns'] if not custom_options else custom_options['Campaigns']
  AllCampaigns = [cpg.replace('MC16','') for cpg in all_campaigns_extended]
  Backgrounds = opt['Backgrounds'] if not custom_options else custom_options['Backgrounds']
  Observables = opt['Observables'] if not custom_options else custom_options['Observables']

  # Replace Zjets from Backgrounds list by Zee/Zmumu/Ztautau
  if 'Zjets' in Backgrounds:
    Backgrounds.remove('Zjets')
    Backgrounds.append('Zee{}'.format(SignalGenerator))
    Backgrounds.append('Zmumu{}'.format(SignalGenerator))
    Backgrounds.append('Ztautau')

  # Total luminosity for needed campaigns
  Luminosity = dict() # collect luminosity for each MC campaign
  Luminosity["MC16a"]= Luminosity_2015 + Luminosity_2016
  Luminosity["MC16d"]= Luminosity_2017
  Luminosity["MC16e"]= Luminosity_2018


  # Open input ROOT files on demand
  def get_input_file(input_files_dict, key):
    """ Get already opened input file or open it if not opened already """
    # protection
    if key not in InputFiles:
      msg = sample + " not found in InputFiles dictionary, exiting"
      FATAL(msg)
    if key not in input_files_dict:  # open file
      input_file_name = InputFiles[key]
      input_file_name = get_xrootd_prefix(InputFiles[key]) + InputFiles[key]
      if Debug:
        print('Opening {}...'.format(input_file_name))
      input_file = ROOT.TFile.Open(input_file_name)
      CHECKObj(input_file, f'{input_file_name} not found, exiting')
      input_files_dict[key] = input_file
    return input_files_dict, input_files_dict[key]


  def get_hist(input_files_dict, sample, histname, debug = False):
    # Get File
    if debug: print("DEBUG: get_hist(): get file")
    input_files_dict, File = get_input_file(input_files_dict, sample)

    # Get Histogram
    if debug: print("DEBUG: get_hist(): get histogram")
    hist_orig = File.Get(histname)
    hist_orig.SetDirectory(0)
    if not hist_orig:
      msg = histname + " not found in " + InputFiles[sample] + ", exiting"
      return input_files_dict, None, msg
    hist = hist_orig.Clone(histname + "_cloned")
    hist.SetDirectory(0)  # detach from file

    return input_files_dict, hist, 'OK'


  def get_total_hist(input_files_dict, samples, histname, debug, luminosity = dict()):
    cpgs = ['MC16a', 'MC16d', 'MC16e']

    # Get first histogram
    if debug: print("DEBUG: get_total_hist(): get first histogram")
    input_files_dict, total_hist, msg = get_hist(input_files_dict, samples[0], histname, debug)
    if msg != 'OK':
      return input_files_dict, None, msg
    for campaign in cpgs:
      if campaign in samples[0] and 'cutflow' not in histname and 'mcEvtWeight' not in histname and 'sampleWeight' not in histname and 'SF' not in histname:
        total_hist.Scale(luminosity[campaign])

    # Now sum all the rest
    if debug: print("DEBUG: get_total_hist(): sum all the rest")
    for isample in range(1, len(samples)):
      input_files_dict, hist, msg = get_hist(input_files_dict, samples[isample], histname, debug)
      if msg != 'OK':
        return input_files_dict, None, msg
      for campaign in cpgs:
        if campaign in samples[isample] and 'cutflow' not in histname and 'mcEvtWeight' not in histname and 'sampleWeight' not in histname and 'SF' not in histname:
          hist.Scale(luminosity[campaign])
      total_hist.Add(hist)
      hist.Delete()

    return input_files_dict, total_hist, 'OK'


  ####################
  # Helpful functions
  ####################

  # Get list of samples
  def getSampleList(channel: str, selection: str, campaign: str, samples: str, extra: str = '') -> list:
    """Get list of TTbar samples based on chosen channel, selection and campaigns"""
    ttbar_samples = ['NonAllHad', 'Dilepton'] if samples == 'All' else [samples]
    if campaign == 'all':
      return ['TTbar{}_MC16{}_{}_{}_{}{}'.format(sample, cpg, channel, selection, Tagger, extra) for cpg in AllCampaigns for sample in ttbar_samples]
    else:
      return ['TTbar{}_MC16{}_{}_{}_{}{}'.format(sample, campaign, channel, selection, Tagger, extra) for sample in ttbar_samples]


  # Get list of data-taking years for a given campaign
  def getDataPeriods(campaign: str) -> list:
    """Get list of data years based on the chosen campaign"""
    return {
             'all': ['15','16','17','18'],
             'a': ['15','16'],
             'd': ['17'],
             'e': ['18'],
           }[campaign]


  # Get data-subtracted for a given CR
  def getDataDict(input_files_dict, crs, dataPeriods, histName, tagger, campaign, bkgs, luminosity, debug, extraCR) -> dict:
    """Get subtracted data for a given Control Region"""
    hDataDict = dict()
    # Loop over CRs
    for CR in crs:
      # Loop over data-taking years
      DataHists = []
      for period in dataPeriods:
        histname = histName.replace(extraCR, '') if extraCR != '' else histName
        if debug: print('DEBUG: Get histogram('+histname+') for data'+period)
        # Open input file
        Key = "Signal_data" + period + "_ELMU_" + CR + "_" + tagger
        input_files_dict, File = get_input_file(input_files_dict, Key)
        # Get histogram
        Hist = File.Get(histname)
        Hist.SetDirectory(0)
        CHECKObj(Hist, histname+" not found in {}, exiting".format(InputFiles[Key]))
        DataHists.append(Hist)

      # Get total data histogram
      if debug: print("DEBUG: Get total data histogram")
      hData = DataHists[0].Clone("Data_"+histName)
      hData.SetDirectory(0)
      for ihist in range(1,len(DataHists)):
        hData.Add(DataHists[ihist])

      # Get background estimations
      if debug: print("DEBUG: Get background estimations")
      hBkgs     = []
      campaigns = ['a', 'd', 'e'] if campaign == 'all' else [campaign]
      for cpg in campaigns:
        for bkg in bkgs:
          # Open File
          Key = bkg+"_MC16"+cpg+"_ELMU_"+CR+"_"+tagger
          input_files_dict, File = get_input_file(input_files_dict, Key)
          # Get histogram
          Hist = File.Get(histName)
          Hist.SetDirectory(0)
          CHECKObj(Hist,histName+" not found in {}, exiting".format(InputFiles[Key]))
          Hist.Scale(luminosity['MC16'+cpg])
          hBkgs.append(Hist)

      # Subtract non-ttbar samples to the total data
      if debug: print("DEBUG: Subtract non-Z backgrounds to the total data")
      for hist in hBkgs: hData.Add(hist, -1)
      hDataDict[CR] = hData
    return input_files_dict, hDataDict


  def process_np(input_files, source, iNP, Hists):
    if not PropagateSystematics and source != 'nominal': return  # skip NPs when not requested
    extraCR = '_{}_{}'.format(source, iNP) if source != 'nominal' else ''
    if source == 'JVT' or source == 'BTag' or source == 'weight_pileup':
      extraSR = '_{}_{}'.format(source, iNP)
    else:  # source != JVT and source != BTag and source != weight_pileup
      if (channel == 'EL' and 'EL' in source) or (channel == 'MU' and 'MU' in source):
        extraSR = '_{}_{}'.format(source, iNP) if source != 'nominal' else ''
      else:
        extraSR = ''
    # Loop over observables
    for obs, cases in Observables.items():
      # Loop over event types
      for case in cases:
        for campaign in [MC16Campaigns]:
          if Debug:
            print('source   = {}'.format(source))
            print('iNP      = {}'.format(iNP))
            print('channel  = {}'.format(channel))
            print('obs      = {}'.format(obs))
            print('case     = {}'.format(case))
            print('campaign = {}'.format(campaign))
          DataPeriods = getDataPeriods(campaign)
          ###############################################
          # Get SFs for this observable+case combination
          ###############################################
          # Get MC ttbar distribution in SR
          if Debug: print("DEBUG: Get SR ttbar distribution for MC16{} campaign".format(campaign))
          histname = obs + '_' + case + extraSR
          if Debug: print('SR histname = {}'.format(histname))
          sample = getSampleList(channel, 'SR', campaign, ttbar_samples)
          input_files, SRhist, msg = get_total_hist(input_files, sample, histname, Debug, Luminosity)
          CHECK(msg)
          SRhist.SetDirectory(0)
          #######################################################################################
          # Get subtracted data distribution on each CR (needed to decide which CRs to sum)
          # for a given bin: sum only CRs in which there are non-zero events in subtracted data)
          histname = obs + '_' + case + extraCR
          input_files, hDataDict = getDataDict(input_files, CRs, DataPeriods, histname, Tagger, campaign, Backgrounds, Luminosity, Debug, extraCR)
          # Get MC ttbar distribution in CR
          # first get distribution for each individual CR
          if Debug: print('histname for CRs = {}'.format(CRs, histname))
          CRhists = dict()
          for CR in CRs:  # Loop over CRs to sum
            if Debug: print("DEBUG: Get {} ttbar distribution for MC16{} campaign".format(CR,campaign))
            sample = getSampleList('ELMU', CR, campaign, ttbar_samples)
            input_files, hist, msg = get_total_hist(input_files, sample, histname, Debug, Luminosity)
            CHECK(msg)
            hist.SetDirectory(0)
            hist.SetName(hist.GetName()+'_{}'.format(CR))  # change name such that it doesn't match to others
            CRhists[CR] = hist
          # Sum CR contributions only if non-zero events in subtracted data in given CR
          CRhist = CRhists[CRs[0]].Clone(CRhists[CRs[0]].GetName() + '_final')
          if len(CRs) > 1:
            for xbin in range(1, CRhist.GetNbinsX() + 1):
              content = 0
              error = 0
              for CR in CRs:
                if hDataDict[CR].GetBinContent(xbin) >= 0:  # consider CR only if data in that CR
                  content += copy.deepcopy(CRhists[CR].GetBinContent(xbin))
                  error += copy.deepcopy(CRhists[CR].GetBinError(xbin))
              CRhist.SetBinContent(xbin, content)
              CRhist.SetBinError(xbin, error)
          ##############
          # Compute SF
          if Debug: print('DEBUG: Compute SF')
          SFhist = SRhist.Clone(histname + '_MC16{}_{}SF'.format(campaign, channel))
          SFhist.Divide(CRhist)
          Hists.append(SFhist)
          ########################################################################
          # Do the same for the leading/combined b-tagging quantile distributions
          ########################################################################
          if Debug: print('DEBUG: Repeat for quantile distributions')
          for QuantileCase in ['Lead', 'Comb']:
            if QuantileCase == 'Comb' and case != 'Ti2': continue  # skip undesired combination
            # SR
            histname = '{}{}Quantile_vs_{}_{}{}'.format(Tagger, QuantileCase, obs, case, extraSR)
            sample = getSampleList(channel, 'SR', campaign, ttbar_samples)
            input_files, SRhist, msg = get_total_hist(input_files, sample, histname, Debug, Luminosity)
            CHECK(msg)
            SRhist.SetDirectory(0)
            # CR
            histname = '{}{}Quantile_vs_{}_{}{}'.format(Tagger, QuantileCase, obs, case, extraCR)
            input_files, hDataDict = getDataDict(input_files, CRs, DataPeriods, histname, Tagger, campaign, Backgrounds, Luminosity, Debug, extraCR)
            # Get MC ttbar distribution in CR
            # first get distribution for each individual CR
            CRhists = dict()
            for CR in CRs: # Loop over CRs to sum
              if Debug: print("DEBUG: Get {} ttbar distribution for MC16{} campaign".format(CR,campaign))
              sample = getSampleList('ELMU', CR, campaign, ttbar_samples)
              input_files, hist, msg = get_total_hist(input_files, sample, histname, Debug, Luminosity)
              CHECK(msg)
              hist.SetDirectory(0)
              hist.SetName(hist.GetName()+'_{}'.format(CR)) # change name such that it doesn't match to others
              CRhists[CR] = hist
            # Sum CR contributions only if non-zero events in subtracted data in given CR
            CRhist = CRhists[CRs[0]].Clone(CRhists[CRs[0]].GetName()+'_final')
            if len(CRs) > 1:
              for xbin in range(1, CRhist.GetNbinsX()+1):  # loop over bins on the x-axis
                for ybin in range(1, CRhist.GetNbinsY()+1):  # loop over bins on the y-axis
                  content = 0
                  error = 0
                  for CR in CRs: # loop over CRs
                    if hDataDict[CR].GetBinContent(xbin, ybin) >= 0: # consider CR only if data in that CR
                      content += copy.deepcopy(CRhists[CR].GetBinContent(xbin, ybin))
                      error += copy.deepcopy(CRhists[CR].GetBinError(xbin, ybin))
                  CRhist.SetBinContent(xbin, ybin, content)
                  CRhist.SetBinError(xbin, ybin, error)
            # Compute SF
            SFhist = SRhist.Clone(histname+'_MC16{}_{}SF'.format(campaign, channel))
            SFhist.Divide(CRhist)
            Hists.append(SFhist)

  def process_campaigns(MC16Campaigns, SignalGenerator, CRs):

    print('Running for MC16{}... this might take long'.format(MC16Campaigns))

    ####################################################################################
    # Calculate transfer factors
    ####################################################################################

    Hists = []
    m_input_files = {}
    # Loop over nominal + SF-based NPs
    for source, nNPs in SFsysts.items():
      for iNP in range(1, nNPs+1):
        process_np(m_input_files, source, iNP, Hists)

    # Write SF hists
    MC16Campaigns = ['MC16{}'.format(case) for case in [MC16Campaigns]]
    Campaigns = '_vs_'.join(MC16Campaigns)
    extra = '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
    extraGen = '' if SignalGenerator == 'SH2211' else '_{}'.format(SignalGenerator)
    outFileName = '{}/ttbarSFs_{}_{}_{}{}{}.root'.format(output_folder, Version, channel, Campaigns, extra, extraGen if len(CRs) > 1 else '')
    outFile = ROOT.TFile(outFileName, 'RECREATE')
    print('Writing outputs to {}'.format(outFileName))
    for hist in Hists:
      hist.Write()
    outFile.Close()

    for _, input_file in m_input_files.items():
      input_file.Close()

  def process_campaigns_systs(MC16Campaigns, SignalGenerator, CRs):
    m_input_files = {}

    print('Running FullSysts for MC16{}... this might take long'.format(MC16Campaigns))

    ####################################################################################
    # Calculate transfer factors
    ####################################################################################

    # Collect histograms for each TTreeName
    # Loop over sytematic TTrees
    for counter, TTreeName in enumerate(SystTTrees):
      if Debug: print(f'DEBUG: Processing {TTreeName}...')
      Hists = []
      # Loop over channels
      #for channel in Channels:
      # Loop over observables
      for obs, cases in Observables.items():
        # Loop over event types
        for case in ['Ti1', 'Ti2']:
          #if case == 'Ti1' and 'Te1' not in cases: continue  # skip nonsense obs+case combination
          if case not in cases: continue  # skip not requested obs+case combination
          for campaign in [MC16Campaigns]:
            if Debug:
              print('channel  = {}'.format(channel))
              print('obs      = {}'.format(obs))
              print('case     = {}'.format(case))
              print('campaign = {}'.format(campaign))
            DataPeriods = getDataPeriods(campaign)
            ###############################################
            # Get SFs for this observable+case combination
            ###############################################
            # Get MC ttbar distribution in SR
            if Debug: print("DEBUG: Get SR ttbar distribution for MC16{} campaign".format(campaign))
            histname = TTreeName + '/' + obs + '_' + case
            if Debug: print('SR histname = {}'.format(histname))
            sample = getSampleList(channel, 'SR', campaign, ttbar_samples, '_FullSysts')
            m_input_files, SRhist, msg = get_total_hist(m_input_files, sample, histname, Debug, Luminosity)
            CHECK(msg)
            SRhist.SetDirectory(0)
            #######################################################################################
            # Get subtracted data distribution on each CR (needed to decide which CRs to sum)
            # for a given bin: sum only CRs in which there are non-zero events in subtracted data)
            histname = obs + '_' + case
            m_input_files, hDataDict = getDataDict(m_input_files, CRs, DataPeriods, histname, Tagger, campaign, Backgrounds, Luminosity, Debug, '')
            # Get MC ttbar distribution in CR
            # first get distribution for each individual CR
            if Debug: print('histname for CRs = {}'.format(CR,histname))
            CRhists = dict()
            histname = TTreeName + '/' + obs + '_' + case
            for CR in CRs: # Loop over CRs to sum
              if Debug: print("DEBUG: Get {} ttbar distribution for MC16{} campaign".format(CR,campaign))
              sample = getSampleList('ELMU', CR, campaign, ttbar_samples, '_FullSysts')
              m_input_files, hist, msg = get_total_hist(m_input_files, sample, histname, Debug, Luminosity)
              CHECK(msg)
              hist.SetDirectory(0)
              hist.SetName(hist.GetName()+'_{}'.format(CR)) # change name such that it doesn't match to others
              CRhists[CR] = hist
            # Sum CR contributions only if non-zero events in subtracted data in given CR
            CRhist = CRhists[CRs[0]].Clone(CRhists[CRs[0]].GetName()+'_final')
            for xbin in range(1, CRhist.GetNbinsX()+1):
              content = 0
              error = 0
              for CR in CRs:
                if hDataDict[CR].GetBinContent(xbin) >= 0: # consider CR only if data in that CR
                  content += copy.deepcopy(CRhists[CR].GetBinContent(xbin))
                  error += copy.deepcopy(CRhists[CR].GetBinError(xbin))
              CRhist.SetBinContent(xbin, content)
              CRhist.SetBinError(xbin, error)
            ##############
            # Compute SF
            histname_base = histname.replace(f'{TTreeName}/', '')
            SFhist = SRhist.Clone(histname_base+'_MC16{}_{}SF'.format(campaign,channel))
            SFhist.Divide(CRhist)
            Hists.append(SFhist)
            ################################################################
            # Do the same for the leading/combined b-tagging quantile distributions
            ################################################################
            for QuantileCase in ['Lead', 'Comb']:
              if QuantileCase == 'Comb' and case != 'Ti2': continue  # skip undesired combination
              # SR
              histname = '{}/{}{}Quantile_vs_{}_{}'.format(TTreeName, Tagger,QuantileCase,obs,case)
              sample = getSampleList(channel, 'SR', campaign, ttbar_samples, '_FullSysts')
              m_input_files, SRhist, msg = get_total_hist(m_input_files, sample, histname, Debug, Luminosity)
              CHECK(msg)
              SRhist.SetDirectory(0)
              # CR
              histname = '{}{}Quantile_vs_{}_{}'.format(Tagger, QuantileCase, obs, case)
              m_input_files, hDataDict = getDataDict(m_input_files, CRs, DataPeriods, histname, Tagger, campaign, Backgrounds, Luminosity, Debug, '')
              # Get MC ttbar distribution in CR
              # first get distribution for each individual CR
              CRhists = dict()
              histname = '{}/{}{}Quantile_vs_{}_{}'.format(TTreeName, Tagger, QuantileCase, obs, case)
              for CR in CRs: # Loop over CRs to sum
                if Debug: print("DEBUG: Get {} ttbar distribution for MC16{} campaign".format(CR, campaign))
                sample = getSampleList('ELMU', CR, campaign, ttbar_samples, '_FullSysts')
                m_input_files, hist, msg = get_total_hist(m_input_files, sample, histname, Debug, Luminosity)
                CHECK(msg)
                hist.SetDirectory(0)
                hist.SetName(hist.GetName()+'_{}'.format(CR)) # change name such that it doesn't match to others
                CRhists[CR] = hist
              # Sum CR contributions only if non-zero events in subtracted data in given CR
              CRhist = CRhists[CRs[0]].Clone(CRhists[CRs[0]].GetName()+'_final')
              for xbin in range(1, CRhist.GetNbinsX()+1):  # loop over bins on the x-axis
                for ybin in range(1, CRhist.GetNbinsY()+1):  # loop ove bins on the y-axis
                  content = 0
                  error = 0
                  for CR in CRs: # loop over CRs
                    if hDataDict[CR].GetBinContent(xbin,ybin) >= 0: # consider CR only if data in that CR
                      content += copy.deepcopy(CRhists[CR].GetBinContent(xbin, ybin))
                      error += copy.deepcopy(CRhists[CR].GetBinError(xbin, ybin))
                  CRhist.SetBinContent(xbin, ybin, content)
                  CRhist.SetBinError(xbin, ybin, error)
              # Compute SF
              histname_base = histname.replace(f'{TTreeName}/', '')
              SFhist = SRhist.Clone(histname_base+'_MC16{}_{}SF'.format(campaign, channel))
              SFhist.Divide(CRhist)
              Hists.append(SFhist)

      # Write SF hists
      AllMC16Campaigns = ['MC16{}'.format(case) for case in [MC16Campaigns]]
      Campaigns = '_vs_'.join(AllMC16Campaigns)
      extra = '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
      extraGen = '' if SignalGenerator == 'SH2211' else '_{}'.format(SignalGenerator)
      outFileName   = '{}/ttbarSFs_{}_{}_{}{}_FullSysts.root'.format(output_folder, Version, channel, Campaigns, extra, extraGen if len(CRs) > 1 else '')
      if not counter:
        outFile = ROOT.TFile(outFileName, 'RECREATE')
      else:
        outFile = ROOT.TFile(outFileName, 'UPDATE')
      if Debug: print('DEBUG: Writing outputs to {}'.format(outFileName))
      if Debug: print(f'DEBUG: Writing histogram to {TTreeName} folder')
      outFile.cd()
      outFile.mkdir(TTreeName)
      outFile.cd(TTreeName)
      for hist in Hists:
        hist.Write()
      outFile.Close()

    for _, input_file in m_input_files.items():
      input_file.Close()

  if TTrees == 'nominal':
    process_campaigns(MC16Campaigns, SignalGenerator, CRs)
  else: # FullSysts
    process_campaigns_systs(MC16Campaigns, SignalGenerator, CRs)


if __name__ == '__main__':
  # NOTE: Other options are set in CommonDefs.py

  PropagateSystematics = True
  Debug = False
  ControlRegion = 'CR'  # options: CR (nominal) or AltCR (alternative) or SumCRs (CR+AltCR)

  # Generator to be used for signal
  SignalGenerator = 'SH2211' # options: FxFx, MG and SH2211

  parser = argparse.ArgumentParser()
  parser.add_argument('--cr', action='store', default='', help='options: CR, AltCR, SumCRs')
  parser.add_argument('--ch', action='store', default='', help='options: EL, MU')
  parser.add_argument('--campaign', action='store', default='', help='options: a, d, e, all')
  parser.add_argument('--ttrees', action='store', default='nominal', help='options: nominal (default), FullSysts')
  args = parser.parse_args()

  if not args.cr:
    print('ERROR: control region (--cr) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.campaign:
    print('ERROR: campaign not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.ch:
    print('ERROR: channel (--ch) not provided, exiting')
    parser.print_help()
    sys.exit(1)

  print('INFO: Running GetSFs.py with cr={}, ch={}, campaign={} and ttrees={}'.format(args.cr, args.ch, args.campaign, args.ttrees))

  main(args.cr, args.ch, args.campaign, Debug, PropagateSystematics, SignalGenerator, args.ttrees)

  print('>>> ALL DONE <<<')
