# Purpose:

Data-driven method for estimating ttbar background

# Method:

ttbar in SR is estimated as ttbar in (ttbar enriched) CR region multiplied by a transfer factor ((MU/ELMU or EL/ELMU depending on channel, also called SF).

The SF is derived for each bin of the measurement (for each bin/observable).

# Setup (setting up needed Python version)

```
source Setup.sh
```

# Procedure:

1. Update ```CommonDefs.py```

2. Run all the following scripts with ```RunAll.py```:

  2.1. Derive nominal (and systematically variated) CR->SR SFs (to then derive data-driven ttbar contribution) with ```GetSFs.py```

  2.2. Perform data-driven ttbar estimation with ```GetTTbarContribution.py```

  2.3. Derive CR->SF SF-based systematic uncertainties on the data-driven ttbar estimation with ```DeriveTTbarContributionSystematics.py```

  2.4. Prepare inputs to CxAOD-based fitter (optional) [note: need python3] with ```PrepareInputs2CxAOD.py```

[Optional] 3. Derive CR->SR SFs systematic uncertainties (by comparing nominal w/ systematically variated SFs) with ```DeriveSFuncertainties.py``` [need to be updated to propagate non-SF-based systematics]

# Plotters:

1. Compare CR->SR transfer factors b/w EL and MU channels: ```CompareSFs.py```

2. Compare CR->SR transfer factors b/w MC16 campaigns (MC16a, MC16d, MC16e and MC16all): ```CompareSFs_byCampaign.py```

3. Compare data-driven ttbar estimation with MC-based ttbar prediction: ```CompareEstimations_MC_vs_DataDriven.py```

4. Compare systematic uncertainties on the CR->SR transfer factors: ```Run_CompareSFuncertainties.py which runs CompareSFuncertainties.py```

5. Compare systematic uncertainties on the data-driven ttbar estimations: ```Run_CompareTTbarUncertainties.py which runs CompareTTbarUncertainties.py```

6. Compare data-driven ttbar estimations depending on the generator used for Zee/Zmumu samples in the subtraction: ```CompareEstimations_UsingDifferentSignalGenerators.py```

7. Compare nominal vs variated SR/CR distributions: ```CompareHists.py```

8. Compare predictions obtained using different control regions: ```CompareEstimations_byCR.py```

9. Make final uncertainties breakdown plot: ```Run_MakeFinalUncertaintiesPlot.py```

# Run validation and derive extrapolation uncertainties

Run ```RunValidation.py```

# Tests

You can test changes w.r.t. a reference running the following:

```
python run_all_ci.py
```

The above will run all steps of the data-driven ttbar estimation, including validation and evaluation of systematics, for a reduced set of variables/samples.
The results will be compared with previously produced references.

The same script runs on the Gitlab CI pipelines.

If differences are found and are expected, the references can be updated running the following:


```
python run_all_ci.py --updateRefs
```
