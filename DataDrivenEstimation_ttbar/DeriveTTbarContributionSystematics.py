################################################################################
#                                                                               #
# Purpose: Compare nominal vs variated ttbar contribution to derive uncertainty #
# Author: Jona Bossio (jbossios@cern.ch)                                        #
#                                                                               #
################################################################################

# Import Python modules
import ROOT
import os
import sys
import math
import argparse
import copy
from multiprocessing import Pool
from functools import partial
from smoother import smooth_hist

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import Binning
from Systematics import *
from SystematicTTrees import *

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles             import *
from PlotterHelperFunctions import *
from Luminosities           import *


def update_dict(uncertainties, result_dicts):
  for result_dict in result_dicts:
    for cpg, cpg_dict in result_dict.items():
      for obs, obs_dict in cpg_dict.items():
        for case, case_dict in obs_dict.items():
          for np, hist in case_dict.items():
            uncertainties[cpg][obs][case][np] = hist


def extract_info(info):
  return (info[key] for key in ('campaign', 'channel', 'obs', 'case'))


def create_info(campaign, channel, obs, case):
  return {
    'campaign': campaign,
    'channel': channel,
    'obs': obs,
    'case': case,
  }


def calculate_relative_uncertainties(np, info, hists, smooth):
  campaign, channel, obs, case = extract_info(info)
  hist = hists[campaign][obs][case][np].Clone('TTbarMC16{}_{}_{}_{}_{}'.format(campaign, channel, obs, case, np))
  hist.Add(hists[campaign][obs][case][''], -1)  # subtract nominal ttbar contribution
  hist.Divide(hists[campaign][obs][case][''])  # divide by nominal ttbar contribution
  if smooth:
    obs = info['obs']
    if obs == 'Zpt' or obs == 'mjjHF' or obs == 'HFjet0_pt':
      hist = smooth_hist(hist, True)
    else:
      hist = smooth_hist(hist, False)
  #if 'EG_SCALE' in np or 'EG_RESOLUTION' in np or 'MUON_CB' in np:  # Temporary
  #  hist.Smooth()
  #if case == 'Ti2' and 'JET' in np:  # Temporary
  #  hist.Smooth()
  return {
    campaign: {
      obs: {
        case: {
          np: hist
        }
      }
    }
  }


def main(
    campaign,
    channel,
    SignalGenerator,
    ControlRegion,
    smooth,
    Debug = False,
    custom_options = None,
    output_folder = 'Outputs',
  ):

  Systematics = 'AllSysts'  # options: SFonly, AllSysts

  ncores = 4

  def set_bin_content(xbin, Hist, value):
    Hist.SetBinContent(xbin, value)

  def get_sqrt_uncert(hist, sign, ibin):
    uncert = hist.GetBinContent(ibin)
    if (uncert >= 0 and sign == 'UP') or (uncert < 0 and sign == 'DOWN'):
      return uncert * uncert
    return 0

  DataYearsOptions = {
    'a': [['15', '16']],
    'd': [['17']],
    'e': [['18']],
    'all': [['15', '16', '17', '18']],
  }[campaign]

  # Read common options
  from CommonDefs import Options as opt
  Tagger = opt['Tagger'] if not custom_options else custom_options['Tagger']
  Version = opt['Version'] if not custom_options else custom_options['Version']
  FinalStates = opt['FinalStates'] if not custom_options else custom_options['FinalStates']
  Observables = opt['Observables'] if not custom_options else custom_options['Observables']

  # Add b-tagging quantile vs obs
  FinalObservables = copy.deepcopy(Observables)
  for obs, cases in Observables.items():
    if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = ['Ti1']
    if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

  # Protection
  if SignalGenerator not in ['SH2211', 'FxFx']:
    FATAL('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))

  # Loop over data year sets
  for DataYears in DataYearsOptions:

    print('Running for {} data year set... this might take long'.format(DataYears))

    dataYears = ''.join(DataYears)

    ######################################################
    # Find out needed campaigns
    ######################################################
    Campaigns = []
    for year in DataYears:
      if year == '15':
        Campaigns.append('a')
      elif year == '16':
        if 'a' not in Campaigns: Campaigns.append('a')
      elif year == '17':
        Campaigns.append('d')
      elif year == '18':
        Campaigns.append('e')
    if 'a' in Campaigns and 'd' in Campaigns and 'e' in Campaigns:
      cpg = ['all']
    elif len(Campaigns) == 1:
      cpg = Campaigns
    else:
      FATAL('The provided combination of campaigns is not supported, exiting')

    ######################################################
    # Retrieve ttbar contributions
    ######################################################
    if Debug: print('Get histograms')
    Hists = dict()
    # Open input file
    extraCR  = '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
    FileName = '{}/ttbarContribution_Signal{}_{}_{}_data{}{}.root'.format(output_folder, SignalGenerator, Version, channel, dataYears, extraCR)
    File = ROOT.TFile.Open(FileName)
    CHECKObj(File,'{} not found, exiting'.format(FileName))
    # Loop over campaigns
    for campaign in cpg:
      Hists[campaign] = dict()
      Hists[campaign] = dict()
      # Loop over observables
      for obs, cases in FinalObservables.items():
        Hists[campaign][obs] = dict()
        # Loop over event types
        for case in cases:
          Hists[campaign][obs][case] = dict()
          # Loop over nominal+NPs
          for source, nNPs in SFsysts.items():
            for iNP in range(1,nNPs+1):
              extra    = '_{}_{}'.format(source, iNP) if source != 'nominal' else ''
              histname = 'ttbar_SR_{}_{}_{}{}'.format(channel, obs, case, extra)
              hist     = File.Get(histname)
              if not hist:
                print('ERROR: {} not found in {}, exiting'.format(histname,FileName))
                sys.exit(1)
              hist.SetDirectory(0)
              Hists[campaign][obs][case][extra] = hist
    File.Close()
    if Systematics == 'AllSysts':  # i.e. not SFonly
      # Open input file
      extraCR= '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
      FileName = '{}/ttbarContribution_Signal{}_{}_{}_data{}{}_FullSysts.root'.format(output_folder, SignalGenerator, Version, channel, dataYears, extraCR)
      File = ROOT.TFile.Open(FileName)
      CHECKObj(File,'{} not found, exiting'.format(FileName))
      # Loop over campaigns
      for campaign in cpg:
        # Loop over observables
        for obs, cases in FinalObservables.items():
          # Loop over event types
          for case in cases:
            # Loop over systematic TTrees
            for ttree_name in SystTTrees:
              histname = '{}/ttbar_SR_{}_{}_{}'.format(ttree_name, channel, obs, case)
              hist = File.Get(histname)
              if not hist:
                print('ERROR: {} not found in {}, exiting'.format(histname, FileName))
                sys.exit(1)
              hist.SetDirectory(0)
              Hists[campaign][obs][case][ttree_name] = hist
    File.Close()

    ########################################################################
    # Calculate relative uncertainty components (variation-nominal)/nominal
    ########################################################################
    if Debug: print('Calculate relative uncertainties')
    Uncertainties = dict()
    for campaign in Hists:
      Uncertainties[campaign] = dict()
      for obs in Hists[campaign]:
        if 'Quantile' not in obs:
          Uncertainties[campaign][obs] = dict()
          for case in Hists[campaign][obs]:
            Uncertainties[campaign][obs][case] = dict()
            info = create_info(campaign, channel, obs, case)
            calculate_relative_uncertainties_partial = partial(calculate_relative_uncertainties, info = info, hists = Hists, smooth = smooth)
            with Pool(ncores) as p:
              result_dicts = p.map(calculate_relative_uncertainties_partial, [np for np in Hists[campaign][obs][case] if np != ''])
            update_dict(Uncertainties, result_dicts)

    #######################################################
    # Sum in quadrature all components (for plotting only)
    #######################################################
    if Debug: print('Sum in quadrature all components')
    TotalUncertainty = dict()
    for campaign in Uncertainties:
      TotalUncertainty[campaign] = dict()
      for obs in Uncertainties[campaign]:
        TotalUncertainty[campaign][obs] = dict()
        for case in Uncertainties[campaign][obs]:
          TotalUncertainty[campaign][obs][case] = dict()
          for sign in ['UP', 'DOWN']:
            Hist = Hists[campaign][obs.replace(case,'')][case][''].Clone('TTbarMC16{}_{}_{}_{}_Total{}Uncertainty'.format(campaign, channel, obs, case, sign))
            # Loop over bins
            for ibin in range(1, Hist.GetNbinsX()+1):
              sum2 = 0
              # Loop over NPs
              for np in Uncertainties[campaign][obs][case]:
                uncert = Uncertainties[campaign][obs][case][np].GetBinContent(ibin)
                if (uncert >= 0 and sign == 'UP') or (uncert < 0 and sign == 'DOWN'):
                  sum2 += uncert * uncert
              if sign == 'UP':
                Hist.SetBinContent(ibin,math.sqrt(sum2))
              else: # DOWN
                Hist.SetBinContent(ibin,-1*math.sqrt(sum2))
              Hist.SetBinError(ibin,0)
            TotalUncertainty[campaign][obs][case][sign] = Hist

    #######################################################
    # Put uncertainties to b-tagging quantile distributions
    #######################################################
    if Debug: print('Put uncertainties to b-tagging quantile distributions')
    # Loop over campaigns
    for campaign in cpg:
      # Loop over observables
      for obs, cases in Observables.items():
        nObsBins = len(Binning[obs])-1 if len(Binning[obs]) > 3 else Binning[obs][0]
        # Loop over event types
        for case in cases:
          # Loop over nominal+NPs
          for source, nNPs in SFsysts.items():
            if source == 'nominal': continue
            for iNP in range(1, nNPs+1):
              np = '_{}_{}'.format(source, iNP)
              # Loop over observable bins
              for obsBin in range(0, nObsBins):
                HistBin = obsBin + 1
                quantileType = 'LeadQuantile' if case == 'Ti1' else 'CombQuantile'
                #for quantileType in ['LeadQuantile', 'CombQuantile']:
                  #if quantileType == 'CombQuantile' and case != 'Ti2': continue
                newobs = '{}{}_vs_{}_bin_{}'.format(Tagger, quantileType, obs, '0{}'.format(HistBin) if HistBin<10 else HistBin)
                if newobs not in Uncertainties[campaign].keys():
                  Uncertainties[campaign][newobs] = dict()
                if case not in Uncertainties[campaign][newobs].keys():
                  Uncertainties[campaign][newobs][case] = dict()
                HistTemp = Hists[campaign][newobs.split('_bin_')[0]][case][''].Clone('TTbarMC16{}_{}_{}_{}_{}_noproj'.format(campaign, channel, newobs, case, np))
                Hist = HistTemp.ProjectionY(HistTemp.GetName().replace('_noproj', ''), HistBin, HistBin)
                for xbin in range(1, Hist.GetNbinsX()+1):
                  Hist.SetBinContent(xbin, Uncertainties[campaign][obs][case][np].GetBinContent(HistBin))
                Uncertainties[campaign][newobs][case][np] = Hist
          if Systematics == 'AllSysts':
            # Loop over systematic TTrees
            for ttree_name in SystTTrees:
              np = '_{}'.format(ttree_name)
              # Loop over observable bins
              for obsBin in range(0, nObsBins):
                HistBin = obsBin + 1
                quantileType = 'LeadQuantile' if case == 'Ti1' else 'CombQuantile'
                #for quantileType in ['LeadQuantile', 'CombQuantile']:
                  #if quantileType == 'CombQuantile' and case != 'Ti2': continue
                newobs  = '{}{}_vs_{}_bin_{}'.format(Tagger, quantileType, obs, '0{}'.format(HistBin) if HistBin<10 else HistBin)
                if newobs not in Uncertainties[campaign].keys():
                  Uncertainties[campaign][newobs] = dict()
                if case not in Uncertainties[campaign][newobs].keys():
                  Uncertainties[campaign][newobs][case] = dict()
                HistTemp = Hists[campaign][newobs.split('_bin_')[0]][case][''].Clone('TTbarMC16{}_{}_{}_{}_{}_noproj'.format(campaign, channel, newobs, case, np))
                Hist = HistTemp.ProjectionY(HistTemp.GetName().replace('_noproj',''), HistBin, HistBin)
                for xbin in range(1, Hist.GetNbinsX()+1):
                  Hist.SetBinContent(xbin, Uncertainties[campaign][obs][case][ttree_name].GetBinContent(HistBin))
                Uncertainties[campaign][newobs][case][ttree_name] = Hist
          # Total uncertainty
          for obsBin in range(0, nObsBins):  # loop over observable bins
            HistBin = obsBin + 1
            quantileType = 'LeadQuantile' if case == 'Ti1' else 'CombQuantile'
            #for quantileType in ['LeadQuantile', 'CombQuantile']:
              #if quantileType == 'CombQuantile' and case != 'Ti2': continue
            newobs  = '{}{}_vs_{}_bin_{}'.format(Tagger, quantileType, obs, '0{}'.format(HistBin) if HistBin < 10 else HistBin)
            if newobs not in TotalUncertainty[campaign].keys():
              TotalUncertainty[campaign][newobs] = dict()
            if case not in TotalUncertainty[campaign][newobs].keys():
              TotalUncertainty[campaign][newobs][case] = dict()
            for sign in ['UP', 'DOWN']:
              HistTemp = Hists[campaign][newobs.split('_bin_')[0]][case][''].Clone('TTbarMC16{}_{}_{}_{}_Total{}Uncertainty_noproj'.format(campaign, channel, newobs, case, sign))
              Hist = HistTemp.ProjectionY(HistTemp.GetName().replace('_noproj',''), HistBin, HistBin)
              for xbin in range(1, Hist.GetNbinsX() + 1):
                Hist.SetBinContent(xbin,TotalUncertainty[campaign][obs][case][sign].GetBinContent(HistBin))
              TotalUncertainty[campaign][newobs][case][sign] = Hist

    # Create output TFile
    if Debug: print('Create output file')
    OutFileName = "{}/TTbarUncertainties_Signal{}_{}_{}_MC16{}{}{}{}.root".format(output_folder, SignalGenerator, Version, channel, cpg[0], extraCR, '_'+Systematics, '_smoothed' if smooth else '')
    OutFile = ROOT.TFile(OutFileName, "RECREATE")
    for campaign in Uncertainties:
      for obs in Uncertainties[campaign]:
        for case in Uncertainties[campaign][obs]:
          for sign in ['UP', 'DOWN']:
            TotalUncertainty[campaign][obs][case][sign].Write()
          for np in Uncertainties[campaign][obs][case]:
            Uncertainties[campaign][obs][case][np].Write()
    OutFile.Close()

  print(">>> ALL DONE <<<")


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--ch', action='store', default='', help='options: EL, MU')
  parser.add_argument('--campaign', action='store', default='', help='options: a, d, e, all')
  parser.add_argument('--smooth', action='store_true', default=False, help='Smooth uncertainties using Gaussian kernel method')
  args = parser.parse_args()

  # protections
  if not args.ch:
    print('ERROR: channel (--ch) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.campaign:
    print('ERROR: campaign not provided, exiting')
    parser.print_help()
    sys.exit(1)

  SignalGenerator = 'SH2211' # options: MG and SH2211
  Debug = False
  ControlRegion = 'CR'

  print('INFO: Running DeriveTTbarContributionSystematics.py with ch={} and campaign={}'.format(args.ch, args.campaign))

  main(args.campaign, args.ch, SignalGenerator, ControlRegion, args.smooth, Debug)
