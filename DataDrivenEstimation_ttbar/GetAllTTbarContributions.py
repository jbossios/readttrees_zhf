import os

if __name__ == '__main__':
    options = {
        'CR': {
            'EL': [
                'nominal',
                'FullSysts',
            ],
            'MU': [
                'nominal',
                'FullSysts',
            ],
        },
        'AltCR': {
            'EL': [
                'nominal',
                #'FullSysts',
            ],
            'MU': [
                'nominal',
                #'FullSysts',
            ],
        },
    }
    commands = []
    for cr, channel_dict in options.items():
        for ch, ttree_types in channel_dict.items():
            for ttree_type in ttree_types:
                  commands.append('nohup python GetTTbarContribution.py --cr {} --ch {} --ttrees {}'.format(cr, ch, ttree_type))
    #print('number of jobs = {}'.format(len(commands)))
    #print('number of jobs per parallel job = {}'.format(len(commands)/4))
    total_counter = -1
    temp_counter = -1
    final_command = ''
    final_commands = []
    for command in commands:
        temp_counter += 1
        total_counter += 1
        if not total_counter:
            final_command = command
        else:
            if temp_counter < 2:
                final_command += ' && {}'.format(command)
            else:
                final_commands.append(final_command + ' &')
                temp_counter = 0
                final_command = command
        if total_counter == len(commands) - 1:
            final_commands.append(final_command + ' &')
    for command in final_commands:
        os.system(command)
