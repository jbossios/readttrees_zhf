import ROOT
import sys
from CommonDefs_CI import Options as opt

# Imports from Reader
sys.path.insert(1, '../')  # insert at 1, 0 is the script path
from checker import compare_hists
from HelperFunctions import get_xrootd_prefix


def compare(inputs_dict):
    # Get reference and test histograms
    hists = {}
    for case in ['test', 'ref']:
        for input_file_name, histnames in inputs_dict.items():
            input_file_name = input_file_name.replace('CI', f'CI{case}')
            if case == 'ref':
                input_file_name = input_file_name.replace('Outputs', opt['PathToRefs'])
                input_file_name = get_xrootd_prefix(input_file_name) + input_file_name
            input_file = ROOT.TFile.Open(input_file_name)
            if not input_file:
                print(f'{input_file_name} not found, exiting...')
                sys.exit(1)
            for hname in histnames:
                hist = input_file.Get(hname)
                if not hist:
                    print(f'{hname} not found in {input_file_name}, exiting...')
                    sys.exit(1)
                hist.SetDirectory(0)
                if hname not in hists:
                    hists[hname] = {case: hist}
                else:
                    hists[hname][case] = hist
            input_file.Close()
    # Compare histograms
    diffs = []
    for hname, hdict in hists.items():
        check = compare_hists('"Data-driven method to estimate ttbar"', input_file_name, hdict['ref'], hdict['test'])
        if len(check) > 0:
            diffs += [hname]
    if len(diffs) > 0:
        raise RuntimeError("Test differs w.r.t the reference for the following histograms: {}".format(diffs))
    print('INFO: test of the data-driven method to estimate ttbar passed successfully!')


def get_data_period(campaign):
    if campaign == 'a':
        return '1516'
    elif campaign == 'd':
        return '17'
    elif campaign == 'e':
        return '18'
    print(f'ERROR: campaign ({campaign}) not recognized, exiting...')
    sys.exit(1)


def get_quantile_case(tag_case):
    if tag_case == 'Ti1':
        return 'Lead'
    return 'Comb'


def compare_to_ref(campaign, channel, signal_generator, smooth_uncertainties):
    observables = opt['Observables']
    to_compare = {  # input file: list of histograms
        f'Outputs/ttbarSFs_CI_{channel}_MC16{campaign}.root':
            [f'{obs}_{cases[0]}_MC16{campaign}_{channel}SF' for obs, cases in observables.items()] + 
            [f'{obs}_{cases[0]}_weight_pileup_2_MC16{campaign}_{channel}SF' for obs, cases in observables.items()] +
            [f'DL1rLeadQuantile_vs_{obs}_{cases[0]}_MC16{campaign}_{channel}SF' for obs, cases in observables.items()] +
            [f'DL1rLeadQuantile_vs_{obs}_{cases[0]}_weight_pileup_2_MC16{campaign}_{channel}SF' for obs, cases in observables.items()],
        f'Outputs/ttbarSFs_CI_{channel}_MC16{campaign}_FullSysts.root':
            [f'JET_EtaIntercalibration_TotalStat__1up/{obs}_{cases[0]}_MC16{campaign}_{channel}SF' for obs, cases in observables.items()] + 
            [f'JET_EtaIntercalibration_TotalStat__1up/DL1rLeadQuantile_vs_{obs}_{cases[0]}_MC16{campaign}_{channel}SF' for obs, cases in observables.items()],
        f'Outputs/ttbarTFs_CI_{channel}_MC16{campaign}_CR_ELMU_to_VR_{channel}.root':
            [f'{obs}_{cases[0]}_MC16{campaign}_VR{channel}TF' for obs, cases in observables.items()],
        f'Outputs/ttbarContribution_Signal{signal_generator}_CI_{channel}_data{get_data_period(campaign)}.root':
            [f'ttbar_SR_{channel}_{obs}_{cases[0]}' for obs, cases in observables.items()] +
            [f'ttbar_SR_{channel}_{obs}_{cases[0]}_JVT_1' for obs, cases in observables.items()] +
            [f'ttbar_SR_{channel}_DL1r{get_quantile_case(cases[0])}Quantile_vs_{obs}_{cases[0]}' for obs, cases in observables.items()] +
            [f'ttbar_SR_{channel}_DL1r{get_quantile_case(cases[0])}Quantile_vs_{obs}_{cases[0]}_JVT_2' for obs, cases in observables.items()],
        f'Outputs/ttbarContribution_Signal{signal_generator}_CI_{channel}_data{get_data_period(campaign)}_FullSysts.root':
            [f'MET_SoftTrk_ResoPara/ttbar_SR_{channel}_{obs}_{cases[0]}' for obs, cases in observables.items()] +
            [f'MET_SoftTrk_ResoPara/ttbar_SR_{channel}_DL1r{get_quantile_case(cases[0])}Quantile_vs_{obs}_{cases[0]}' for obs, cases in observables.items()],
        f'Outputs/ttbarContribution_Signal{signal_generator}_CI_data{get_data_period(campaign)}_CR_ELMU_to_VR_{channel}.root':
            [f'ttbar_VR_{channel}_{obs}_{cases[0]}' for obs, cases in observables.items()],
        f'Outputs/TTbarUncertainties_Signal{signal_generator}_CI_{channel}_MC16{campaign}_AllSysts_smoothed.root':
            [f'TTbarMC16{campaign}_{channel}_{obs}_{cases[0]}__{channel}Trig_1' for obs, cases in observables.items()] +
            [f'TTbarMC16{campaign}_{channel}_{obs}_{cases[0]}_TotalUPUncertainty' for obs, cases in observables.items()] +
            [f'TTbarMC16{campaign}_{channel}_{obs}_{cases[0]}_TotalDOWNUncertainty' for obs, cases in observables.items()] +
            [f'TTbarMC16{campaign}_{channel}_DL1r{get_quantile_case(cases[0])}Quantile_vs_{obs}_bin_07_{cases[0]}__JET_SingleParticle_HighPt__1down' for obs, cases in observables.items()] +
            [f'TTbarMC16{campaign}_{channel}_DL1r{get_quantile_case(cases[0])}Quantile_vs_{obs}_bin_06_{cases[0]}_TotalUPUncertainty' for obs, cases in observables.items()] +
            [f'TTbarMC16{campaign}_{channel}_DL1r{get_quantile_case(cases[0])}Quantile_vs_{obs}_bin_06_{cases[0]}_TotalDOWNUncertainty' for obs, cases in observables.items()],
    }
    compare(to_compare)
