#############################################################################
#                                                                           #
# Purpose: Compare data-driven ttbar estimation w/ MC prediction            #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

# Generator to be used for signal
SignalGenerator = 'SH2211' # options: MG and SH2211

# Control region
ControlRegion = 'SumVRs' # options: ['VR','AltVR','SumVRs']

Debug = False

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

formats = ['pdf', 'eps']

import ROOT
import os
import sys

from style import *
from helpful_functions import check_status

# Protection
if SignalGenerator not in ['SH2211', 'MG']:
  print('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))
  sys.exit(1)

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Version  = opt['Version']
Campaign = opt['Campaign']
Tagger   = opt['Tagger']
Channels = opt['Channels']

# Create output folder for plots
outputFolder = 'Plots/{}/MC_vs_DataDriven'.format(Version)
if not os.path.exists(outputFolder):
  os.makedirs(outputFolder)

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts import *

# Import things from Plotter
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style                  import *
from InputFiles             import *
from Luminosities           import *
from PlotterHelperFunctions import *

Luminosity = dict()
Luminosity['MC16a'] = Luminosity_2015 + Luminosity_2016
Luminosity['MC16d'] = Luminosity_2017
Luminosity['MC16e'] = Luminosity_2018

DataPeriod = 'data'
if Campaign == 'MC16all':
  DataPeriod += '15161718'
elif Campaign == 'MC16a':
  DataPeriod += '1516'
elif Campaign == 'MC16d':
  DataPeriod += '17'
elif Campaign == 'MC16e':
  DataPeriod += '18'
else:
  print('ERROR: {} campaign not recognized, exiting')
  sys.exit(1)

# Open input file with data-driven ttbar estimation
InFiles = dict()
extra = '_{}'.format(ControlRegion) if ControlRegion != 'VR' else ''
for channel in Channels:
  InputFileName = '../Outputs/ttbarContribution_Signal{}_{}_{}_{}{}.root'.format(SignalGenerator, Version, channel, DataPeriod, extra)
  InFiles[channel] = ROOT.TFile.Open(InputFileName)
  if not InFiles[channel]:
    print('ERROR: {} not found, exiting'.format(InputFileName))
    sys.exit(1)

# Add b-tagging quantile vs obs
Observables = opt['Observables']
FinalObservables = copy.deepcopy(Observables)
for obs, cases in Observables.items():
  if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = cases
  if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

for ftype in formats:
  # Loop over channels
  for channel in Channels:
    MCFiles = []
    if Campaign == 'MC16all':
      for cpg in ['MC16a', 'MC16d', 'MC16e']:
        MCFiles.append('TTbarNonAllHad_{}_{}_SR_{}'.format(cpg, channel, Tagger))
    else:
      MCFiles.append('TTbarNonAllHad_{}_{}_SR_{}'.format(Campaign, channel, Tagger))
    # Loop over observables
    for obs, cases in FinalObservables.items():
      # Loop over event types
      for case in cases:
        if 'Quantile' not in obs:
          # TCanvas
          Canvas  = ROOT.TCanvas()
          outName = "{}/ttbar_DataDriven_Signal{}_vs_{}_{}_{}_{}{}.{}".format(outputFolder, SignalGenerator, Campaign, obs, case, channel, extra, ftype)
          Canvas.Print(outName+']')

          # TPad for upper panel
          pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
          pad1.SetTopMargin(0.08)
          pad1.SetBottomMargin(0.03)
          pad1.Draw()
          pad1.cd()

          # Set log scales (if requested)
          if obs in Logx:
            pad1.SetLogx()
          if obs in Logy:
            pad1.SetLogy()

          # Legend
          Legends = ROOT.TLegend(0.75, 0.7, 0.92, 0.9)
          Legends.SetTextFont(42)

          # Get data-driven ttbar estimation
          DataHist = InFiles[channel].Get('ttbar_SR_{}_{}_{}'.format(channel,obs,case))
          if not DataHist:
            print('ERROR: {} not found, exiting'.format('ttbar_SR_{}_{}_{}'.format(channel,obs,case)))
            sys.exit()
          DataHist.SetDirectory(0)
          DataHist.Scale(1, 'width')  # divide by bin width
          DataHist.Draw('E P')

          # X-axis range
          if obs in XaxisRange:
            minX = XaxisRange[obs][0]
            maxX = XaxisRange[obs][1]
          else:
            minX  = DataHist.GetXaxis().GetBinLowEdge(1)
            nbins = DataHist.GetNbinsX()
            maxX  = DataHist.GetXaxis().GetBinUpEdge(nbins)

          # Style
          DataHist.SetLineColor(ROOT.kBlack)
          DataHist.SetMarkerColor(ROOT.kBlack)
          DataHist.GetXaxis().SetRangeUser(minX,maxX)
          DataHist.GetXaxis().SetLabelSize(0.)
          DataHist.GetXaxis().SetTitleSize(0.)
          DataHist.GetYaxis().SetTitleSize(20)
          DataHist.GetYaxis().SetTitleFont(43)
          DataHist.GetYaxis().SetLabelFont(43)
          DataHist.GetYaxis().SetLabelSize(19)
          DataHist.GetYaxis().SetTitleOffset(1.3)
          DataHist.GetYaxis().SetTitle('Events / bin-width')
          if obs in XaxisTitles:
            if 'Zpt' in obs and channel == 'EL':
              DataHist.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
            else:
              DataHist.GetXaxis().SetTitle(XaxisTitles[obs])
          Legends.AddEntry(DataHist, 'data-driven', 'p')

          # Get MC ttbar prediction
          MCHist, msg = GetTotalHist(MCFiles, '{}_{}'.format(obs, case), Debug, Luminosity)
          if msg != 'OK':
            print(msg)
            print(histname+" not found, exiting")
            sys.exit(0)
          MCHist.SetDirectory(0)
          MCHist.Draw('E P same')
          MCHist.GetXaxis().SetLabelSize(0.)
          MCHist.GetXaxis().SetTitleSize(0.)
          MCHist.GetYaxis().SetTitleSize(20)
          MCHist.GetYaxis().SetTitleFont(43)
          MCHist.GetYaxis().SetLabelFont(43)
          MCHist.GetYaxis().SetLabelSize(19)
          MCHist.GetYaxis().SetTitleOffset(1.3)
          MCHist.GetYaxis().SetTitle('Events / bin-width')
          MCHist.SetLineColor(ROOT.kRed+1)
          MCHist.SetMarkerColor(ROOT.kRed+1)
          MCHist.GetXaxis().SetRangeUser(minX,maxX)
          if obs in XaxisTitles:
            if 'Zpt' in obs and channel == 'EL':
              MCHist.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
            else:
              MCHist.GetXaxis().SetTitle(XaxisTitles[obs])
          Legends.AddEntry(MCHist,'MC','p')

          # Draw legends
          Legends.Draw('same')

          # Add ATLAS legend
          atlas_legend_block, status = get_atlas_legend_block(True, False, 0.18 if 'delta' not in obs else 0, -0.03)
          check_status(status)
          atlas_legend_block.SetNDC()
          atlas_legend_block.Draw('same')
          # Add sqrt(s) and lumi info
          info_block = get_info_block(0.18 if 'delta' not in obs else 0, -0.05)
          info_block.SetNDC()
          info_block.Draw('same')

          # Show event type
          DecayType = 'ee' if channel == 'EL' else '#mu#mu'
          EvtType = '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 1 b-tagged jet}' if case == 'Ti1' else '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 2 b-tagged jets}'
          TextBlock = ROOT.TLatex(0.38 if 'delta' not in obs else 0.2, 0.68, EvtType)
          TextBlock.SetNDC()
          TextBlock.Draw('same')

          ROOT.gPad.RedrawAxis()

          # TPad for bottom plot
          Canvas.cd()
          pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
          pad2.SetTopMargin(0.03)
          pad2.SetBottomMargin(0.32)
          pad2.Draw()
          pad2.cd()

          # Set log-x scale (if requested)
          if obs in Logx:
            pad2.SetLogx()

          # Create and draw ratio plot
          ratioHist = DataHist.Clone('ratioHist_{}_{}_{}'.format(channel,obs,case))
          ratioHist.Divide(MCHist)
          ratioHist.Draw('e0')
          ratioHist.GetYaxis().SetTitle('data-driven / MC')
          ratioHist.GetXaxis().SetRangeUser(minX,maxX)
          ratioHist.GetXaxis().SetTitleSize(20)
          ratioHist.GetXaxis().SetTitleFont(43)
          ratioHist.GetXaxis().SetLabelFont(43)
          ratioHist.GetXaxis().SetLabelSize(19)
          ratioHist.GetXaxis().SetTitleOffset(1.2)  # Temporary
          if obs in XaxisTitles:
            if 'Zpt' in obs and channel == 'EL':
              ratioHist.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
            else:
              ratioHist.GetXaxis().SetTitle(XaxisTitles[obs])
          ratioHist.GetYaxis().SetTitleSize(20)
          ratioHist.GetYaxis().SetTitleFont(43)
          ratioHist.GetYaxis().SetLabelFont(43)
          ratioHist.GetYaxis().SetLabelSize(19)
          ratioHist.GetYaxis().SetTitleOffset(1.3)
          ratioHist.GetYaxis().SetRangeUser(0.39,1.09)

          # Draw line at data/MC==1
          Line = ROOT.TLine(minX,1,maxX,1)
          Line.SetLineStyle(7)
          Line.Draw("same")

          Canvas.Print(outName)
          Canvas.Print(outName+']')
        else:
          quantileType = 'LeadQuantile' if 'Lead' in obs else 'CombQuantile'
          Var          = obs.replace('{}{}_vs_'.format(Tagger,quantileType),'')
          nObsBins     = len(Binning[Var])-1 if len(Binning[Var]) > 3 else Binning[Var][0]
          for obsBin in range(0,nObsBins):
            HistBin = obsBin + 1

            # TCanvas
            Canvas  = ROOT.TCanvas()
            outName = "{}/ttbar_DataDriven_Signal{}_vs_{}_{}_{}_{}{}.{}".format(outputFolder, SignalGenerator, Campaign, obs + '_bin_{}'.format(HistBin), case, channel, extra, ftype)
            Canvas.Print(outName+']')

            # TPad for upper panel
            pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
            pad1.SetTopMargin(0.08)
            pad1.SetBottomMargin(0.03)
            pad1.Draw()
            pad1.cd()

            # Set log scales (if requested)
            if obs in Logx:
              pad1.SetLogx()
            if obs in Logy:
              pad1.SetLogy()

            # Legend
            #Legends = ROOT.TLegend(0.2, 0.73, 0.32, 0.9)
            #Legends = ROOT.TLegend(0.2, 0.7, 0.35, 0.9)
            Legends = ROOT.TLegend(0.5, 0.7, 0.65, 0.9)
            Legends.SetTextFont(42)

            # THStack
            Stack = ROOT.THStack()

            # Get data-driven ttbar estimation
            DataHist = InFiles[channel].Get('ttbar_SR_{}_{}_{}'.format(channel,obs,case))
            if not DataHist:
              print('ERROR: {} not found, exiting'.format('ttbar_SR_{}_{}_{}'.format(channel,obs,case)))
              sys.exit()
            DataHist.SetDirectory(0)
            DataHist = DataHist.ProjectionY(DataHist.GetName()+'_proj{}'.format(obsBin), HistBin, HistBin)
            DataHist.SetLineColor(ROOT.kBlack)
            DataHist.SetMarkerColor(ROOT.kBlack)
            Legends.AddEntry(DataHist,'data-driven','p')
            Stack.Add(DataHist,"p")

            # X-axis range
            minX  = DataHist.GetXaxis().GetBinLowEdge(1)
            nbins = DataHist.GetNbinsX()
            maxX  = DataHist.GetXaxis().GetBinUpEdge(nbins)

            # Get MC ttbar prediction
            MCHist, msg = GetTotalHist(MCFiles, '{}_{}'.format(obs, case), Debug, Luminosity)
            if msg != 'OK':
              print(msg)
              print(histname+" not found, exiting")
              sys.exit(0)
            MCHist.SetDirectory(0)
            MCHist = MCHist.ProjectionY(MCHist.GetName()+'_proj{}'.format(obsBin), HistBin, HistBin)
            MCHist.SetLineColor(ROOT.kRed+1)
            MCHist.SetMarkerColor(ROOT.kRed+1)
            Legends.AddEntry(MCHist,'MC','p')
            Stack.Add(MCHist,"p")
            Stack.Draw('nostack')
            Stack.GetXaxis().SetLabelSize(0.)
            Stack.GetXaxis().SetTitleSize(0.)
            Stack.GetYaxis().SetTitleSize(20)
            Stack.GetYaxis().SetTitleFont(43)
            Stack.GetYaxis().SetLabelFont(43)
            Stack.GetYaxis().SetLabelSize(19)
            Stack.GetYaxis().SetTitleOffset(1.3)
            Stack.GetYaxis().SetTitle('Events')

            # Draw legends
            Legends.Draw('same')

            # Add ATLAS legend
            atlas_legend_block, status = get_atlas_legend_block(True, False, 0.18 if 'delta' not in obs else 0, -0.03)
            check_status(status)
            atlas_legend_block.SetNDC()
            atlas_legend_block.Draw('same')
            # Add sqrt(s) and lumi info
            info_block = get_info_block(0.18 if 'delta' not in obs else 0, -0.05)
            info_block.SetNDC()
            info_block.Draw('same')

            # Show event type
            DecayType = 'ee' if channel == 'EL' else '#mu#mu'
            EvtType = '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 1 b-tagged jet}' if case == 'Ti1' else '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 2 b-tagged jets}'
            TextBlock = ROOT.TLatex(0.38 if 'delta' not in obs else 0.2, 0.68, EvtType)
            TextBlock.SetNDC()
            TextBlock.Draw('same')

            # Show observable's bin
            ObsBin = '#scale[1.3]{' + Var + ' bin ' + str(HistBin) + '}'
            TextBlock2 = ROOT.TLatex(0.38 if 'delta' not in obs else 0.2, 0.60, ObsBin)
            TextBlock2.SetNDC()
            TextBlock2.Draw('same')

            ROOT.gPad.RedrawAxis()

            # TPad for bottom plot
            Canvas.cd()
            pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
            pad2.SetTopMargin(0.03)
            pad2.SetBottomMargin(0.32)
            pad2.Draw()
            pad2.cd()

            # Set log-x scale (if requested)
            if obs in Logx:
              pad2.SetLogx()

            # Create and draw ratio plot
            ratioHist = DataHist.Clone('ratioHist_{}_{}_{}'.format(channel,obs,case))
            ratioHist.Divide(MCHist)
            ratioHist.Draw('e0')
            ratioHist.GetYaxis().SetTitle('data-driven / MC')
            ratioHist.GetXaxis().SetTitleSize(20)
            ratioHist.GetXaxis().SetTitleFont(43)
            ratioHist.GetXaxis().SetLabelFont(43)
            ratioHist.GetXaxis().SetLabelSize(19)
            ratioHist.GetXaxis().SetTitleOffset(1.2)
            ratioHist.GetXaxis().SetTitle('b-tagging quantile')
            ratioHist.GetYaxis().SetTitleSize(20)
            ratioHist.GetYaxis().SetTitleFont(43)
            ratioHist.GetYaxis().SetLabelFont(43)
            ratioHist.GetYaxis().SetLabelSize(19)
            ratioHist.GetYaxis().SetTitleOffset(1.3)
            ratioHist.GetYaxis().SetRangeUser(0.71,1.29)

            # Draw line at data/MC==1
            Line = ROOT.TLine(minX,1,maxX,1)
            Line.SetLineStyle(7)
            Line.Draw("same")

            Canvas.Print(outName)
            Canvas.Print(outName+']')

# Close files
for _, File in InFiles.items():
  File.Close()

print('>>> ALL DONE <<<')
