######################################################################################
#                                                                                     #
# Purpose: Compare TTbar uncertainties between EL and MU channels for all observables #
# Author:  Jona Bossio (jbossios@cern.ch)                                             #
#                                                                                     #
######################################################################################

Debug = False

import ROOT
import os
import sys
import argparse

from style import *
from helpful_functions import check_status

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--channel',   action='store', dest="channel",   default = '', help = 'Channel (i.e. EL or MU)')
parser.add_argument('--obs',       action='store', dest="obs",       default = '', help = "Observable's name")
parser.add_argument('--case',      action='store', dest="case",      default = '', help = 'Btagging case (i.e. Ti1 or Ti2)')
parser.add_argument('--type',      action='store', dest="Type",      default = '', help = 'Type of uncertainty source (options: Reco, PID, Iso, Trig, BTag_B, BTag_C, BTag_Light, JET, MET, MUON, EG')
parser.add_argument('--signalGen', action='store', dest="signalGen", default = '', help = 'Generator for signal samples')
parser.add_argument('--systs',     action='store', default = '', help = 'options: SFonly, AllSysts')
parser.add_argument('--smooth',    action='store_true', default = False, help = 'Use inputs smoothed by Gaussian kernel')
args      = parser.parse_args()
channel   = args.channel
obs       = args.obs
case      = args.case
Type      = args.Type
SignalGen = args.signalGen
Systs     = args.systs
smooth    = args.smooth

# Protection
if not channel or not obs or not case or not Type or not SignalGen:
  print('ERROR: missing arguments')
  parser.print_help()
  sys.exit(1)

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Version  = opt['Version']
Campaign = opt['Campaign']
Tagger   = opt['Tagger']

from getSFvarNames import getBTagSFvarNames

outputFolder = 'Plots/{}/CompareTTbarUncertainties'.format(Version)
if not os.path.exists(outputFolder):
  os.makedirs(outputFolder)

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts       import *
from Systematics import *
from SystematicTTrees import *

# Import things from Reader
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style import *

# Open input file
InputFileName = '../Outputs/TTbarUncertainties_Signal{}_{}_{}_{}{}{}.root'.format(SignalGen, Version, channel, Campaign, '_AllSysts' if Systs=='AllSysts' else '', '_smoothed' if smooth else '')
InputFile = ROOT.TFile.Open(InputFileName)
if not InputFile:
  print('ERROR: {} not found, exiting'.format(InputFile))
  sys.exit(1)

# TCanvas
Canvas = ROOT.TCanvas()
outName = "{}/{}_{}_{}_Signal{}_compareTTbarUncertainties_{}{}.pdf".format(outputFolder,obs,case,channel,SignalGen,Type, '_smoothed' if smooth else '')
Canvas.Print(outName+']')

# Legend
Legends = ROOT.TLegend(0.8,0.63,0.92,0.9)
Legends.SetTextFont(42)

counter = 0

# Plot total uncertainties
TMG = ROOT.TMultiGraph()
for sign in ['UP', 'DOWN']:
  HistName = 'TTbar{}_{}_{}_{}_Total{}Uncertainty'.format(Campaign,channel,obs,case,sign)
  Hist = InputFile.Get(HistName)
  if not Hist:
    print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
    sys.exit(1)
  Hist.SetDirectory(0)
  #if smooth: Hist.Smooth()
  Graph = ROOT.TGraph(Hist)
  Graph.SetLineColor(counter+1)
  Graph.SetMarkerColor(counter+1)
  if sign == 'UP': Legends.AddEntry(Graph,'Total','p')
  TMG.Add(Graph)
counter += 1

# Loop over nominal+NPs
for source, nNPs in SFsysts.items():
  if source == 'nominal': continue # skip nominal
  if 'BTag' not in Type:
    if Type not in source: # include JVT/weight_pileup when plotting Iso systematics
      if Type != 'Iso':
        continue
      if Type == 'Iso' and 'JVT' not in source and 'weight_pileup' not in source:
        continue
  else: # BTag in Type
    if 'BTag' not in source: continue
    BTagFlavour        = Type.split('_')[1]
    BTagSFvarNames,msg = getBTagSFvarNames()
    if msg != 'OK':
      print('ERROR: {}'.format(msg))
      sys.exit(1)
  if Debug: print('source == {}'.format(source))
  for iNP in range(1,nNPs+1):
    # Skip undesired flavours for BTag case
    if 'BTag' in Type:
      # Check if this iNP corresponds to the desired flavour (B,C,Light)
      if '_{}_'.format(BTagFlavour) not in BTagSFvarNames[iNP-1]: continue
    # Get uncertainty hist
    HistName = 'TTbar{}_{}_{}_{}__{}_{}'.format(Campaign, channel, obs, case, source, iNP)
    Hist = InputFile.Get(HistName)
    if not Hist:
      print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
      sys.exit(1)
    Hist.SetDirectory(0)
    #if smooth: Hist.Smooth()
    Graph = ROOT.TGraph(Hist)
    color = counter + 1
    Graph.SetLineColor(color if color != 10 and color != 19 else 49)
    Graph.SetMarkerColor(color if color != 10 and color != 19 else 49)
    TMG.Add(Graph)
    Legends.AddEntry(Graph,'{}_{}'.format(source,iNP),'p')
    counter += 1

# Loop over non-SF-based systematic sources
for ttree_name in SystTTrees:
  if Type not in ttree_name: continue
  if Debug: print('source == {}'.format(ttree_name))
  # Get uncertainty hist
  HistName = 'TTbar{}_{}_{}_{}_{}'.format(Campaign,channel,obs,case,ttree_name if 'Quantile' not in obs else '_{}'.format(ttree_name))
  Hist = InputFile.Get(HistName)
  if not Hist:
    print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
    sys.exit(1)
  Hist.SetDirectory(0)
  #if smooth: Hist.Smooth()
  Graph = ROOT.TGraph(Hist)
  Graph.SetLineColor(counter+1)
  Graph.SetMarkerColor(counter+1)
  TMG.Add(Graph)
  Legends.AddEntry(Graph,ttree_name,'p')
  counter += 1

TMG.Draw('a')
TMG.GetYaxis().SetTitle('Uncertainty')
if obs in XaxisTitles:
  if obs == 'Zpt':
    if channel == 'EL':
      TMG.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
    if channel == 'MU':
      TMG.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]")
  else:
    TMG.GetXaxis().SetTitle(XaxisTitles[obs])
if 'LeadQuantile' in obs:
  TMG.GetXaxis().SetTitle('Leading b-tagging quantile')
elif 'CombQuantile' in obs:
  TMG.GetXaxis().SetTitle('Combined b-tagging quantiles')

ROOT.gPad.Modified();
#TMG.GetYaxis().SetRangeUser(-0.06,0.13)
#TMG.GetYaxis().SetRangeUser(-0.06,0.30)
TMG.GetYaxis().SetRangeUser(-0.1,0.1)

if Debug: print('Before drawing legends')

# Add ATLAS legend
atlas_legend_block, status = get_atlas_legend_block(False, False)
check_status(status)
atlas_legend_block.SetNDC()
atlas_legend_block.Draw('same')
# Add sqrt(s) and lumi info
info_block = get_info_block()
info_block.SetNDC()
info_block.Draw('same')

Legends.Draw('same')
# Show event type
DecayMode = 'ee' if channel == 'EL' else '#mu#mu'
EvtType = 'Z(#rightarrow' + DecayMode + ') + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z(#rightarrow' + DecayMode + ') + #geq 2 b-tagged jets'
TextBlock = ROOT.TLatex(0.2, 0.75, EvtType)
TextBlock.SetNDC()
TextBlock.Draw('same')

# Show observable bin
if 'Quantile' in obs:
  quantileType = 'LeadQuantile' if 'Lead' in obs else 'CombQuantile'
  var          = obs.split('_bin_')[0].replace('{}{}_vs_'.format(Tagger,quantileType),'')
  ibin         = int(obs.split('_bin_')[1])
  ObsBin       = '{} bin {}'.format(var,ibin)
  TextBlock2   = ROOT.TLatex(0.2, 0.7, ObsBin)
  TextBlock2.SetNDC()
  TextBlock2.Draw('same')

Canvas.Print(outName)
Canvas.Print(outName+']')
    
InputFile.Close()
print('>>> ALL DONE <<<')
