#############################################################################
#                                                                           #
# Purpose: Compare SFs between EL and MU channels for all observables       #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

import ROOT
import os
import sys
import copy

ControlRegion = 'SumVRs' # options: ['VR','AltVR','SumVRs']

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

formats = ['pdf', 'eps']

from style import *
from helpful_functions import check_status

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Campaign = opt['Campaign']
Tagger   = opt['Tagger']
Version  = opt['Version']
Observables = opt['Observables']

# Create output folder for plots
output_folder = 'Plots/{}/CompareSFs_EL_vs_MU/'.format(Version)
if not os.path.exists(output_folder):
  os.makedirs(output_folder)
#os.system('mkdir -p Plots/{}/CompareSFs_EL_vs_MU/'.format(Version))

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts import Binning

# Import things from Reader
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style import *

# Open input file
extra = '_{}'.format(ControlRegion) if ControlRegion != 'VR' else ''
InputFile = dict()
for channel in ['EL', 'MU']:
  InputFileName = '../Outputs/ttbarSFs_{}_{}_{}{}.root'.format(Version, channel, Campaign, extra)
  InputFile[channel] = ROOT.TFile.Open(InputFileName)
  if not InputFile:
    print('ERROR: {} not found, exiting'.format(InputFile))
    sys.exit(1)

# Add b-tagging quantile vs obs
FinalObservables = copy.deepcopy(Observables)
for obs,cases in Observables.items():
  # Loop over bins for this observable
  nObsBins = len(Binning[obs])-1 if len(Binning[obs]) > 3 else Binning[obs][0]
  for obsBin in range(0,nObsBins):
    vsBin   = '_bin_'+str(obsBin) if obsBin > 9 else '_bin_0'+str(obsBin)
    FinalObservables['{}LeadQuantile_vs_{}{}'.format(Tagger,obs,vsBin)] = cases
    if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}{}'.format(Tagger,obs,vsBin)] = ['Ti2']

for ftype in formats:
  # Loop over observables
  for obs, cases in FinalObservables.items():
    # Loop over event types
    for case in cases:
      # TCanvas
      Canvas  = ROOT.TCanvas()
      outName = "{}{}_{}_{}_EL_vs_MU_ttbar_{}toSRSF.{}".format(output_folder, Campaign, obs, case, ControlRegion, ftype)
      Canvas.Print(outName+']')

      # Legend
      Legends = ROOT.TLegend(0.8,0.63,0.92,0.9)
      Legends.SetTextFont(42)

      # Loop over channels and compare SFs
      counter = 0
      for channel in ['EL', 'MU']:
        # Get SF histogram
        HistName = '{}_{}_{}_{}SF'.format(obs.split('_bin_')[0],case,Campaign,channel)
        Hist = InputFile[channel].Get(HistName)
        if not Hist:
          print('ERROR: {} not found in {}, exiting'.format(HistName, InputFile[channel]))
          sys.exit(1)
        Hist.SetDirectory(0)
        if '_bin_' in obs:
          HistBin  = int(obs.split('_bin_')[1])+1
          HistProj = Hist.ProjectionY(Hist.GetName()+'_proj', HistBin, HistBin)
          Hist     = HistProj
        if counter == 0:
          Hist.Draw('E P')
        else:
          Hist.Draw('E P same')
        extra = {
          'VR'     : 'CR',
          'AltVR'  : 'AltCR',
          'SumVRs' : 'SumCRs',
        }[ControlRegion]
        Hist.GetYaxis().SetTitle('{}#rightarrow SR transfer factor'.format(extra))
        Hist.SetLineColor(ROOT.kBlack if channel == 'EL' else ROOT.kRed+1)
        Hist.SetMarkerColor(ROOT.kBlack if channel == 'EL' else ROOT.kRed+1)
        if extra == 'CR':
          Hist.GetYaxis().SetRangeUser(0,1)
        elif extra == 'AltCR':
          Hist.GetYaxis().SetRangeUser(1,2.4)
        elif extra == 'SumCRs':
          Hist.GetYaxis().SetRangeUser(0.25,0.7)
        if obs in XaxisTitles:
          if 'Zpt' in obs:
            Hist.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-} or #mu^{+}#mu^{-}) [GeV]")
          else:
            Hist.GetXaxis().SetTitle(XaxisTitles[obs])
        if 'LeadQuantile' in obs:
          Hist.GetXaxis().SetTitle('Leading b-tagging quantile')
        elif 'CombQuantile' in obs:
          Hist.GetXaxis().SetTitle('Combined b-tagging quantiles')
        Legends.AddEntry(Hist,'ee/e#mu' if channel == 'EL' else '#mu#mu/e#mu','p')
        counter += 1
      Legends.Draw('same')
      # Add ATLAS legend
      atlas_legend_block, status = get_atlas_legend_block(False, True)
      check_status(status)
      atlas_legend_block.SetNDC()
      atlas_legend_block.Draw('same')
      # Add sqrt(s) and lumi info
      info_block = get_info_block()
      info_block.SetNDC()
      info_block.Draw('same')
      # Show event type
      EvtType = 'Z + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z + #geq 2 b-tagged jets'
      #TextBlock = ROOT.TLatex(0.2, 0.85, EvtType)
      TextBlock = ROOT.TLatex(0.2, 0.75, EvtType)
      TextBlock.SetNDC()
      TextBlock.Draw('same')
      if '_bin_' in obs:
        ObsSplit   = obs.split('_bin_')
        ObsBin     = '{} bin {}'.format(ObsSplit[0].split('_vs_')[1],ObsSplit[1])
        TextBlock2 = ROOT.TLatex(0.2, 0.7, ObsBin)
        TextBlock2.SetNDC()
        TextBlock2.Draw('same')
      Canvas.Print(outName)
      Canvas.Print(outName+']')

for _, input_file in InputFile.items():
  input_file.Close()
print('>>> ALL DONE <<<')
