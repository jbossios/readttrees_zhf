#############################################################################
#                                                                           #
# Purpose: Compare SF uncertainties for both channels and all observables   #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

Debug = False

import ROOT,os,sys,argparse

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--channel',  action='store', dest="channel", default = '', help = 'Channel (i.e. EL or MU)')
parser.add_argument('--obs',      action='store', dest="obs",     default = '', help = "Observable's name")
parser.add_argument('--case',     action='store', dest="case",    default = '', help = 'Btagging case (i.e. Ti1 or Ti2)')
parser.add_argument('--type',     action='store', dest="Type",    default = '', help = 'Type of uncertainty source')
parser.add_argument('--systs',    action='store', dest="systs",   default = '', help = 'options: SFonly, AllSysts')
args     = parser.parse_args()
channel  = args.channel
obs      = args.obs
case     = args.case
Type     = args.Type
Systs    = args.systs

# Protection
if not channel or not obs or not case or not Type:
  print('ERROR: missing arguments')
  parser.print_help()
  sys.exit(1)

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Version  = opt['Version']
Campaign = opt['Campaign']
from getSFvarNames import getBTagSFvarNames

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts       import *
from Systematics import *
from SystematicTTrees import *

# Import things from Reader
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style import *

# Open input file
InputFileName = '../Outputs/SFuncertainties_{}_{}_{}_{}.root'.format(Version, channel, Campaign, Systs)  # FIXME add SignalGenerator
InputFile = ROOT.TFile.Open(InputFileName)
if not InputFile:
  print('ERROR: {} not found, exiting'.format(InputFile))
  sys.exit(1)

# Create output folder
os.system('mkdir -p Plots/{}/CompareSFuncertainties'.format(Version))

# TCanvas
Canvas  = ROOT.TCanvas()
outName = "Plots/{}/CompareSFuncertainties/{}_{}_{}_{}_{}_compareSFuncertainties_{}.pdf".format(Version,Campaign,Version,obs,case,channel,Type)
Canvas.Print(outName+']')

# Legend
Legends = ROOT.TLegend(0.8,0.63,0.92,0.9)
Legends.SetTextFont(42)

counter = 0

# Plot total uncertainties
TMG = ROOT.TMultiGraph()
for sign in ['UP', 'DOWN']:
  HistName = '{}_{}_{}_{}_Total{}SFuncertainty'.format(Campaign,channel,obs,case,sign)
  Hist = InputFile.Get(HistName)
  if not Hist:
    print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
    sys.exit(1)
  Hist.SetDirectory(0)
  Graph = ROOT.TGraph(Hist)
  Graph.SetLineColor(counter+1)
  Graph.SetMarkerColor(counter+1)
  if sign == 'UP': Legends.AddEntry(Graph,'Total','p')
  TMG.Add(Graph)
counter += 1

# Loop over nominal+NPs
for source, nNPs in SFsysts.items():
  if source == 'nominal': continue # skip nominal SF
  if 'Btag' not in Type:
    #if Type not in source: continue
    if Type not in source:  # include JVT when plotting Iso systematics
      if Type != 'Iso':
        continue
      if Type == 'Iso' and 'JVT' not in source and 'weight_pileup' not in source:
        continue
  else:  # BTag in Type
    if 'BTag' not in source: continue
    BTagFlavour = Type.split('_')[1]
    BTagSFvarNames, msg = getBTagSFvarNames()
    if msg != 'OK':
      print('ERROR: {}'.format(msg))
      sys.exit(1)
  if Debug: print('source == {}'.format(source))
  for iNP in range(1, nNPs+1):
    if 'BTag' in Type:
      if '_{}_'.format(BTagFlavour) not in BTagSFvarNames[iNP-1]: continue
    # Get uncertainty hist
    HistName = '{}_{}_{}_{}__{}_{}'.format(Campaign,channel,obs,case,source,iNP)
    Hist     = InputFile.Get(HistName)
    if not Hist:
      print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
      sys.exit(1)
    Hist.SetDirectory(0)
    Graph = ROOT.TGraph(Hist)
    Graph.SetLineColor(counter+1)
    Graph.SetMarkerColor(counter+1)
    TMG.Add(Graph)
    Legends.AddEntry(Graph,'{}_{}'.format(source,iNP),'p')
    counter += 1

# Loop over non-SF-based systematic sources
if Systs == 'AllSysts':
  for ttree_name in SystTTrees:
    if Type not in ttree_name: continue
    if Debug: print('source == {}'.format(ttree_name))
    # Get uncertainty hist
    HistName = '{}_{}_{}_{}_{}'.format(Campaign, channel, obs, case, ttree_name)
    Hist = InputFile.Get(HistName)
    if not Hist:
      print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
      sys.exit(1)
    Hist.SetDirectory(0)
    Graph = ROOT.TGraph(Hist)
    Graph.SetLineColor(counter+1)
    Graph.SetMarkerColor(counter+1)
    TMG.Add(Graph)
    Legends.AddEntry(Graph, ttree_name, 'p')
    counter += 1

TMG.Draw('a')
TMG.GetYaxis().SetTitle('SF uncertainty')
if XaxisTitles.has_key(obs):
  TMG.GetXaxis().SetTitle(XaxisTitles[obs])

ROOT.gPad.Modified();
#TMG.GetYaxis().SetRangeUser(-0.06,0.13)
#TMG.GetYaxis().SetRangeUser(-0.06,0.30)
TMG.GetYaxis().SetRangeUser(-0.1,0.1)

if Debug: print('Before drawing legends')

Legends.Draw('same')
# Show event type
EvtType = 'Z + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z + #geq 2 b-tagged jets'
TextBlock = ROOT.TLatex(0.2,0.85,EvtType)
TextBlock.SetNDC()
TextBlock.Draw('same')
Canvas.Print(outName)
Canvas.Print(outName+']')
    
InputFile.Close()
print('>>> ALL DONE <<<')
