#############################################################################
#                                                                           #
# Purpose: Compare data-driven ttbar predictions (VR vs AltVR vs SumVRs)    #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

# Generator to be used for signal
SignalGenerator = 'SH2211' # options: MG and SH2211

# Control regions to compare
#CRs = ['VR','AltVR','SumVRs']
CRs = ['VR', 'AltVR']

Debug = False

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

formats = ['pdf', 'eps']

import ROOT
import os
import sys

from style import *
from helpful_functions import check_status

# Protection
if SignalGenerator not in ['SH2211','MG']:
  print('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))
  sys.exit(1)
if 'VR' not in CRs:
  print('VR is missing in CRs, exiting')
  sys.exit(1)

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Version  = opt['Version']
Campaign = opt['Campaign']
Tagger   = opt['Tagger']
Channels = opt['Channels']

# Create output folder for plots
VS = '_vs_'.join(CRs)
os.system('mkdir -p Plots/{}/{}/'.format(Version,VS))

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts import *

# Import things from Plotter
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style                  import *
from InputFiles             import *
from Luminosities           import *
from PlotterHelperFunctions import *

# Get luminosity for each MC16 campaign
Luminosity          = dict()
Luminosity['MC16a'] = Luminosity_2015 + Luminosity_2016
Luminosity['MC16d'] = Luminosity_2017
Luminosity['MC16e'] = Luminosity_2018

# Set the correspoding set of data-taking years
DataPeriod = 'data'
if Campaign == 'MC16all':
  DataPeriod += '15161718'
elif Campaign == 'MC16a':
  DataPeriod += '1516'
elif Campaign == 'MC16d':
  DataPeriod += '17'
elif Campaign == 'MC16e':
  DataPeriod += '18'
else:
  print('ERROR: {} campaign not recognized, exiting')
  sys.exit(1)

# Open input file with data-driven ttbar estimations
InFiles = dict()
for case in CRs:
  extra = '_{}'.format(case) if case != 'VR' else ''
  InFiles[case] = dict()
  for channel in Channels:
    InputFileName = '../Outputs/ttbarContribution_Signal{}_{}_{}_{}{}.root'.format(SignalGenerator, Version, channel, DataPeriod, extra)
    InFiles[case][channel] = ROOT.TFile.Open(InputFileName)
    if not InFiles[case][channel]:
      print('ERROR: {} not found, exiting'.format(InputFileName))
      sys.exit(1)

# Add b-tagging quantile vs obs to the list of observables
Observables = opt['Observables']
FinalObservables = copy.deepcopy(Observables)
for obs,cases in Observables.items():
  if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = cases
  if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

# Identify alternative CRs
AltCRs = [cr for cr in CRs if cr in ['AltVR', 'SumVRs']]

Colors = [ROOT.kBlack, ROOT.kRed+1, ROOT.kBlue]
Markers = [24, 25, 26]

for ftype in formats:
  # Loop over channels
  for channel in Channels:
    # Loop over observables
    for obs, cases in FinalObservables.items():
      # Loop over event types
      for case in cases:
        if 'Quantile' not in obs:
          # TCanvas
          Canvas  = ROOT.TCanvas()
          outName = "Plots/{}/{}/ttbar_DataDriven_Signal{}_{}_{}_{}_{}.{}".format(Version, VS, SignalGenerator, Campaign, obs, case, channel, ftype)
          Canvas.Print(outName+']')

          # TPad for upper panel
          pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
          pad1.SetTopMargin(0.08)
          pad1.SetBottomMargin(0.03)
          pad1.Draw()
          pad1.cd()

          # Set log scales (if requested)
          if obs in Logx:
            pad1.SetLogx()
          if obs in Logy:
            pad1.SetLogy()

          # Legend
          Legends = ROOT.TLegend(0.8, 0.73, 0.92, 0.9)
          Legends.SetTextFont(42)

          # THStack
          Stack = ROOT.THStack()

          # Loop over CRs
          Hists = dict()
          for counter, CR in enumerate(CRs):
            # Get data-driven ttbar estimation based on CR
            DataHist = InFiles[CR][channel].Get('ttbar_SR_{}_{}_{}'.format(channel,obs,case))
            if not DataHist:
              print('ERROR: {} not found for {}, exiting'.format('ttbar_SR_{}_{}_{}'.format(channel,obs,case,CR)))
              sys.exit()
            DataHist.SetDirectory(0)
            DataHist.SetName(DataHist.GetName()+'_'+CR)
            DataHist.Scale(1, 'width')  # divide by bin width
            DataHist.SetMarkerColor(Colors[counter])
            DataHist.SetLineColor(Colors[counter])
            DataHist.SetMarkerStyle(Markers[counter])
            Legend = {
              'VR': 'CR',
              'AltVR': 'AltCR',
              'SumVRs': 'CR + AltCR',
            }[CR]
            Legends.AddEntry(DataHist, Legend, 'p')
            Stack.Add(DataHist, "p")
            Hists[CR] = DataHist

          # Draw Stack
          Stack.Draw('nostack')

          # X-axis range
          if obs in XaxisRange:
            minX = XaxisRange[obs][0]
            maxX = XaxisRange[obs][1]
          else:
            minX  = Hists[CRs[0]].GetXaxis().GetBinLowEdge(1)
            nbins = Hists[CRs[0]].GetNbinsX()
            maxX  = Hists[CRs[0]].GetXaxis().GetBinUpEdge(nbins)

          # Style
          Stack.GetXaxis().SetRangeUser(minX,maxX)
          Stack.GetXaxis().SetLabelSize(0.)
          Stack.GetXaxis().SetTitleSize(0.)
          Stack.GetYaxis().SetTitleSize(20)
          Stack.GetYaxis().SetTitleFont(43)
          Stack.GetYaxis().SetLabelFont(43)
          Stack.GetYaxis().SetLabelSize(19)
          Stack.GetYaxis().SetTitleOffset(1.6)
          Stack.GetYaxis().SetTitle('Events / bin-width')

          # Draw legends
          Legends.Draw('same')

          # Add ATLAS legend
          atlas_legend_block, status = get_atlas_legend_block(True, False, 0.18 if 'delta' not in obs else 0, -0.03)
          check_status(status)
          atlas_legend_block.SetNDC()
          atlas_legend_block.Draw('same')
          # Add sqrt(s) and lumi info
          info_block = get_info_block(0.18 if 'delta' not in obs else 0, -0.05)
          info_block.SetNDC()
          info_block.Draw('same')

          # Show event type
          DecayType = 'ee' if channel == 'EL' else '#mu#mu'
          EvtType = '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 1 b-tagged jet}' if case == 'Ti1' else '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 2 b-tagged jets}'
          TextBlock = ROOT.TLatex(0.38 if 'delta' not in obs else 0.2, 0.68, EvtType)
          TextBlock.SetNDC()
          TextBlock.Draw('same')

          ROOT.gPad.RedrawAxis()

          # TPad for bottom plot
          Canvas.cd()
          pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
          pad2.SetTopMargin(0.03)
          pad2.SetBottomMargin(0.32)
          pad2.Draw()
          pad2.cd()

          # Set log-x scale (if requested)
          if obs in Logx:
            pad2.SetLogx()

          # Create and draw ratio plots
          RatioStack = ROOT.THStack()
          for CR in AltCRs:
            ratioHist = Hists[CR].Clone('ratioHist_{}_{}_{}_{}'.format(channel,obs,case,CR))
            ratioHist.Divide(Hists['VR'])
            RatioStack.Add(ratioHist,'p')
          RatioStack.Draw('nostack')
          RatioStack.GetYaxis().SetTitle("CR'/CR")
          RatioStack.GetXaxis().SetRangeUser(minX,maxX)
          RatioStack.GetXaxis().SetTitleSize(20)
          RatioStack.GetXaxis().SetTitleFont(43)
          RatioStack.GetXaxis().SetLabelFont(43)
          RatioStack.GetXaxis().SetLabelSize(19)
          RatioStack.GetXaxis().SetTitleOffset(1.3)
          RatioStack.GetYaxis().SetTitleSize(20)
          RatioStack.GetYaxis().SetTitleFont(43)
          RatioStack.GetYaxis().SetLabelFont(43)
          RatioStack.GetYaxis().SetLabelSize(19)
          RatioStack.GetYaxis().SetTitleOffset(1.6)
          RatioStack.GetYaxis().SetRangeUser(0.79,1.21)
          if obs in XaxisTitles:
            if 'Zpt' in obs and channel == 'EL':
              RatioStack.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
            else:
              RatioStack.GetXaxis().SetTitle(XaxisTitles[obs])

          # Draw line at data/MC==1
          Line = ROOT.TLine(minX,1,maxX,1)
          Line.SetLineStyle(7)
          Line.Draw("same")

          Canvas.Print(outName)
          Canvas.Print(outName+']')
        else:
          quantileType = 'LeadQuantile' if 'Lead' in obs else 'CombQuantile'
          Var          = obs.replace('{}{}_vs_'.format(Tagger,quantileType),'')
          nObsBins     = len(Binning[Var])-1 if len(Binning[Var]) > 3 else Binning[Var][0]
          for obsBin in range(0,nObsBins):
            HistBin = obsBin + 1

            # TCanvas
            Canvas  = ROOT.TCanvas()
            outName = "Plots/{}/{}/ttbar_DataDriven_Signal{}_{}_{}_{}_{}.{}".format(Version, VS, SignalGenerator, Campaign, obs + '_bin_{}'.format(HistBin), case, channel, ftype)
            Canvas.Print(outName+']')

            # TPad for upper panel
            pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
            pad1.SetTopMargin(0.08)
            pad1.SetBottomMargin(0.03)
            pad1.Draw()
            pad1.cd()

            # Set log scales (if requested)
            if obs in Logx:
              pad1.SetLogx()
            if obs in Logy:
              pad1.SetLogy()

            # Legend
            Legends = ROOT.TLegend(0.2,0.73,0.32,0.9)
            Legends.SetTextFont(42)

            # THStack
            Stack = ROOT.THStack()

            # Loop over CRs
            Hists = dict()
            for counter, CR in enumerate(CRs):
              # Get data-driven ttbar estimation
              DataHist = InFiles[CR][channel].Get('ttbar_SR_{}_{}_{}'.format(channel,obs,case))
              if not DataHist:
                print('ERROR: {} not found in {}, exiting'.format('ttbar_SR_{}_{}_{}'.format(channel,obs,case),CR))
                sys.exit()
              DataHist.SetDirectory(0)
              DataHist = DataHist.ProjectionY(DataHist.GetName()+'_{}_proj{}'.format(CR,obsBin), HistBin, HistBin)
              DataHist.SetLineColor(Colors[counter])
              DataHist.SetMarkerColor(Colors[counter])
              DataHist.SetMarkerStyle(Markers[counter])
              Legend = {
                'VR'     : 'CR',
                'AltVR'  : 'Alternative CR',
                'SumVRs' : 'CR + AltCR',
              }[CR]
              Legends.AddEntry(DataHist,Legend,'p')
              Hists[CR] = DataHist
              Stack.Add(DataHist,"p")

            # Draw Stack
            Stack.Draw('nostack')
            Stack.GetXaxis().SetLabelSize(0.)
            Stack.GetXaxis().SetTitleSize(0.)
            Stack.GetYaxis().SetTitleSize(20)
            Stack.GetYaxis().SetTitleFont(43)
            Stack.GetYaxis().SetLabelFont(43)
            Stack.GetYaxis().SetLabelSize(19)
            Stack.GetYaxis().SetTitleOffset(1.6)
            Stack.GetYaxis().SetTitle('Events')

            # X-axis range
            minX  = Hists[CRs[0]].GetXaxis().GetBinLowEdge(1)
            nbins = Hists[CRs[0]].GetNbinsX()
            maxX  = Hists[CRs[0]].GetXaxis().GetBinUpEdge(nbins)

            # Draw legends
            Legends.Draw('same')

            # Add ATLAS legend
            atlas_legend_block, status = get_atlas_legend_block(True, False, 0.18 if 'delta' not in obs else 0, -0.03)
            check_status(status)
            atlas_legend_block.SetNDC()
            atlas_legend_block.Draw('same')
            # Add sqrt(s) and lumi info
            info_block = get_info_block(0.18 if 'delta' not in obs else 0, -0.05)
            info_block.SetNDC()
            info_block.Draw('same')

            # Show event type
            DecayType = 'ee' if channel == 'EL' else '#mu#mu'
            EvtType = '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 1 b-tagged jet}' if case == 'Ti1' else '#scale[1.3]{Z(#rightarrow' + DecayType + ') + #geq 2 b-tagged jets}'
            TextBlock = ROOT.TLatex(0.38 if 'delta' not in obs else 0.2, 0.68, EvtType)
            TextBlock.SetNDC()
            TextBlock.Draw('same')

            # Show observable's bin
            ObsBin = '{} bin {}'.format(Var,HistBin)
            TextBlock2 = ROOT.TLatex(0.38 if 'delta' not in obs else 0.2, 0.63, ObsBin)
            TextBlock2.SetNDC()
            TextBlock2.Draw('same')

            ROOT.gPad.RedrawAxis()

            # TPad for bottom plot
            Canvas.cd()
            pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
            pad2.SetTopMargin(0.03)
            pad2.SetBottomMargin(0.32)
            pad2.Draw()
            pad2.cd()

            # Set log-x scale (if requested)
            if obs in Logx:
              pad2.SetLogx()

            # Create and draw ratio plots
            RatioStack = ROOT.THStack()
            for CR in AltCRs:
              ratioHist = Hists[CR].Clone('ratioHist_{}_{}_{}_{}'.format(channel,obs,case,CR))
              ratioHist.Divide(Hists['VR'])
              RatioStack.Add(ratioHist,'p')
            RatioStack.Draw('nostack')
            RatioStack.GetYaxis().SetTitle("CR'/CR")
            RatioStack.GetXaxis().SetTitleSize(20)
            RatioStack.GetXaxis().SetTitleFont(43)
            RatioStack.GetXaxis().SetLabelFont(43)
            RatioStack.GetXaxis().SetLabelSize(19)
            RatioStack.GetXaxis().SetTitleOffset(1.3)
            RatioStack.GetXaxis().SetTitle('b-tagging quantile')
            RatioStack.GetYaxis().SetTitleSize(20)
            RatioStack.GetYaxis().SetTitleFont(43)
            RatioStack.GetYaxis().SetLabelFont(43)
            RatioStack.GetYaxis().SetLabelSize(19)
            RatioStack.GetYaxis().SetTitleOffset(1.6)
            RatioStack.GetYaxis().SetRangeUser(0.71,1.29)
            if obs in XaxisTitles:
              if 'Zpt' in obs and channel == 'EL':
                RatioStack.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
              else:
                RatioStack.GetXaxis().SetTitle(XaxisTitles[obs])

            # Draw line at data/MC==1
            Line = ROOT.TLine(minX,1,maxX,1)
            Line.SetLineStyle(7)
            Line.Draw("same")

            Canvas.Print(outName)
            Canvas.Print(outName+']')

# Close files
for _, files in InFiles.items():
  for _, ifile in files.items():
    ifile.Close()

print('>>> ALL DONE <<<')
