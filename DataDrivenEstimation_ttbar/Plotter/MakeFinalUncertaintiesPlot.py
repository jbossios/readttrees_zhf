######################################################################################
#                                                                                     #
# Purpose: Compare systematic uncertainties on data-driven ttbar estimation           #
# Author:  Jona Bossio (jbossios@cern.ch)                                             #
#                                                                                     #
######################################################################################

Debug = False

import ROOT
import os
import sys
import math
import argparse

from style import *
from helpful_functions import check_status

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
from getSFvarNames import getBTagSFvarNames

# Import things from Reader
sys.path.insert(1, '../../')  # insert at 1, 0 is the script path
from Dicts       import *
from Systematics import *
from SystematicTTrees import *

# Import things from Reader's plotter
sys.path.insert(1, '../../Plotter')  # insert at 1, 0 is the script path
from Style import *

def main(args):
    # Get provided arguments
    signal_gen = args.signalGen
    channel = args.channel
    obs = args.obs
    case = args.case
    smooth = args.smooth

    # Read common definitions
    version = opt['Version']
    campaign = opt['Campaign']
    tagger = opt['Tagger']

    # Set output folder
    output_folder = 'Plots/{}/CompareTTbarUncertainties'.format(version)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # AtlasStyle
    ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
    ROOT.SetAtlasStyle()
    ROOT.gROOT.SetBatch(True)

    # Open input file
    extra_cr = '{}' if args.control_region == 'VR' else '_{}'.format(args.control_region)
    InputFileName = '../Outputs/TTbarUncertainties_Signal{}_{}_{}_{}{}_AllSysts{}.root'.format(signal_gen, version, channel, campaign, extra_cr, '_smoothed' if smooth else '')
    InputFile = ROOT.TFile.Open(InputFileName)
    if not InputFile:
        print('ERROR: {} not found, exiting'.format(InputFile))
        sys.exit(1)

    formats = ['pdf', 'eps']
    for ftype in formats:
      # TCanvas
      Canvas  = ROOT.TCanvas()
      outName = "{}/{}_{}_{}_Signal{}_FinalTTbarUncertainties{}.{}".format(output_folder, obs, case, channel, signal_gen, '_smoothed' if smooth else '', ftype)
      Canvas.Print(outName + '[')

      # Legend
      Legends = ROOT.TLegend(0.2, 0.2, 0.32, 0.47)
      Legends.SetTextFont(42)

      # Get total uncertainties
      if Debug: print('Get total uncertainty histograms...')
      hists = dict()
      signs = ['UP', 'DOWN']
      for sign in signs:
          HistName = 'TTbar{}_{}_{}_{}_Total{}Uncertainty'.format(campaign,channel,obs,case,sign)
          Hist = InputFile.Get(HistName)
          if not Hist:
              print('ERROR: {} not found in {}, exiting'.format(HistName, InputFileName))
              sys.exit(1)
          Hist.SetDirectory(0)
          hists['Total_' + sign] = Hist

      # Procedure: make list of histograms for each final source, then create a histogram cloning Total_UP hist and compute quad sum and fill histogram

      # Loop over SF-based systematic uncertainties
      if Debug: print('Loop over SF-based systematic uncertainties...')
      for source, nNPs in SFsysts.items():
          if source == 'nominal': continue # skip nominal
          for iNP in range(1, nNPs+1):
              # Get uncertainty hist
              HistName = 'TTbar{}_{}_{}_{}__{}_{}'.format(campaign, channel, obs, case, source, iNP)
              Hist = InputFile.Get(HistName)
              if not Hist:
                  print('ERROR: {} not found in {}, exiting'.format(HistName, InputFileName))
                  sys.exit(1)
              Hist.SetDirectory(0)
              hists[source + '_' + str(iNP)] = Hist

      # Loop over non-SF-based systematic sources
      if Debug: print('Loop over non-SF-based systematic uncertainties...')
      for ttree_name in SystTTrees:
          # Get uncertainty hist
          HistName = 'TTbar{}_{}_{}_{}_{}'.format(campaign, channel, obs, case, ttree_name if 'Quantile' not in obs else '_{}'.format(ttree_name))
          Hist = InputFile.Get(HistName)
          if not Hist:
              print('ERROR: {} not found in {}, exiting'.format(HistName, InputFileName))
              sys.exit(1)
          Hist.SetDirectory(0)
          hists[ttree_name] = Hist

      # Prepare final histograms for each source
      if Debug: print('Prepare final histograms for each source...')
      final_sources = ['JET', 'MET', 'PRW', 'FT', 'MUON', 'EG']
      labels_by_final_sources = {
          'JET': ['JET', 'JVT'],
          'MET': ['MET'],
          'PRW': ['weight_pileup'],
          'FT': ['BTag'],
          'MUON': ['MU'],
          'EG': ['EL', 'EG'],
      }
      for source in final_sources:
          for sign in signs:
              hist = hists['Total_UP'].Clone('Total_{}_{}_{}_{}'.format(obs, case, source, sign))
              for xbin in range(1, hist.GetNbinsX() + 1): # loop over bins
                  sum_squared_uncerts = 0
                  for name, h in hists.items():
                      if 'Total' in name: continue  # skip total uncertainty components
                      for label in labels_by_final_sources[source]:
                          if label in name:
                              uncert = h.GetBinContent(xbin)
                              if (sign == 'UP' and uncert >= 0) or (sign == 'DOWN' and uncert < 0):
                                  sum_squared_uncerts += uncert * uncert
                  bin_content = math.sqrt(sum_squared_uncerts)
                  hist.SetBinContent(xbin, bin_content if sign == 'UP' else -1 * bin_content)
              hists['Total_{}_{}'.format(source, sign)] = hist

      # Make figure
      if Debug: print('Make TMG...')
      colors = {
          'Total': ROOT.kBlack,
          'JET': ROOT.kBlue,
          'MET': ROOT.kRed + 1,
          'FT': ROOT.kGreen,
          'PRW': ROOT.kMagenta,
          'MUON': ROOT.kCyan,
          'EG': ROOT.kOrange,
      }
      TMG = ROOT.TMultiGraph()
      for key, color in colors.items():
          for sign in signs:
              graph = ROOT.TGraph(hists['Total_' + key + '_' + sign if key != 'Total' else 'Total_' + sign])
              graph.SetLineColor(color)
              graph.SetMarkerColor(color)
              if sign == 'UP': Legends.AddEntry(graph, key, 'p')
              TMG.Add(graph)

      if Debug: print('Draw TMG...')
      TMG.Draw('a')
      TMG.GetYaxis().SetTitle('Uncertainty')
      if obs in XaxisTitles:
          if obs == 'Zpt':
              if channel == 'EL':
                  TMG.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
              if channel == 'MU':
                  TMG.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]")
          else:
              TMG.GetXaxis().SetTitle(XaxisTitles[obs])
      if 'LeadQuantile' in obs:
          TMG.GetXaxis().SetTitle('Leading b-tagging quantile')
      elif 'CombQuantile' in obs:
          TMG.GetXaxis().SetTitle('Combined b-tagging quantiles')

      ROOT.gPad.Modified();
      TMG.GetYaxis().SetRangeUser(-0.1, 0.1)

      if Debug: print('Before drawing legends')
      Legends.Draw('same')

      # Add ATLAS legend
      atlas_legend_block, status = get_atlas_legend_block(False, False)
      check_status(status)
      atlas_legend_block.SetNDC()
      atlas_legend_block.Draw('same')
      # Add sqrt(s) and lumi info
      info_block = get_info_block()
      info_block.SetNDC()
      info_block.Draw('same')

      # Show event type
      DecayMode = 'ee' if channel == 'EL' else '#mu#mu'
      EvtType = 'Z(#rightarrow' + DecayMode + ') + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z(#rightarrow' + DecayMode + ') + #geq 2 b-tagged jets'
      TextBlock = ROOT.TLatex(0.2, 0.75, EvtType)
      TextBlock.SetNDC()
      TextBlock.Draw('same')

      if Debug: print('Save PDF')
      Canvas.Print(outName)
      Canvas.Print(outName + ']')

      if Debug: print('close input file')
    InputFile.Close()
    print('>>> ALL DONE <<<')

if __name__ == '__main__':
    # Read arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--channel', action='store', dest="channel", default = '', help = 'Channel (i.e. EL or MU)')
    parser.add_argument('--obs', action='store', dest="obs", default = '', help = "Observable's name")
    parser.add_argument('--case',action='store', dest="case", default = '', help = 'Btagging case (i.e. Ti1 or Ti2)')
    parser.add_argument('--signalGen', action='store', dest="signalGen", default = '', help = 'Generator for signal samples')
    parser.add_argument('--cr', action='store', dest='control_region', default = 'SumVRs', help = 'Control region (VR, AltVR, SumVRs)')
    parser.add_argument('--smooth', action='store_true', help = 'Use smoothed inputs')
    args = parser.parse_args()
    
    # Protection
    if not args.channel or not args.obs or not args.case or not args.signalGen:
      print('ERROR: missing arguments')
      parser.print_help()
      sys.exit(1)

    main(args)
