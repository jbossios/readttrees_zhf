#############################################################################
#                                                                           #
# Purpose: Compare systematic uncertainties on data-driven ttbar estimation #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

# Generator to be used for signal
SignalGenerator = 'SH2211' # options: MG and SH2211

Systematics = 'AllSysts'

Smooth = True  # Use Gaussian kernel smoothing

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

import ROOT
import os
import sys
import copy

# Protection
if SignalGenerator not in ['SH2211', 'MG']:
  print('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))
  sys.exit(1)

# Sources of uncertainties
Sources2Compare = [
  'Reco',
  'PID',
  'Iso',
  'Trig',
  'BTag_B',
  'BTag_C',
  'BTag_Light',
  'JET',
  'MET',
  'MUON',
  'EG',
]

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Tagger   = opt['Tagger']
Channels = opt['Channels']
Observables = opt['Observables']

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts       import Binning
from Systematics import *

# Import things from Reader
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style import *

# Add b-tagging quantile vs obs
FinalObservables = copy.deepcopy(Observables)
for obs, cases in Observables.items():
  # Loop over bins for this observable
  nObsBins = len(Binning[obs])-1 if len(Binning[obs]) > 3 else Binning[obs][0]
  for obsBin in range(1,nObsBins+1):
    vsBin   = '_bin_'+str(obsBin) if obsBin > 9 else '_bin_0'+str(obsBin)
    if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}{}'.format(Tagger,obs,vsBin)] = ['Ti1']
    if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}{}'.format(Tagger,obs,vsBin)] = ['Ti2']

commands = []
# Loop over channels
for channel in Channels:
  # Loop over type of uncertainties to compare
  for Type in Sources2Compare:
    command = ''
    # Loop over observables
    for obs, cases in FinalObservables.items():
      # Loop over event types
      for case in cases:
        command += 'python CompareTTbarUncertainties.py --channel {} --obs {} --case {} --type {} --signalGen {} --systs {}{} && '.format(channel,obs,case,Type,SignalGenerator,Systematics, ' --smooth' if Smooth else '')
    command = command[:-2]
    commands.append(command)

for command in commands:
  os.system(command)
