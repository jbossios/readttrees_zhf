import ROOT

ATLAS_LEGEND = 'Internal'

def get_atlas_legend_block(scale_text, simulation = False, shift_x = 0, shift_y = 0):
    sim = '' if not simulation else 'Simulation '
    if ATLAS_LEGEND == "Internal":
        atlas = "#font[72]{ATLAS} #font[42]{" + sim + "Internal}"
    elif ATLAS_LEGEND == "Preliminary":
        atlas = "#font[72]{ATLAS} #font[42]{" + sim + "Preliminary}"
    elif ATLAS_LEGEND == "ATLAS":
        atlas = "#font[72]{ATLAS}"
    else:
        return "ATLAS legend not recognized, exiting"
    if scale_text:
        atlas = '#scale[1.3]{#scale[1.4]{' + atlas + '}}'
    atlas_block = ROOT.TLatex(0.2 + shift_x, 0.85 + shift_y, atlas)
    return atlas_block, 'OK'


def get_info_block(shift_x = 0, shift_y = 0):
    info_block = ROOT.TLatex(0.2 + shift_x, 0.8 + shift_y, "#sqrt{s} = 13 TeV, 139 fb^{-1}")
    return info_block
