#############################################################################
#                                                                           #
# Purpose: Compare SFs between MC16 campaigns for all observables/channels  #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

Campaigns = ['MC16all', 'MC16a', 'MC16d', 'MC16e']

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

formats = ['pdf', 'eps']

import ROOT
import os
import sys

from style import *
from helpful_functions import check_status

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Channels = opt['Channels']
Tagger   = opt['Tagger']
Version  = opt['Version']

# Create output folder for plots
os.system('mkdir -p Plots/{}/CompareSFs_BetweenCampaigns'.format(Version))

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts import *

# Import things from Reader
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style import *

# Styles
Colors  = [ROOT.kBlack,ROOT.kRed,ROOT.kBlue,ROOT.kMagenta]
Markers = [20,21,22,23]

# Add b-tagging quantile vs obs
Observables = opt['Observables']
FinalObservables = copy.deepcopy(Observables)
for obs, cases in Observables.items():
  # Loop over bins for this observable
  nObsBins = len(Binning[obs])-1 if len(Binning[obs]) > 3 else Binning[obs][0]
  for obsBin in range(0,nObsBins):
    vsBin   = '_bin_'+str(obsBin) if obsBin > 9 else '_bin_0'+str(obsBin)
    if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}{}'.format(Tagger, obs, vsBin)] = cases
    if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}{}'.format(Tagger,obs,vsBin)] = ['Ti2']

for ftype in formats:
  # Loop over observables
  for obs, cases in FinalObservables.items():
    # Loop over event types
    for case in cases:
      # Loop over channels
      for channel in Channels:

        # TCanvas
        Canvas = ROOT.TCanvas()
        extra = '_vs_'.join(Campaigns)
        outName = "Plots/{}/CompareSFs_BetweenCampaigns/{}_{}_{}_{}_ttbar_CRtoSRSF.{}".format(Version, obs, case, channel, extra, ftype)
        Canvas.Print(outName+']')
        if obs in Logx:
          Canvas.SetLogx()

        # Legend
        Legends = ROOT.TLegend(0.8,0.63,0.92,0.9)
        Legends.SetTextFont(42)

        # Loop over channels and compare SFs
        counter = 0
        for campaign in Campaigns:
          # Open input file
          InputFileName = '../Outputs/ttbarSFs_{}_{}_{}.root'.format(Version, channel, campaign)
          InputFile     = ROOT.TFile.Open(InputFileName)
          if not InputFile:
            print('ERROR: {} not found, exiting'.format(InputFile))
            sys.exit(1)
          # Get SF histogram
          HistName = '{}_{}_{}_{}SF'.format(obs.split('_bin_')[0],case,campaign,channel)
          Hist     = InputFile.Get(HistName)
          if not Hist:
            print('ERROR: {} not found in {}, exiting'.format(HistName,InputFile))
            sys.exit(1)
          Hist.SetDirectory(0)
          InputFile.Close()
          if '_bin_' in obs:
            HistBin  = int(obs.split('_bin_')[1])+1
            HistProj = Hist.ProjectionY(Hist.GetName()+'_proj', HistBin, HistBin)
            Hist     = HistProj
          if counter == 0:
            Hist.Draw('E P')
          else:
            Hist.Draw('E P same')
          Hist.GetYaxis().SetTitle('CR#rightarrow SR transfer factor')
          Hist.SetLineColor(Colors[counter])
          Hist.SetMarkerColor(Colors[counter])
          Hist.SetMarkerStyle(Markers[counter])
          if channel == 'EL':
            Hist.GetYaxis().SetRangeUser(0.25,0.65)
          else: # MU
            Hist.GetYaxis().SetRangeUser(0.4,0.9)
          if obs in XaxisTitles:
            if 'Zpt' in obs and channel == 'EL':
              Hist.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
            else:
              Hist.GetXaxis().SetTitle(XaxisTitles[obs])
          if 'LeadQuantile' in obs:
            Hist.GetXaxis().SetTitle('Leading b-tagging quantile')
          elif 'CombQuantile' in obs:
            Hist.GetXaxis().SetTitle('Combined b-tagging quantiles')
          Legends.AddEntry(Hist,campaign,'p')
          counter += 1
        Legends.Draw('same')
        # Add ATLAS legend
        atlas_legend_block, status = get_atlas_legend_block(False, True)
        check_status(status)
        atlas_legend_block.SetNDC()
        atlas_legend_block.Draw('same')
        # Add sqrt(s) and lumi info
        info_block = get_info_block()
        info_block.SetNDC()
        info_block.Draw('same')
        # Show event type
        if channel == 'EL':
          Zdecay = '(#rightarrow ee)'
          EvtType = 'Z{} + #geq 1 b-tagged jet'.format(Zdecay) if case == 'Ti1' else 'Z' + Zdecay + ' + #geq 2 b-tagged jets'
        else: # MU
          Zdecay = '(#rightarrow#mu#mu)'
          EvtType = 'Z{} + #geq 1 b-tagged jet'.format(Zdecay) if case == 'Ti1' else 'Z' + Zdecay + ' + #geq 2 b-tagged jets'
        TextBlock = ROOT.TLatex(0.2, 0.75, EvtType)
        TextBlock.SetNDC()
        TextBlock.Draw('same')
        if '_bin_' in obs:
          ObsSplit = obs.split('_bin_')
          ObsBin = '{} bin {}'.format(ObsSplit[0].split('_vs_')[1],ObsSplit[1])
          TextBlock2 = ROOT.TLatex(0.2, 0.7, ObsBin)
          TextBlock2.SetNDC()
          TextBlock2.Draw('same')
        Canvas.Print(outName)
        Canvas.Print(outName+']')
    
print('>>> ALL DONE <<<')
