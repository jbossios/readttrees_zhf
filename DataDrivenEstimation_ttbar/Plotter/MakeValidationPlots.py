#####################################################################################
#                                                                                   #
# Purpose: Compare data in target region with ttbar prediction using another region #
# Author:  Jona Bossio (jbossios@cern.ch)                                           #
#                                                                                   #
#####################################################################################

import ROOT
import copy
import os
import sys

# Import settings and smoother
sys.path.insert(1, '../')  # insert at 1, 0 is the script path
from CommonDefs import Options as opt
from smoother import smooth_hist

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../')  # insert at 1, 0 is the script path
from Dicts import *

# Import things from Plotter
sys.path.insert(1, '../../Plotter')  # insert at 1, 0 is the script path
from Style import *
from InputFiles import *
from Luminosities import *
from PlotterHelperFunctions import *


def get_input_file(input_files_dict, key):
    """ Get already opened input file or open it if not opened already """
    # protection
    if key not in InputFiles:
        msg = key + " not found in InputFiles dictionary, exiting"
        FATAL(msg)
    if key not in input_files_dict:  # open file
        input_file_name = InputFiles[key]
        input_file = ROOT.TFile.Open(input_file_name)
        CHECKObj(input_file, f'{input_file_name} not found, exiting')
        input_files_dict[key] = input_file
    return input_files_dict, input_files_dict[key]


def get_variated_predictions(nominal_file, smoothed_ratio, target_channel, obs, case):
    variated_histograms = []
    # Observable distribution
    nominal_hist = nominal_file.Get(f'ttbar_SR_{target_channel}_{obs}_{case}')
    nominal_hist.SetDirectory(0)
    nominal_hist.SetName(f'ttbar_nominal_SR_{target_channel}_{obs}_{case}')
    nominal_variated = nominal_hist.Clone(f'ttbar_SR_{target_channel}_{obs}_{case}')
    nominal_variated.Multiply(smoothed_ratio)
    nominal_variated.Add(nominal_hist)
    variated_histograms.append(nominal_variated)
    # PCBT vs observable distribution
    quantile_case = 'Lead' if case == 'Ti1' else 'Comb'
    pcbt_vs_obs_hist = nominal_file.Get(f'ttbar_SR_{target_channel}_DL1r{quantile_case}Quantile_vs_{obs}_{case}')
    pcbt_vs_obs_hist.SetDirectory(0)
    pcbt_vs_obs_hist.SetName(f'ttbar_nominal_SR_{target_channel}_DL1r{quantile_case}Quantile_vs_{obs}_{case}')
    pcbt_vs_obs_variated = pcbt_vs_obs_hist.Clone(f'ttbar_SR_{target_channel}_DL1r{quantile_case}Quantile_vs_{obs}_{case}')
    nxbins = pcbt_vs_obs_variated.GetNbinsX()
    nybins = pcbt_vs_obs_variated.GetNbinsY()
    for xbin in range(1, nxbins + 1):  # loop over observable bins
        # use uncertainty vs PCBT bins
        uncertainty = smoothed_ratio.GetBinContent(xbin) + 1.0
        for ybin in range(1, nybins + 1):  # loop over quantile bins
            binvalue = pcbt_vs_obs_variated.GetBinContent(xbin, ybin)
            pcbt_vs_obs_variated.SetBinContent(xbin, ybin, binvalue * uncertainty)
    variated_histograms.append(pcbt_vs_obs_variated)
    return variated_histograms


def main(campaign, cr, channel, target_region, target_channel, gen, debug):

    # Protections
    if gen not in ['SH2211', 'MG']:
        print(f'SignalGenerator ({gen}) not recognized, exiting')
        sys.exit(1)

    # Read common options
    Version = opt['Version']
    Tagger = opt['Tagger']
    Observables = opt['Observables']
    Backgrounds = opt['Backgrounds']

    # Replace Zjets from Backgrounds list by Zee/Zmumu/Ztautau
    if 'Zjets' in Backgrounds:
        Backgrounds.remove('Zjets')
        Backgrounds.append('Zee{}'.format(gen))
        Backgrounds.append('Zmumu{}'.format(gen))
        Backgrounds.append('Ztautau')

    # Create output folder for plots
    output_folder = f'Plotter/Plots/{Version}/Validation_{cr}_{channel}_to_{target_region}/'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Get luminosity for each MC16 campaign
    Luminosity = dict()
    Luminosity['a'] = Luminosity_2015 + Luminosity_2016
    Luminosity['d'] = Luminosity_2017
    Luminosity['e'] = Luminosity_2018

    DataPeriod = 'data'
    if campaign == 'all':
        DataPeriod += '15161718'
        DataPeriods = ['15', '16', '17', '18']
    elif campaign == 'a':
        DataPeriod += '1516'
        DataPeriods = ['15', '16']
    elif campaign == 'd':
        DataPeriod += '17'
        DataPeriods = ['17']
    elif campaign == 'e':
        DataPeriod += '18'
        DataPeriods = ['18']
    else:
        print('ERROR: {} campaign not recognized, exiting')
        sys.exit(1)

    campaigns = []
    if '15' in DataPeriod:
        campaigns.append('a')
    if '16' in DataPeriod and 'a' not in campaigns:
        campaigns.append('a')
    if '17' in DataPeriod:
        campaigns.append('d')
    if '18' in DataPeriod:
        campaigns.append('e')
      
    # Collect opened files
    input_files = {}

    # Get root file with nominal data-driven ttbar predictions
    nominal_file_name = f'Outputs/ttbarContribution_Signal{gen}_{Version}_{target_channel}_{DataPeriod}_SumVRs.root'
    nominal_file = ROOT.TFile.Open(nominal_file_name)

    # Loop over observables
    out_hists = []
    for obs, cases in Observables.items():
        # Loop over event types
        for case in cases:
            # TCanvas
            Canvas = ROOT.TCanvas()
            outName = f"{output_folder}/{DataPeriod}_{cr}_{channel}_vs_extrapolatedData_{target_region}_{target_channel}_Signal{gen}_{obs}_{case}.pdf"
            Canvas.Print(outName + ']')
    
            # TPad for upper panel
            pad1 = ROOT.TPad("pad1", "pad1", 0, 0.4, 1, 1.0)
            pad1.SetTopMargin(0.08)
            pad1.SetBottomMargin(0.03)
            pad1.Draw()
            pad1.cd()
    
            # Set log scales (if requested)
            if obs in Logx:
                pad1.SetLogx()
            if obs in Logy:
                pad1.SetLogy()
    
            # Legend
            Legends = ROOT.TLegend(0.8, 0.73, 0.92, 0.9)
            Legends.SetTextFont(42)
    
            # THStack
            Stack = ROOT.THStack()

            Hists = {}
    
            # Get data-subtracted in target region in target channel
            hist_name = obs + '_' + case
            # Loop over data periods
            data_hists = []
            for period in DataPeriods:
                if debug: 'ERROR: Get histogram(' + hist_name + ') for data' + period
                # Open input file
                Key = f"Signal_data{period}_{target_channel}_{target_region}_{Tagger}"
                input_files, File = get_input_file(input_files, Key)
                # Get histogram
                Hist = File.Get(hist_name)
                CHECKObj(Hist, hist_name+" not found on {}, exiting".format(InputFiles[Key]))
                Hist.SetDirectory(0)
                data_hists.append(Hist)

            # Get total data-subtrated distribution
            if debug: print("DEBUG: Get total data histogram")
            hData = data_hists[0].Clone("Data_" + hist_name + '_' + cr)
            for ihist in range(1, len(data_hists)):
                hData.Add(data_hists[ihist])

            # Set list of MC samples to subtract from data 
            bkgs = copy.deepcopy(Backgrounds)
            if target_region == 'CR':
              bkgs = [f'Signal{gen}', 'Ztautau', 'SingleTop', 'Diboson', 'Wtaunu', 'VH']
              if target_channel == 'MU':
                bkgs += ['Wmunu']
              elif target_channel == 'EL':
                bkgs += ['Wenu']

            # Get background estimations
            if debug: print("DEBUG: Get background estimations")
            hBkgs = []
            for cpg in campaigns:
                for bkg in bkgs:
                    # Open File
                    Key = f"{bkg}_MC16{cpg}_{target_channel}_{target_region}_{Tagger}"
                    input_files, File = get_input_file(input_files, Key)
                    # Get histogram
                    Hist = File.Get(hist_name)
                    CHECKObj(Hist,hist_name+" not found on {}, exiting".format(InputFiles[Key]))
                    Hist.SetDirectory(0)
                    Hist.Scale(Luminosity[cpg])
                    hBkgs.append(Hist)

            # Subtract non-ttbar samples to the total data
            if debug: print("DEBUG: Subtract non-ttbar backgrounds to the total data")
            for hist in hBkgs:
                hData.Add(hist, -1)

            hData.Scale(1, 'width')  # divide by bin width
            hData.SetLineColor(Colors[0])
            hData.SetMarkerColor(Colors[0])
            Hists['real'] = hData
            Stack.Add(hData, "p")
            Legends.AddEntry(hData, 'data', 'p')

            # Get data-driven ttbar estimation

            # Open input file with data-driven ttbar estimations
            InputFileName = f'Outputs/ttbarContribution_Signal{gen}_{Version}_{DataPeriod}_{cr}_{channel}_to_{target_region}_{target_channel}.root'
            InFile = ROOT.TFile.Open(InputFileName)
            if not InFile:
                print('ERROR: {} not found, exiting'.format(InputFileName))
                sys.exit(1)
    
            # Get histogram
            hist_name = f'ttbar_{target_region}_{target_channel}_{obs}_{case}'
            Hist = InFile.Get(hist_name)
            if not Hist:
                print(f'ERROR: {hist_name} not found, exiting')
                sys.exit()
            Hist.SetDirectory(0)
            InFile.Close()  # close file
            pred_hist = Hist.Clone("Prediction_" + hist_name + '_' + cr)
            pred_hist.Scale(1, 'width')  # divide by bin width
            pred_hist.SetLineColor(Colors[1])
            pred_hist.SetMarkerColor(Colors[1])
            Hists['prediction'] = pred_hist
            Stack.Add(pred_hist,"p")
            Legends.AddEntry(pred_hist, 'prediction', 'p')
    
            # X-axis range
            if obs in XaxisRange:
                minX = XaxisRange[obs][0]
                maxX = XaxisRange[obs][1]
            else:
                minX  = Hist.GetXaxis().GetBinLowEdge(1)
                nbins = Hist.GetNbinsX()
                maxX  = Hist.GetXaxis().GetBinUpEdge(nbins)
    
            # Draw histograms and legends
            Stack.Draw('nostack')
            Legends.Draw('same')
    
            # Style
            Stack.GetXaxis().SetRangeUser(minX, maxX)
            Stack.GetXaxis().SetLabelSize(0.)
            Stack.GetXaxis().SetTitleSize(0.)
            Stack.GetYaxis().SetTitleSize(20)
            Stack.GetYaxis().SetTitleFont(43)
            Stack.GetYaxis().SetLabelFont(43)
            Stack.GetYaxis().SetLabelSize(19)
            #Stack.GetYaxis().SetTitleOffset(1.3)
            Stack.GetYaxis().SetTitleOffset(1.5)
            Stack.GetYaxis().SetTitle('Events / bin-width')

            # Show ATLAS legend
            atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
            ATLASBlock = ROOT.TLatex(0.4, 0.80, atlas)
            ATLASBlock.SetNDC()
            ATLASBlock.Draw("same")
    
            # Show event type
            ll = '#rightarrow#mu#mu' if target_channel == 'MU' else '#rightarrow ee'
            EvtType = 'Z('+ll+') + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z('+ll+') + #geq 2 b-tagged jets'
            #TextBlock = ROOT.TLatex(0.4, 0.85, EvtType)
            TextBlock = ROOT.TLatex(0.4, 0.75, EvtType)
            TextBlock.SetNDC()
            TextBlock.Draw('same')

            # Show event type
            region = 'Validation region'
            #TextBlock2 = ROOT.TLatex(0.4, 0.8, region)
            TextBlock2 = ROOT.TLatex(0.4, 0.7, region)
            TextBlock2.SetNDC()
            TextBlock2.Draw('same')
    
            ROOT.gPad.RedrawAxis()
    
            # TPad for bottom plot
            Canvas.cd()
            pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.4)
            pad2.SetTopMargin(0.03)
            pad2.SetBottomMargin(0.32)
            pad2.Draw()
            pad2.cd()
    
            # Set log-x scale (if requested)
            if obs in Logx:
                pad2.SetLogx()
    
            # Create and draw ratio plot
            ratioHist = Hists['prediction'].Clone('ratioHist_{}_{}'.format(obs, case))
            ratioHist.SetLineColor(Colors[1])
            ratioHist.SetMarkerColor(Colors[1])
            ratioHist.Divide(Hists['real'])
            ratioHist.Draw('e0')
            ratioHist.GetYaxis().SetTitle('prediction / data')
            ratioHist.GetXaxis().SetRangeUser(minX, maxX)
            ratioHist.GetXaxis().SetTitleSize(20)
            ratioHist.GetXaxis().SetTitleFont(43)
            ratioHist.GetXaxis().SetLabelFont(43)
            ratioHist.GetXaxis().SetLabelSize(19)
            ratioHist.GetXaxis().SetTitleOffset(1.2)
            if obs in XaxisTitles:
                if obs == 'Zpt':
                    if target_channel == 'EL':
                        ratioHist.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
                    elif target_channel == 'MU':
                        ratioHist.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]")
                    else:
                        ratioHist.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}e^{-}/#mu^{-}e^{+}) [GeV]")
                else:
                    ratioHist.GetXaxis().SetTitle(XaxisTitles[obs])
            ratioHist.GetYaxis().SetTitleSize(20)
            ratioHist.GetYaxis().SetTitleFont(43)
            ratioHist.GetYaxis().SetLabelFont(43)
            ratioHist.GetYaxis().SetLabelSize(19)
            ratioHist.GetYaxis().SetTitleOffset(1.5)
            ratioHist.GetYaxis().SetRangeUser(0.79, 1.21)
            #ratioHist.GetYaxis().SetRangeUser(0.19, 1.81)  # Temporary
    
            # Draw line at data/MC==1
            Line = ROOT.TLine(minX, 1, maxX, 1)
            Line.SetLineStyle(7)
            Line.Draw("same")
    
            Canvas.Print(outName)
            Canvas.Print(outName+']')

            # Calculate relative uncertainty
            uncertHist = Hists['prediction'].Clone('TTbarExtrapolationUncertainty_{}_{}'.format(obs, case))
            uncertHist.Add(Hists['real'], -1)
            uncertHist.Divide(Hists['real'])

            # Smooth ratio and compare with unsmoothed ratio
            #smoothed_ratio = uncertHist.Clone(uncertHist.GetName() + '_smoothed')
            smoothed_ratio = uncertHist.Clone(f'ExtrapolationUncertainty_ttbar_SR_{target_channel}_{obs}_{case}')
            if obs == 'Zpt' or obs == 'mjjHF' or obs == 'HFjet0_pt':
                smoothed_ratio = smooth_hist(smoothed_ratio, True, True)
            else:
                smoothed_ratio = smooth_hist(smoothed_ratio, False, True)
	
            # Get variated prediction
            h_variated_predictions = get_variated_predictions(nominal_file, smoothed_ratio, target_channel, obs, case)
            out_hists += h_variated_predictions

            # Make nonsmoothed vs smoothed plot
            uncertHist.SetLineColor(Colors[0])
            uncertHist.SetMarkerColor(Colors[0])
            smoothed_ratio.SetLineColor(Colors[1])
            smoothed_ratio.SetMarkerColor(Colors[1])
            # TCanvas
            canvas = ROOT.TCanvas("smoothed")
            outName = f"{output_folder}/{DataPeriod}_{cr}_{channel}_vs_extrapolatedData_{target_region}_{target_channel}_Signal{gen}_{obs}_{case}_smoothed.pdf"
            canvas.Print(outName + '[')
            # Set log scales (if requested)
            if obs in Logx:
                canvas.SetLogx()
            # Legend
            legends = ROOT.TLegend(0.75, 0.73, 0.92, 0.9)
            legends.SetTextFont(42)
            # THStack
            stack = ROOT.THStack()
            stack.Add(uncertHist, "e0")
            legends.AddEntry(uncertHist, 'not smoothed', 'p')
            stack.Add(smoothed_ratio, "e0")
            legends.AddEntry(smoothed_ratio, 'smoothed', 'p')
            stack.Draw('nostack')
            legends.Draw('same')
            stack.GetYaxis().SetTitle('uncertainty')
            stack.GetXaxis().SetRangeUser(minX, maxX)
            stack.GetXaxis().SetTitleSize(20)
            stack.GetXaxis().SetTitleFont(43)
            stack.GetXaxis().SetLabelFont(43)
            stack.GetXaxis().SetLabelSize(19)
            #stack.GetXaxis().SetTitleOffset(2)
            if obs in XaxisTitles:
                if obs == 'Zpt':
                    if target_channel == 'EL':
                        stack.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
                    elif target_channel == 'MU':
                        stack.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]")
                    else:
                        stack.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}e^{-}/#mu^{-}e^{+}) [GeV]")
                else:
                    stack.GetXaxis().SetTitle(XaxisTitles[obs])
            stack.GetYaxis().SetTitleSize(20)
            stack.GetYaxis().SetTitleFont(43)
            stack.GetYaxis().SetLabelFont(43)
            stack.GetYaxis().SetLabelSize(19)
            #stack.GetYaxis().SetRangeUser(-0.05, 0.3)
            canvas.Print(outName)
            canvas.Print(outName+']')

            # Compare smoothed ratio (i.e. final method uncertainty vs total propagated uncertainty)
            InputFileName = 'Outputs/TTbarUncertainties_Signal{}_{}_{}_MC16{}_SumVRs_AllSysts_smoothed.root'.format(gen, Version, target_channel, campaign if campaign != 'all' else 'e')
            InputFile = ROOT.TFile.Open(InputFileName)
            if not InputFile:
                print('ERROR: {} not found, exiting'.format(InputFile))
                sys.exit(1)
            sign = 'UP'
            HistName = 'TTbarMC16{}_{}_{}_{}_Total{}Uncertainty'.format(campaign if campaign != 'all' else 'e', target_channel, obs, case, sign)
            TotalHistUP = InputFile.Get(HistName)
            if not TotalHistUP:
                print('ERROR: {} not found in {}, exiting'.format(HistName, InputFileName))
                sys.exit(1)
            TotalHistUP.SetDirectory(0)
            sign = 'DOWN'
            HistName = 'TTbarMC16{}_{}_{}_{}_Total{}Uncertainty'.format(campaign if campaign != 'all' else 'e', target_channel, obs, case, sign)
            TotalHistDOWN = InputFile.Get(HistName)
            if not TotalHistDOWN:
                print('ERROR: {} not found in {}, exiting'.format(HistName, InputFileName))
                sys.exit(1)
            TotalHistDOWN.SetDirectory(0)
            # TCanvas
            canvas = ROOT.TCanvas("comparison")
            outName = f"{output_folder}/{DataPeriod}_{cr}_{channel}_vs_extrapolatedData_{target_region}_{target_channel}_Signal{gen}_{obs}_{case}_smoothed_vs_propagated.pdf"
            canvas.Print(outName + '[')
            # Set log scales (if requested)
            if obs in Logx:
                canvas.SetLogx()
            # Legend
            legends = ROOT.TLegend(0.76, 0.77, 0.92, 0.94)
            legends.SetTextFont(42)
            legends.AddEntry(TotalHistUP, 'propagated', 'p')
            legends.AddEntry(smoothed_ratio, 'extrapolation', 'p')
            TotalHistUP.Draw("HIST][")
            TotalHistDOWN.Draw("HIST][ same")
            smoothed_ratio.Draw("HIST][ same")
            legends.Draw('same')
            TotalHistUP.GetYaxis().SetTitle('uncertainty')
            TotalHistUP.GetXaxis().SetRangeUser(minX, maxX)
            TotalHistUP.GetXaxis().SetTitleSize(20)
            TotalHistUP.GetXaxis().SetTitleFont(43)
            TotalHistUP.GetXaxis().SetLabelFont(43)
            TotalHistUP.GetXaxis().SetLabelSize(19)
            if obs in XaxisTitles:
                if obs == 'Zpt':
                    if target_channel == 'EL':
                        TotalHistUP.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
                    elif target_channel == 'MU':
                        TotalHistUP.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]")
                    else:
                        TotalHistUP.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}e^{-}/#mu^{-}e^{+}) [GeV]")
                else:
                    TotalHistUP.GetXaxis().SetTitle(XaxisTitles[obs])
            TotalHistUP.GetYaxis().SetTitleSize(20)
            TotalHistUP.GetYaxis().SetTitleFont(43)
            TotalHistUP.GetYaxis().SetLabelFont(43)
            TotalHistUP.GetYaxis().SetLabelSize(19)
            TotalHistUP.GetYaxis().SetRangeUser(-0.3, 0.3)
            TotalHistDOWN.GetYaxis().SetRangeUser(-0.3, 0.3)
            smoothed_ratio.GetYaxis().SetRangeUser(-0.3, 0.3)

            # Show ATLAS legend
            atlas = "#font[72]{ATLAS} #font[42]{Internal}";
            ATLASBlock = ROOT.TLatex(0.2, 0.87, atlas)
            ATLASBlock.SetNDC()
            ATLASBlock.Draw("same")

            # Show event type
            ll = '#rightarrow#mu#mu' if target_channel == 'MU' else '#rightarrow ee'
            EvtType = 'Z('+ll+') + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z('+ll+') + #geq 2 b-tagged jets'
            TextBlock = ROOT.TLatex(0.2, 0.82, EvtType)
            TextBlock.SetNDC()
            TextBlock.Draw('same')

            canvas.Print(outName)
            canvas.Print(outName+']')


            # Compare total (propagated + extrapolation) with theory uncertainties
            # Prepare final data-driven uncertainties
            nbins = TotalHistUP.GetNbinsX()
            htotal_up = TotalHistUP.Clone('FinalDataDrivenUncertainty_UP_{}_{}'.format(obs, case))
            htotal_down = TotalHistUP.Clone('FinalDataDrivenUncertainty_DOWN_{}_{}'.format(obs, case))
            for ibin in range(1, nbins + 1):
                htotal_up.SetBinContent(ibin, TotalHistUP.GetBinContent(ibin) + smoothed_ratio.GetBinContent(ibin))
                htotal_down.SetBinContent(ibin, TotalHistDOWN.GetBinContent(ibin) - smoothed_ratio.GetBinContent(ibin))
            # Prepare theory uncertainty
            htheory_up = TotalHistUP.Clone('TheoryUncertainty_UP_{}_{}'.format(obs, case))
            htheory_up.SetLineColor(ROOT.kRed)
            htheory_down = TotalHistUP.Clone('TheoryUncertainty_DOWN_{}_{}'.format(obs, case))
            htheory_down.SetLineColor(ROOT.kRed)
            theory_file_name = '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_reader/readttrees_zhf/DeriveTTbarTheoryUncertainties/output_dir/SR_{}_{}_{}/all.root'.format(
                target_channel,
                obs,
                case,
            )
            theory_file = ROOT.TFile.Open(theory_file_name)
            graph = theory_file.Get("{}_{}_EvtWgtVar".format(obs, case))
            for ibin in range(0, nbins):
                nominal = graph.GetPointY(ibin)
                #if obs == 'mjjHF': print(f'nominal({ibin}) = {nominal}')
                if graph.GetPointX(ibin) != htotal_up.GetBinCenter(ibin + 1):
                    print('ERROR: graph center != hist center, exiting')
                    sys.exit(1)
                if graph.GetN() != nbins:
                    print('ERROR: graph.GetN() != hist.GetNbinsX(), exiting')
                    sys.exit(1)
                if nominal != 0:
                    absolute_error_up = graph.GetErrorYhigh(ibin)
                    relative_error_up = absolute_error_up / nominal
                    htheory_up.SetBinContent(ibin + 1, relative_error_up)
                    absolute_error_down = graph.GetErrorYlow(ibin)
                    relative_error_down = absolute_error_down / nominal
                    relative_error_down *= -1
                    htheory_down.SetBinContent(ibin + 1, relative_error_down)
                    #if obs == 'mjjHF': print(f'rel_error_up({ibin}) = {relative_error_up}')
                    #if obs == 'mjjHF': print(f'rel_error_down({ibin}) = {relative_error_down}')
                else:
                    htheory_up.SetBinContent(ibin + 1, 0.0)
                    htheory_down.SetBinContent(ibin + 1, 0.0)
                    #if obs == 'mjjHF': print(f'rel_error_up({ibin}) = 0')
                    #if obs == 'mjjHF': print(f'rel_error_down({ibin}) = 0')
            # TCanvas
            canvas = ROOT.TCanvas("final_comparison")
            outName = f"{output_folder}/{DataPeriod}_{cr}_{channel}_vs_extrapolatedData_{target_region}_{target_channel}_Signal{gen}_{obs}_{case}_smoothed__final_comparison.pdf"
            canvas.Print(outName + '[')
            # Set log scales (if requested)
            if obs in Logx:
                canvas.SetLogx()
            # Legend
            legends = ROOT.TLegend(0.8, 0.73, 0.92, 0.9)
            legends.SetTextFont(42)
            legends.AddEntry(htotal_up, 'data-driven', 'l')
            legends.AddEntry(htheory_up, 'theory', 'l')
            htotal_up.Draw("HIST][")
            htotal_down.Draw("HIST][ same")
            htheory_up.Draw("HIST][ same")
            htheory_down.Draw("HIST][ same")
            legends.Draw('same')
            htotal_up.GetYaxis().SetTitle('uncertainty')
            htotal_up.GetXaxis().SetRangeUser(minX, maxX)
            htotal_up.GetXaxis().SetTitleSize(20)
            htotal_up.GetXaxis().SetTitleFont(43)
            htotal_up.GetXaxis().SetLabelFont(43)
            htotal_up.GetXaxis().SetLabelSize(19)
            if obs in XaxisTitles:
                if obs == 'Zpt':
                    if target_channel == 'EL':
                        htotal_up.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
                    elif target_channel == 'MU':
                        htotal_up.GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]")
                    else:
                       htotal_up .GetXaxis().SetTitle("#it{p}_{T} (#mu^{+}e^{-}/#mu^{-}e^{+}) [GeV]")
                else:
                    htotal_up.GetXaxis().SetTitle(XaxisTitles[obs])
            htotal_up.GetYaxis().SetTitleSize(20)
            htotal_up.GetYaxis().SetTitleFont(43)
            htotal_up.GetYaxis().SetLabelFont(43)
            htotal_up.GetYaxis().SetLabelSize(19)
            if obs != 'Zpt' and obs != 'HFjet0_XF':
                htotal_up.GetYaxis().SetRangeUser(-0.7, 0.7)
                htotal_down.GetYaxis().SetRangeUser(-0.7, 0.7)
                htheory_up.GetYaxis().SetRangeUser(-0.7, 0.7)
                htheory_down.GetYaxis().SetRangeUser(-0.7, 0.7)
            elif obs == 'Zpt':
                htotal_up.GetYaxis().SetRangeUser(-0.9, 0.9)
                htotal_down.GetYaxis().SetRangeUser(-0.9, 0.9)
                htheory_up.GetYaxis().SetRangeUser(-0.9, 0.9)
                htheory_down.GetYaxis().SetRangeUser(-0.9, 0.9)
            else:  # XF
                htotal_up.GetYaxis().SetRangeUser(-1.2, 1.2)
                htotal_down.GetYaxis().SetRangeUser(-1.2, 1.2)
                htheory_up.GetYaxis().SetRangeUser(-1.2, 1.2)
                htheory_down.GetYaxis().SetRangeUser(-1.2, 1.2)
            if obs == 'mjjHF':
                htotal_up.GetXaxis().SetRangeUser(0, 1200)
                htotal_down.GetXaxis().SetRangeUser(0, 1200)
                htheory_up.GetXaxis().SetRangeUser(0, 1200)
                htheory_down.GetXaxis().SetRangeUser(0, 1200)
            # Show ATLAS legend
            atlas = "#font[72]{ATLAS} #font[42]{Internal}";
            ATLASBlock = ROOT.TLatex(0.18, 0.87, atlas)
            ATLASBlock.SetNDC()
            ATLASBlock.Draw("same")

            # Show event type
            ll = '#rightarrow#mu#mu' if target_channel == 'MU' else '#rightarrow ee'
            EvtType = 'Z('+ll+') + #geq 1 b-tagged jet' if case == 'Ti1' else 'Z('+ll+') + #geq 2 b-tagged jets'
            TextBlock = ROOT.TLatex(0.18, 0.82, EvtType)
            TextBlock.SetNDC()
            TextBlock.Draw('same')

            canvas.Print(outName)
            canvas.Print(outName+']')


    # Close input files
    for _, input_file in input_files.items():
        input_file.Close()

    # Write uncertainty histograms
    output_file_name = f'Outputs/ttbarExtrapolationUncertainty_Signal{gen}_{Version}_{DataPeriod}_SumVRs_{target_channel}.root'
    output_file = ROOT.TFile(output_file_name, 'RECREATE')
    for hist in out_hists:
        hist.Write()
    output_file.Close()
