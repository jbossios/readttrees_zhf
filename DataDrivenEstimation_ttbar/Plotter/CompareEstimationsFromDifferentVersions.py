#############################################################################
#                                                                           #
# Purpose: Compare data-driven ttbar predictions from different versions    #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

Versions      = ['25102021','27102021']
ControlRegion = ['VR']

# Generator to be used for signal
SignalGenerator = 'SH2211' # options: MG and SH2211

Debug = False

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

import ROOT,os,sys

# Protections

if len(Versions) != 2:
  print('ERROR: It is only possible to compare two versions, exiting')
  sys.exit(1)

if SignalGenerator not in ['SH2211','MG']:
  print('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))
  sys.exit(1)

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
#Version  = opt['Version']
Campaign = opt['Campaign']
Tagger   = opt['Tagger']
Channels = opt['Channels']

# Create output folder for plots
VS = '_vs_'.join(Versions)
os.system('mkdir -p Plots/{}/'.format(VS))

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts import *

# Import things from Plotter
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style                  import *
from InputFiles             import *
from Luminosities           import *
from PlotterHelperFunctions import *

Luminosity          = dict()
Luminosity['MC16a'] = Luminosity_2015 + Luminosity_2016
Luminosity['MC16d'] = Luminosity_2017
Luminosity['MC16e'] = Luminosity_2018

DataPeriod = 'data'
if Campaign == 'MC16all':
  DataPeriod += '15161718'
elif Campaign == 'MC16a':
  DataPeriod += '1516'
elif Campaign == 'MC16d':
  DataPeriod += '17'
elif Campaign == 'MC16e':
  DataPeriod += '18'
else:
  print('ERROR: {} campaign not recognized, exiting')
  sys.exit(1)

# Add b-tagging quantile vs obs
FinalObservables = copy.deepcopy(Observables)
for obs,cases in Observables.items():
  FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = cases
  if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

# Loop over channels
for channel in Channels:
  # Loop over observables
  for obs,cases in FinalObservables.items():
    # Loop over event types
    for case in ['Ti1','Ti2']:
      if case == 'Ti1' and 'Te1' not in cases: continue # skip nonsense obs+case combination
      # Observables
      if 'Quantile' not in obs:
        # TCanvas
        Canvas  = ROOT.TCanvas()
        outName = "Plots/{}/ttbar_DataDriven_Signal{}_vs_{}_{}_{}_{}.pdf".format(VS,SignalGenerator,Campaign,obs,case,channel)
        Canvas.Print(outName+']')

        # TPad for upper panel
        pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
        pad1.SetTopMargin(0.08)
        pad1.SetBottomMargin(0.03)
        pad1.Draw()
        pad1.cd()

        # Set log scales (if requested)
        if obs in Logx:
          pad1.SetLogx()
        if obs in Logy:
          pad1.SetLogy()

        # Legend
        Legends = ROOT.TLegend(0.8,0.73,0.92,0.9)
        Legends.SetTextFont(42)

        # THStack
        Stack = ROOT.THStack()

        # Loop over versions
        counter = -1
	Hists   = dict()
        for Version in Versions:
          counter += 1

          # Open input file with data-driven ttbar estimations
          extra         = '_AltVR' if ControlRegion == 'AltVR' else ''
          InputFileName = '../Outputs/ttbarContribution_Signal{}_{}_{}{}.root'.format(SignalGenerator,Version,DataPeriod,extra)
          InFile = ROOT.TFile.Open(InputFileName)
          if not InFile:
            print('ERROR: {} not found, exiting'.format(InputFileName))
            sys.exit(1)

          # Get data-driven ttbar estimation based on ControlRegion
          Hist = InFile.Get('ttbar_SR_{}_{}_{}'.format(channel,obs,case))
          if not Hist:
            print('ERROR: {} not found, exiting'.format('ttbar_SR_{}_{}_{}'.format(channel,obs,case)))
            sys.exit()
          Hist.SetDirectory(0)
          InFile.Close() # close file
          Hist.Scale(1,'width') # divide by bin width
          Hist.SetLineColor(Colors[counter])
          Hist.SetMarkerColor(Colors[counter])
	  Hists[Version] = Hist
          #Hist.Draw('E P')
          Stack.Add(Hist,"p")
          Legends.AddEntry(Hist,Version,'p')

          # X-axis range
          if obs in XaxisRange:
            minX = XaxisRange[obs][0]
            maxX = XaxisRange[obs][1]
          else:
            minX  = Hist.GetXaxis().GetBinLowEdge(1)
            nbins = Hist.GetNbinsX()
            maxX  = Hist.GetXaxis().GetBinUpEdge(nbins)

        # Draw histograms and legends
	Stack.Draw('nostack')
        Legends.Draw('same')

        # Style
        Stack.GetXaxis().SetRangeUser(minX,maxX)
        Stack.GetXaxis().SetLabelSize(0.)
        Stack.GetXaxis().SetTitleSize(0.)
        Stack.GetYaxis().SetTitleSize(20)
        Stack.GetYaxis().SetTitleFont(43)
        Stack.GetYaxis().SetLabelFont(43)
        Stack.GetYaxis().SetLabelSize(19)
        Stack.GetYaxis().SetTitleOffset(1.3)
        Stack.GetYaxis().SetTitle('Events / bin-width')
        if XaxisTitles.has_key(obs):
          Stack.GetXaxis().SetTitle(XaxisTitles[obs])

        # Show event type
        DecayType = '(ee)' if channel == 'EL' else '(#mu#mu)'
        EvtType   = 'Z{} + #geq 1 b-tagged jet'.format(DecayType) if case == 'Ti1' else 'Z{} + #geq 2 b-tagged jets'.format(DecayType)
        TextBlock = ROOT.TLatex(0.4,0.85,EvtType)
        TextBlock.SetNDC()
        TextBlock.Draw('same')

        ROOT.gPad.RedrawAxis()

        # TPad for bottom plot
        Canvas.cd()
        pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
        pad2.SetTopMargin(0.03)
        pad2.SetBottomMargin(0.32)
        pad2.Draw()
        pad2.cd()

        # Set log-x scale (if requested)
        if obs in Logx:
          pad2.SetLogx()

        # Create and draw ratio plot
        ratioHist = Hists[Versions[1]].Clone('ratioHist_{}_{}_{}'.format(channel,obs,case))
        ratioHist.Divide(Hists[Versions[0]])
        ratioHist.Draw('e0')
        ratioHist.GetYaxis().SetTitle('{} / {}'.format(Versions[1],Versions[0]))
        ratioHist.GetXaxis().SetRangeUser(minX,maxX)
        ratioHist.GetXaxis().SetTitleSize(20)
        ratioHist.GetXaxis().SetTitleFont(43)
        ratioHist.GetXaxis().SetLabelFont(43)
        ratioHist.GetXaxis().SetLabelSize(19)
        ratioHist.GetXaxis().SetTitleOffset(3)
        ratioHist.GetYaxis().SetTitleSize(20)
        ratioHist.GetYaxis().SetTitleFont(43)
        ratioHist.GetYaxis().SetLabelFont(43)
        ratioHist.GetYaxis().SetLabelSize(19)
        ratioHist.GetYaxis().SetTitleOffset(1.3)
        ratioHist.GetYaxis().SetRangeUser(0.79,1.21)

        # Draw line at data/MC==1
        Line = ROOT.TLine(minX,1,maxX,1)
        Line.SetLineStyle(7)
        Line.Draw("same")

        Canvas.Print(outName)
        Canvas.Print(outName+']')

      else: # quantile vs observable
        quantileType = 'LeadQuantile' if 'Lead' in obs else 'CombQuantile'
        Var          = obs.replace('{}{}_vs_'.format(Tagger,quantileType),'')
        nObsBins     = len(Binning[Var])-1 if len(Binning[Var]) > 3 else Binning[Var][0]
        for obsBin in range(0,nObsBins):
          HistBin = obsBin + 1

          # TCanvas
          Canvas  = ROOT.TCanvas()
          outName = "Plots/{}/ttbar_DataDriven_Signal{}_vs_{}_{}_{}_{}.pdf".format(VS,SignalGenerator,Campaign,obs+'_bin_{}'.format(HistBin),case,channel)
          Canvas.Print(outName+']')

          # TPad for upper panel
          pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
          pad1.SetTopMargin(0.08)
          pad1.SetBottomMargin(0.03)
          pad1.Draw()
          pad1.cd()

          # Set log scales (if requested)
          if obs in Logx:
            pad1.SetLogx()
          if obs in Logy:
            pad1.SetLogy()

          # Legend
          Legends = ROOT.TLegend(0.2,0.73,0.32,0.9)
          Legends.SetTextFont(42)

          # THStack
          Stack = ROOT.THStack()

          # Loop over versions
          counter = -1
	  Hists   = dict()
          for Version in Versions:
            counter += 1

            # Open input file with data-driven ttbar estimations
            extra         = '_AltVR' if ControlRegion == 'AltVR' else ''
            InputFileName = '../Outputs/ttbarContribution_Signal{}_{}_{}{}.root'.format(SignalGenerator,Version,DataPeriod,extra)
            InFile        = ROOT.TFile.Open(InputFileName)
            if not InFile:
              print('ERROR: {} not found, exiting'.format(InputFileName))
              sys.exit(1)

            # Get data-driven ttbar estimation
            Hist = InFile.Get('ttbar_SR_{}_{}_{}'.format(channel,obs,case))
            if not Hist:
              print('ERROR: {} not found, exiting'.format('ttbar_SR_{}_{}_{}'.format(channel,obs,case)))
              sys.exit()
            Hist.SetDirectory(0)
            InFile.Close() # close file
            Hist = Hist.ProjectionY(Hist.GetName()+'{}_proj{}'.format(Version,obsBin), HistBin, HistBin)
            Hist.SetLineColor(Colors[counter])
            Hist.SetMarkerColor(Colors[counter])
	    Hists[Version] = Hist
            Stack.Add(Hist,"p")
            Legends.AddEntry(Hist,Version,'p')

            # X-axis range
            minX  = Hist.GetXaxis().GetBinLowEdge(1)
            nbins = Hist.GetNbinsX()
            maxX  = Hist.GetXaxis().GetBinUpEdge(nbins)

          # Draw histograms and legends
          Stack.Draw('nostack')
          Legends.Draw('same')

          # Style
          Stack.Draw('nostack')
          Stack.GetXaxis().SetLabelSize(0.)
          Stack.GetXaxis().SetTitleSize(0.)
          Stack.GetYaxis().SetTitleSize(20)
          Stack.GetYaxis().SetTitleFont(43)
          Stack.GetYaxis().SetLabelFont(43)
          Stack.GetYaxis().SetLabelSize(19)
          Stack.GetYaxis().SetTitleOffset(1.3)
          Stack.GetYaxis().SetTitle('Events')

          # Show event type
          DecayType = '(ee)' if channel == 'EL' else '(#mu#mu)'
          EvtType   = 'Z{} + #geq 1 b-tagged jet'.format(DecayType) if case == 'Ti1' else 'Z{} + #geq 2 b-tagged jets'.format(DecayType)
          TextBlock = ROOT.TLatex(0.4,0.85,EvtType)
          TextBlock.SetNDC()
          TextBlock.Draw('same')

          # Show observable's bin
          ObsBin = '{} bin {}'.format(Var,HistBin)
          TextBlock2 = ROOT.TLatex(0.4,0.8,ObsBin)
          TextBlock2.SetNDC()
          TextBlock2.Draw('same')

          ROOT.gPad.RedrawAxis()

          # TPad for bottom plot
          Canvas.cd()
          pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
          pad2.SetTopMargin(0.03)
          pad2.SetBottomMargin(0.32)
          pad2.Draw()
          pad2.cd()

          # Set log-x scale (if requested)
          if obs in Logx:
            pad2.SetLogx()

          # Create and draw ratio plot
          ratioHist = Hists[Versions[1]].Clone('ratioHist_{}_{}_{}'.format(channel,obs,case))
          ratioHist.Divide(Hists[Versions[0]])
          ratioHist.Draw('e0')
          ratioHist.GetYaxis().SetTitle('{} / {}'.format(Versions[1],Versions[0]))
          ratioHist.GetXaxis().SetTitleSize(20)
          ratioHist.GetXaxis().SetTitleFont(43)
          ratioHist.GetXaxis().SetLabelFont(43)
          ratioHist.GetXaxis().SetLabelSize(19)
          ratioHist.GetXaxis().SetTitleOffset(3)
          ratioHist.GetXaxis().SetTitle('b-tagging quantile')
          ratioHist.GetYaxis().SetTitleSize(20)
          ratioHist.GetYaxis().SetTitleFont(43)
          ratioHist.GetYaxis().SetLabelFont(43)
          ratioHist.GetYaxis().SetLabelSize(19)
          ratioHist.GetYaxis().SetTitleOffset(1.3)
          ratioHist.GetYaxis().SetRangeUser(0.71,1.29)

          # Draw line at data/MC==1
          Line = ROOT.TLine(minX,1,maxX,1)
          Line.SetLineStyle(7)
          Line.Draw("same")

          Canvas.Print(outName)
          Canvas.Print(outName+']')

print('>>> ALL DONE <<<')
