#############################################################################
#                                                                           #
# Purpose: Compare SFs between EL and MU channels for all observables       #
# Author:  Jona Bossio (jbossios@cern.ch)                                   #
#                                                                           #
#############################################################################

import ROOT,os,sys

#############################################################################
# DO NOT MODIFY (below this line)
#############################################################################

Sources2Compare = [
  'Reco',
  'PID',
  'Iso',
  'Trig',
  'BTag_B',
  'BTag_C',
  'BTag_Light',
  'JET',
  'MET',
  'MUON',
  'EG',
]

# Read common options
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from CommonDefs import Options as opt
Version  = opt['Version']
Channels = opt['Channels']

# Create output folder for plots
os.system('mkdir Plots')

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../../') # insert at 1, 0 is the script path
from Dicts       import *
from Systematics import *

# Import things from Reader
sys.path.insert(1, '../../Plotter') # insert at 1, 0 is the script path
from Style import *

# Construct command
command = ''

Observables = opt['Observables']
# Loop over channels
for channel in Channels:
  # Loop over observables
  for obs, cases in Observables.items():
    # Loop over event types
    for case in cases:
      # Loop over type of uncertainties to compare
      for Type in Sources2Compare:
        command += 'python CompareSFuncertainties.py --channel {} --obs {} --case {} --type {} --systs AllSysts && '.format(channel,obs,case,Type)

command = command[:-2]
print(command)
os.system(command)
