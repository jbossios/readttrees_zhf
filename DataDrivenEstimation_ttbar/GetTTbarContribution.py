##########################################################################
#                                                                         #
# Purpose: Data-driven method to estimate ttbar contribution              #
# Author: Jona Bossio (jbossios@cern.ch)                                  #
#                                                                         #
# Procedure: subtract non-ttbar backgrounds from data in CR and apply SF  #
#                                                                         #
##########################################################################

# Imports
import ROOT
import os
import sys
import argparse
import copy
from CommonDefs import Options as opt

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Systematics import *
from SystematicTTrees import *
from HelperFunctions import get_xrootd_prefix

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles import *
from PlotterHelperFunctions import *
from Luminosities import *


def main(
    Campaign,
    ControlRegion,
    channel,
    TTrees,
    PropagateSFuncertainties,
    UseAlwaysMC16all,
    SignalGenerator,
    tf_vs_pcbt,
    Debug,
    custom_options = None,
    output_folder = 'Outputs',
  ):

  DataYearsOptions = {
    'a': [['15', '16']],
    'd': [['17']],
    'e': [['18']],
    'all': [['15', '16', '17', '18']],
  }[Campaign]

  # Protections
  if SignalGenerator not in ['SH2211', 'MG', 'FxFx']:
    FATAL('SignalGenerator ({}) not recognized, exiting'.format(SignalGenerator))
  if ControlRegion not in ['CR', 'AltCR', 'SumCRs']:
    FATAL('ControlRegion ({}) not recognized, exiting'.format(ControlRegion))

  # Prepare list of CRs
  CRs = [ControlRegion] if ControlRegion != 'SumCRs' else ['CR', 'AltCR']

  # Read common options
  Tagger = opt['Tagger'] if not custom_options else custom_options['Tagger']
  Version = opt['Version'] if not custom_options else custom_options['Version']
  FinalStates = opt['FinalStates'] if not custom_options else custom_options['FinalStates']
  Backgrounds = opt['Backgrounds'] if not custom_options else custom_options['Backgrounds']
  Observables = opt['Observables'] if not custom_options else custom_options['Observables']

  # Replace Zjets from Backgrounds list by Zee/Zmumu/Ztautau
  if 'Zjets' in Backgrounds:
    Backgrounds.remove('Zjets')
    Backgrounds.append('Zee{}'.format(SignalGenerator))
    Backgrounds.append('Zmumu{}'.format(SignalGenerator))
    Backgrounds.append('Ztautau')

  def get_input_file(input_files_dict, key):
    """ Get already opened input file or open it if not opened already """
    # protection
    if key not in InputFiles:
      msg = sample + " not found in InputFiles dictionary, exiting"
      FATAL(msg)
    if key not in input_files_dict:  # open file
      input_file_name = InputFiles[key]
      input_file_name = get_xrootd_prefix(input_file_name) + input_file_name
      input_file = ROOT.TFile.Open(input_file_name)
      CHECKObj(input_file, f'{input_file_name} not found, exiting')
      input_files_dict[key] = input_file
    return input_files_dict, input_files_dict[key]

  if TTrees == 'nominal':

    # Loop over data year sets
    for DataYears in DataYearsOptions:

      print('Running for {} data year set... this might take long'.format(DataYears))

      ######################################################
      # Find out needed campaigns
      ######################################################
      Campaigns = []
      if not UseAlwaysMC16all:
        for year in DataYears:
          if year == '15':
            Campaigns.append('a')
          if year == '16':
            if 'a' not in Campaigns: Campaigns.append('a')
          if year == '17':
            Campaigns.append('d')
          if year == '18':
            Campaigns.append('e')
      else:
        Campaigns = ['a', 'd', 'e']

      ######################################################
      # Retrieve SFs
      ######################################################
      SFHists = dict()
      # Open input file
      if 'a' in Campaigns and 'd' in Campaigns and 'e' in Campaigns:
        cpg = ['all']
      elif len(Campaigns) == 1:
        cpg = Campaigns
      else:
        FATAL('ERROR: The provided combination of campaigns is not supported, exiting')
      extraOutname = '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
      extraGen = '' if SignalGenerator == 'SH2211' else '_{}'.format(SignalGenerator)
      inputFileName = '{}/ttbarSFs_{}_{}_MC16{}{}{}.root'.format(output_folder, Version, channel, cpg[0], extraOutname, extraGen if len(CRs) > 1 else '')
      sfFile = ROOT.TFile.Open(inputFileName)
      if not sfFile:
        FATAL('ERROR: {} not found, exiting'.format(inputFileName,cpg[0]))
      # Loop over campaigns
      for campaign in cpg:
        SFHists[campaign] = dict()
        SFHists[campaign][channel] = dict()
        # Loop over observables
        for obs, cases in Observables.items():
          SFHists[campaign][channel][obs] = dict()
          SFHists[campaign][channel]['{}LeadQuantile_vs_{}'.format(Tagger, obs)] = dict()
          SFHists[campaign][channel]['{}CombQuantile_vs_{}'.format(Tagger, obs)] = dict()
          # Loop over event types
          for case in cases:
            SFHists[campaign][channel][obs][case] = dict()
            SFHists[campaign][channel]['{}LeadQuantile_vs_{}'.format(Tagger, obs)][case] = dict()
            if case == 'Ti2': SFHists[campaign][channel]['{}CombQuantile_vs_{}'.format(Tagger, obs)][case] = dict()
            # Loop over nominal+NPs
            for source, nNPs in SFsysts.items():
              if source != 'nominal' and not PropagateSFuncertainties: continue
              for iNP in range(1, nNPs+1):
                # Observable distribution
                extra = '_{}_{}'.format(source, iNP) if source != 'nominal' else ''
                histname = obs + '_' + case + extra + '_MC16{}_{}SF'.format(campaign, channel)
                hist = sfFile.Get(histname)
                CHECKObj(hist, '{} not found in {}, exiting'.format(histname, inputFileName))
                hist.SetDirectory(0)
                SFHists[campaign][channel][obs][case][extra] = hist
                if tf_vs_pcbt:
                  # Get TF vs PCBT bins
                  # LeadQuantile case
                  histname = Tagger + 'LeadQuantile_vs_' + obs + '_' + case + extra + '_MC16{}_{}SF'.format(campaign, channel)
                  hist = sfFile.Get(histname)
                  CHECKObj(hist, '{} not found in {}, exiting'.format(histname, inputFileName))
                  hist.SetDirectory(0)
                  SFHists[campaign][channel]['{}LeadQuantile_vs_{}'.format(Tagger, obs)][case][extra] = hist
                  if case == 'Ti2':
                    # CombQuantile case
                    histname = Tagger + 'CombQuantile_vs_' + obs + '_' + case + extra + '_MC16{}_{}SF'.format(campaign, channel)
                    hist = sfFile.Get(histname)
                    CHECKObj(hist, '{} not found in {}, exiting'.format(histname, inputFileName))
                    hist.SetDirectory(0)
                    SFHists[campaign][channel]['{}CombQuantile_vs_{}'.format(Tagger, obs)][case][extra] = hist
      sfFile.Close()

      # Add b-tagging quantile vs obs
      FinalObservables = copy.deepcopy(Observables)
      for obs, cases in Observables.items():
        if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = cases
        if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

      #############################################################################
      # Prepare ttbar SR contribution separately for data15+16, data17 and data18
      #############################################################################
      DataDict = dict()
      if len(Campaigns) == 1:
        if '15' in DataYears and '16' in DataYears: DataDict['1516'] = ['15','16']
        elif '15' in DataYears:                     DataDict['15']   = ['15']
        elif '16' in DataYears:                     DataDict['16']   = ['16']
        if '17' in DataYears:                       DataDict['17']   = ['17']
        if '18' in DataYears:                       DataDict['18']   = ['18']
      else:
        if   '15' in DataYears and '16' in DataYears and '17' in DataYears and '18' in DataYears: DataDict['15161718'] = ['15','16','17','18']
        elif '15' in DataYears and '16' in DataYears:                                             DataDict['1516']     = ['15','16']
        elif '17' in DataYears:                                                                   DataDict['17']       = ['17']
        elif '18' in DataYears:                                                                   DataDict['18']       = ['18']
      for key, DataPeriods in DataDict.items():

        if Debug: print('DEBUG: Prepare inputs for '+key+' period')

        ############################
        # Prepare data distribution
        ############################

        campaigns = []
        Luminosities = dict()
        Luminosities['a'] = 0
        Luminosity = 0
        for period in DataPeriods:
          if period == '15':
            campaigns.append('a')
            Luminosity += Luminosity_2015
            Luminosities['a'] += Luminosity_2015
          elif period == '16':
            Luminosity += Luminosity_2016
            if 'a' not in campaigns: campaigns.append('a')
            Luminosities['a'] += Luminosity_2016
          elif period == '17':
            Luminosity += Luminosity_2017
            campaigns.append('d')
            Luminosities['d'] = Luminosity_2017
          elif period == '18':
            Luminosity += Luminosity_2018
            campaigns.append('e')
            Luminosities['e'] = Luminosity_2018

        # Protection
        if 'a' in campaigns and 'd' in campaigns and 'e' in campaigns:
          cpg = 'all'
        elif len(campaigns) == 1:
          cpg = campaigns[0]
        else:
          FATAL('ERROR: The provided combination of campaigns is not supported, exiting')

        if Debug: print('DEBUG: Total luminosity: '+str(Luminosity))
        if Debug: print('DEBUG: Luminosities dict: ')
        if Debug: print(Luminosities)

        # Create output TFile
        OutFileName = output_folder + "/ttbarContribution_Signal"+SignalGenerator+"_"+Version+"_"+channel+"_data"+key+extraOutname+'.root'
        OutFile = ROOT.TFile(OutFileName, "RECREATE")
        print('Writing outputs to {}'.format(OutFileName))

        # Loop over distributions
        if Debug: print("DEBUG: Loop over distributions")

        # Collect opened files
        input_files = {}

        # Loop over observables
        for obs, cases in FinalObservables.items():
          # Loop over event types
          for case in cases:

            histname = obs + '_' + case

            # Loop over SF-based uncertainty sources
            for source, nNPs in SFsysts.items():
              if source != 'nominal' and not PropagateSFuncertainties: continue
              for iNP in range(1, nNPs+1):
                extra = '_{}_{}'.format(source, iNP) if source != 'nominal' else ''

                #################################
                # Get subtracted data for each CR
                hDataDict = dict()

                # Loop over control regions
                for CR in CRs:
                  # Loop over data-taking periods
                  if Debug: print("DEBUG: Loop over data-taking periods")
                  DataHists = []
                  for period in DataPeriods:
                    if Debug: 'ERROR: Get histogram('+histname+') for data'+period
                    # Open input file
                    Key = "Signal_data" + period + "_ELMU_" + CR + "_" + Tagger
                    input_files, File = get_input_file(input_files, Key)
                    # Get histogram
                    Hist = File.Get(histname)
                    CHECKObj(Hist, histname+" not found on {}, exiting".format(InputFiles[Key]))
                    Hist.SetDirectory(0)
                    DataHists.append(Hist)

                  if Debug: 'DEBUG: DatHists dict:'
                  if Debug: print(DataHists)

                  # Get total data histogram for this CR
                  if Debug: print("DEBUG: Get total data histogram")
                  hData = DataHists[0].Clone("Data_" + histname + '_' + CR + extra)
                  hData.SetDirectory(0)
                  for ihist in range(1,len(DataHists)):
                    hData.Add(DataHists[ihist])

                  # Get background estimations
                  if Debug: print("DEBUG: Get background estimations")
                  hBkgs = []
                  for campaign in campaigns:
                    for bkg in Backgrounds:
                      # Open File
                      Key = bkg + "_MC16" + campaign + "_ELMU_" + CR + "_" + Tagger
                      input_files, File = get_input_file(input_files, Key)
                      # Get histogram
                      Hist = File.Get(histname+extra)
                      CHECKObj(Hist,histname+extra+" not found on {}, exiting".format(InputFiles[Key]))
                      Hist.SetDirectory(0)
                      Hist.Scale(Luminosities[campaign])
                      hBkgs.append(Hist)

                  # Subtract non-ttbar samples to the total data
                  if Debug: print("DEBUG: Subtract non-Z backgrounds to the total data")
                  for hist in hBkgs: hData.Add(hist, -1)
                  hDataDict[CR] = hData

                # Sum CRs (only if content is non-negative)
                hData = hDataDict[CRs[0]].Clone(hDataDict[CRs[0]].GetName()+'_final')
                if len(CRs) > 1:
                  if 'TH1' in str(type(hData)):
                    for xbin in range(1,hData.GetNbinsX()+1): # loop over bins
                      content = 0
                      for CR in CRs: # loop over CRs
                        if hDataDict[CR].GetBinContent(xbin) >= 0: # sum only if non-negative content
                          content += hDataDict[CR].GetBinContent(xbin)
                      hData.SetBinContent(xbin,content)
                  elif 'TH2' in str(type(hData)):
                    for xbin in range(1,hData.GetNbinsX()+1): # loop over x bins
                      for ybin in range(1,hData.GetNbinsY()+1): # loop over y bins
                        content = 0
                        for CR in CRs: # loop over CRs
                          if hDataDict[CR].GetBinContent(xbin,ybin) >= 0: # sum only if non-negative content
                            content += hDataDict[CR].GetBinContent(xbin,ybin)
                        hData.SetBinContent(xbin,ybin,content)
                  else: FATAL('Type of hData not recognized')

                # Apply SF to data
                if Debug: print("DEBUG: Apply SF to get prediction for SR {}".format(channel))
                hFinalData = hData.Clone('ttbar_SR_' + channel + '_' + histname + extra)
                if 'Quantile' in obs:
                  QuantileType = 'LeadQuantile' if 'Lead' in obs else 'CombQuantile'
                  nxbins = hFinalData.GetNbinsX()
                  nybins = hFinalData.GetNbinsY()
                  for xbin in range(1, nxbins + 1):  # loop over observable bins
                    for ybin in range(1, nybins + 1):  # loop over quantile bins
                      binvalue = hFinalData.GetBinContent(xbin, ybin)
                      var = obs.replace('{}{}_vs_'.format(Tagger, QuantileType), '')
                      if not tf_vs_pcbt:  # use constant TF vs PCBT bins
                        TF = SFHists[cpg if not UseAlwaysMC16all else 'all'][channel][var][case][extra].GetBinContent(xbin, ybin)
                      else:
                        TF = SFHists[cpg if not UseAlwaysMC16all else 'all'][channel][obs][case][extra].GetBinContent(xbin, ybin)
                      hFinalData.SetBinContent(xbin, ybin, binvalue * TF)
                else:
                  hFinalData.Multiply(SFHists[cpg if not UseAlwaysMC16all else 'all'][channel][obs][case][extra])

                # Write ttbar predictions to file
                OutFile.cd()
                hFinalData.Write()

        # Close output file
        OutFile.Close()

        # Close input files
        for _, input_file in input_files.items():
          input_file.Close()

  elif TTrees == 'FullSysts':

    out_dir_created = {}

    for counter, TTreeName in enumerate(SystTTrees):

      # Loop over data year sets
      for DataYears in DataYearsOptions:

        print('Running for {} data year set and {} syst... this might take long'.format(DataYears, TTreeName))

        ######################################################
        # Find out needed campaigns
        ######################################################
        Campaigns = []
        if not UseAlwaysMC16all:
          for year in DataYears:
            if year == '15':
              Campaigns.append('a')
            if year == '16':
              if 'a' not in Campaigns: Campaigns.append('a')
            if year == '17':
              Campaigns.append('d')
            if year == '18':
              Campaigns.append('e')
        else:
          Campaigns = ['a', 'd', 'e']

        ######################################################
        # Retrieve SFs
        ######################################################
        SFHists = dict()
        # Open input file
        if 'a' in Campaigns and 'd' in Campaigns and 'e' in Campaigns:
          cpg = ['all']
        elif len(Campaigns) == 1:
          cpg = Campaigns
        else:
          FATAL('ERROR: The provided combination of campaigns is not supported, exiting')
        extraOutname = '_{}'.format(ControlRegion) if ControlRegion != 'CR' else ''
        extraGen = '' if SignalGenerator == 'SH2211' else '_{}'.format(SignalGenerator)
        inputFileName = '{}/ttbarSFs_{}_{}_MC16{}{}{}_FullSysts.root'.format(output_folder, Version, channel, cpg[0], extraOutname, extraGen if len(CRs) > 1 else '')
        sfFile = ROOT.TFile.Open(inputFileName)
        if not sfFile:
          FATAL('ERROR: {} not found, exiting'.format(inputFileName,cpg[0]))
        # Loop over campaigns
        for campaign in cpg:
          SFHists[campaign] = dict()
          SFHists[campaign][channel] = dict()
          # Loop over observables
          for obs, cases in Observables.items():
            SFHists[campaign][channel][obs] = dict()
            SFHists[campaign][channel]['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = dict()
            SFHists[campaign][channel]['{}CombQuantile_vs_{}'.format(Tagger,obs)] = dict()
            # Loop over event types
            for case in cases:
              # Observable distribution
              histname = TTreeName+'/'+obs+'_'+case+'_MC16{}_{}SF'.format(campaign, channel)
              hist = sfFile.Get(histname)
              CHECKObj(hist,'{} not found in {}, exiting'.format(histname,inputFileName))
              hist.SetDirectory(0)
              SFHists[campaign][channel][obs][case] = hist
              if tf_vs_pcbt:
                # Get TF vs PCBT bins
                # LeadQuantile case
                histname = TTreeName + '/' + Tagger + 'LeadQuantile_vs_' + obs + '_' + case + '_MC16{}_{}SF'.format(campaign, channel)
                hist = sfFile.Get(histname)
                CHECKObj(hist, '{} not found in {}, exiting'.format(histname, inputFileName))
                hist.SetDirectory(0)
                SFHists[campaign][channel]['{}LeadQuantile_vs_{}'.format(Tagger, obs)][case] = hist
                if case == 'Ti2':
                  # CombQuantile case
                  histname = TTreeName + '/' + Tagger + 'CombQuantile_vs_' + obs + '_' + case + '_MC16{}_{}SF'.format(campaign, channel)
                  hist = sfFile.Get(histname)
                  CHECKObj(hist, '{} not found in {}, exiting'.format(histname, inputFileName))
                  hist.SetDirectory(0)
                  SFHists[campaign][channel]['{}CombQuantile_vs_{}'.format(Tagger, obs)][case] = hist
        sfFile.Close()

        # Add b-tagging quantile vs obs
        FinalObservables = copy.deepcopy(Observables)
        for obs, cases in Observables.items():
          if 'Ti1' in cases: FinalObservables['{}LeadQuantile_vs_{}'.format(Tagger,obs)] = cases
          if 'Ti2' in cases: FinalObservables['{}CombQuantile_vs_{}'.format(Tagger,obs)] = ['Ti2']

        #############################################################################
        # Prepare ttbar SR contribution separately for data15+16, data17 and data18
        #############################################################################
        DataDict = dict()
        if len(Campaigns) == 1:
          if '15' in DataYears and '16' in DataYears: DataDict['1516'] = ['15', '16']
          elif '15' in DataYears:                     DataDict['15']   = ['15']
          elif '16' in DataYears:                     DataDict['16']   = ['16']
          if '17' in DataYears:                       DataDict['17']   = ['17']
          if '18' in DataYears:                       DataDict['18']   = ['18']
        else:
          if   '15' in DataYears and '16' in DataYears and '17' in DataYears and '18' in DataYears: DataDict['15161718'] = ['15', '16', '17', '18']
          elif '15' in DataYears and '16' in DataYears:                                             DataDict['1516']     = ['15', '16']
          elif '17' in DataYears:                                                                   DataDict['17']       = ['17']
          elif '18' in DataYears:                                                                   DataDict['18']       = ['18']
        for key, DataPeriods in DataDict.items():

          if Debug: print('DEBUG: Prepare inputs for '+key+' period')

          ############################
          # Prepare data distribution
          ############################

          campaigns = []
          Luminosities = dict()
          Luminosities['a'] = 0
          Luminosity = 0
          for period in DataPeriods:
            if period == '15':
              campaigns.append('a')
              Luminosity += Luminosity_2015
              Luminosities['a'] += Luminosity_2015
            elif period == '16':
              Luminosity += Luminosity_2016
              if 'a' not in campaigns: campaigns.append('a')
              Luminosities['a'] += Luminosity_2016
            elif period == '17':
              Luminosity += Luminosity_2017
              campaigns.append('d')
              Luminosities['d'] = Luminosity_2017
            elif period == '18':
              Luminosity += Luminosity_2018
              campaigns.append('e')
              Luminosities['e'] = Luminosity_2018

          # Protection
          if 'a' in campaigns and 'd' in campaigns and 'e' in campaigns:
            cpg = 'all'
          elif len(campaigns) == 1:
            cpg = campaigns[0]
          else:
            FATAL('ERROR: The provided combination of campaigns is not supported, exiting')

          if Debug: print('DEBUG: Total luminosity: '+str(Luminosity))
          if Debug: print('DEBUG: Luminosities dict: ')
          if Debug: print(Luminosities)

          # Create output TFile
          OutFileName = output_folder + "/ttbarContribution_Signal" + SignalGenerator + "_" + Version + "_" + channel + "_data" + key + extraOutname + '_FullSysts.root'
          if not counter:
            OutFile = ROOT.TFile(OutFileName, "RECREATE")
          else:
            OutFile = ROOT.TFile(OutFileName, "UPDATE")
          if OutFileName not in out_dir_created:
            out_dir_created[OutFileName] = [TTreeName]
            OutFile.mkdir(TTreeName)
          elif TTreeName not in out_dir_created[OutFileName]:
            out_dir_created[OutFileName].append(TTreeName)
            OutFile.mkdir(TTreeName)
          print('Writing outputs to {}'.format(OutFileName))

          # Loop over distributions
          if Debug: print("DEBUG: Loop over distributions")

          # Collect opened files
          input_files = {}

          # Loop over observables
          for obs, cases in FinalObservables.items():
            # Loop over event types
            for case in FinalStates:
              #if case == 'Ti1' and 'Te1' not in cases: continue # skip nonsense obs+case combination
              if case not in cases: continue

              histname = obs + '_' + case

              #################################
              # Get subtracted data for each CR
              hDataDict = dict()

              # Loop over control regions
              for CR in CRs:
                # Loop over data-taking periods
                if Debug: print("DEBUG: Loop over data-taking periods")
                DataHists = []
                for period in DataPeriods:
                  if Debug: 'ERROR: Get histogram('+histname+') for data'+period
                  # Open input file
                  Key = "Signal_data" + period + "_ELMU_" + CR + "_" + Tagger
                  input_files, File = get_input_file(input_files, Key)
                  # Get histogram
                  Hist = File.Get(histname)
                  CHECKObj(Hist, histname+" not found on {}, exiting".format(InputFiles[Key]))
                  Hist.SetDirectory(0)
                  DataHists.append(Hist)

                if Debug: 'DEBUG: DatHists dict:'
                if Debug: print(DataHists)

                # Get total data histogram for this CR
                if Debug: print("DEBUG: Get total data histogram")
                hData = DataHists[0].Clone("Data_"+histname+'_'+CR)
                hData.SetDirectory(0)
                for ihist in range(1,len(DataHists)):
                  hData.Add(DataHists[ihist])

                # Get background estimations
                if Debug: print("DEBUG: Get background estimations")
                hBkgs = []
                for campaign in campaigns:
                  for bkg in Backgrounds:
                    # Open File
                    Key = bkg + "_MC16" + campaign + "_ELMU_" + CR + "_" + Tagger + '_FullSysts'
                    input_files, File = get_input_file(input_files, Key)
                    # Get histogram
                    Hist = File.Get(TTreeName+'/'+histname)
                    CHECKObj(Hist, TTreeName+'/'+histname+" not found, exiting")
                    Hist.SetDirectory(0)
                    #File.Close()
                    Hist.Scale(Luminosities[campaign])
                    hBkgs.append(Hist)

                # Subtract non-ttbar samples to the total data
                if Debug: print("DEBUG: Subtract non-Z backgrounds to the total data")
                for hist in hBkgs: hData.Add(hist, -1)
                hDataDict[CR] = hData

              # Sum CRs (only if content is non-negative)
              hData = hDataDict[CRs[0]].Clone(hDataDict[CRs[0]].GetName()+'_final')
              if len(CRs) > 1:
                if 'TH1' in str(type(hData)):
                  for xbin in range(1,hData.GetNbinsX()+1): # loop over bins
                    content = 0
                    for CR in CRs: # loop over CRs
                      if hDataDict[CR].GetBinContent(xbin) >= 0: # sum only if non-negative content
                        content += hDataDict[CR].GetBinContent(xbin)
                    hData.SetBinContent(xbin, content)
                elif 'TH2' in str(type(hData)):
                  for xbin in range(1,hData.GetNbinsX()+1): # loop over x bins
                    for ybin in range(1,hData.GetNbinsY()+1): # loop over y bins
                      content = 0
                      for CR in CRs: # loop over CRs
                        if hDataDict[CR].GetBinContent(xbin,ybin) >= 0: # sum only if non-negative content
                          content += hDataDict[CR].GetBinContent(xbin,ybin)
                      hData.SetBinContent(xbin, ybin, content)
                else: FATAL('Type of hData not recognized')

              # Apply SF to data
              if Debug: print("DEBUG: Apply SF to get prediction for SR {}".format(channel))
              hFinalData = hData.Clone('ttbar_SR_'+channel+'_'+histname)
              if 'Quantile' in obs:
                QuantileType = 'LeadQuantile' if 'Lead' in obs else 'CombQuantile'
                nxbins = hFinalData.GetNbinsX()
                nybins = hFinalData.GetNbinsY()
                for xbin in range(1,nxbins + 1):  # loop over observable bins
                  for ybin in range(1,nybins + 1):  # loop over quantile bins
                    binvalue = hFinalData.GetBinContent(xbin,ybin)
                    var = obs.replace('{}{}_vs_'.format(Tagger, QuantileType), '')
                    if not tf_vs_pcbt:  # use constant TF vs PCBT bins
                      TF = SFHists[cpg if not UseAlwaysMC16all else 'all'][channel][var][case].GetBinContent(xbin, ybin)
                    else:
                      TF = SFHists[cpg if not UseAlwaysMC16all else 'all'][channel][obs][case].GetBinContent(xbin, ybin)
                    hFinalData.SetBinContent(xbin,ybin,binvalue * TF)
              else:
                hFinalData.Multiply(SFHists[cpg if not UseAlwaysMC16all else 'all'][channel][obs][case])

              # Write ttbar predictions to file
              OutFile.cd(TTreeName)
              hFinalData.Write()

          # Close output file
          OutFile.Close()

          # Close input files
          for _, input_file in input_files.items():
            input_file.Close()

  print(">>> ALL DONE <<<")


if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--cr', action='store', default='', help='options: CR, AltCR, SumCRs')
  parser.add_argument('--ch', action='store', default='', help='options: EL, MU')
  parser.add_argument('--ttrees', action='store', default='', help='options: nominal, FullSysts')
  parser.add_argument('--campaign', action='store', default='', help='options: a, d, e, all')
  parser.add_argument('--vsPCBT', action='store_true', default=False, help='Use TF vs PCBT')
  parser.add_argument('--debug', action='store_true', default=False, help='Enable debugging')
  args = parser.parse_args()

  if not args.cr:
    print('ERROR: ControlRegion was not provided (--cr is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.ttrees:
    print('ERROR: TTrees was not provided (--ttrees is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.ch:
    print('ERROR: Channel was not provided (--ch is missing), exiting')
    parser.print_help()
    sys.exit(1)
  if not args.campaign:
    print('ERROR: campaign not provided, exiting')
    parser.print_help()
    sys.exit(1)

  Campaign = args.campaign
  Debug = args.debug
  ControlRegion = args.cr
  Channel = args.ch
  TTrees = args.ttrees
  PropagateSFuncertainties = True
  tf_vs_pcbt = args.vsPCBT
  if ControlRegion == 'AltCR':
    UseAlwaysMC16all = True  # used when not using full Run 2 data (Temporary?)
  else:  # Temporary
    #UseAlwaysMC16all = False  # used when not using full Run 2 data (Temporary?)
    UseAlwaysMC16all = True  # used when not using full Run 2 data (Temporary?)

  # Generator to be used for signal
  SignalGenerator = 'SH2211' # options: MG, SH2211 and FxFx

  print('INFO: Running GetTTbarContribution.py with cr={}, ch={}, ttrees={} and campaign={}'.format(args.cr, args.ch, args.ttrees, args.campaign))

  # NOTE: Other options are set in CommonDefs.py
  main(Campaign, ControlRegion, Channel, TTrees, PropagateSFuncertainties, UseAlwaysMC16all, SignalGenerator, tf_vs_pcbt, Debug)
