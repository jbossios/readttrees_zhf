#####################################################################
#                                                                    #
# Purpose: Run scripts used to obtain a data-driven ttbar estimation #
# Author:  Jona Bossio (jbossios@cern.ch)                            #
#                                                                    #
#####################################################################

# Import Python modules
import ROOT
import os
import sys
import copy
import argparse
import math
import logging
import time
from multiprocessing import Pool

# Import common options
from CommonDefs import Options as opt

# Import functions to get SF-based variation names
from getSFvarNames import getBTagSFvarNames, getLeptonSFvarNames, getJVTSFvarNames, getPRWvarNames

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Systematics import *
from SystematicTTrees import *
from Dicts import Binning
from InputLists import Inputs
from HelperFunctions import *

# Import things from Plotter
sys.path.insert(1, '../Plotter/') # insert at 1, 0 is the script path
from InputFiles             import *
from PlotterHelperFunctions import *
from Luminosities           import *


if __name__ == '__main__':

    ###########
    # Settings

    # Chose steps to run
    steps = [
        1,
        2,
        3,
        4,
    ]

    # Chose version when preparing CxAOD inputs ONLY! (None will use version from CommonDefs.py)
    #version = '21072022'  # used only for production of inputs to CxAODs (options: string or None)
    version = None

    # Overwrite uncertainties?
    overwrite = True

    campaigns = ['all', 'a', 'd', 'e']
    crs = [
        'CR',
        'AltCR',
        'SumCRs',
    ]
    channels = ['EL', 'MU']
    propagate_systematics = True
    ttrees_options = ['nominal', 'FullSysts']
    #ttrees_options = ['nominal']
    smooth_uncertainties = True
    tf_vs_pcbt = False  # Use TFs vs PCBT (instead of constant TFs vs PCBT when preparing data-driven ttbar predictions on PCBT distributions)
    
    generators = ('SH2211', 'FxFx')  # signal generators (first is considered nominal)
    use_mc16all_sfs = True
    nominal_cr = 'SumCRs'  # options CR, AltCR, SumCRs
    debug = False
    ttbar_samples = 'All'  # options: All (NonAllHad+Dilepton), NonAllHad, Dilepton

    merge_channels = True  # merge channels for CxAOD

    ###########

    # Start timer
    timer = {0: time.perf_counter()}

    # Protection
    if version and (1 in steps or 2 in steps or 3 in steps):
        print('ERROR: version should be set only when producing only CxAOD inputs, exiting')
        sys.exit(1)

    # 1. Run GetSFs.py
    if 1 in steps:
        from GetSFs import main as get_sfs
        for gen in generators:
            for campaign in campaigns:
                for cr in crs:
                    for ch in channels:
                        for ttrees in  ttrees_options:
                            if ttrees == 'FullSysts' and cr != nominal_cr:
                                continue
                            print(f'INFO: Running GetSFs.py w/ cr={cr}, ch={ch}, campaign={campaign} and ttrees={ttrees}...')
                            get_sfs(cr, ch, campaign, debug, propagate_systematics, gen, ttrees, ttbar_samples)
    timer[1] = time.perf_counter()

    # 2. Run GetTTbarContribution.py
    if 2 in steps:
        from GetTTbarContribution import main as get_ttbar_contribution
        for gen in generators:
            for campaign in campaigns:
                for cr in crs:
                    for ch in channels:
                        for ttrees in  ttrees_options:
                            if ttrees == 'FullSysts' and cr != nominal_cr:
                                continue
                            print(f'INFO: Running GetTTbarContribution.py w/ cr={cr}, ch={ch}, campaign={campaign} and ttrees={ttrees}...')
                            get_ttbar_contribution(campaign, cr, ch, ttrees, propagate_systematics, use_mc16all_sfs, gen, tf_vs_pcbt, debug)
    timer[2] = time.perf_counter()

    # 3. Run DeriveTTbarContributionSystematics.py
    if 3 in steps:
        from DeriveTTbarContributionSystematics import main as derive_systs
        from CommonDefs import Options
        gen = generators[0]  # uncertainties derived for nominal generator only
        output_path = 'Outputs/'
        output_files = list(os.listdir(output_path))
        version_to_use = opt['Version'] if not version else version
        for campaign in campaigns:
            for ch in channels:
                output_file_name = f'TTbarUncertainties_Signal{gen}_{version_to_use}_{ch}_MC16{campaign}_AllSysts{"_smoothed" if smooth_uncertainties else ""}.root'
                if not overwrite and output_file_name in output_files:
                    continue
                print(f'INFO: Running DeriveTTbarContributionSystematics w/ ch={ch} and campaign={campaign}')
                derive_systs(campaign, ch, gen, nominal_cr, smooth_uncertainties)
    timer[3] = time.perf_counter()
    
    # 4. Run PrepareInputs2CxAOD.py
    if 4 in steps:
        from PrepareInputs2CxAOD import main as prepare_cxaod_inputs
        gen = generators[0]  # inputs are prepraed using nominal generator only
        prepare_cxaod_inputs(gen, nominal_cr, merge_channels, debug, version)
        # copy files to EOS
        eos_path = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/DataDrivenTTbarEstimation/ForCxAOD/'
        version_to_use = opt['Version'] if not version else version
        for campaign in campaigns:
            cases = ['']
            if merge_channels:
                cases.append('_Lep')
            for case in cases:
                # create folder
                folder_case_name = 'mc16{}_wSys{}/'.format(campaign if campaign != 'all' else 'All', case)
                output_folder = eos_path + folder_case_name + version_to_use + '/'
                if not os.path.exists(output_folder):
                    os.makedirs(output_folder)
                # copy file
                os.system('cp Outputs/ForCxAOD/{}/{}/hist-data-driven-ttbar.root {}'.format(folder_case_name, version_to_use, output_folder))
    timer[4] = time.perf_counter()

    print('>>> RunAll.py: ALL DONE <<<')
    for step in steps:
        print('Time spent on step {}: {} seconds'.format(step, timer[step]-timer[step - 1]))
    print('Total time: {} seconds'.format(timer[4]-timer[0]))
