Options = {
  'Version'     : 'CItest',
  'Campaign'    : 'MC16a',
  'Tagger'      : 'DL1r',
  'Channels'    : ['EL'],
  'Campaigns'   : ['MC16a', 'MC16d', 'MC16e'],
  'FinalStates' : ['Ti1', 'Ti2'],
  'Backgrounds' : ['SingleTop'],
  'Observables': {
    'Zpt': ['Ti1'],
    'mjjHF': ['Ti2'],
  },
  'PathToRefs': '/eos/user/j/jbossios/SM/WZgroup/ZHF/DDTRefs',
}
