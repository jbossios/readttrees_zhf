##################################################################
#                                                                #
# Purpose: Derive CR->SR SFs (transfer factors) for ttbar        #
# Method:  SF = EL/ELMU (MU/ELMU) from ttbar MC                  #
# Author:  Jona Bossio (jbossios@cern.ch)                        #
#                                                                #
##################################################################

# Import modules
import ROOT
import os
import sys
import copy
import argparse
from multiprocessing import Pool
from CommonDefs import Options as opt

# Import things from Reader
sys.path.insert(1, '../')  # insert at 1, 0 is the script path
from HelperFunctions import get_xrootd_prefix

# Import things from Plotter
sys.path.insert(1, '../Plotter/')  # insert at 1, 0 is the script path
from InputFiles import *
from PlotterHelperFunctions import *
from Luminosities import *


def main(
    base_cr,
    cr_channel,
    target_region,
    target_channel,
    MC16Campaigns,
    Debug,
    SignalGenerator,
    TTree,
    ttbar_samples = 'All',
    custom_options = None,
    CustomInputFiles = None,
    output_folder = 'Outputs',
  ):

  # Protections
  if base_cr not in ['CR', 'AltCR']:
    print('Base region ({}) not recognized, exiting'.format(base_cr))
    sys.exit(1)
  if target_region not in ['CR', 'AltCR', 'SR', 'VR']:
    print('Target region ({}) not recognized, exiting'.format(target_region))
    sys.exit(1)
  if target_region == base_cr:
    print(f'Target region ({target_region}) should not match the base ({base_cr}) region, exiting')
    sys.exit(1)
  if TTree != 'nominal':
    print('{} not supported, exiting'.format(TTrees))
    sys.exit(1)

  # Get dict with input ROOT files
  InputFiles = InputFiles if not CustomInputFiles else CustomInputFiles

  # Read common options
  Tagger = opt['Tagger'] if not custom_options else custom_options['Tagger']
  Version = opt['Version'] if not custom_options else custom_options['Version']
  all_campaigns_extended = opt['Campaigns'] if not custom_options else custom_options['Campaigns']
  AllCampaigns = [cpg.replace('MC16', '') for cpg in all_campaigns_extended]
  Backgrounds = opt['Backgrounds'] if not custom_options else custom_options['Backgrounds']
  Observables = opt['Observables'] if not custom_options else custom_options ['Observables']

  # Replace Zjets from Backgrounds list by Zee/Zmumu/Ztautau
  if 'Zjets' in Backgrounds:
    Backgrounds.remove('Zjets')
    Backgrounds.append('Zee{}'.format(SignalGenerator))
    Backgrounds.append('Zmumu{}'.format(SignalGenerator))
    Backgrounds.append('Ztautau')

  # Total luminosity for needed campaigns
  Luminosity = dict() # collect luminosity for each MC campaign
  Luminosity["MC16a"]= Luminosity_2015 + Luminosity_2016
  Luminosity["MC16d"]= Luminosity_2017
  Luminosity["MC16e"]= Luminosity_2018

  # Open input ROOT files on demand
  def get_input_file(input_files_dict, key):
    """ Get already opened input file or open it if not opened already """
    # protection
    if key not in InputFiles:
      msg = f"{key} not found in InputFiles dictionary, exiting"
      FATAL(msg)
    if key not in input_files_dict:  # open file
      input_file_name = InputFiles[key]
      input_file_name = get_xrootd_prefix(input_file_name) + input_file_name
      input_file = ROOT.TFile.Open(input_file_name)
      CHECKObj(input_file, f'{input_file_name} not found, exiting')
      input_files_dict[key] = input_file
    return input_files_dict, input_files_dict[key]

  # Get histogram
  def get_hist(input_files_dict, sample, histname, debug = False):
    # Get File
    if debug: print("DEBUG: get_hist(): get file")
    input_files_dict, File = get_input_file(input_files_dict, sample)

    # Get Histogram
    if debug: print("DEBUG: get_hist(): get histogram")
    hist_orig = File.Get(histname)
    hist_orig.SetDirectory(0)
    if not hist_orig:
      msg = histname + " not found in " + InputFiles[sample] + ", exiting"
      return input_files_dict, None, msg
    hist = hist_orig.Clone(histname + "_cloned")
    hist.SetDirectory(0)  # detach from file

    return input_files_dict, hist, 'OK'


  def get_total_hist(input_files_dict, samples, histname, debug, luminosity = dict()):
    cpgs = ['MC16a', 'MC16d', 'MC16e']

    # Get first histogram
    if debug: print("DEBUG: get_total_hist(): get first histogram")
    input_files_dict, total_hist, msg = get_hist(input_files_dict, samples[0], histname, debug)
    if msg != 'OK':
      return input_files_dict, None, msg
    for campaign in cpgs:
      if campaign in samples[0] and 'cutflow' not in histname and 'mcEvtWeight' not in histname and 'sampleWeight' not in histname and 'SF' not in histname:
        total_hist.Scale(luminosity[campaign])

    # Now sum all the rest
    if debug: print("DEBUG: get_total_hist(): sum all the rest")
    for isample in range(1, len(samples)):
      input_files_dict, hist, msg = get_hist(input_files_dict, samples[isample], histname, debug)
      if msg != 'OK':
        return input_files_dict, None, msg
      for campaign in cpgs:
        if campaign in samples[isample] and 'cutflow' not in histname and 'mcEvtWeight' not in histname and 'sampleWeight' not in histname and 'SF' not in histname:
          hist.Scale(luminosity[campaign])
      total_hist.Add(hist)
      hist.Delete()

    return input_files_dict, total_hist, 'OK'


  ####################
  # Helpful functions
  ####################

  # Get list of samples
  def getSampleList(channel: str, selection: str, campaign: str, samples: str, extra: str = '') -> list:
    """Get list of TTbar samples based on chosen channel, selection and campaigns"""
    ttbar_samples = ['NonAllHad', 'Dilepton'] if samples == 'All' else [samples]
    if campaign == 'all':
      return ['TTbar{}_MC16{}_{}_{}_{}{}'.format(sample, cpg, channel, selection, Tagger, extra) for cpg in AllCampaigns for sample in ttbar_samples]
    else:
      return ['TTbar{}_MC16{}_{}_{}_{}{}'.format(sample, campaign, channel, selection, Tagger, extra) for sample in ttbar_samples]


  # Do I need to check if I have data in CR/AltCR?
  ## Get list of data-taking years for a given campaign
  #def getDataPeriods(campaign: str) -> list:
  #  """Get list of data years based on the chosen campaign"""
  #  return {
  #           'all': ['15','16','17','18'],
  #           'a': ['15','16'],
  #           'd': ['17'],
  #           'e': ['18'],
  #         }[campaign]


  def process_np(input_files, Hists, base_cr, cr_channel, target_region, target_channel, mc_campaigns):
    # Loop over observables
    for obs, cases in Observables.items():
      # Loop over event types
      for case in cases:
        for campaign in [mc_campaigns]:
          if Debug:
            print('obs      = {}'.format(obs))
            print('case     = {}'.format(case))
            print('campaign = {}'.format(campaign))
          #DataPeriods = getDataPeriods(campaign)
          ###############################################
          # Get TFs for this observable+case combination
          ###############################################
          # Get MC ttbar distribution in target_region
          if Debug: print(f"DEBUG: Get {target_region} ttbar distribution for MC16{campaign} campaign")
          histname = obs + '_' + case
          if Debug: print(f'{target_region} histname = {histname}')
          sample = getSampleList(target_channel, target_region, campaign, ttbar_samples)
          input_files, target_hist, msg = get_total_hist(input_files, sample, histname, Debug, Luminosity)
          CHECK(msg)
          target_hist.SetDirectory(0)
          # Get MC ttbar distribution in base_cr
          #histname = obs + '_' + case
          # Get MC ttbar distribution in CR
          if Debug: print("DEBUG: Get {} ttbar distribution for MC16{} campaign".format(base_cr, campaign))
          if Debug: print(f'{base_cr} histname = {histname}')
          sample = getSampleList(cr_channel, base_cr, campaign, ttbar_samples)
          input_files, cr_hist, msg = get_total_hist(input_files, sample, histname, Debug, Luminosity)
          CHECK(msg)
          cr_hist.SetDirectory(0)
          cr_hist.SetName(cr_hist.GetName()+'_{}'.format(base_cr))  # change name such that it doesn't match to others
          ##############
          # Compute TF
          if Debug: print('DEBUG: Compute TF')
          #TFhist = target_hist.Clone(histname + '_MC16{}_{}TF'.format(campaign, target_channel))
          TFhist = target_hist.Clone(histname + '_MC16{}_{}{}TF'.format(campaign, target_region, target_channel))
          TFhist.Divide(cr_hist)
          Hists.append(TFhist)

  def process_campaigns(mc_campaigns, SignalGenerator, base_cr, cr_channel, target_region, target_channel):

    print('Running for MC16{}... this might take long'.format(mc_campaigns))

    ####################################################################################
    # Calculate transfer factors
    ####################################################################################

    Hists = []
    m_input_files = {}
    process_np(m_input_files, Hists, base_cr, cr_channel, target_region, target_channel, mc_campaigns)
  
    # Write TF hists
    mc16_campaigns = ['MC16{}'.format(case) for case in [mc_campaigns]]
    Campaigns = '_vs_'.join(mc16_campaigns)
    extra = f'{base_cr}_{cr_channel}_to_{target_region}_{target_channel}'
    extraGen = '' if SignalGenerator == 'SH2211' else '_{}'.format(SignalGenerator)
    outFileName = '{}/ttbarTFs_{}_{}_{}_{}{}.root'.format(output_folder, Version, target_channel, Campaigns, extra, extraGen)
    outFile = ROOT.TFile(outFileName, 'RECREATE')
    print('Writing outputs to {}'.format(outFileName))
    for hist in Hists:
      hist.Write()
    outFile.Close()

    for _, input_file in m_input_files.items():
      input_file.Close()

  process_campaigns(MC16Campaigns, SignalGenerator, base_cr, cr_channel, target_region, target_channel)


if __name__ == '__main__':
  # NOTE: Other options are set in CommonDefs.py

  Debug = False

  # Generator to be used for signal
  SignalGenerator = 'SH2211' # options: FxFx, MG and SH2211

  parser = argparse.ArgumentParser()
  parser.add_argument('--baseCR', action='store', default='', help='options: CR and AltCR')
  parser.add_argument('--baseChannel', action='store', default='', help='options: ELMU')
  parser.add_argument('--targetRegion', action='store', default='', help='options: CR, AltCR and SR')
  parser.add_argument('--targetChannel', action='store', default='', help='options: ELMU, EL or MU (depending on targetRegion)')
  parser.add_argument('--campaign', action='store', default='', help='options: a, d, e, all')
  args = parser.parse_args()

  if not args.baseCR:
    print('ERROR: base control region (--baseCR) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.baseChannel:
    print('ERROR: channel for base control region (--baseChannel) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.targetRegion:
    print('ERROR: target region (--targetRegion) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.targetChannel:
    print('ERROR: channel for target region (--targetChannel) not provided, exiting')
    parser.print_help()
    sys.exit(1)
  if not args.campaign:
    print('ERROR: campaign not provided, exiting')
    parser.print_help()
    sys.exit(1)

  print('INFO: Running GetValTFs.py with base_cr={}, base_channel={}, target_region={}, target_channel={}, campaign={}'.format(args.baseCR, args.baseChannel, args.targetCR, args.targetChannel, args.campaign))

  main(args.baseCR, args.baseChannel, args.targetRegion, args.targetChannel, args.campaign, Debug, SignalGenerator, 'nominal')

  print('>>> ALL DONE <<<')
