##############################################################
# Author:  Jona Bossio (jbossios@cern.ch)                    #
# Purpose: Produce input files to CxAOD-based flavour fitter #
# Usage:   python3 PrepareInputs2CxAOD.py                    #
##############################################################

# Imports
from getSFvarNames import getBTagSFvarNames, getLeptonSFvarNames, getJVTSFvarNames, getPRWvarNames
import logging
import ROOT
import os
import sys

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *
from SystematicTTrees import *
from InputLists import Inputs
from HelperFunctions import *


def symmetrize(hist, nom_hists):
  """
  1down = 2 nominal - 1up
  explanation:
    if 1up > nominal => 1up = nom + diff with diff > 0
      then 1down = nominal - diff, i.e 1down < nominal
    if 1up < nominal => 1up = nom - diff with diff > 0
      then 1down = nominal + diff, i.e 1down > nominal
  """
  hist.Scale(-1)
  nom_hist = [h for h in nom_hists if hist.GetName() == h.GetName()][0]
  #print(f'hist.GetName() = {hist.GetName()}')
  #print(f'Type(hist) = {type(hist)}')
  #print(f'nom_hist.GetName() = {nom_hist.GetName()}')
  #print(f'Type(nom_hist) = {type(nom_hist)}')
  hist.Add(nom_hist, 2)


def main(SignalGenerator, control_region, merge_channels, Debug, version = None):

  # Convert TH2D->TH1D?
  convert = False

  # Use dummy histograms for non-SF systematic variations
  UseDummySysts = False

  # Rename syst dirs to match CxAOD
  def rename_dir_name(dir_name):
    if 'MET_SoftTrk_ResoPara' in dir_name:
      return dir_name.replace('MET_SoftTrk_ResoPara', 'MET_SoftTrk_ResoPara__1up')
    if 'MET_SoftTrk_ResoPerp' in dir_name:
      return dir_name.replace('MET_SoftTrk_ResoPerp', 'MET_SoftTrk_ResoPerp__1up')
    if 'MUON_EFF_TTVA' in dir_name:
      return dir_name.replace('EFF_TTVA', 'TTVA')
    if 'MUON_EFF_ISO' in dir_name:
      return dir_name.replace('EFF_ISO', 'ISO')
    if 'MUON_EFF_RECO' in dir_name:
      return dir_name.replace('EFF_RECO', 'EFF')
    return dir_name

  logging.basicConfig(level='INFO',format='%(levelname)s: %(message)s')
  log = logging.getLogger('main')
  log.info('Running... this might take long')

  Years = ['1516', '17', '18', '15161718']

  # Read common options
  from CommonDefs import Options as opt
  Channels = opt['Channels']
  Tagger = opt['Tagger']
  Version = opt['Version'] if not version else version
  TagRegions = opt['FinalStates']
  MC16Campaigns = [opt['Campaign'].replace('MC16','')]
  AllCampaigns = [cpg.replace('MC16','') for cpg in opt['Campaigns']]
  extraCR  = '_{}'.format(control_region) if control_region != 'CR' else ''

  # Dictionary with all xAH->CxAOD string transformations
  Transforms = {
    'Channel': {'EL': 'El', 'MU': 'Mu'},
    'TagRegion': {'Ti1': 'Geq1BiJ', 'Ti2': 'Geq2BiJ'},
    'Obs': {
      'HFjet0_pt'     : 'leadBJet_Pt',
      'HFjet0_absy'   : 'leadBJet_Y',
      'HFjet0_XF'     : 'leadBJet_xF',
      'HFjet1_pt'     : 'subLeadBJet_Pt',
      'HFjet1_absy'   : 'subLeadBJet_Y',
      'HFjet1_XF'     : 'subLeadBJet_xF',
      'Zpt'           : 'Z_Pt',
      'Zabsy'         : 'Z_Y',
      'deltaRZjHF'    : 'ZleadBJet_DR',
      'mjjHF'         : 'diBJets_M',
      'pTjjovermjjHF' : 'diBJets_PtjjMjj',
      'deltaYjjHF'    : 'diBJets_DY',
      'deltaRjjHF'    : 'diBJets_DR',
      'deltaPhijjHF'  : 'diBJets_DPhi',
    },
    'QuantileType': {
      'LeadQuantile' : 'bTagQuantiles',
      'CombQuantile' : 'bTagQuantiles2Jets',
    },
  }

  # Labels to avoid in nominal TTrees
  SkipLabelsInNominal = [
    'BTag',
    'ELReco',
    'ELIso',
    'ELTrig',
    'ELPID',
    'MUReco',
    'MUIso',
    'MUTrig',
    'MUPID',
    'JVT',
    'weight_pileup',
  ]

  def check_msg(msg, log):
    if msg != 'OK':
      log.fatal(msg)
      sys.exit(1)

  # Identify b-tagging requirement
  def get_tag_region(name:str) -> str:
    """Identify tag region from histogram's name"""
    for case in ['Te1', 'Te2', 'Ti1', 'Ti2']:
      if case in name:
        return case
    return ''

  # Identify observable's name
  def get_observable(name: str, channel: str, tagRegion: str) -> str:
    """Identify observable's name from histogram's name"""
    Obs = name.split('_'+tagRegion)[0].replace('ttbar_SR_{}_'.format(channel), '')
    if 'Quantile' in Obs:
      Obs = Obs.split('_vs_')[1]
    return Obs

  # Identify uncertainty source
  def get_base_name(name:str) -> str:
    """Identify uncertainty source"""
    basename = 'EL' if 'EL' in name else 'MU'
    if 'EL' in name:
      if   'Reco'    in name: return 'ELReco'
      elif 'Iso'     in name: return 'ELIso'
      elif 'ID'      in name: return 'ELPID'
      elif 'Trigger' in name: return 'ELTrig'
    else: # MU
      if   'RECO' in name: return 'MUReco'
      elif 'ISO'  in name: return 'MUIso'
      elif 'TTVA' in name: return 'MUPID'
      elif 'Trig' in name: return 'MUTrig'

  # Get histograms from input ROOT file
  def get_hists(kname: str, iFile: ROOT.TFile, log) -> list:
    """Get histograms from input ROOT file"""
    # Extract b-tagging requirement
    TagRegion = get_tag_region(kname)
    tagRegion = Transforms['TagRegion'][TagRegion]
    # Extract channel
    Channel = kname.split('_')[2]
    channel = Transforms['Channel'][Channel]
    # Extract observable name
    Obs = get_observable(kname, Channel, TagRegion)
    if Obs not in Transforms['Obs']:
      return [] # skip observable w/o a transform
    if 'Quantile' in kname:
      QuantileType = 'LeadQuantile' if 'Lead' in kname else 'CombQuantile'
      quantileType = Transforms['QuantileType'][QuantileType]
      obs          = Transforms['Obs'][Obs]
      TempHist     = iFile.Get(kname)
      if not convert:
        obs_quantile = '{}_{}'.format(obs, quantileType)
        #NewName = 'ttbar_{}_{}_FlavFitCalibBins_{}'.format(channel, tagRegion, obs_quantile)
        NewName = 'ttbar_{}_{}_FlavFit_{}'.format(channel, tagRegion, obs_quantile)
        Hist = TempHist.Clone(NewName)
        Hist.SetDirectory(0)
        return [Hist]
      # Produce one TH1D histogram per bin
      nObsBins = len(Binning[Obs])-1 if len(Binning[Obs]) > 3 else Binning[Obs][0]
      Hists = []
      for obsBin in range(1, nObsBins+1):  # loop over observable's bins
        obsBinUpperEdge = TempHist.GetXaxis().GetBinLowEdge(obsBin+1)
        if obsBinUpperEdge >= 10:
          obsBinStr = str(int(obsBinUpperEdge))
        else: # value < 10
          obsBinStr = '{}p{}'.format(int(obsBinUpperEdge),int((obsBinUpperEdge-int(obsBinUpperEdge))*100))
        obs_bin_quantile = '{}_{}_{}'.format(obs,obsBinStr,quantileType)
        NewName          = 'ttbar_{}_{}_FlavFit_{}'.format(channel,tagRegion,obs_bin_quantile)
        Hist             = TempHist.ProjectionY(NewName, obsBin, obsBin)
        Hist.SetDirectory(0)
        Hists.append(Hist)
      return Hists
    else:
      # Get histogram and change name
      obs = Transforms['Obs'][Obs]
      NewName = 'ttbar_{}_{}_{}'.format(channel,tagRegion,obs)
      TempHist = iFile.Get(kname)
      Hist = TempHist.Clone(NewName)
      Hist.SetDirectory(0)
      return [Hist]

  def save_hists(opt, iFile, ObjectKeys, outFile, Debug, log):
    # Get variation names
    function = {
      'JVT': getJVTSFvarNames,
      'BTag': getBTagSFvarNames,
      'Lepton': getLeptonSFvarNames,
      'PRW': getPRWvarNames,
    }[opt]
    SFsystNames, msg = function()
    check_msg(msg, log)

    if opt == 'Lepton':
      # Map index from SF syst names list with index used by maker
      LeptonSources = ['Reco', 'PID', 'Iso', 'Trig']
      IndexDict = {}
      Counter = {'{}{}'.format(channel, source) : 0 for channel in Channels for source in LeptonSources}
      for index, name in enumerate(SFsystNames):
        key = get_base_name(name)
        Counter[key] += 1
        IndexDict[name] = Counter[key]

    # Loop over systematic variations
    for index, SystName in enumerate(SFsystNames):
      # Get histograms
      if opt != 'Lepton':
        sysName = '{}_{}'.format(opt if opt != 'PRW' else 'weight_pileup', index + 1)
      else:
        sysName = '{}_{}'.format(get_base_name(SystName), IndexDict[SystName])
      Knames = [key.GetName() for key in ObjectKeys if get_tag_region(key.GetName()) in TagRegions and key.GetName().endswith(sysName)]  # hist names
      if Debug:
        print('Knames for {} = {}'.format(opt, Knames))
      SystHists = []  # collec systematic variation hists to write
      for kname in Knames:
        hists = get_hists(kname, iFile, log)
        if hists:
          SystHists += hists
      # Write histograms to file
      outFile.cd()
      SystName = rename_dir_name(SystName)
      outFile.mkdir(SystName)
      outFile.cd(SystName)
      for hist in SystHists:
        hist.Write()
      outFile.cd()

  ###################
  # Loop over years
  ###################
  for year in Years:
    log.info(f'Preparing histograms for data{year}')

    # Merge EL/MU ROOT files
    base_file_name = 'Outputs/ttbarContribution_Signal{}_{}CH_data{}{}.root'.format(SignalGenerator, Version, year, extraCR)
    merged_file_name = base_file_name.replace('CH', '')
    if os.path.exists(merged_file_name):
      os.system('rm {}'.format(merged_file_name))
    command = 'hadd {} {} {}'.format(merged_file_name, base_file_name.replace('CH', '_EL'), base_file_name.replace('CH', '_MU'))
    os.system(command)

    # Open merged input file
    iFile = ROOT.TFile.Open(merged_file_name)
    if not iFile:
      log.fatal('{} not found, exiting')
      sys.exit(1)

    # Get all relevant histograms, update hist name and write to output file
    ObjectKeys = iFile.GetListOfKeys()

    ############################################
    # Get nominal data-driven ttbar estimations
    ############################################

    # Get histograms

    def check_labels(name: str) -> list:
      return [True if label in name else False for label in SkipLabelsInNominal]

    # Histograms' names
    Knames = [key.GetName() for key in ObjectKeys if get_tag_region(key.GetName()) in TagRegions and not sum(check_labels(key.GetName()))]
    if Debug:
      print('Knames for nominal = {}'.format(Knames))
    Hists = []  # collec nominal histograms to write
    for kname in Knames:
      hists = get_hists(kname, iFile, log)
      if hists:
        Hists += hists

    # Create output file
    MC16campaign = {
      '1516': 'mc16a',
      '17': 'mc16d',
      '18': 'mc16e',
      '15161718': 'mc16All',
    }[year]
    output_folder_name = 'Outputs/ForCxAOD/{}_wSys/{}/'.format(MC16campaign, Version)
    if not os.path.exists(output_folder_name):
      os.makedirs(output_folder_name)
    outFileName = '{}hist-data-driven-ttbar.root'.format(output_folder_name)
    outFile = ROOT.TFile(outFileName, 'RECREATE')

    # Write nominal hists to output file
    outFile.cd()
    for hist in Hists:
      hist.Write()

    # Close input file
    iFile.Close()

    ###########################
    # Add SF variations
    ###########################
    # Merge EL/MU ROOT files
    base_file_name = 'Outputs/ttbarContribution_Signal{}_{}CH_data{}{}.root'.format(SignalGenerator, Version, year, extraCR)
    merged_file_name = base_file_name.replace('CH', '')
    if os.path.exists(merged_file_name):
      os.system('rm {}'.format(merged_file_name))
    command = 'hadd {} {} {}'.format(merged_file_name, base_file_name.replace('CH', '_EL'), base_file_name.replace('CH', '_MU'))
    os.system(command)

    # Open merged input file
    iFile = ROOT.TFile.Open(merged_file_name)
    if not iFile:
      log.fatal('{} not found, exiting')
      sys.exit(1)

    # Get all relevant histograms, update hist name and write to output file
    ObjectKeys = iFile.GetListOfKeys()

    for case in ['JVT', 'BTag', 'Lepton', 'PRW']:
      save_hists(case, iFile, ObjectKeys, outFile, Debug, log)

    # Close input file
    iFile.Close()

    #############################
    # Get systematic TTree names
    SysNames = ['Sys'+name for name in SystTTrees]

    if UseDummySysts:
      # Add nominal hists as variated hists in separated dictories
      for name in SysNames:
        outFile.cd()
        outFile.mkdir(name)
        outFile.cd(name)
        for hist in Hists:
          hist.Write()
        outFile.cd()
    else:
      # Merge input files
      base_file_name = 'Outputs/ttbarContribution_Signal{}_{}CH_data{}{}_FullSysts.root'.format(SignalGenerator, Version, year, extraCR)
      merged_file_name = base_file_name.replace('CH', '')
      if os.path.exists(merged_file_name):
        os.system('rm {}'.format(merged_file_name))
      command = 'hadd {} {} {}'.format(merged_file_name, base_file_name.replace('CH', '_EL'), base_file_name.replace('CH', '_MU'))
      os.system(command)
      
      # Open input file
      iFile = ROOT.TFile.Open(merged_file_name)
      if not iFile:
        log.fatal('{} not found, exiting')
        sys.exit(1)

      # Get histograms
      ObjectKeys = [key.GetName() for key in iFile.GetListOfKeys()]
      SystHists = dict()
      for kname in ObjectKeys:
        SystHists[kname] = []
        tdir = iFile.Get(kname)
        for hname in tdir.GetListOfKeys():
          hist = get_hists(hname.GetName(), tdir, log)
          SystHists[kname] += hist

      if len(SystHists.keys()) != len(SysNames):
        log.fatal('The number of non-SF systematics is not the expected one, exiting')
        print('SysNames = {}'.format(sorted(SysNames)))
        print('Keys = {}'.format(sorted(list(SystHists.keys()))))
        sys.exit(1)

      # Write histograms to file
      for kname, hists in SystHists.items():
        if Debug:
          print('Writing histograms for kname = {}'.format(kname))
        outFile.cd()
        dir_name = 'Sys{}'.format(kname)
        dir_name = rename_dir_name(dir_name)
        outFile.mkdir(dir_name)
        outFile.cd(dir_name)
        for hist in hists:
          if 'JER' in kname and '1down' in kname:  # symmetrize JER uncertainty
            symmetrize(hist, Hists)
          hist.Write()
        outFile.cd()

      # Create 1down MET uncertainties
      for kname in ['MET_SoftTrk_ResoPara', 'MET_SoftTrk_ResoPerp']:
        for hist in SystHists[kname]:
          symmetrize(hist, Hists)
        dir_name = 'Sys{}'.format(kname)
        dir_name += '__1down'
        outFile.mkdir(dir_name)
        outFile.cd(dir_name)
        for hist in SystHists[kname]:
          hist.Write()
        outFile.cd()

    ################################
    # Add extrapolation uncertainty
    name = 'SysTTbarExtrapolationUncertainty__1up'

    # Merge input files
    base_file_name = f'Outputs/ttbarExtrapolationUncertainty_Signal{SignalGenerator}_{Version}_data{year}{extraCR}CH.root'
    merged_file_name = base_file_name.replace('CH', '')
    if os.path.exists(merged_file_name):
      os.system('rm {}'.format(merged_file_name))
    command = 'hadd {} {} {}'.format(merged_file_name, base_file_name.replace('CH', '_EL'), base_file_name.replace('CH', '_MU'))
    os.system(command)

    # Open merged file
    extrap_uncert_file = ROOT.TFile.Open(merged_file_name)
    extrap_uncert_hists = [extrap_uncert_file.Get(key.GetName()) for key in extrap_uncert_file.GetListOfKeys()]
    # rename histograms
    for hist in extrap_uncert_hists:
      old_name = hist.GetName()
      xah_tag_region = get_tag_region(old_name)
      xah_channel = old_name.split('_')[2]
      xah_obs = get_observable(old_name, xah_channel, xah_tag_region)
      cxaod_tag_region = Transforms['TagRegion'][xah_tag_region]
      cxaod_channel = Transforms['Channel'][xah_channel]
      cxaod_obs = Transforms['Obs'][xah_obs]
      if 'Quantile' not in old_name:
        new_name = f'ttbar_{cxaod_channel}_{cxaod_tag_region}_{cxaod_obs}'
      else:
        xah_quantile_case = 'LeadQuantile' if 'Lead' in old_name else 'CombQuantile'
        cxaod_quantile_type = Transforms['QuantileType'][xah_quantile_case]
        new_name = f'ttbar_{cxaod_channel}_{cxaod_tag_region}_FlavFit_{cxaod_obs}_{cxaod_quantile_type}'
        #print(f'hist = {hist}')
        #print(f'type(hist) = {type(hist)}')
      hist.SetName(new_name)
    outFile.cd()
    outFile.mkdir(name)
    outFile.cd(name)
    for hist in extrap_uncert_hists:
      hist.Write()
    # Symmetrize
    outFile.cd()
    outFile.mkdir(name.replace('1up', '1down'))
    outFile.cd(name.replace('1up', '1down'))
    for hist in extrap_uncert_hists:
      symmetrize(hist, Hists)
    for hist in extrap_uncert_hists:
      hist.Write()
    outFile.cd()

    # Close output file
    outFile.Close()

    # Merge El/Mu predictions
    if merge_channels:
      # Open input file with El/Mu histograms
      inFileName = '{}hist-data-driven-ttbar.root'.format(output_folder_name)
      inFile = ROOT.TFile.Open(inFileName)
      if not inFile:
        print(f'ERROR: {inFileName} not found, exiting')
        sys.exit(1)
      # Get list of nominal histograms for El channel
      el_hists = [folder.GetName() for folder in inFile.GetListOfKeys() if 'El' in folder.GetName()]
      # Get list of systematics
      dirs = [''] + [folder.GetName() for folder in inFile.GetListOfKeys() if 'Sys' in folder.GetName()]
      histograms = {k: [] for k in dirs}
      # Merge El/Mu histograms
      for dirname in dirs:
        folder = dirname + '/' if dirname else dirname
        for el_hist_name in el_hists:
          el_hist = inFile.Get(folder + el_hist_name)
          el_hist.SetDirectory(0)
          mu_hist = inFile.Get(folder + el_hist_name.replace('El', 'Mu'))
          mu_hist.SetDirectory(0)
          sum_hist = el_hist.Clone(el_hist_name.replace('El', 'Lep'))
          sum_hist.SetDirectory(0)
          sum_hist.Add(mu_hist)
          histograms[dirname].append(sum_hist)
      inFile.Close()
      # Write merged histograms
      output_folder_name = output_folder_name.replace('wSys', 'wSys_Lep')
      if not os.path.exists(output_folder_name):
        os.makedirs(output_folder_name)
      outFileName = '{}/hist-data-driven-ttbar.root'.format(output_folder_name)
      outFile = ROOT.TFile(outFileName, 'RECREATE')
      outFile.cd()
      for dirname, hists in histograms.items():
        if dirname:
          outFile.mkdir(dirname)
          outFile.cd(dirname)
        for hist in hists:
          hist.Write()
        outFile.cd()
      outFile.Close()

  log.info('>>> ALL DONE <<<')

if __name__ == '__main__':
  # Generator to be used for signal
  SignalGenerator = 'SH2211' # options: MG and SH2211
  control_region = 'CR'
  merge_channels = True
  # Increase verbosity
  Debug = False
  main(SignalGenerator, control_region, merge_channels, Debug)
