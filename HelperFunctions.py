from Selections import *

import ROOT
import sys
import os
import array

from Arrays import Flavours
from Selections import *
from InputLists import Inputs
from metadata_dict import MetadataFiles


def configure(args):
  # Create dict with all the provided settings
  settings = {}

  # Sample type
  settings['Channel'] = args.channel
  settings['Dataset'] = args.dataset

  # Input path/file
  settings['inputPath'] = args.path
  settings['inputFile'] = args.inputFile

  # Get sample type
  if '2015' in settings['inputPath']:
    settings['sampleType'] = '2015'
  elif '2016' in settings['inputPath']:
    settings['sampleType'] = '2016'
  elif '2017' in settings['inputPath']:
    settings['sampleType'] = '2017'
  elif '2018' in settings['inputPath']:
    settings['sampleType'] = '2018'
  elif 'MC16a' in settings['inputPath']:
    settings['sampleType'] = 'MC16a'
  elif 'MC16d' in settings['inputPath']:
    settings['sampleType'] = 'MC16d'
  elif 'MC16e' in settings['inputPath']:
    settings['sampleType'] = 'MC16e'
  else:
    print('ERROR: sample type not recognized from input path ({}), exiting...'.format(settings['inputPath']))
    sys.exit(0)

  # Output path
  settings['outPath'] = args.outPATH

  # Jet collection
  settings['JetColl'] = args.jetcoll

  # Apply SF systematic variations?
  settings['ApplySFsysts'] = args.applySFsysts

  # Jet flavour tagger
  settings['Tagger'] = args.tagger

  # Nominal only, full systs or subset of systematics separated by comma (MC only)
  settings['TTrees'] = args.ttrees

  # MC weights (only meaningful for MC, i.e. not used in data)
  settings['useSFs'] = not args.noSFs
  settings['useSampleWeights'] = not args.noSampleWeights
  settings['usePRW'] = not args.noPRW
  settings['readAMI'] = args.readAMI
  settings['getWeightSysts'] = args.getWeightSysts
  settings['cxaod_validation'] = args.cxaodval

  # Selection
  settings['Selection'] = args.selection

  # Filling of distributions
  settings['fillReco'] = not args.noReco
  settings['fillTruth'] = not args.noTruth

  # Debugging/Testing
  settings['Debug'] = args.debug
  settings['Test'] = args.test
  settings['Ref'] = args.ref
  settings['LocalTest'] = args.localTest
  settings['ci_test'] = args.ci_test

  # Print event-level info?
  settings['PrintRecoInfo'] = args.printRecoInfo
  settings['PrintTruthInfo'] = args.printTruthInfo
  settings['PrintMatchedInfo'] = args.printMatchedInfo

  # Timing and memory usage
  settings['Perf'] = args.perf

  # Use XRootD?
  settings['noxrootd'] = args.noxrootd

  # MC?
  settings['MC'] = False
  if 'Data' not in settings['Dataset']:
    settings['MC'] = True
    # Identify DSID
    settings['DSID'] = getDSID(settings['inputPath'])
    # Running on MC signal?
    settings['isMCsignal'] = False
    if settings['Channel'] == "MU" and "Zmumu" in settings['Dataset']:
      settings['isMCsignal'] = True
    elif settings['Channel'] == "EL" and "Zee" in settings['Dataset']:
      settings['isMCsignal'] = True
    elif settings['Channel'] == "ELMU" and "Zee" in settings['Dataset']:
      settings['isMCsignal'] = True
    if settings['Channel'] == "ELMU" and "Zmumu" in settings['Dataset']:
      settings['isMCsignal'] = True
    # Produce transfer matrices?
    settings['makeTMs'] = True if settings['isMCsignal'] else False
    # Fill truth distributions only for MC signal
    if not settings['isMCsignal']:
      settings['fillTruth'] = False
  else:  # Identify data period
    settings['dataPeriod'] = getPeriod(settings['inputPath'])

  # Protections
  if not settings['MC']:  # data
    settings['useSFs'] = False
    settings['useSampleWeights'] = False
    settings['usePRW'] = False
    settings['ApplySFsysts'] = False
    settings['readAMI'] = False

  # Get MC16a campaign
  if settings['MC']:
    if "MC16a" in settings['Dataset']:
      settings['campaign'] = "a"
    elif "MC16d" in settings['Dataset']:
      settings['campaign'] = "d"
    elif "MC16e" in settings['Dataset']:
      settings['campaign'] = "e"
    else:
      print('MC16 campaign not recognized from the Dataset name ({}), exiting...'.format(settings['Dataset']))
      sys.exit(1)

  # Do flavour classification only in MC signal samples
  settings['Flavours'] = Flavours
  settings['doFlavourClassification'] = True
  if settings['MC'] and not settings['isMCsignal']:
    settings['Flavours']['reco'] = [""]
    settings['doFlavourClassification'] = False

  # Protections
  if settings['TTrees'] != 'nominal':
    # NOTE: I only use systematic uncertainties for data-driven ttbar estimation,
    # not for flav fitting/unfolding
    settings['Flavours']['reco'] = [""]
    settings['doFlavourClassification'] = False
    settings['makeTMs'] = False
    # Do not variate event weight in non-nominal TTrees,
    # because info is not available
    settings['getWeightSysts'] = False
  if not settings['fillTruth']:  # no TMs if no truth info is wanted
    settings['makeTMs'] = False

  # No reco flavour distributions when propagating SF variations
  if settings['ApplySFsysts']:
    settings['Flavours']['reco'] = [""]
    settings['doFlavourClassification'] = False

  # Name of TDirectoryFile object inside ROOT files
  settings['TDirectoryName'] = "ZHFTreeAlgo"

  # Max number of events for debugging
  settings['DebugMAXevents'] = 1000

  # Selection class used for event/object selections
  if settings['JetColl'] == "PFlow":
    if settings['Selection'] == "SR":
      settings['SelectionClass'] = PFlowSRselection()
    elif settings['Selection'] == "SR2":
      settings['SelectionClass'] = PFlowSR2selection()
    elif settings['Selection'] == "SRa1jet":
      settings['SelectionClass'] = PFlowSRa1jetselection()
    elif settings['Selection'] == "CR":
      settings['SelectionClass'] = PFlowCRselection()
    elif settings['Selection'] == "AltCR":
      settings['SelectionClass'] = PFlowAltCRselection()
    elif settings['Selection'] == "VR":
      settings['SelectionClass'] = PFlowVRselection()
    else:
      print("ERROR: Selection ({}) not recognized, exiting...".format(settings['Selection']))
      sys.exit(0)
  elif settings['JetColl'] == "EMTopo":
    if settings['Selection'] == "SR":
      settings['SelectionClass'] = EMTopoSRselection()
    else:
      print("ERROR: Selection ({}) not recognized, exiting...".format(settings['Selection']))
      sys.exit(0)
  settings['MUisoWP'] = MUisoWP
  settings['ELisoWP'] = ELisoWP
  settings['minLepPt'] = minLepPt
  settings['UseJVTineffSFs'] = UseJVTineffSFs
  settings['btagWP'] = '85'

  return settings


def get_xrootd_prefix(path):
  """ Get XRootD prefix """
  if '/eos/atlas/' in path:
    return 'root://eosatlas.cern.ch/'
  elif '/eos/user/' in path:
    return 'root://eosuser.cern.ch/'
  return ''


def read_ami_info():
  ami_file_name = "ami_reader.txt"
  AMIFile = open(ami_file_name, 'r')
  lines = AMIFile.readlines()
  ami_xss = dict()  # cross section values
  ami_eff = dict()  # efficiency values
  ami_k   = dict()  # k-factor values
  for line in lines:  # loop over lines
    Line = line.split(" ")
    dsid = int(Line[0])  # first column is dsid
    ami_xss[dsid] = float(Line[1])  # second column is cross section
    ami_eff[dsid] = float(Line[2]) if '\n' not in Line[2] else float(Line[2][:-2])  # third column is efficiency
    if len(Line) > 3:  # fourth column (if available) is k-factor
      kFactor = Line[3]
    else:
      kFactor = '1.0'
    ami_k[dsid] = float(kFactor) if '\n' not in kFactor else float(kFactor)
  return ami_xss, ami_eff, ami_k


def get_output_file_name(settings):
  """ Return output file name """
  # Prepare name of output file name based on input file name and settings
  input_file_name = settings['inputFile'].replace(".root", "")
  output_file_name = str(settings['outPath'])
  if output_file_name[-1] != '/':
    output_file_name += '/'
  output_file_name += settings['Dataset']
  if settings['MC']:  # identify DSID
    output_file_name += "_" + settings['DSID']
  else:  # identify data period
    output_file_name += "_" + settings['dataPeriod']
  output_file_name += "_" + settings['Channel'] + "_" + settings['Selection']
  output_file_name += "_" + settings['Tagger'] + "Tagger"
  if settings['MC']:
    if settings['useSFs']:
      output_file_name += "_useSFs"
    else:
      output_file_name += "_noSFs"
    if settings['useSampleWeights']:
      output_file_name += "_useSampleWeights"
    else:
      output_file_name += "_noSampleWeights"
    if settings['usePRW']:
      output_file_name += "_usePRW"
    else:
      output_file_name += "_noPRW"
    if settings['TTrees'] == 'Full':
      output_file_name += "_full"
    elif settings['TTrees'] == 'FullSysts':
      output_file_name += "_fullSysts"
    elif settings['TTrees'] == 'nominal':
      output_file_name += "_nominalOnly"
    else:
      TTreeNames = settings['TTrees'].replace(',','_')
      output_file_name += "_" + TTreeNames
  output_file_name += "_" + input_file_name
  if settings['Debug']:
    output_file_name += "_Debug"
  if settings['Test']:
    output_file_name += "_Test"
  if settings['Ref']:
    output_file_name += "_Ref"
  output_file_name += ".root"

  return output_file_name


def get_sum_of_weights(settings):
  """ Get sumWeights """
  sumWeights = dict()
  if not settings['MC']:
    return sumWeights
  if not settings['LocalTest']:
    # Loop over samples
    sample = settings['Dataset'] if settings['TTrees'] == 'nominal' else settings['Dataset'] + '_systs'
    PATHs = Inputs[settings['JetColl']][sample]  # get list of paths where folders with metadata files are located
    for path in PATHs:  # loop over paths
      for folder in os.listdir(path):  # loop over folders from each path
        if '_metadata.root' not in folder:
          continue  # skip cutflows/trees
        # open metadata file
        key = int(getDSID(folder))
        MetadataFileName = '{}{}{}/{}_metadata.root'.format(get_xrootd_prefix(settings['inputPath']) if not settings['noxrootd'] else '', path, folder, key)
        MetadataFile = ROOT.TFile.Open(MetadataFileName)
        if not MetadataFile:
          print("ERROR: {} file not found, exiting...".format(MetadataFileName))
          sys.exit(0)
        # get histogram
        MetadataHist = MetadataFile.Get("MetaData_EventCount")
        sumWeights[key] = MetadataHist.GetBinContent(3)
        MetadataFile.Close()
  else:  # use local metadata files meant for testing
    # Loop over DSIDs
    for key in MetadataFiles:
      # open metadata file
      if not settings['ci_test']:
        metadata_file_name = '{}{}'.format(get_xrootd_prefix(MetadataFiles[key]) if not settings['noxrootd'] else '', MetadataFiles[key])
      else:  # CI test
        metadata_file_name = '{}{}'.format(get_xrootd_prefix(MetadataFiles[key]), MetadataFiles[key])
      MetadataFile = ROOT.TFile.Open(metadata_file_name)
      if not MetadataFile:
        print("ERROR: {} file not found, exiting...".format(MetadataFiles[key]))
        sys.exit(0)
      # get histogram
      MetadataHist = MetadataFile.Get("MetaData_EventCount")
      sumWeights[key] = MetadataHist.GetBinContent(3)
      MetadataFile.Close()
  return sumWeights



#####################
# TRUTH LEPTONS
#####################
def getNLeptonTruth(Branches,Channel):
  if "EL" in Channel:
    return len(Branches["truthElectron_pt_dressed"])
  elif "MU" in Channel:
    return len(Branches["truthMuon_pt_dressed"])

def getLeptonPtTruth(Branches, i, channel):
  if "EL" in channel:
    return Branches["truthElectron_pt_dressed"][i]
  elif "MU" in channel:
    return Branches["truthMuon_pt_dressed"][i]

def getLeptonEtaTruth(Branches, i, channel):
  if "EL" in channel:
    return Branches["truthElectron_eta_dressed"][i]
  elif "MU" in channel:
    return Branches["truthMuon_eta_dressed"][i]

def getLeptonPhiTruth(Branches, i, channel):
  if "EL" in channel:
    return Branches["truthElectron_phi_dressed"][i]
  elif "MU" in channel:
    return Branches["truthMuon_phi_dressed"][i]

def getLeptonETruth(Branches, i, channel):
  if "EL" in channel:
    return Branches["truthElectron_e_dressed"][i]
  elif "MU" in channel:
    return Branches["truthMuon_e_dressed"][i]

def getLeptonPassTruthSelection(Branches):
  return Branches["PassTruthSelections"][0]

#####################
# RECO LEPTONS
#####################
def getNLepton(Branches,channel):
  if channel == "EL":
    return len(Branches["el_pt"])
  elif channel == "MU":
    return len(Branches["muon_pt"])
  elif channel == 'ELMU':
    return len(Branches["el_pt"])+len(Branches["muon_pt"])

def getLeptonPt(Branches, i, channel):
  if channel == "EL":
    return Branches["el_pt"][i]
  elif channel == "MU":
    return Branches["muon_pt"][i]
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches,'EL')
    return Branches['el_pt'][i] if i < nElectrons else Branches['muon_pt'][i-nElectrons]

def getLeptonEta(Branches,i,channel):
  if channel == "EL":
    return Branches["el_eta"][i]
  elif channel == "MU":
    return Branches["muon_eta"][i]
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches,'EL')
    return Branches['el_eta'][i] if i < nElectrons else Branches['muon_eta'][i-nElectrons]

def get_electron_calo_cluster_eta(branches, i_el):
  return branches['el_caloCluster_eta'][i_el]

def getLeptonPhi(Branches,i,channel):
  if channel == "EL":
    return Branches["el_phi"][i]
  elif channel == "MU":
    return Branches["muon_phi"][i]
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches,'EL')
    return Branches['el_phi'][i] if i < nElectrons else Branches['muon_phi'][i-nElectrons]

def getLeptonM(Branches,i,channel):
  if channel == "EL":
    return Branches["el_m"][i]
  elif channel == "MU":
    return Branches["muon_m"][i]
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches,'EL')
    return Branches['el_m'][i] if i < nElectrons else Branches['muon_m'][i-nElectrons]

def getLeptonCharge(Branches, i, channel):
  if channel == "EL":
    return Branches["el_charge"][i]
  elif channel == "MU":
    return Branches["muon_charge"][i]
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches, 'EL')
    return Branches['el_charge'][i] if i < nElectrons else Branches['muon_charge'][i-nElectrons]

def getLeptonPassRecoSelection(Branches):
  return Branches["PassRecoSelections"][0]

def getLeptonRecoSF(Branches,i,channel):
  if channel == "EL":
    return Branches["el_RecoEff_SF"][i] # return array of SFs (first is nominal)
  elif channel == "MU":
    return Branches["muon_RecoEff_SF_RecoMedium"][i] # return array of SFs (first is nominal)
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches,'EL')
    return Branches['el_RecoEff_SF'][i] if i < nElectrons else Branches['muon_RecoEff_SF_RecoMedium'][i-nElectrons]

def getLeptonIsoSF(Branches,i,channel):
  if channel == "EL":
    return Branches["el_IsoEff_SF_Tight_isol{}".format(ELisoWP)][i] # return array of SFs (first is nominal)
  elif channel == "MU":
    return Branches["muon_IsoEff_SF_Iso{}".format(MUisoWP)][i] # return array of SFs (first is nominal)
  elif channel == "ELMU":
    nElectrons = getNLepton(Branches,'EL')
    return Branches['el_IsoEff_SF_Tight_isol{}'.format(ELisoWP)][i] if i < nElectrons else Branches['muon_IsoEff_SF_Iso{}'.format(MUisoWP)][i-nElectrons]

def getMuonTTVASF(Branches,i):
  return Branches["muon_TTVAEff_SF"][i] # return array of SFs (first is nominal)

def getElectronPIDSF(Branches,i):
  return Branches["el_PIDEff_SF_Tight"][i] # return array of SFs (first is nominal)

def getTrigThreshold(trigger):
  return int(trigger.split('_')[1].replace('mu','').replace('e',''))

def isLeptonTrigMatched(Branches,ilep,channel,reqTrigs): # reqTrigs: list of triggers requested
  # Find at least one trigger that matches the lepton and satisfies pt > trigThreshold + 1
  if channel == 'ELMU':
    nElectrons = getNLepton(Branches,'EL')
    if ilep < nElectrons: # it's an electron
      isTrigMatchedToChain = "el_isTrigMatchedToChain"
      listTrigChains       = "el_listTrigChains"
    else: # it's a muon
      isTrigMatchedToChain = "muon_isTrigMatchedToChain"
      listTrigChains       = "muon_listTrigChains"
      ilep                 = ilep-nElectrons
  else: # EL or MU
    if "MU" in channel:
      isTrigMatchedToChain = "muon_isTrigMatchedToChain"
      listTrigChains       = "muon_listTrigChains"
    elif "EL" in channel:
      isTrigMatchedToChain = "el_isTrigMatchedToChain"
      listTrigChains       = "el_listTrigChains"
  Matches    = Branches[isTrigMatchedToChain][ilep] # match decision for each trigger (matching order of previous array)
  TrigList   = Branches[listTrigChains][ilep]       # full list of triggers (provided in xAH config)
  lepPt      = getLeptonPt(Branches,ilep,channel)
  Chains     = []
  Thresholds = []
  for itrig in range(0,len(TrigList)): # loop over full list of triggers
    TrigName = TrigList[itrig]
    if TrigName not in reqTrigs: continue # skip trigger that is not requested
    trigThreshold = getTrigThreshold(TrigName)
    Matched = False
    if TrigName != 'HLT_e300_etcut':
      if Matches[itrig]: Matched = True
    else: # HLT_e300_etcut
      Matched = True # no need to request matching for this chain
    if Matched and TrigName in Branches["passedTriggers"]: # lepton matched to a fired trigger
      Chains.append(TrigName)
      Thresholds.append(trigThreshold+1)
  # Loop over (trigger) threshold of matched chains
  for th in Thresholds:
    if lepPt > th: # trigger matched and above trigger threshold
      return True, Chains
  return False, Chains

def getMuonTrigEffSF(Branches,i,campaign):
  SF = [1]
  if campaign == "a":
    if   Branches["muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"][i][0] != -1: SF = Branches["muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"][i] # return array of SFs (first is nominal)
    elif Branches["muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"][i][0]    != -1: SF = Branches["muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"][i] # return array of SFs (first is nominal)
  elif campaign == "d" or campaign == "e":
    SF = Branches["muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"][i] # return array of SFs (first is nominal)
  return SF

def getMuonTrigEff(Branches,i,campaign):
  Eff = [1]
  if campaign == "a":
    if   Branches["muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"][i][0] != -1: Eff = Branches["muon_TrigMCEff_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"][i] # return array of Efficiencies (first is nominal)
    elif Branches["muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"][i][0]    != -1: Eff = Branches["muon_TrigMCEff_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"][i] # return array of Efficiencies (first is nominal)
  elif campaign == "d" or campaign == "e":
    Eff = Branches["muon_TrigMCEff_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"][i] # return array of Efficiencies (first is nominal)
  return Eff

def getElectronTrigEffSF(Branches,i):
  return Branches["el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(ELisoWP)][i] # return array of SFs (first is nominal)

def getElectronTrigEff(Branches,i):
  return Branches["el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(ELisoWP)][i] # return array of MC Efficiencies (first is nominal)


#####################
# TRUTH JETS
#####################
def getNJetTruth(Branches):
  return len(Branches["truthJet_pt"])

def getJetPtTruth(Branches,i):
  return Branches["truthJet_pt"][i]

def getJetPhiTruth(Branches,i):
  return Branches["truthJet_phi"][i]

def getJetEtaTruth(Branches,i):
  return Branches["truthJet_eta"][i]

def getJetETruth(Branches,i):
  return Branches["truthJet_E"][i]

def getJetPartonTruthLabelID(Branches,i):
  return Branches["truthJet_PartonTruthLabelID"][i]

def getTruthJetHadronTruthLabelID(Branches,i):
  return Branches["truthJet_HadronConeExclTruthLabelID"][i]

#####################
# RECO JETS
#####################
def getNJet(Branches):
  return len(Branches["jet_pt"])

def getRecoJetPt(Branches,i):
  return Branches["jet_pt"][i]

def getRecoJetPhi(Branches,i):
  return Branches["jet_phi"][i]

def getRecoJetEta(Branches,i):
  return Branches["jet_eta"][i]

def getRecoJetE(Branches,i):
  return Branches["jet_E"][i]

def eventIsClean(Branches):
  return Branches["DFCommonJets_eventClean_LooseBad"][0]

def jetPassJVT(Branches,i,wp):
  return Branches['jet_JvtPass_'+wp][i]

def getJVTSF(Branches,i,wp):
  return Branches['jet_JvtEff_SF_'+wp][i] # return array of SFs (first is nominal)

def getJetBTag(Branches, i, tagger, wp = '85'):
  branch   = 'jet_is_{0}_FixedCutBEff_{1}'.format(tagger, wp)
  return Branches[branch][i]  # return array of SFs (first is nominal)

def getJetBTagSF(Branches, i, tagger):
  branch = 'jet_BTagSF_{0}'.format(tagger)  # PCBT SF
  return Branches[branch][i]  # return array of SFs (first is nominal)

def getBTagQuantile(Branches, i, tagger):
  branch = 'jet_BTagQuantile_{0}'.format(tagger)
  return Branches[branch][i]

def getJetHadronTruthLabelID(Branches,i):
  return Branches["jet_HadronConeExclTruthLabelID"][i]

#####################
# MET
#####################
def getMET(Branches):
  return Branches["metFinalTrk"][0]

#####################
# Other
#####################
def getPtBinMin(array,i):
  ptrange = array[i].split(":")
  return int(ptrange[0])

def getPtBinMax(array,i):
  ptrange = array[i].split(":")
  return int(ptrange[1])

def getDSID(path):
  if path.find("3611") != -1:
    return path[path.find("3611"):path.find("3611")+6]
  elif path.find("7001") != -1:
    return path[path.find("7001"):path.find("7001")+6]
  elif path.find("7003") != -1:
    return path[path.find("7003"):path.find("7003")+6]
  elif path.find("3641") != -1:
    return path[path.find("3641"):path.find("3641")+6]
  elif path.find("4104") != -1:
    return path[path.find("4104"):path.find("4104")+6]
  elif path.find("3616") != -1:
    return path[path.find("3616"):path.find("3616")+6]
  elif path.find("4106") != -1:
    return path[path.find("4106"):path.find("4106")+6]
  elif path.find("3457") != -1:
    return path[path.find("3457"):path.find("3457")+6]
  elif path.find("3450") != -1:
    return path[path.find("3450"):path.find("3450")+6]
  elif path.find("3642") != -1:
    return path[path.find("3642"):path.find("3642")+6]
  elif path.find("3636") != -1:
    return path[path.find("3636"):path.find("3636")+6]
  elif path.find("3631") != -1:
    return path[path.find("3631"):path.find("3631")+6]
  elif path.find("3633") != -1:
    return path[path.find("3633"):path.find("3633")+6]
  elif path.find("3634") != -1:
    return path[path.find("3634"):path.find("3634")+6]
  elif path.find("3451") != -1:
    return path[path.find("3451"):path.find("3451")+6]
  elif path.find("5061") != -1:
    return path[path.find("5061"):path.find("5061")+6]

def getPeriod(path):
  return path[path.find("period"):path.find("period")+7]

def getQuantileBin(bjets): # used only in Ti2 case
  q1 = bjets[0].btagQuantile
  q2 = bjets[1].btagQuantile
  if    q1==2 and q2 ==2:                      return 6
  elif (q1==2 and q2==3) or (q1==3 and q2==2): return 7
  elif (q1==2 and q2==4) or (q1==4 and q2==2): return 8
  elif (q1==2 and q2==5) or (q1==5 and q2==2): return 9
  elif  q1==3 and q2==3:                       return 10
  elif (q1==3 and q2==4) or (q1==4 and q2==3): return 11
  elif (q1==3 and q2==5) or (q1==5 and q2==3): return 12
  elif  q1==4 and q2==4:                       return 13
  elif (q1==4 and q2==5) or (q1==5 and q2==4): return 14
  elif  q1==5 and q2==5:                       return 15
  else: return -1

def myDeltaR(vec1, vec2):
  drap = vec1.Rapidity()-vec2.Rapidity()
  dphi = ROOT.TVector2.Phi_mpi_pi(vec1.Phi()-vec2.Phi())
  return ROOT.TMath.Sqrt(drap*drap + dphi*dphi)

################################
# Hadron flavour classification
################################
def getEventHadronFlavour(jets):
  nBHadrons = 0
  nCHadrons = 0
  # loop over jets
  for jet in jets:
    if jet.hadronTruthLabelID == 5: nBHadrons += 1
    elif jet.hadronTruthLabelID == 4: nCHadrons += 1
  if nBHadrons==0 and nCHadrons==0: flavour = "FlavL_" # light
  else:
    # Mixed cases
    if nBHadrons==1 and nCHadrons==1: # exactly one B hadron and one C hadron
      flavour = "FlavMixedE1B1C_"
      return flavour
    elif nBHadrons==1 and nCHadrons==2: # exactly one B hadron and two C hadrons
      flavour = "FlavMixedE1B2C_"
      return flavour
    elif nBHadrons==2 and nCHadrons==1: # exactly two B hadrons and one C hadron
      flavour = "FlavMixedE2B1C_"
      return flavour
    elif nBHadrons==2 and nCHadrons==2: # exactly two B hadrons and two C hadrons
      flavour = "FlavMixedE2B2C_"
      return flavour
    elif nBHadrons==1 and nCHadrons>2: # exactly one B hadron and more than two C hadrons
      flavour = "FlavMixedE1BM2C_"
      return flavour
    elif nBHadrons==2 and nCHadrons>2: # exactly two B hadrons and more than two C hadrons
      flavour = "FlavMixedE2BM2C_"
      return flavour
    elif nBHadrons>2 and nCHadrons==1: # More than two B hadrons and exactly one C hadron
      flavour = "FlavMixedM2BE1C_"
      return flavour
    elif nBHadrons>2 and nCHadrons==2: # More than two B hadrons and exactly two C hadrons
      flavour = "FlavMixedM2BE2C_"
      return flavour
    # Pure B/C cases
    elif nBHadrons == 1: flavour = "FlavE1B_" # exactly one B hadron
    elif nBHadrons > 1:  flavour = "FlavM1B_" # more than one B hadron
    elif nCHadrons == 1: flavour = "FlavE1C_" # exactly one C hadron
    elif nCHadrons > 1:  flavour = "FlavM1C_" # more than one C hadron
  return flavour

############
# safeFill
############
from Arrays      import *
from Dicts       import *
from Systematics import *
import copy
def safeFill(Var,Key,Type,flavour,tagCase,Hists,Values,Weights,Debug):
  # Loop over event weight systs, b-tagging SF, muon SFs and JVT SF systematics (MC nominal only) + nominal
  SFvariations = copy.deepcopy(SFsysts)
  SFvariations.pop('nominal')
  for SystVar in Weights:
    # Protection
    if Type == 'truth' and SystVar != 'nominal': continue  # no weight variations for truth
    if SystVar != 'nominal' and flavour != '': continue  # skip systematic variations for non-inclusive flavours (I personally don't need this)
    if SystVar != 'nominal' and tagCase == '': continue  # skip systematic variations for inclusive events
    obs_weight_variations = copy.deepcopy(Observables)
    obs_weight_variations['Zmass'] = ['Te1', 'Ti2']
    if 'EvtWgtVar' in SystVar and Var not in obs_weight_variations: continue  # skip event weight variations for non observables
    SFvariationFound = sum([name in SystVar for name in SFvariations]) > 0  # check if this weight corresponds to a SF variation
    if SFvariationFound:
      if Var not in Observables: continue  # skip SF variations for non observables
      else:
        if tagCase not in Observables[Var]: continue  # skip SF variations for a non-supported var+tag combination
    # Protection (this shouldn't happen though)
    if tagCase == '' and 'BTag' in SystVar: continue  # no BTag SF systematics in inclusive Z+jets events
    # Extra key for SF variation
    extraSyst = '' if SystVar == 'nominal' else '_'+SystVar
    # Non-HF variable
    if 'F' not in Var:
      # Check if variable exists
      if Var+'_'+Type not in Values: return None
      # Fill reco variables and truth observables
      if Type == 'reco' or (Type == 'truth' and (Var in Observables or Var in ExtraTruthVariables)):
        # Fill only if requested
        if Type == 'truth' or tagCase in AllVariables[Var]:
        #if tagCase in AllVariables[Var] or Type == 'truth':
          # Loop over values (variable could be event-level or object-level)
          for ivalue in range(0,len(Values[Var+'_'+Type])):
            value = Values[Var+'_'+Type][ivalue]
            Hists[Var+Key+extraSyst].Fill(value, Weights[SystVar])
            # Fill jet_y distributions for different pt bins
            #if Var == 'jet_pt':
            #  for ptbin in range(0,len(pTbins)): # loop over pt bins
            #    ptmin = getPtBinMin(pTbins,ptbin)
            #    ptmax = getPtBinMax(pTbins,ptbin)
            #    if value >= ptmin and value < ptmax:
            #      ptbinstr = '__pt_'+str(ptmin)+'_'+str(ptmax)
            #      Hists['jet_y'+ptbinstr+Key].Fill(Values['jet_y_'+Type][ivalue],Weight)
    else: # HF variable
      # Note:
      #   Reco:  use b-tagged jets to fill all HF-based distributions (example: FlavA1B_HFjet0_pt and FlavA1C_HFjet0_pt is b-tagged jet pt)
      #   Truth: use only truth b-jets to fill b-jet distributions, and only truth c-jets to fill c-jet distributions (filling depends on the event flavour)
      # Fill reco
      if Type == "reco" and 'T' in tagCase:
          if   'jjHF' in Var:
            var = Var.replace('jjHF','bb')
            if 'jj'  in var: var = var.replace('jj','bb')
            if 'HFj' in var: var = var.replace('HFj','bj')
          elif 'jHF' in Var: var = Var.replace('jHF','b')
          elif 'HFj' in Var: var = Var.replace('HFj','bj')
          if var+'_'+Type in Values:
            for value in Values[var+'_'+Type]: Hists[Var+Key+extraSyst].Fill(value, Weights[SystVar])
      # Fill truth
      elif Type == "truth":
        if "B" in flavour: # Inclusive, light and B
          if   'jjHF' in Var:
            var = Var.replace('jjHF','bb')
            if 'jj'  in var: var = var.replace('jj','b')
            if 'HFj' in Var: var = var.replace('HFj','bj')
          elif 'jHF' in Var: var = Var.replace('jHF','b')
          elif 'HFj' in Var: var = Var.replace('HFj','bj')
          if var+'_'+Type in Values:
            for value in Values[var+'_'+Type]: Hists[Var+Key+extraSyst].Fill(value,Weights[SystVar])
        elif "C" in flavour:
          if   'jjHF' in Var:
            var = Var.replace('jjHF','cc')
            if 'jj'  in var: var = var.replace('jj','cc')
            if 'HFj' in Var: var = var.replace('HFj','cj')
          elif 'jHF' in Var: var = Var.replace('jHF','c')
          elif 'HFj' in Var: var = Var.replace('HFj','cj')
          if var+'_'+Type in Values:
            for value in Values[var+'_'+Type]: Hists[Var+Key+extraSyst].Fill(value,Weights[SystVar])
        else: # Inclusive (light will have no b-/c-jet)
          # if there is a b- and c-jet, fill only the b-jet
          if   'jjHF' in Var:
            var = Var.replace('jjHF','bb')
            if 'jj'  in var: var = var.replace('jj','b')
            if 'HFj' in Var: var = var.replace('HFj','bj')
          elif 'jHF' in Var: var = Var.replace('jHF','b')
          elif 'HFj' in Var: var = Var.replace('HFj','bj')
          if var+'_'+Type in Values:
            for value in Values[var+'_'+Type]: Hists[Var+Key+extraSyst].Fill(value,Weights[SystVar])
          else: # try with c-jet
            if   'jjHF' in Var:
              var = Var.replace('jjHF','cc')
              if 'jj'  in var: var = var.replace('jj','cc')
              if 'HFj' in Var: var = var.replace('HFj','cj')
            elif 'jHF' in Var: var = Var.replace('jHF','c')
            elif 'HFj' in Var: var = Var.replace('HFj','cj')
            if var+'_'+Type in Values:
              for value in Values[var+'_'+Type]: Hists[Var+Key+extraSyst].Fill(value,Weights[SystVar])


# safeFill for TH2 b-tagging quantile vs observable
def safeFillTH2(Hists,BaseKey,Xvalue,Yvalue,Weights,isTM=False):
  # Loop over b-tagging SF, muon SFs and JVT SF systematics (MC nominal only) + nominal
  for SFvar in Weights:
    #if 'EvtWgtVar' in SFvar: continue # skip event weight variations
    if SFvar != 'nominal' and 'Flav' in BaseKey: continue  # skip systematic variations for non-inclusive flavours (I personally don't need this)
    if isTM and SFvar != 'nominal': continue  # skip systematic variations for TMs (I personally don't need this)
    # Extra key for SF variation
    extraSFsyst = '' if SFvar == 'nominal' else '_'+SFvar
    Hists[BaseKey+extraSFsyst].Fill(Xvalue,Yvalue,Weights[SFvar])

# Pring memory usage
def printMemory(i):
  import resource
  print("PERF: Memory usage ({0}) = {1} (MB)".format(i,resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024))


# Get list of triggers
def get_trigger_list(Branches, settings):
  # Set list of triggers to be checked in this event
  TriggerList = []
  if "MU" in settings['Channel']:
    if settings['sampleType'] == "2015":
      TriggerList.append("HLT_mu20_iloose_L1MU15")
      TriggerList.append("HLT_mu50")
    elif settings['sampleType'] == "2016" or settings['sampleType'] == "2017" or settings['sampleType'] == "2018" or settings['sampleType'] == "MC16d" or settings['sampleType'] == "MC16e":
      TriggerList.append("HLT_mu26_ivarmedium")
      TriggerList.append("HLT_mu50")
    elif settings['sampleType'] == "MC16a":
      if int(Branches["RandomRunNumber"][0]) < 297730: # 2015
        TriggerList.append("HLT_mu20_iloose_L1MU15")
        TriggerList.append("HLT_mu50")
      else: # 2016
        TriggerList.append("HLT_mu26_ivarmedium")
        TriggerList.append("HLT_mu50")
  if "EL" in settings['Channel']:
    if settings['sampleType'] == "2015":
      TriggerList.append("HLT_e24_lhmedium_L1EM20VH")
      TriggerList.append("HLT_e60_lhmedium")
      TriggerList.append("HLT_e120_lhloose")
    elif settings['sampleType'] == "2016" or settings['sampleType'] == "2017" or settings['sampleType'] == "2018" or settings['sampleType'] == "MC16d" or settings['sampleType'] == "MC16e":
      TriggerList.append("HLT_e26_lhtight_nod0_ivarloose")
      TriggerList.append("HLT_e60_lhmedium_nod0")
      TriggerList.append("HLT_e140_lhloose_nod0")
      TriggerList.append("HLT_e300_etcut")
    elif settings['sampleType'] == "MC16a":
      if int(Branches["RandomRunNumber"][0]) < 297730: # 2015
        TriggerList.append("HLT_e24_lhmedium_L1EM20VH")
        TriggerList.append("HLT_e60_lhmedium")
        TriggerList.append("HLT_e120_lhloose")
      else: # 2016
        TriggerList.append("HLT_e26_lhtight_nod0_ivarloose")
        TriggerList.append("HLT_e60_lhmedium_nod0")
        TriggerList.append("HLT_e140_lhloose_nod0")
        TriggerList.append("HLT_e300_etcut")
  return TriggerList

# For CxAOD vs xAH studies
def cxaod_check(my_dict, run_number, event_number):
  return True if run_number in my_dict and event_number in my_dict[run_number] else False

# SetBranchStatus
def setBranchStatus(tree, settings, nominal):
  tree.SetBranchStatus("*",0)  # disable all branches
  if settings['MC']:
    tree.SetBranchStatus("mcEventNumber", 1)
  else:  # data
    tree.SetBranchStatus("runNumber", 1)  # only needed for CxAOD vs xAH studies
    tree.SetBranchStatus("eventNumber", 1)
  if not settings['readAMI'] and settings['MC']:
    tree.SetBranchStatus("weight", 1)
  tree.SetBranchStatus("correctedAndScaledAverageMu", 1)
  tree.SetBranchStatus("averageInteractionsPerCrossing", 1)
  tree.SetBranchStatus("actualInteractionsPerCrossing", 1)
  tree.SetBranchStatus("correctedAndScaledActualMu", 1)
  tree.SetBranchStatus("correctedAverageMu", 1)
  tree.SetBranchStatus("correctedActualMu", 1)
  tree.SetBranchStatus("passedTriggers", 1)
  if settings['MC']:
    if not nominal:  # syst TTree
      #tree.SetBranchStatus("PassRecoSelections",1)
      if "EL" in settings['Channel']:
        tree.SetBranchStatus("el_charge", 1)
      if "MU" in settings['Channel']:
        tree.SetBranchStatus("muon_charge", 1)
    elif settings['isMCsignal']:  # nominal TTree for MC signal sample
      tree.SetBranchStatus("DFCommonJets_eventClean_LooseBad", 1)
      tree.SetBranchStatus("PassRecoSelections", 1)
    if nominal:
      tree.SetBranchStatus("mcEventWeights", 1)
      tree.SetBranchStatus("weight_pileup_up", 1)
      tree.SetBranchStatus("weight_pileup_down", 1)
    else:
      tree.SetBranchStatus("mcEventWeight", 1)
    tree.SetBranchStatus("mcChannelNumber", 1)
    tree.SetBranchStatus("weight_pileup", 1)
    tree.SetBranchStatus("rand_run_nr", 1)
  if "EL" in settings['Channel']:
    tree.SetBranchStatus("el_pt", 1)
    tree.SetBranchStatus("el_eta", 1)
    tree.SetBranchStatus("el_caloCluster_eta", 1)
    tree.SetBranchStatus("el_phi", 1)
    tree.SetBranchStatus("el_m", 1)
    tree.SetBranchStatus("el_isTrigMatched", 1)
    tree.SetBranchStatus("el_isTrigMatchedToChain", 1)
    tree.SetBranchStatus("el_listTrigChains", 1)
    if settings['MC']:
      tree.SetBranchStatus("el_RecoEff_SF", 1)
      tree.SetBranchStatus("el_IsoEff_SF_Tight_isol{}".format(settings['ELisoWP']), 1)
      tree.SetBranchStatus("el_PIDEff_SF_Tight", 1)
      tree.SetBranchStatus("el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP']), 1)
      tree.SetBranchStatus("el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP']), 1)
  if "MU" in settings['Channel']:
    tree.SetBranchStatus("muon_pt", 1)
    tree.SetBranchStatus("muon_eta", 1)
    tree.SetBranchStatus("muon_phi", 1)
    tree.SetBranchStatus("muon_m", 1)
    tree.SetBranchStatus("muon_isTrigMatched", 1)
    tree.SetBranchStatus("muon_isTrigMatchedToChain", 1)
    tree.SetBranchStatus("muon_listTrigChains", 1)
    if settings['MC']:
      tree.SetBranchStatus("muon_RecoEff_SF_RecoMedium", 1)
      tree.SetBranchStatus("muon_IsoEff_SF_Iso{}".format(settings['MUisoWP']), 1)
      tree.SetBranchStatus("muon_TTVAEff_SF", 1)
      if settings['campaign'] == 'a':
        tree.SetBranchStatus("muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium", 1)
        tree.SetBranchStatus("muon_TrigMCEff_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium", 1)
      tree.SetBranchStatus("muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium", 1)
      tree.SetBranchStatus("muon_TrigMCEff_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium", 1)
  if settings['MC']:
    if settings['fillTruth']:
      tree.SetBranchStatus("PassTruthSelections", 1)
      if "EL" in settings['Channel']:
        tree.SetBranchStatus("truthElectron_pt_dressed", 1)
        tree.SetBranchStatus("truthElectron_eta_dressed", 1)
        tree.SetBranchStatus("truthElectron_phi_dressed", 1)
        tree.SetBranchStatus("truthElectron_e_dressed", 1)
      if "MU" in settings['Channel']:
        tree.SetBranchStatus("truthMuon_pt_dressed", 1)
        tree.SetBranchStatus("truthMuon_eta_dressed", 1)
        tree.SetBranchStatus("truthMuon_phi_dressed", 1)
        tree.SetBranchStatus("truthMuon_e_dressed", 1)
      tree.SetBranchStatus("truthJet_pt", 1)
      tree.SetBranchStatus("truthJet_phi", 1)
      tree.SetBranchStatus("truthJet_eta", 1)
      tree.SetBranchStatus("truthJet_E", 1)
      tree.SetBranchStatus("truthJet_PartonTruthLabelID", 1)
      tree.SetBranchStatus("truthJet_HadronConeExclTruthLabelID", 1)
    tree.SetBranchStatus("jet_JvtEff_SF_" + settings['SelectionClass'].jet_JVTwp, 1)
    tree.SetBranchStatus("jet_SF_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP'], 1)
    tree.SetBranchStatus("jet_HadronConeExclTruthLabelID", 1)
    tree.SetBranchStatus("jet_BTagSF_" + settings['Tagger'], 1)  # PCBT SF
  tree.SetBranchStatus("jet_pt", 1)
  tree.SetBranchStatus("jet_eta", 1)
  tree.SetBranchStatus("jet_phi", 1)
  tree.SetBranchStatus("jet_E", 1)
  tree.SetBranchStatus("jet_JvtPass_" + settings['SelectionClass'].jet_JVTwp, 1)
  tree.SetBranchStatus("jet_is_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP'], 1)
  tree.SetBranchStatus("jet_BTagQuantile_" + settings['Tagger'], 1)
  tree.SetBranchStatus("metFinalTrk", 1)

# SetBranchAddress
def setBranchAddress(tree, Branches, settings, nominal):
  Branches["runNumber"] = array.array('i', [0])
  Branches["eventNumber"] = array.array('l', [0])
  if settings['MC']:
    tree.SetBranchAddress("mcEventNumber", Branches["eventNumber"])
  else:  # data
    tree.SetBranchAddress("runNumber", Branches["runNumber"])  # only needed for CxAOD vs xAH studies
    tree.SetBranchAddress("eventNumber", Branches["eventNumber"])
  if not settings['readAMI'] and settings['MC']:
    Branches["weight"] = array.array('f', [0])
    tree.SetBranchAddress("weight", Branches["weight"])
  Branches["correctedAndScaledAverageMu"] = array.array('f',[0])
  tree.SetBranchAddress("correctedAndScaledAverageMu", Branches["correctedAndScaledAverageMu"])
  Branches["averageInteractionsPerCrossing"] = array.array('f',[0])
  tree.SetBranchAddress("averageInteractionsPerCrossing", Branches["averageInteractionsPerCrossing"])
  Branches["actualInteractionsPerCrossing"] = array.array('f',[0])
  tree.SetBranchAddress("actualInteractionsPerCrossing", Branches["actualInteractionsPerCrossing"])
  Branches["correctedAndScaledActualMu"] = array.array('f',[0])
  tree.SetBranchAddress("correctedAndScaledActualMu", Branches["correctedAndScaledActualMu"])
  Branches["correctedAverageMu"] = array.array('f',[0])
  tree.SetBranchAddress("correctedAverageMu", Branches["correctedAverageMu"])
  Branches["correctedActualMu"] = array.array('f',[0])
  tree.SetBranchAddress("correctedActualMu", Branches["correctedActualMu"])
  Branches["passedTriggers"] = ROOT.std.vector('string')()
  tree.SetBranchAddress("passedTriggers", Branches["passedTriggers"])
  if settings['MC']:
    Branches["weight_pileup"] = array.array('f', [0])
    tree.SetBranchAddress("weight_pileup", Branches["weight_pileup"])
    Branches["RandomRunNumber"] = array.array('i', [0])
    tree.SetBranchAddress("rand_run_nr", Branches["RandomRunNumber"])
    if not nominal: # syst TTree
      #Branches["PassRecoSelections"] = ROOT.std.vector('int')()
      #tree.SetBranchAddress("PassRecoSelections",Branches["PassRecoSelections"])
      if "EL" in settings['Channel']:
        Branches["el_charge"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("el_charge",Branches["el_charge"])
      if "MU" in settings['Channel']:
        Branches["muon_charge"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("muon_charge",Branches["muon_charge"])
    elif settings['isMCsignal']:  # nominal TTree for MC signal sample
      Branches["DFCommonJets_eventClean_LooseBad"] = ROOT.std.vector('int')()
      tree.SetBranchAddress("DFCommonJets_eventClean_LooseBad",Branches["DFCommonJets_eventClean_LooseBad"])
      Branches["PassRecoSelections"] = ROOT.std.vector('int')()
      tree.SetBranchAddress("PassRecoSelections",Branches["PassRecoSelections"])
    if nominal:
      Branches["mcEventWeights"] = ROOT.std.vector('float')()
      tree.SetBranchAddress("mcEventWeights", Branches["mcEventWeights"])
      Branches["weight_pileup_up"] = array.array('f', [0])
      tree.SetBranchAddress("weight_pileup_up", Branches["weight_pileup_up"])
      Branches["weight_pileup_down"] = array.array('f', [0])
      tree.SetBranchAddress("weight_pileup_down", Branches["weight_pileup_down"])
    else:
      Branches["mcEventWeight"] = array.array('f', [0])
      tree.SetBranchAddress("mcEventWeight", Branches["mcEventWeight"])
    Branches["mcChannelNumber"] = array.array('i', [0])
    tree.SetBranchAddress("mcChannelNumber", Branches["mcChannelNumber"])
  if "EL" in settings['Channel']:
    Branches["el_pt"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("el_pt",Branches["el_pt"])
    Branches["el_eta"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("el_eta",Branches["el_eta"])
    Branches["el_caloCluster_eta"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("el_caloCluster_eta",Branches["el_caloCluster_eta"])
    Branches["el_phi"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("el_phi",Branches["el_phi"])
    Branches["el_m"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("el_m",Branches["el_m"])
    Branches["el_isTrigMatched"] = ROOT.std.vector('int')()
    tree.SetBranchAddress("el_isTrigMatched",Branches["el_isTrigMatched"])
    Branches["el_isTrigMatchedToChain"] = ROOT.std.vector(ROOT.std.vector('int'))()
    tree.SetBranchAddress("el_isTrigMatchedToChain",Branches["el_isTrigMatchedToChain"])
    Branches["el_listTrigChains"] = ROOT.std.vector(ROOT.std.vector('string'))()
    tree.SetBranchAddress("el_listTrigChains",Branches["el_listTrigChains"])
    if settings['MC']:
      Branches["el_RecoEff_SF"] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("el_RecoEff_SF",Branches["el_RecoEff_SF"])
      Branches["el_IsoEff_SF_Tight_isol{}".format(settings['ELisoWP'])] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("el_IsoEff_SF_Tight_isol{}".format(settings['ELisoWP']),Branches["el_IsoEff_SF_Tight_isol{}".format(settings['ELisoWP'])])
      Branches["el_PIDEff_SF_Tight"] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("el_PIDEff_SF_Tight",Branches["el_PIDEff_SF_Tight"])
      Branches["el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP'])] = ROOT.std.vector(ROOT.std.vector('float'))()
      Branches["el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP'])] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP']),Branches["el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP'])])
      tree.SetBranchAddress("el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP']),Branches["el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isol{}".format(settings['ELisoWP'])])
  if "MU" in settings['Channel']:
    Branches["muon_pt"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("muon_pt",Branches["muon_pt"])
    Branches["muon_eta"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("muon_eta",Branches["muon_eta"])
    Branches["muon_phi"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("muon_phi",Branches["muon_phi"])
    Branches["muon_m"] = ROOT.std.vector('float')()
    tree.SetBranchAddress("muon_m",Branches["muon_m"])
    Branches["muon_isTrigMatched"] = ROOT.std.vector('int')()
    tree.SetBranchAddress("muon_isTrigMatched",Branches["muon_isTrigMatched"])
    Branches["muon_isTrigMatchedToChain"] = ROOT.std.vector(ROOT.std.vector('int'))()
    tree.SetBranchAddress("muon_isTrigMatchedToChain",Branches["muon_isTrigMatchedToChain"])
    Branches["muon_listTrigChains"] = ROOT.std.vector(ROOT.std.vector('string'))()
    tree.SetBranchAddress("muon_listTrigChains",Branches["muon_listTrigChains"])
    if settings['MC']:
      Branches["muon_RecoEff_SF_RecoMedium"] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("muon_RecoEff_SF_RecoMedium",Branches["muon_RecoEff_SF_RecoMedium"])
      Branches["muon_IsoEff_SF_Iso{}".format(settings['MUisoWP'])] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("muon_IsoEff_SF_Iso{}".format(settings['MUisoWP']),Branches["muon_IsoEff_SF_Iso{}".format(settings['MUisoWP'])])
      Branches["muon_TTVAEff_SF"] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("muon_TTVAEff_SF",Branches["muon_TTVAEff_SF"])
      if settings['campaign'] == 'a':
        Branches["muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"] = ROOT.std.vector(ROOT.std.vector('float'))()
        tree.SetBranchAddress("muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium",Branches["muon_TrigEff_SF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"])
        Branches["muon_TrigMCEff_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"] = ROOT.std.vector(ROOT.std.vector('float'))()
        tree.SetBranchAddress("muon_TrigMCEff_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium",Branches["muon_TrigMCEff_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_RecoMedium"])
      Branches["muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium",Branches["muon_TrigEff_SF_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"])
      Branches["muon_TrigMCEff_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"] = ROOT.std.vector(ROOT.std.vector('float'))()
      tree.SetBranchAddress("muon_TrigMCEff_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium",Branches["muon_TrigMCEff_HLT_mu26_ivarmedium_OR_HLT_mu50_RecoMedium"])
  if settings['MC']:
    if settings['fillTruth']:
      Branches["PassTruthSelections"] = ROOT.std.vector('int')()
      tree.SetBranchAddress("PassTruthSelections",Branches["PassTruthSelections"])
      if "EL" in settings['Channel']:
        Branches["truthElectron_pt_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthElectron_pt_dressed", Branches["truthElectron_pt_dressed"])
        Branches["truthElectron_eta_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthElectron_eta_dressed", Branches["truthElectron_eta_dressed"])
        Branches["truthElectron_phi_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthElectron_phi_dressed", Branches["truthElectron_phi_dressed"])
        Branches["truthElectron_e_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthElectron_e_dressed", Branches["truthElectron_e_dressed"])
      if "MU" in settings['Channel']:
        Branches["truthMuon_pt_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthMuon_pt_dressed", Branches["truthMuon_pt_dressed"])
        Branches["truthMuon_eta_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthMuon_eta_dressed", Branches["truthMuon_eta_dressed"])
        Branches["truthMuon_phi_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthMuon_phi_dressed", Branches["truthMuon_phi_dressed"])
        Branches["truthMuon_e_dressed"] = ROOT.std.vector('float')()
        tree.SetBranchAddress("truthMuon_e_dressed", Branches["truthMuon_e_dressed"])
      Branches["truthJet_pt"] = ROOT.std.vector('float')()
      tree.SetBranchAddress("truthJet_pt", Branches["truthJet_pt"])
      Branches["truthJet_eta"] = ROOT.std.vector('float')()
      tree.SetBranchAddress("truthJet_eta", Branches["truthJet_eta"])
      Branches["truthJet_phi"] = ROOT.std.vector('float')()
      tree.SetBranchAddress("truthJet_phi", Branches["truthJet_phi"])
      Branches["truthJet_E"] = ROOT.std.vector('float')()
      tree.SetBranchAddress("truthJet_E", Branches["truthJet_E"])
      Branches["truthJet_PartonTruthLabelID"] = ROOT.std.vector('int')()
      tree.SetBranchAddress("truthJet_PartonTruthLabelID",Branches["truthJet_PartonTruthLabelID"])
      Branches["truthJet_HadronConeExclTruthLabelID"] = ROOT.std.vector('int')()
      tree.SetBranchAddress("truthJet_HadronConeExclTruthLabelID",Branches["truthJet_HadronConeExclTruthLabelID"])
    Branches["jet_SF_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP']] = ROOT.std.vector(ROOT.std.vector('float'))()
    tree.SetBranchAddress("jet_SF_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP'], Branches["jet_SF_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP']])
    Branches["jet_JvtEff_SF_" + settings['SelectionClass'].jet_JVTwp] = ROOT.std.vector(ROOT.std.vector('float'))()
    tree.SetBranchAddress("jet_JvtEff_SF_" + settings['SelectionClass'].jet_JVTwp, Branches["jet_JvtEff_SF_" + settings['SelectionClass'].jet_JVTwp])
    Branches["jet_HadronConeExclTruthLabelID"] = ROOT.std.vector('int')()
    tree.SetBranchAddress("jet_HadronConeExclTruthLabelID",Branches["jet_HadronConeExclTruthLabelID"])
    Branches["jet_BTagSF_" + settings['Tagger']] = ROOT.std.vector(ROOT.std.vector('float'))()
    tree.SetBranchAddress("jet_BTagSF_" + settings['Tagger'], Branches["jet_BTagSF_" + settings['Tagger']])
  Branches["jet_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jet_pt",Branches["jet_pt"])
  Branches["jet_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jet_eta",Branches["jet_eta"])
  Branches["jet_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jet_phi",Branches["jet_phi"])
  Branches["jet_E"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jet_E",Branches["jet_E"])
  Branches["jet_JvtPass_" + settings['SelectionClass'].jet_JVTwp] = ROOT.std.vector('int')()
  tree.SetBranchAddress("jet_JvtPass_" + settings['SelectionClass'].jet_JVTwp, Branches["jet_JvtPass_" + settings['SelectionClass'].jet_JVTwp])
  Branches["jet_is_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP']] = ROOT.std.vector('int')()
  tree.SetBranchAddress("jet_is_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP'], Branches["jet_is_" + settings['Tagger'] + "_FixedCutBEff_" + settings['btagWP']])
  Branches["jet_BTagQuantile_" + settings['Tagger']] = ROOT.std.vector('int')()
  tree.SetBranchAddress("jet_BTagQuantile_" + settings['Tagger'], Branches["jet_BTagQuantile_" + settings['Tagger']])
  Branches["metFinalTrk"] = array.array('f',[0])
  tree.SetBranchAddress("metFinalTrk",Branches["metFinalTrk"])


def get_branches(tree, branches, settings, nominal):
  setBranchStatus(tree, settings, nominal)
  setBranchAddress(tree, branches, settings, nominal)
