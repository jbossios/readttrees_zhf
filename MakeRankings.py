import sys
import os
import ROOT
import tqdm
from pprint import pprint
from CommonRunningDefs import Tagger as tagger

sys.path.insert(1, 'Plotter/')
from InputFiles import *
from Luminosities import *

DEBUG = False


def prepare(value):
  rounded = round(value * 100, 2)
  if rounded < 0:
      rounded = 0.0
  return str(rounded)


def convert(sample, channel):
    if sample == 'Ztautau':
        return '\Ztautau'
    if channel == 'EL' and ('Signal' in sample or 'Zee' in sample):
        return '\Z $\\to$ $e^{-}e^{+}$'
    if channel == 'MU' and ('Signal' in sample or 'Zmumu' in sample):
        return '\Z $\\to$ $\mu^{-}\mu^{+}$'
    if channel == 'ELMU' and 'Zee' in sample:
        return '\Z $\\to$ $e^{-}e^{+}$'
    if channel == 'ELMU' and 'Zmumu' in sample:
        return '\Z $\\to$ $\mu^{-}\mu^{+}$'
    if sample == 'SingleTop':
        return 'Single top ($s$/$t$/$Wt$-channel)'
    if sample == 'Diboson':
        return 'VV $\\to \ell\ell/\ell\\nu/\\nu\\nu +q\\bar{q}$'
    if 'TTbar' in sample:
        return '\\ttbar'
    if 'VH' in sample:
        return 'VH'
    if 'Top' in sample:
        return 'Top'
    print(f'{sample} in {channel} not recognized, exiting')
    sys.exit(1)


# Print message and exit
def FATAL(msg):
    print('FATAL: '+msg)
    sys.exit(0)


# Check return
def CHECK(msg):
    if msg != 'OK': FATAL(msg)


# Check object (file,histogram,etc)
def CHECKObj(obj,msg):
    if not obj: FATAL(msg)


# Open input ROOT files on demand
def get_input_file(input_files_dict, key):
    """ Get already opened input file or open it if not opened already """
    # protection
    if key not in InputFiles:
        msg = f"key = {key} not found in InputFiles dictionary, exiting"
        FATAL(msg)
    if key not in input_files_dict:  # open file
        input_file_name = InputFiles[key]
        input_file = ROOT.TFile.Open(input_file_name)
        CHECKObj(input_file, f'{input_file_name} not found, exiting')
        input_files_dict[key] = input_file
    return input_files_dict, input_files_dict[key]


def get_hist(input_files_dict, sample, histname, debug = False):
    # Get File
    if debug: print("DEBUG: get_hist(): get file")
    input_files_dict, File = get_input_file(input_files_dict, sample)

    # Get Histogram
    if debug: print("DEBUG: get_hist(): get histogram")
    hist_orig = File.Get(histname)
    hist_orig.SetDirectory(0)
    if not hist_orig:
        msg = histname + " not found in " + InputFiles[sample] + ", exiting"
        return input_files_dict, None, msg
    hist = hist_orig.Clone(histname + "_cloned")
    hist.SetDirectory(0)  # detach from file

    return input_files_dict, hist, 'OK'


def get_total_hist(input_files_dict, samples, histname, debug, luminosity = dict()):
    cpgs = ['MC16a', 'MC16d', 'MC16e']

    # Get first histogram
    if debug: print("DEBUG: get_total_hist(): get first histogram")
    input_files_dict, total_hist, msg = get_hist(input_files_dict, samples[0], histname, debug)
    if msg != 'OK':
        return input_files_dict, None, msg
    for campaign in cpgs:
        if campaign in samples[0] and 'cutflow' not in histname and 'mcEvtWeight' not in histname and 'sampleWeight' not in histname and 'SF' not in histname:
          total_hist.Scale(luminosity[campaign])

    # Now sum all the rest
    if debug: print("DEBUG: get_total_hist(): sum all the rest")
    for isample in range(1, len(samples)):
        input_files_dict, hist, msg = get_hist(input_files_dict, samples[isample], histname, debug)
        if msg != 'OK':
            return input_files_dict, None, msg
        for campaign in cpgs:
            if campaign in samples[isample] and 'cutflow' not in histname and 'mcEvtWeight' not in histname and 'sampleWeight' not in histname and 'SF' not in histname:
              hist.Scale(luminosity[campaign])
        total_hist.Add(hist)
        hist.Delete()

    return input_files_dict, total_hist, 'OK'


# Get list of samples
def getSampleList(channel: str, selection: str, campaign: str, sample: str, tagger) -> list:
    """Get list of TTbar samples based on chosen channel, selection and campaigns"""
    if campaign == 'all':
        return ['{}_MC16{}_{}_{}_{}'.format(sample, cpg, channel, selection, tagger) for cpg in ['a', 'd', 'e']]
    else:
        return ['{}_MC16{}_{}_{}_{}'.format(sample, campaign, channel, selection, tagger)]

# Get list of data-taking years for a given campaign
def getDataPeriods(campaign: str) -> list:
    """Get list of data years based on the chosen campaign"""
    return {
             'all': ['15','16','17','18'],
             'a': ['15','16'],
             'd': ['17'],
             'e': ['18'],
           }[campaign]


# Get data for a given region
def getDataDict(input_files_dict, region, channel, dataPeriods, histname, tagger, campaign, debug):
    """Get data for a given region"""
    # Loop over data-taking years
    DataHists = []
    for period in dataPeriods:
        if debug: print('DEBUG: Get histogram('+histname+') for data'+period)
        # Open input file
        Key = "Signal_data" + period + "_" + channel + "_" + region + "_" + tagger
        input_files_dict, File = get_input_file(input_files_dict, Key)
        # Get histogram
        Hist = File.Get(histname)
        Hist.SetDirectory(0)
        CHECKObj(Hist, histname+" not found in {}, exiting".format(InputFiles[Key]))
        DataHists.append(Hist)

    # Get total data histogram
    if debug: print("DEBUG: Get total data histogram")
    hData = DataHists[0].Clone("Data_"+histname)
    hData.SetDirectory(0)
    for ihist in range(1, len(DataHists)):
        hData.Add(DataHists[ihist])

    return input_files_dict, hData



def main(version, region, channel, campaign, signal_gen):
   if DEBUG:
       print(f'DEBUG: region = {region}')
       print(f'DEBUG: channel = {channel}')
       print(f'DEBUG: campaign = {campaign}')
   samples = [
       'Ztautau', 'Diboson',
       'TTbarNonAllHad', 'VH',
   ]
   if region == 'VR':
       samples += ['SingleTop', f'Signal{signal_gen}']
   elif region != 'SR':  # CR or AltCR
       samples += ['SingleTop',  f'Zee{signal_gen}', f'Zmumu{signal_gen}']
   else:  # SR
       samples = [f'Signal{signal_gen}', 'Top', 'Ztautau', 'Diboson', 'VH']
   observable = 'Zpt'
   cases = ['Ti1', 'Ti2']
   luminosity = {
       'MC16a': Luminosity_2015 + Luminosity_2016,
       'MC16d': Luminosity_2017,
       'MC16e': Luminosity_2018,
   }

   # Get list of samples
   sample_keys = {sample: [] for sample in samples}
   for sample in samples:
       sample_keys[sample] += getSampleList(channel, region, campaign, sample, tagger)
   if DEBUG:
       print(f'sample_keys = {sample_keys}')

   # Get number of events per final state and sample
   nevents_by_sample = {}
   input_files_dict = {}
   # Loop over Ti1/Ti2
   for case in cases:
       nevents_by_sample[case] = {}
       histname = f'{observable}_{case}'
       # Get histograms
       for sample_key, samples in sample_keys.items():
           input_files_dict, hist, msg = get_total_hist(input_files_dict, samples, histname, DEBUG, luminosity)
           nevents_by_sample[case][sample_key] = hist.Integral()
  
   # Get fraction of events per sample
   total_events = {case: sum([nevents for key, nevents in nevents_dict.items() if region != 'SR' or (region == 'SR' and key != 'TTbarNonAllHad')]) for case, nevents_dict in nevents_by_sample.items()}
   frac_by_sample = {case: {key: nevents/total_events[case] for key, nevents in nevents_dict.items()} for case, nevents_dict in nevents_by_sample.items()}

   # Get data
   data_periods = getDataPeriods(campaign)
   hists_data = {}
   for case in cases:
       histname = f'{observable}_{case}'
       input_files_dict, hists_data[case] = getDataDict(input_files_dict, region, channel, data_periods, histname, tagger, campaign, DEBUG)
   
   # Get fraction of MC events per sample w.r.t data
   fracData_by_sample = {case: {key: nevents/hists_data[case].Integral() for key, nevents in nevents_dict.items()} for case, nevents_dict in nevents_by_sample.items()}

   # Write results to an output file
   output_folder_name = f'SampleRanking/{version}/'
   if not os.path.exists(output_folder_name):
       os.makedirs(output_folder_name)
   with open(f"{output_folder_name}{region}_{channel}_{campaign}.log", "w") as out_file:
       out_file.write('Number of events by sample:\n')
       pprint(nevents_by_sample, out_file, sort_dicts = False)
       out_file.write('\nFraction of events by sample w.r.t. total MC prediction:\n')
       pprint(frac_by_sample, out_file, sort_dicts = False)
       out_file.write('\nFraction of events by sample w.r.t. data:\n')
       pprint(fracData_by_sample, out_file, sort_dicts = False)

   return frac_by_sample



if __name__ == '__main__':
    regions = {  # region: channels
        'CR': ['ELMU'],
        'AltCR': ['ELMU'],
        'SR': ['EL', 'MU'],
        'VR': ['EL', 'MU'],
    }
    signal_gen = 'SH2211'
    version = '10112022'
    campaigns = ['a', 'd', 'e'] #, 'all']
    results = {}
    for region, channels in tqdm.tqdm(regions.items()):
        results[region] = {}
        for channel in channels:
            results[region][channel] = {}
            for campaign in campaigns:
                results[region][channel][campaign] = main(version, region, channel, campaign, signal_gen)

    # Create LaTeX tables

    # Write results to an output file
    output_folder_name = f'SampleRanking/{version}/'
    if not os.path.exists(output_folder_name):
        os.makedirs(output_folder_name)
    with open(f"{output_folder_name}latex_tables.tex", "w") as out_file:
        for region, region_dict in results.items():
            rg = {
                'CR': 'control region',
                'AltCR': 'alternative control region',
                'VR': 'validation region',
                'SR': 'signal region',
            }[region]
            for channel, table_dict in region_dict.items():
                ch = ' in the electron channel' if channel == 'EL' else ' in the muon channel'
                if 'CR' in region:
                    ch = ''
                label = f'tab:sample_ranking_{region}_{channel}'
                out_file.write('\\begin{table}\n')
                out_file.write('    \centering\n')
                out_file.write('    \\renewcommand{\\arraystretch}{1.1}\n')
                out_file.write('    \\begin{tabular}{c|c|c|c|c|c|c}\n')
                out_file.write('        \hline\n')
                out_file.write('        \\textbf{Process } & \multicolumn{3}{c|}{$Z + \ge$ 1 \\bjet} & \multicolumn{3}{c}{$Z + \ge$ 2 \\bjets}\\\\\n')
                out_file.write('                           & MC16a & MC16d & MC16e & MC16a & MC16d & MC16e\\\\\n')
                out_file.write('        \hline\n')
                for sample in table_dict['a']['Ti1']:
                    sample_text = convert(sample, channel)
                    mc16a_frac_Ti1 = prepare(table_dict['a']['Ti1'][sample])
                    mc16d_frac_Ti1 = prepare(table_dict['d']['Ti1'][sample])
                    mc16e_frac_Ti1 = prepare(table_dict['e']['Ti1'][sample])
                    mc16a_frac_Ti2 = prepare(table_dict['a']['Ti2'][sample])
                    mc16d_frac_Ti2 = prepare(table_dict['d']['Ti2'][sample])
                    mc16e_frac_Ti2 = prepare(table_dict['e']['Ti2'][sample])
                    out_file.write(f'{sample_text} & {mc16a_frac_Ti1} & {mc16d_frac_Ti1} & {mc16e_frac_Ti1} & {mc16a_frac_Ti2} & {mc16d_frac_Ti2} & {mc16e_frac_Ti2} \\\\ \n')
                out_file.write('\end{tabular}\n')
                if region != 'SR':
                    out_file.write('\caption{Contribution (in percentage) from each MC simulation sample to the total MC prediction in the \\ttbar{} '+rg+ch+'.}\n')
                else:
                    out_file.write('\caption{Contribution (in percentage) from each MC simulation sample to the total MC prediction in '+rg+ch+'.}\n')
                out_file.write('\label{'+label+'}\n')
                out_file.write('\end{table}\n')
                out_file.write('\n\n\n')

