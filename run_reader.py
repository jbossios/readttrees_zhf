
import os
import Reader
import argparse

cases = [
    'Data15_EL_SR',
    'Data15_EL_VR',
    'Data15_ELMU_CR',
    'Data15_MU_SR',
    'Data15_MU_VR',
    'Data15_ELMU_CR',
    'Data16_EL_SR',
    'Data16_EL_VR',
    'Data16_ELMU_CR',
    'Data16_MU_SR',
    'Data16_MU_VR',
    'Data16_ELMU_CR',
    'Data17_EL_SR',
    'Data17_EL_VR',
    'Data17_ELMU_CR',
    'Data17_MU_SR',
    'Data17_MU_VR',
    'Data17_ELMU_CR',
    'Data18_EL_SR',
    'Data18_EL_VR',
    'Data18_ELMU_CR',
    'Data18_MU_SR',
    'Data18_MU_VR',
    'Data18_ELMU_CR',
    'ZeeMC16aMG_EL_SR_nominal',
    'ZeeMC16aMG_EL_SR_FullSysts',
    'ZeeMC16aMG_EL_VR_nominal',
    'ZeeMC16aMG_ELMU_CR_nominal',
    'ZeeMC16aMG_ELMU_CR_FullSysts',
    'ZmumuMC16aSherpa2211_MU_SR_nominal',
    'ZmumuMC16aSherpa2211_MU_SR_FullSysts',
    'ZmumuMC16aSherpa2211_MU_VR_nominal',
    'ZmumuMC16aSherpa2211_ELMU_CR_nominal',
    'ZmumuMC16aSherpa2211_ELMU_CR_FullSysts',
    'MC16eTTbarNonAllHad_EL_SR_nominal',
    'MC16eTTbarNonAllHad_MU_SR_nominal',
    'MC16eTTbarNonAllHad_EL_VR_nominal',
    'MC16eTTbarNonAllHad_MU_VR_nominal',
    'MC16eTTbarNonAllHad_ELMU_CR_nominal',
]

samples = {
    'Data15': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_data2015_periodJ_14July2022/data-tree/',
    'Data16': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_data2016_periodA_14July2022/data-tree/',
    'Data17': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_data2017_periodB_14July2022/data-tree/',
    'Data18': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_data2018_periodB_14July2022/data-tree/',
    'ZeeMC16aMG_nominal': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_MC16a_Zee_MG_363149_nominalOnly_14July2022/data-tree/',
    'ZeeMC16aMG_FullSysts': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_MC16a_Zee_MG_363149_systematics_14July2022/data-tree/',
    'ZmumuMC16aSherpa2211_nominal': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_MC16a_Zmumu_SH2211_700323_nominalOnly_14July2022/data-tree/',
    'ZmumuMC16aSherpa2211_FullSysts': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_MC16a_Zmumu_SH2211_700323_systematics_14July2022/data-tree/',
    'MC16eTTbarNonAllHad_nominal': '/eos/user/j/jbossios/SM/WZgroup/ZHF/TestTTrees/v11/submitDir_v11_MC16e_ttbar_410470_nominalOnly_14July2022/data-tree/',
}

class Settings:
    def __init__(self, path, out_path, input_file, channel, dataset, selection, ttrees, fill_truth, use_sfs, use_sample_weights, use_prw, apply_sf_systs, local):
        self.path = path
        self.outPATH = out_path
        self.inputFile = input_file
        self.channel = channel
        self.dataset = dataset
        self.selection = selection
        self.ttrees = ttrees
        self.noTruth = True if not fill_truth else False
        self.noSFs = True if not use_sfs else False
        self.noSampleWeights = True if not use_sample_weights else False
        self.noPRW = True if not use_prw else False
        self.applySFsysts = True if apply_sf_systs else False
        self.localTest = True
        self.noxrootd = True
        self.test = True
        self.jetcoll = 'PFlow'
        self.tagger = 'DL1r'
        self.debug = False
        self.ref = False
        self.perf = False
        self.noReco = False
        self.readAMI = True
        self.printRecoInfo = False 
        self.printTruthInfo = False
        self.printMatchedInfo = False
        self.getWeightSysts = False
        self.cxaodval = False
        self.useBootstraps = False
        self.ci_test = not local


def test(local = False):
    print('Running test()...')

    if not os.path.exists('tests/'):
        os.makedirs('tests')
    
    commands = []
    for case in cases:
        print('Running test for {}'.format(case))
        # Prepare settings
        options = case.split('_')
        sample = options[0]
        channel = options[1]
        selection = options[2]
        sample_key = '{}_{}'.format(sample, options[3]) if 'Data' not in sample else sample
        input_path = samples[sample_key]
        nominal = True
        if 'Data' not in sample and options[3] == 'FullSysts':
            nominal = False
        fill_truth = True if nominal else False
        apply_sf_systs = True if nominal else False
        ttrees = 'nominal' if nominal else 'FullSysts'
        mc = True
        if 'Data' in sample:
            mc = False
        use_sfs = True
        use_sample_weights = True
        use_prw = True
        # Set input file
        folder = input_path.split('/data-tree')[0].split('/')[-1] + '/' if not local else input_path
        if mc:
            input_file = 'mc16_13TeV.root'
        else:  # data
            year = sample.replace('Data', '')
            input_file = 'data{}_13TeV.root'.format(year)
        # Run Reader
        args = Settings(folder, 'tests/', input_file, channel, sample, selection, ttrees, fill_truth, use_sfs, use_sample_weights, use_prw, apply_sf_systs, local)
        Reader.run_reader(args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--local', action='store_true', default=False)
    args = parser.parse_args()
    test(args.local)
