Cases = {
  #"EL_SR"      : {'Channel' : 'EL',   'Selections' : ["SR"]},
  #"MU_SR"      : {'Channel' : 'MU',   'Selections' : ["SR"]},
  #"EL_SR"      : {'Channel' : 'EL',   'Selections' : ["SRa1jet"]},
  #"MU_SR"      : {'Channel' : 'MU',   'Selections' : ["SRa1jet"]},
  #"ELMU_CR"    : {'Channel' : 'ELMU', 'Selections' : ["CR"]},
  #"ELMU_AltCR" : {'Channel' : 'ELMU', 'Selections' : ["AltCR"]},
  #"EL_VR"      : {'Channel' : 'EL',   'Selections' : ["VR"]},
  #"MU_VR"      : {'Channel' : 'MU',   'Selections' : ["VR"]},
  #"EL_SR2"      : {'Channel' : 'EL',   'Selections' : ["SR2"]},
  #"MU_SR2"      : {'Channel' : 'MU',   'Selections' : ["SR2"]},
}
Datasets = [
#  "Data15",
#  "Data16",
#  "Data17",
#  "Data18",
#  "ZmumuMC16aSherpa2211",
##  "ZmumuMC16aMG",
#  "ZmumuMC16aFxFx",
#  "ZmumuMC16dSherpa2211",
##  "ZmumuMC16dMG",
#  "ZmumuMC16dFxFx",
#  "ZmumuMC16eSherpa2211",
##  "ZmumuMC16eMG",
#  "ZmumuMC16eFxFx",
#  "ZeeMC16aSherpa2211",
##  "ZeeMC16aMG",
#  "ZeeMC16aFxFx",
#  "ZeeMC16dSherpa2211",
##  "ZeeMC16dMG",
#  "ZeeMC16dFxFx",
#  "ZeeMC16eSherpa2211",
##  "ZeeMC16eMG",
#  "ZeeMC16eFxFx",
#  "MC16aTop",
#  "MC16dTop",
#  "MC16eTop",
#  "MC16aTTbarNonAllHad",
#  "MC16dTTbarNonAllHad",
#  "MC16eTTbarNonAllHad",
#  "MC16aTTbarDilepton",
#  "MC16dTTbarDilepton",
#  "MC16eTTbarDilepton",
#  "MC16aSingleTop",
#  "MC16dSingleTop",
#  "MC16eSingleTop",
#  "MC16aZtautau",
#  "MC16dZtautau",
#  "MC16eZtautau",
#  "MC16aDiboson",
#  "MC16dDiboson",
#  "MC16eDiboson",
#  "MC16aVH",
#  "MC16dVH",
#  "MC16eVH",
] # options: ZmumuMC16xSherpa, ZmumuMC16xSherpa2210, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, ZmumuMC16xFxFx, ZeeMC16xFxFx, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16xZtautau, MC16xVH, MC16xWtaunu (where x = "a", "d" or "e")

# Use CERN ATLAS group higher priority (only works for members of the CERN ATLAS TEAM == CAT)
UseATLASCERNGroupPriority = True

###################################################
## DO NOT MODIFY (below this line)
###################################################

from CommonRunningDefs import *

# Imports
from ROOT import TFile
import os,sys

# Final output path
outPATH = BasePATH + DATE
if not os.path.exists(outPATH):
  os.makedirs(outPATH)

print("<<< Removing old condor submission scripts <<<")

# Remove folder with previous submission scripts
if os.path.exists('DAGMANScripts/'):
  os.system('find DAGMANScripts/ -type f -delete')
  os.system("rm -r DAGMANScripts/")

print("<<< Creating condor submission scripts for DAGMAN <<<")

# More imports
from InputLists       import Inputs
from HelperFunctions  import *
from Arrays           import *
from Dicts            import *
from SystematicTTrees import *

RATE_NO_SF_VARIATIONS = 30 # processing rate (events/second) when running w/o SF variations
RATE_SF_VARIATIONS = 25 # processing rate (events/second) when running w/ SF variations
#RATE_FULLSysts = 800 / 106 # processing rate (events/second) when running a single job over all systematic TTrees FIXME: update 104 w/ new # SystTTrees using SimplerJER
RATE_FULLSysts = 700 / 106 # processing rate (events/second) when running a single job over all systematic TTrees FIXME: update 104 w/ new # SystTTrees using SimplerJER

def seconds_to_job_flavour(seconds):
  if seconds < 3600:
    return 'microcentury' # 1h
  elif seconds < 7200:
    return 'longlunch' # 2h
  elif seconds < 28800:
    return 'workday' # 8h
  else:
    return 'tomorrow' # 1d

#############
# Protections
#############
# Check if all datasets are recognized
NOT_OK_DICT = {Dataset : False if Dataset in DatasetOptions else True for Dataset in Datasets}
if sum(NOT_OK_DICT.values()):
  for Dataset,not_ok in NOT_OK_DICT.items():
    if not_ok:
      print("ERROR: The following dataset was not recognized: {}".format(Dataset))
  print('exiting...')
  sys.exit(1)
# Check if there are inputs for all datasets
NOT_OK_DICT = {Dataset : False if Dataset in Inputs[JetColl] else True for Dataset in Datasets}
if sum(NOT_OK_DICT.values()):
  for Dataset,not_ok in NOT_OK_DICT.items():
    if not_ok:
      print("ERROR: There are no inputs yet for the following dataset: {}".format(Dataset))
  print('exiting...')
  sys.exit(1)

# Get TTree names
if TTrees == 'nominal':
  TTreeNames = ['nominal']
elif TTrees == 'FullSysts':
  TTreeNames = ['FullSysts']
elif TTrees == 'Full': # nominal + systs TTrees
  TTreeNames = ['nominal']+SystTTrees
  # Protection (check if not missing a syst TTree)
  PATHs = Inputs[JetColl]['ZeeMC16aSherpa2211_systs']
  counter = 0
  for path in PATHs: # Loop over paths
    if counter > 1: break # check only one file
    for folder in os.listdir(path): # Loop over folders from each path
      if counter > 1: break # check only one file
      if '_tree.root' not in folder: continue
      for File in os.listdir(path+folder): # Loop over files
        if counter > 1: break # check only one file
        if ".root" not in File: continue
        # Get list of syst names
        tfile        = TFile.Open(path+folder+'/'+File)
        Dir          = tfile.Get("ZHFTreeAlgo")
        AllSystNames = [x.GetName() for x in Dir.GetListOfKeys()] # Get list of TTrees
	# Loop over syst TTrees and see if I'm missing one
        for name in AllSystNames:
          if 'AF2' in name: continue
          if name not in TTreeNames:
            print('{} is missing in SystTTrees from SystematicTTrees.py, exiting'.format(name))
            sys.exit(1)
elif TTrees == 'AllSysts':
  TTreeNames = SystTTrees
  # Protection (check if not missing a syst TTree)
  PATHs = Inputs[JetColl]['ZeeMC16aSherpa2211_systs']
  counter = 0
  for path in PATHs: # Loop over paths
    if counter > 1: break # check only one file
    for folder in os.listdir(path): # Loop over folders from each path
      if counter > 1: break # check only one file
      if '_tree.root' not in folder: continue
      for File in os.listdir(path+folder): # Loop over files
        if counter > 1: break # check only one file
        if ".root" not in File: continue
        # Get list of syst names
        tfile        = TFile.Open(path+folder+'/'+File)
        Dir          = tfile.Get("ZHFTreeAlgo")
        AllSystNames = [x.GetName() for x in Dir.GetListOfKeys()] # Get list of TTrees
	# Loop over syst TTrees and see if I'm missing one
        for name in AllSystNames:
          if 'AF2' in name: continue
          if name not in TTreeNames:
            print('{} is missing in SystTTrees from SystematicTTrees.py, exiting'.format(name))
            sys.exit(1)
else:
  print('TTrees option ({}) not recognized, exiting'.format(TTrees))
  sys.exit(1)

# Dictionary that helps opening files only once
file_checks = {} # file : list of available TTrees

# Dictionary that helps deciding the job flavour (based on number of events on nominal TTree)
file_nevents = {} # file : number of events on nominal TTree

# Loop over channels
for Case,Dict in Cases.items():
  Channel    = Dict['Channel']
  Selections = Dict['Selections']
  # Protections
  if Channel!='EL' and Channel!='MU' and Channel!='ELMU':
    print("ERROR: Channel ({}) not recognised, exiting".format(Channel))
    sys.exit(1)

  # Loop over Selection types
  for Selection in Selections:

    # Loop over datasets
    for Dataset in Datasets:

      # Protections
      if Channel == 'EL' and ('mumu' in Dataset or 'munu' in Dataset):
        continue # skip undesired combination
      if Channel == 'MU' and ('ee' in Dataset or 'enu' in Dataset):
        continue # skip undesired combination

      # Is MC?
      MC = True if "MC" in Dataset else False

      # Loop over TTree names
      for TTreeName in TTreeNames:

        # Protection
        if 'Data' in Dataset and TTreeName != 'nominal': continue # skip systematics for data

        # Get paths to input files for given dataset
        PATHs = Inputs[JetColl][Dataset if TTreeName == 'nominal' else Dataset+'_systs']

        # Create output folder
        Extra = ''
        if   TTreeName == 'Full': Extra = "_Full"
        elif TTreeName == 'FullSysts': Extra = "_FullSysts"
        elif TTreeName == 'nominal': Extra = ""
        elif ',' not in TTreeName: Extra = "_"+TTreeName
        else: Extra = "_PartialSysts"
        FolderName = "{}_{}_{}_{}{}".format(Dataset,Channel,Tagger,Selection,Extra)
        os.system("mkdir -p DAGMANScripts/{}".format(FolderName))

        ###########################
        # Loop over input files
        ###########################
        for path in PATHs: # Loop over paths

          Folders = [folder for folder in os.listdir(path) if '_tree.root' in folder]
          #for folder in os.listdir(path): # Loop over folders from each path (as long they have TTrees)
          for folder in Folders: # Loop over folders from each path (as long they have TTrees)

            #if '_tree.root' not in folder: continue # skip folder w/o TTrees

            # Get DSID or data-period (as appropriate)
            if MC:
              DSID = getDSID(folder)
            else:
              period = getPeriod(folder)

            Files = [File for File in os.listdir(path+folder) if '.root' in File]
            #for File in os.listdir(path+folder): # Loop over files (as long as they are ROOT files)
            for File in Files: # Loop over files (as long as they are ROOT files)

              #if ".root" not in File: continue

              full_file_name = '{}{}/{}'.format(path,folder,File)

	      # Get list of available TTrees and number of events on nominal TTree (only once!)
              if full_file_name not in file_checks:
                tfile = TFile.Open(full_file_name)
                Dir = tfile.Get("ZHFTreeAlgo")
                file_checks[full_file_name] = [x.GetName() for x in Dir.GetListOfKeys()]
                if file_checks[full_file_name]: # at least one TTree
                  nom_ttree                    = Dir.Get('nominal' if 'nominal' in file_checks[full_file_name] else file_checks[full_file_name][0]) # use any other TTree is nominal is not available
                  file_nevents[full_file_name] = nom_ttree.GetEntries()
                tfile.Close()

              # Check if input file has at least one of the requested TTrees
              if ',' not in TTreeName and 'Full' not in TTreeName: # single TTree requested
                if TTreeName not in file_checks[full_file_name]:
                  print("Skipping "+path+folder+'/'+File+' w/o requested TTree ({})'.format(TTreeName))
                  continue # file w/o requested TTree
              elif ',' in TTreeName: # set of TTrees requested (comma-separated list of TTrees)
                  keys = TTreeName.split(',')
                  # Check if there is at least one of the requested TTrees
                  oneTreeFound = sum([True if key in file_checks[full_file_name] else False for key in keys])
                  if not oneTreeFound:
                    print("Skipping "+path+folder+'/'+File+' w/o any of the requested TTrees')
                    continue # file with no TTree
              elif TTreeName == 'FullSysts': # will run over all available systs
                if not len(file_checks[full_file_name]):
                  print("Skipping "+path+folder+'/'+File+' w/o any TTree')
                  continue # file with no TTree

              # Create submission script
              ExtraArgs   = ""
              ScriptName  = Dataset
              if MC:
                ScriptName += "_" + str(DSID)
              else:
                ScriptName += "_" + period
              ScriptName += "_" + Channel + "_" + Selection + "Selection"
              if MC:
                if not useSFs:
                  ScriptName += "_noSFs"
                  ExtraArgs  += " --noSFs"
                if not useSampleWeights:
                  ScriptName += "_noSampleWeights"
                  ExtraArgs  += " --noSampleWeights"
                if not usePRW:
                  ScriptName += "_noPRW"
                  ExtraArgs  += " --noPRW"
                if redoWeights:
                  ExtraArgs  += " --readAMI"
                ExtraArgs  += " --ttrees "+TTreeName
                if ApplySFsysts: ExtraArgs += " --applySFsysts"
                if GetWgtSysts:  ExtraArgs += " --getWeightSysts"

              ScriptName += "_" + File
              ScriptName  = ScriptName.replace('user.jbossios.','')
              ScriptName  = ScriptName.replace('.tree.root','')
              with open("DAGMANScripts/{}/{}{}.sub".format(FolderName,ScriptName,Extra),"w") as out_file:
                out_file.write("executable = ../DAGMANScripts/"+FolderName+"/"+ScriptName+Extra+".sh\n")
                out_file.write("input      = FilesForSubmission.tar.gz\n")
                out_file.write("output     = Logs/"+DATE+FolderName+'/'+ScriptName+".$(ClusterId).$(ProcId).out\n")
                out_file.write("error      = Logs/"+DATE+FolderName+'/'+ScriptName+".$(ClusterId).$(ProcId).err\n")
                out_file.write("log        = Logs/"+DATE+FolderName+'/'+ScriptName+".$(ClusterId).log\n")
                out_file.write("transfer_output_files = \"\" \n")
                if ApplySFsysts and MC:
                  pred_time   = file_nevents[full_file_name]/RATE_SF_VARIATIONS # prediction (in seconds)
                  job_flavour = seconds_to_job_flavour(pred_time)
                  out_file.write('+JobFlavour = "{}"\n'.format(job_flavour))
                  #out_file.write('+JobFlavour = "tomorrow"\n') # 1d
                  #outputFile.write('+JobFlavour = "workday"\n') # 8h
                elif TTrees == 'FullSysts':
                  pred_time   = file_nevents[full_file_name]/RATE_FULLSysts # prediction (in seconds)
                  job_flavour = seconds_to_job_flavour(pred_time)
                  if job_flavour == 'microcentury': # make longlunch (2h) the minimum for FullSysts jobs
                    job_flavour = 'longlunch'
                  out_file.write('+JobFlavour = "{}"\n'.format(job_flavour))
                else:
                  pred_time   = file_nevents[full_file_name]/RATE_NO_SF_VARIATIONS # prediction (in seconds)
                  job_flavour = seconds_to_job_flavour(pred_time)
                  #outputFile.write('+JobFlavour = "workday"\n')
                  #outputFile.write('+JobFlavour = "microcentury"\n') # 1h
                  #out_file.write('+JobFlavour = "longlunch"\n') # 2h
                  out_file.write('+JobFlavour = "{}"\n'.format(job_flavour))
                if UseATLASCERNGroupPriority:
                  out_file.write('+AccountingGroup = "group_u_ATLAST3.all"\n')
                out_file.write("arguments  = $(ClusterId) $(ProcId)\n")
                out_file.write("queue")

              # Create bash script which will run the Reader
              with open("DAGMANScripts/{}/{}{}.sh".format(FolderName,ScriptName,Extra),"w") as out_file:
                out_file.write("#!/bin/bash\n")
                out_file.write("tar xvzf FilesForSubmission.tar.gz\n")
                out_file.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n") # for setupATLAS
                out_file.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n") # setupATLAS
                # prepare final output path
                final_output_path = outPATH + FolderName + '/'
                if not os.path.exists(final_output_path):
                  os.makedirs(final_output_path)
                out_file.write("python3 Reader.py --path "+path+folder+"/ --outPATH "+final_output_path+" --file "+File+" --jetcoll "+JetColl+" --channel "+Channel+" --dataset "+Dataset+" --selection "+Selection+ExtraArgs+" --tagger "+Tagger)

print("<<< All DONE <<<")
