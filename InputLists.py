
# Dictionary for each jet definition pointing to where all TTrees are located (one folder for each DSID/period is expected inside each directory)

PATH = '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TTrees/STDM3_PFlow_DL1r_v11/'
Inputs = {
  'PFlow' : {
    # nominal
    #'ZmumuMC16aSherpa'     : [PATH+'MC16a_Zmumu/'],                         # MC16a Zmumu Sherpa 2.2.1
    #'ZmumuMC16aSherpa2210' : [PATH+'MC16a_Zmumu_Sh_2210/'],                 # MC16a Zmumu Sherpa 2.2.10
    #'ZmumuMC16dSherpa'     : [PATH+'MC16d_Zmumu/'],                         # MC16d Zmumu Sherpa 2.2.1
    #'ZmumuMC16dSherpa2210' : [PATH+'MC16d_Zmumu_Sh_2210/'],                 # MC16d Zmumu Sherpa 2.2.10
    #'ZmumuMC16eSherpa'     : [PATH+'MC16e_Zmumu/'],                         # MC16e Zmumu Sherpa 2.2.1
    #'ZmumuMC16eSherpa2210' : [PATH+'MC16e_Zmumu_Sh_2210/'],                 # MC16e Zmumu Sherpa 2.2.10
    #'ZeeMC16aSherpa'       : [PATH+'MC16a_Zee/'],                           # MC16a Zee Sherpa 2.2.1
    #'ZeeMC16dSherpa'       : [PATH+'MC16d_Zee/'],                           # MC16d Zee Sherpa 2.2.1
    #'ZeeMC16eSherpa'       : [PATH+'MC16e_Zee/'],                           # MC16e Zee Sherpa 2.2.1
    'ZmumuMC16aSherpa2211' : [PATH+'MC16a_Zmumu_Sh_2211/'],                 # MC16a Zmumu Sherpa 2.2.11
    'ZmumuMC16aMG'         : [PATH+'MC16a_Zmumu_MG/'],                      # MC16a Zmumu MG
    'ZmumuMC16aFxFx'       : [PATH+'MC16a_Zmumu_MG_FxFx/'],                 # MC16a Zmumu MG FxFx
    'ZmumuMC16dSherpa2211' : [PATH+'MC16d_Zmumu_Sh_2211/'],                 # MC16d Zmumu Sherpa 2.2.11
    'ZmumuMC16dMG'         : [PATH+'MC16d_Zmumu_MG/'],                      # MC16d Zmumu MG
    'ZmumuMC16dFxFx'       : [PATH+'MC16d_Zmumu_MG_FxFx/'],                 # MC16d Zmumu MG FxFx
    'ZmumuMC16eSherpa2211' : [PATH+'MC16e_Zmumu_Sh_2211/'],                 # MC16e Zmumu Sherpa 2.2.11
    'ZmumuMC16eMG'         : [PATH+'MC16e_Zmumu_MG/'],                      # MC16e Zmumu MG
    'ZmumuMC16eFxFx'       : [PATH+'MC16e_Zmumu_MG_FxFx/'],                 # MC16e Zmumu MG FxFx
    'ZeeMC16aSherpa2211'   : [PATH+'MC16a_Zee_Sh_2211/'],                   # MC16a Zee Sherpa 2.2.11
    'ZeeMC16aMG'           : [PATH+'MC16a_Zee_MG/'],                        # MC16a Zee MG
    'ZeeMC16aFxFx'         : [PATH+'MC16a_Zee_MG_FxFx/'],                   # MC16a Zee MG FxFx
    'ZeeMC16dSherpa2211'   : [PATH+'MC16d_Zee_Sh_2211/'],                   # MC16d Zee Sherpa 2.2.11
    'ZeeMC16dMG'           : [PATH+'MC16d_Zee_MG/'],                        # MC16d Zee MG
    'ZeeMC16dFxFx'         : [PATH+'MC16d_Zee_MG_FxFx/'],                   # MC16d Zee MG FxFx
    'ZeeMC16eSherpa2211'   : [PATH+'MC16e_Zee_Sh_2211/'],                   # MC16e Zee Sherpa 2.2.11
    'ZeeMC16eMG'           : [PATH+'MC16e_Zee_MG/'],                        # MC16e Zee MG
    'ZeeMC16eFxFx'         : [PATH+'MC16e_Zee_MG_FxFx/'],                   # MC16e Zee MG FxFx
    'MC16aZtautau'         : [PATH+'MC16a_Ztautau/'],                       # MC16a Ztautau
    'MC16dZtautau'         : [PATH+'MC16d_Ztautau/'],                       # MC16d Ztautau
    'MC16eZtautau'         : [PATH+'MC16e_Ztautau/'],                       # MC16e Ztautau
    'MC16aWmunu'           : [PATH+'MC16a_Wmunu/'],                         # MC16a Wmunu
    'MC16dWmunu'           : [PATH+'MC16d_Wmunu/'],                         # MC16d Wmunu
    'MC16eWmunu'           : [PATH+'MC16e_Wmunu/'],                         # MC16e Wmunu
    'MC16aWenu'            : [PATH+'MC16a_Wenu/'],                          # MC16a Wenu
    'MC16dWenu'            : [PATH+'MC16d_Wenu/'],                          # MC16d Wenu
    'MC16eWenu'            : [PATH+'MC16e_Wenu/'],                          # MC16e Wenu
    'MC16aTop'             : [PATH+'MC16a_ttbar/', PATH+'MC16a_SingleTop/'],  # MC16a Top (ttbar + SingleTop)
    'MC16dTop'             : [PATH+'MC16d_ttbar/', PATH+'MC16d_SingleTop/'],  # MC16d Top (ttbar + SingleTop)
    'MC16eTop'             : [PATH+'MC16e_ttbar/', PATH+'MC16e_SingleTop/'],  # MC16e Top (ttbar + SingleTop)
    'MC16aTTbarNonAllHad'  : [PATH+'MC16a_ttbar/'],                         # MC16a TTbar nonallhad
    'MC16dTTbarNonAllHad'  : [PATH+'MC16d_ttbar/'],                         # MC16d TTbar nonallhad
    'MC16eTTbarNonAllHad'  : [PATH+'MC16e_ttbar/'],                         # MC16e TTbar nonallhad
    'MC16aTTbarDilepton'   : [PATH+'MC16a_ttbar_dilepton/'],                # MC16a TTbar dilepton
    'MC16dTTbarDilepton'   : [PATH+'MC16d_ttbar_dilepton/'],                # MC16d TTbar dilepton
    'MC16eTTbarDilepton'   : [PATH+'MC16e_ttbar_dilepton/'],                # MC16e TTbar dilepton
    'MC16aSingleTop'       : [PATH+'MC16a_SingleTop/'],                     # MC16a Top (SingleTop)
    'MC16dSingleTop'       : [PATH+'MC16d_SingleTop/'],                     # MC16d Top (SingleTop)
    'MC16eSingleTop'       : [PATH+'MC16e_SingleTop/'],                     # MC16e Top (SingleTop)
    'MC16aDiboson'         : [PATH+'MC16a_Diboson/'],                       # MC16a Diboson
    'MC16dDiboson'         : [PATH+'MC16d_Diboson/'],                       # MC16d Diboson
    'MC16eDiboson'         : [PATH+'MC16e_Diboson/'],                       # MC16e Diboson
    'MC16aVH'              : [PATH+'MC16a_VHbb/',PATH+'MC16a_VHcc/'],       # MC16a VH
    'MC16dVH'              : [PATH+'MC16d_VHbb/',PATH+'MC16d_VHcc/'],       # MC16d VH
    'MC16eVH'              : [PATH+'MC16e_VHbb/',PATH+'MC16e_VHcc/'],       # MC16e VH
    'MC16aWtaunu'          : [PATH+'MC16a_Wtaunu/'],                        # MC16a Wtaunu
    'MC16dWtaunu'          : [PATH+'MC16d_Wtaunu/'],                        # MC16d Wtaunu
    'MC16eWtaunu'          : [PATH+'MC16e_Wtaunu/'],                        # MC16e Wtaunu
    'Data15'               : [PATH+'data2015/'],                            # data15
    'Data16'               : [PATH+'data2016/'],                            # data16
    'Data17'               : [PATH+'data2017/'],                            # data17
    'Data18'               : [PATH+'data2018/'],                            # data18
    # systematic TTrees
    'ZmumuMC16aSherpa2211_systs' : [PATH+'MC16a_Zmumu_Sh_2211_systs/'],                       # MC16a Zmumu Sherpa 2.2.11
    'ZmumuMC16aMG_systs'         : [PATH+'MC16a_Zmumu_MG_systs/'],                            # MC16a Zmumu MG
    'ZmumuMC16aFxFx_systs'       : [PATH+'MC16a_Zmumu_MG_FxFx_systs/'],                       # MC16a Zmumu MG FxFx
    'ZmumuMC16dSherpa2211_systs' : [PATH+'MC16d_Zmumu_Sh_2211_systs/'],                       # MC16d Zmumu Sherpa 2.2.11
    'ZmumuMC16dMG_systs'         : [PATH+'MC16d_Zmumu_MG_systs/'],                            # MC16d Zmumu MG
    'ZmumuMC16dFxFx_systs'       : [PATH+'MC16d_Zmumu_MG_FxFx_systs/'],                       # MC16d Zmumu MG FxFx
    'ZmumuMC16eSherpa2211_systs' : [PATH+'MC16e_Zmumu_Sh_2211_systs/'],                       # MC16e Zmumu Sherpa 2.2.11
    'ZmumuMC16eMG_systs'         : [PATH+'MC16e_Zmumu_MG_systs/'],                            # MC16e Zmumu MG
    'ZmumuMC16eFxFx_systs'       : [PATH+'MC16e_Zmumu_MG_FxFx_systs/'],                       # MC16e Zmumu MG FxFx
    'ZeeMC16aSherpa2211_systs'   : [PATH+'MC16a_Zee_Sh_2211_systs/'],                         # MC16a Zee Sherpa 2.2.11
    'ZeeMC16aMG_systs'           : [PATH+'MC16a_Zee_MG_systs/'],                              # MC16a Zee MG
    'ZeeMC16aFxFx_systs'         : [PATH+'MC16a_Zee_MG_FxFx_systs/'],                         # MC16a Zee MG FxFx
    'ZeeMC16dSherpa2211_systs'   : [PATH+'MC16d_Zee_Sh_2211_systs/'],                         # MC16d Zee Sherpa 2.2.11
    'ZeeMC16dMG_systs'           : [PATH+'MC16d_Zee_MG_systs/'],                              # MC16d Zee MG
    'ZeeMC16dFxFx_systs'         : [PATH+'MC16d_Zee_MG_FxFx_systs/'],                         # MC16d Zee MG FxFx
    'ZeeMC16eSherpa2211_systs'   : [PATH+'MC16e_Zee_Sh_2211_systs/'],                         # MC16e Zee Sherpa 2.2.11
    'ZeeMC16eMG_systs'           : [PATH+'MC16e_Zee_MG_systs/'],                              # MC16e Zee MG
    'ZeeMC16eFxFx_systs'         : [PATH+'MC16e_Zee_MG_FxFx_systs/'],                         # MC16e Zee MG FxFx
    'MC16aZtautau_systs'         : [PATH+'MC16a_Ztautau_systs/'],                             # MC16a Ztautau
    'MC16dZtautau_systs'         : [PATH+'MC16d_Ztautau_systs/'],                             # MC16d Ztautau
    'MC16eZtautau_systs'         : [PATH+'MC16e_Ztautau_systs/'],                             # MC16e Ztautau
    'MC16aWmunu_systs'           : [PATH+'MC16a_Wmunu_systs/'],                               # MC16a Wmunu
    'MC16dWmunu_systs'           : [PATH+'MC16d_Wmunu_systs/'],                               # MC16d Wmunu
    'MC16eWmunu_systs'           : [PATH+'MC16e_Wmunu_systs/'],                               # MC16e Wmunu
    'MC16aWenu_systs'            : [PATH+'MC16a_Wenu_systs/'],                                # MC16a Wenu
    'MC16dWenu_systs'            : [PATH+'MC16d_Wenu_systs/'],                                # MC16d Wenu
    'MC16eWenu_systs'            : [PATH+'MC16e_Wenu_systs/'],                                # MC16e Wenu
    'MC16aTop_systs'             : [PATH+'MC16a_ttbar_systs/',PATH+'MC16a_SingleTop_systs/'], # MC16a Top (ttbar + SingleTop)
    'MC16dTop_systs'             : [PATH+'MC16d_ttbar_systs/',PATH+'MC16d_SingleTop_systs/'], # MC16d Top (ttbar + SingleTop)
    'MC16eTop_systs'             : [PATH+'MC16e_ttbar_systs/',PATH+'MC16e_SingleTop_systs/'], # MC16e Top (ttbar + SingleTop)
    'MC16aTTbarNonAllHad_systs'  : [PATH+'MC16a_ttbar_systs/'],                               # MC16a ttbar nonallhad
    'MC16dTTbarNonAllHad_systs'  : [PATH+'MC16d_ttbar_systs/'],                               # MC16d ttbar nonallhad
    'MC16eTTbarNonAllHad_systs'  : [PATH+'MC16e_ttbar_systs/'],                               # MC16e ttbar nonallhad
    'MC16aTTbarDilepton_systs'   : [PATH+'MC16a_ttbar_dilepton_systs/'],                      # MC16a ttbar dilepton
    'MC16dTTbarDilepton_systs'   : [PATH+'MC16d_ttbar_dilepton_systs/'],                      # MC16d ttbar dilepton
    'MC16eTTbarDilepton_systs'   : [PATH+'MC16e_ttbar_dilepton_systs/'],                      # MC16e ttbar dilepton
    'MC16aSingleTop_systs'       : [PATH+'MC16a_SingleTop_systs/'],                           # MC16a Top (SingleTop)
    'MC16dSingleTop_systs'       : [PATH+'MC16d_SingleTop_systs/'],                           # MC16d Top (SingleTop)
    'MC16eSingleTop_systs'       : [PATH+'MC16e_SingleTop_systs/'],                           # MC16e Top (SingleTop)
    'MC16aDiboson_systs'         : [PATH+'MC16a_Diboson_systs/'],                             # MC16a Diboson
    'MC16dDiboson_systs'         : [PATH+'MC16d_Diboson_systs/'],                             # MC16d Diboson
    'MC16eDiboson_systs'         : [PATH+'MC16e_Diboson_systs/'],                             # MC16e Diboson
    'MC16aVH_systs'              : [PATH+'MC16a_VHbb_systs/',PATH+'MC16a_VHcc_systs/'],       # MC16a VH
    'MC16dVH_systs'              : [PATH+'MC16d_VHbb_systs/',PATH+'MC16d_VHcc_systs/'],       # MC16d VH
    'MC16eVH_systs'              : [PATH+'MC16e_VHbb_systs/',PATH+'MC16e_VHcc_systs/'],       # MC16e VH
    'MC16aWtaunu_systs'          : [PATH+'MC16a_Wtaunu_systs/'],                              # MC16a Wtaunu
    'MC16dWtaunu_systs'          : [PATH+'MC16d_Wtaunu_systs/'],                              # MC16d Wtaunu
    'MC16eWtaunu_systs'          : [PATH+'MC16e_Wtaunu_systs/'],                              # MC16e Wtaunu
  }
}
