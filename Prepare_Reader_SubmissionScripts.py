Channel       = "MU"   # options: EL, MU and ELMU
Datasets      = [
#  "Data15",
#  "Data16",
#  "Data17",
#  "Data18",
#  "ZmumuMC16aSherpa2210",
#  "ZmumuMC16aSherpa2211",
#  "ZmumuMC16aSherpa",
#  "ZmumuMC16aMG",
#  "ZmumuMC16dSherpa2210",
#  "ZmumuMC16dSherpa2211",
#  "ZmumuMC16dSherpa",
#  "ZmumuMC16dMG",
#  "ZmumuMC16eSherpa2210",
#  "ZmumuMC16eSherpa2211",
#  "ZmumuMC16eSherpa",
#  "ZmumuMC16eMG",
#  "ZeeMC16aSherpa",
#  "ZeeMC16aSherpa2211",
#  "ZeeMC16aMG",
#  "ZeeMC16dSherpa",
#  "ZeeMC16dSherpa2211",
#  "ZeeMC16dMG",
#  "ZeeMC16eSherpa",
#  "ZeeMC16eSherpa2211",
#  "ZeeMC16eMG",
#  "MC16aTop",
#  "MC16dTop",
#  "MC16eTop",
#  "MC16aTTbar",
#  "MC16dTTbar",
#  "MC16eTTbar",
#  "MC16aSingleTop",
#  "MC16dSingleTop",
#  "MC16eSingleTop",
#  "MC16aWmunu",
#  "MC16dWmunu",
#  "MC16eWmunu",
#  "MC16aWenu",
#  "MC16dWenu",
#  "MC16eWenu",
#  "MC16aZtautau",
#  "MC16dZtautau",
#  "MC16eZtautau",
#  "MC16aDiboson",
#  "MC16dDiboson",
#  "MC16eDiboson",
#  "MC16aVH",
#  "MC16dVH",
#  "MC16eVH",
#  "MC16aWtaunu",
#  "MC16dWtaunu",
#  "MC16eWtaunu",
] # options: ZmumuMC16xSherpa, ZmumuMC16xSherpa2210, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16xZtautau, MC16xVH, MC16xWtaunu (where x = "a", "d" or "e")

# Location where the output ROOT files will be written
BasePATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/"
#DATE     = "26062021/" # VR
#DATE     = "09062021/" # SR
#DATE     = "16072021/" # SRa1jet
#DATE     = "20072021/" # VR (for ttbar systematics)
#DATE     = "23072021/" # VR (for ttbar systematics) Fixed
#DATE     = "27072021/"   # VR/SR EL/MU/ELMU (for ttbar systematics) Fixed
#DATE     = "29072021/"  # SR and VR for ttbar SF (lepton SF) uncertainties
#DATE     = "05082021/" # ELMU/VR data and MC ttbar
#DATE     = "06082021/" # ELMU/VR data and MC ttbar
#DATE     = "07082021/" # ELMU/VR ttbar with SF weight variations
#DATE     = "27082021/" # EL/MU SR (more truth-level plots)
DATE     = "03092021/" # ELMU VR ttbar with SF weight variations fixed

# MC Weights (only meaningful for MC, not used at all when running on data)
useSFs           = True  # reco, ID, isolation and trigger lepton SFs
useSampleWeights = True  # eventWeight * eff * xs / totalevents (sumWeights for Sherpa)
usePRW           = True  # pileup reweighting

# Fill Distributions (only meaningful for MC)
fillReco         = True
fillTruth        = True

# Flavour Jet Tagger
taggers          = ["DL1r"]

# Apply SF variations?
ApplySFsysts = False

# Produce distributions using alternative event weights
GetWgtSysts = False

# Selection
Selections       = ["SR"] # options: SR, VR, SR20, Study, Medium1, Medium2, SRnoMET, SRa1jet
#Selections       = ["VR"] # options: SR, VR, SR20, Study, Medium1, Medium2, SRnoMET, SRa1jet
#Selections       = ["SRa1jet"] # options: SR, VR, SR20, Study, Medium1, Medium2, SRnoMET, SRa1jet

# Systematics
TTrees = 'nominal' # options: nominal, Full, subset of ttrees separated by comma or any other single TTree

# Debugging
Debug     = False

# Re-calculate sample weights
redoWeights = True

# Jet collection
JetColl = 'PFlow' # options: PFlow, EMTopo

###################################################
## DO NOT MODIFY
###################################################

# Final output path
outPATH = BasePATH + DATE

# old
#outPATH = "/eos/user/j/jbossios/SM/WZgroup/ZHF/TreetoHists_outputs/25062020/"
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/25062020/" # soft met
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/03072020/" # final met
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/06072020/" # final met fixed
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/08072020/" # fixed Zpt and Zabs
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/13072020/" # derive SF uncertainties
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/05082020/" # w/ PRW
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/10082020/" # w/ PRW and DL1 (instead of DL1r)
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/16092020/" # w/ PRW and DL1r, extended Truth and XF
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/17092020/" # w/ PRW and DL1r, extended Truth and *fixed* XF
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/02102020/" # w/ PRW and DL1r, extended Truth and *fixed* XF (now looking also at zero jet event)
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/21102020/" # w/ PRW and DL1r, extended Truth and *fixed* XF (now looking also at zero jet event) (bugfixes and Sherpa2210)
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/10022021/" # w/ PRW and DL1r, using v4 TTrees
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/09062021/" # w/ PRW and DL1r, using v6 TTrees (SR)
#outPATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/26062021/" # w/ PRW and DL1r, using v6 TTrees (VR)

print "<<< Creating submission scripts <<<"

from ROOT import *
import os,sys

os.system("rm SubmissionScripts/*")
os.system("mkdir SubmissionScripts")

from InputLists import Inputs
from HelperFunctions import *
from Arrays import *
from Dicts import *

TAGGERS = [tag for tag in Taggers if tag in taggers]

# Protections
if "EL" not in Channel and "MU" not in Channel:
  print "ERROR: Channel not recognised, exiting"
  sys.exit(0)

# Loop over taggers
for Tagger in TAGGERS:
  # Loop over Selection types
  for Selection in Selections:
    # Loop over datasets
    for Dataset in Datasets:

      # Protections
      if Dataset not in DatasetOptions:
        print "ERROR: {} not recognised, exiting".format(Dataset)
        sys.exit(0)
      if not Inputs[JetColl].has_key(Dataset):
        print "ERROR: There are no inputs yet for the dataset provided, exiting"
        sys.exit(0)

      # Get paths to input files for given dataset
      PATHs = Inputs[JetColl][Dataset]

      # MC?
      MC = False
      if "MC" in Dataset:
        MC = True

      ###########################
      # Loop over input files
      ###########################
      for path in PATHs: # Loop over paths

        for folder in os.listdir(path): # Loop over folders from each path

          if '_tree.root' not in folder: continue

          if MC:
            DSID = getDSID(folder)
          else:
            period = getPeriod(folder)

          for File in os.listdir(path+folder): # Loop over files

            if ".root" not in File: continue

            # Check if at least one of the requested TTrees exists
            tfile = TFile.Open(path+folder+'/'+File)
            Dir   = tfile.Get("ZHFTreeAlgo")
            if ',' not in TTrees and 'Full' not in TTrees: # single TTree requested
              tree  = Dir.Get(TTrees)
              if not tree:
                print "Skipping "+path+folder+'/'+File+' w/o requested TTree ({})'.format(TTrees)
                continue # file w/o requested TTree
            else: # set of TTrees requested
              if TTrees == 'Full':
                keys = [x.GetName() for x in Dir.GetListOfKeys()] # Get list of TTrees
              else: # comma-separated list of TTrees
                keys = TTrees.split(',')
              # Check if there is at least one of the requested TTrees
              oneTreeFound = False
              for key in keys:
                tree = Dir.Get(key)
                if tree:
                  oneTreeFound = True
              if not oneTreeFound:
                print "Skipping "+path+folder+'/'+File+' w/o any of the requested TTrees'
                continue # file with no TTree

            # Create submission script
            ExtraArgs   = ""
            ScriptName  = Dataset
            if MC:
              ScriptName += "_" + DSID
            else:
              ScriptName += "_" + period
            ScriptName += "_" + JetColl
            ScriptName += "_" + Channel + "_" + Selection + "Selection"
            ScriptName += "_" + Tagger + "Tagger"
            if MC:
              if useSFs:
                ScriptName += "_useSFs"
              else:
                ScriptName += "_noSFs"
                ExtraArgs  += " --noSFs"
              if useSampleWeights:
                ScriptName += "_useSampleWeights"
              else:
                ScriptName += "_noSampleWeights"
                ExtraArgs  += " --noSampleWeights"
              if usePRW:
                ScriptName += "_usePRW"
              else:
                ScriptName += "_noPRW"
                ExtraArgs  += " --noPRW"
              if not fillTruth:
                ScriptName += "noTruth"
                ExtraArgs  += " --noTruth"
              if not fillReco:
                ScriptName += "noReco"
                ExtraArgs  += " --noReco"
              if redoWeights:
                ExtraArgs  += " --readAMI"
              ExtraArgs  += " --ttrees "+TTrees
              if ApplySFsysts: ExtraArgs += " --applySFsysts"
              if GetWgtSysts:  ExtraArgs += " --getWeightSysts"
              if   TTrees == 'Full':    ScriptName += "_fullSysts"
              elif TTrees == 'nominal': ScriptName += "_nominalOnly"
              else:                     ScriptName += "_partialSysts"

            ScriptName += "_" + File
            outputFile = open("SubmissionScripts/"+ScriptName+".sub","w")
            outputFile.write("executable = ../SubmissionScripts/"+ScriptName+".sh\n")
            outputFile.write("input      = FilesForSubmission.tar.gz\n")
            outputFile.write("output     = Logs/"+DATE+ScriptName+".$(ClusterId).$(ProcId).out\n")
            outputFile.write("error      = Logs/"+DATE+ScriptName+".$(ClusterId).$(ProcId).err\n")
            outputFile.write("log        = Logs/"+DATE+ScriptName+".$(ClusterId).log\n")
            #outputFile.write("RequestMemory   = 6000\n")
            #outputFile.write("RequestCpus = 4\n")
            #outputFile.write("+JobFlavour = 'tomorrow'\n")
            outputFile.write("transfer_output_files = \"\" \n")
            #outputFile.write('+JobFlavour = "tomorrow"\n')
            #outputFile.write('+JobFlavour = "testmatch"\n')
            #outputFile.write('+JobFlavour = "espresso"\n')
            #outputFile.write('+JobFlavour = "microcentury"\n')
            #outputFile.write('+JobFlavour = "longlunch"\n')
	    if ApplySFsysts:
              outputFile.write('+JobFlavour = "tomorrow"\n')
	    else:
              outputFile.write('+JobFlavour = "workday"\n')
            outputFile.write("arguments  = $(ClusterId) $(ProcId)\n")
            outputFile.write("queue")
            outputFile.close()

            # Create bash script which will run the Reader
            outputFile = open("SubmissionScripts/"+ScriptName+".sh","w")
            outputFile.write("#!/bin/bash\n")
            outputFile.write("tar xvzf FilesForSubmission.tar.gz\n")
            outputFile.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n") # for setupATLAS
            outputFile.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n") # setupATLAS
            outputFile.write("python Reader.py --path "+path+folder+"/ --outPATH "+outPATH+" --file "+File+" --jetcoll "+JetColl+" --channel "+Channel+" --dataset "+Dataset+" --selection "+Selection+ExtraArgs+" --tagger "+Tagger)
            outputFile.close()

print("<<< All DONE <<<")
