# Number of SF systematics for each source
SFsysts = {
  'nominal': 1,  # used only to book nominal histograms
  'BTag': 170,  # PCBT (38 for FixedCut 85%)
  'JVT': 2,
  'MUIso': 4,
  'MUReco': 8,
  'MUPID': 4,
  'MUTrig': 4,
  'ELIso': 2,
  'ELReco': 2,
  'ELPID': 2,
  'ELTrig': 2,
  'weight_pileup': 2,
}
