#!/usr/bin/env python

import ROOT,os,sys

DataPeriods = ['15','16','17','18']
Date        = '30042020'
Debug       = False
ATLASlegend = "Internal"
FinalStates = ['Zb','Zc']
Channel     = 'MU'
Methods     = {
  'Bayes'    : '4',
  'SVD'      : 'default',
  'BinByBin' : -1,
}
UnfoldingColors = {
  'Bayes'    : ROOT.kBlack,
  'SVD'      : ROOT.kBlue,
  'BinByBin' : ROOT.kRed,
}

###########################################
## DO NOT MODIFY
###########################################

# List of histogram names from ReadTTrees_ZHF
HistNames = [
  "HFjet0_pt_Ti1",
  "HFjet0_absy_Ti1",
  "Zpt_Ti1",
  "Zabsy_Ti1",
  "deltaYZjHF_Ti1",
  "deltaPhiZjHF_Ti1",
  "deltaRZjHF_Ti1",
]

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Import input files from Plotter
PATH = "../Plotter/"
sys.path.insert(1, PATH) # insert at 1, 0 is the script path
from Luminosities import *
from Style import *

# Total luminosity
Luminosity = 0
for year in DataPeriods:
  if year == '15':   Luminosity += Luminosity_2015
  elif year == '16': Luminosity += Luminosity_2016
  elif year == '17': Luminosity += Luminosity_2017
  elif year == '18': Luminosity += Luminosity_2018

# Input file name
Dataset = 'data'
for period in DataPeriods: Dataset += period
InputFileName = 'Outputs/unfoldingOutputs_{0}_{1}.root'.format(Date,Dataset)
InputFile     = ROOT.TFile.Open(InputFileName)
if not InputFile:
  print "ERROR: "+InputFileName+" not found, exiting"
  sys.exit(0)

# Loop over histograms
for histname in HistNames:
  # Loop over final states
  for case in FinalStates:

    if Debug: print "##############################################################################"
    if Debug: print "DEBUG: Comparing unfolding methods for '"+histname+"' histogram in "+case

    # TCanvas
    if Debug: print "DEBUG: Create TCanvas"
    Canvas  = ROOT.TCanvas()
    outName = "Plots/CompareUnfoldingMethods_{0}_{1}_{2}_{3}.pdf".format(Date,Dataset,histname,case)
    Canvas.Print(outName+"[")

    # TPad for upper panel
    if Debug: print "DEBUG: Create TPad for upper panel"
    pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.03)
    pad1.Draw()
    pad1.cd()

    # Set log scales (if requested)
    if Debug: print "DEBUG: Set log scales if requested"
    if histname in Logx: pad1.SetLogx()
    if histname in Logy: pad1.SetLogy()

    ############################
    # Get Histograms
    ############################
    Histograms = dict()
    # Loop over unfolding methods
    for method,param in Methods.iteritems():
      if method == 'Bayes':      HistName = 'UnfoldedData_{0}_{1}_{2}_iterations_{3}'.format(case,histname,method,param)
      elif method == 'SVD':      HistName = 'UnfoldedData_{0}_{1}_{2}_kTerm_{3}'.format(case,histname,method,param)
      elif method == 'BinByBin': HistName = 'UnfoldedData_{0}_{1}_{2}'.format(case,histname,method)
      InputFile.cd()
      Hist = InputFile.Get(HistName)
      if not Hist:
        print "ERROR: "+HistName+" not found, exiting"
	sys.exit(0)
      Hist.SetDirectory(0)
      Hist.SetMarkerColor(UnfoldingColors[method])
      Hist.SetLineColor(UnfoldingColors[method])
      Histograms[method] = Hist
   
    # Add histograms to THStack and draw legends
    Legends = ROOT.TLegend(0.75,0.5,0.92,0.9)
    Legends.SetTextFont(42)

    # THStack
    Stack = ROOT.THStack()
    for key,hist in Histograms.iteritems():
      Stack.Add(hist,"HIST][")
      Legends.AddEntry(hist,key,"l")
    
    if Debug: print "DEBUG: Draw THSTack"
    Stack.Draw("nostack")
    
    if XaxisRange.has_key(histname):
      Stack.GetXaxis().SetRangeUser(XaxisRange[histname][0],XaxisRange[histname][1])
    maxY = Stack.GetMaximum()
    maxYscaling = 1E5
    if "absy" in histname or "phi" in histname or "eta" in histname or "deltaR" in histname or "deltaPhi" in histname or "Weight" in histname: maxYscaling = 2E7
    Stack.GetYaxis().SetRangeUser(1,maxY*maxYscaling)

    Stack.GetXaxis().SetLabelSize(0.)
    Stack.GetXaxis().SetTitleSize(0.)
    Stack.GetYaxis().SetTitleSize(20)
    Stack.GetYaxis().SetTitleFont(43)
    Stack.GetYaxis().SetLabelFont(43)
    Stack.GetYaxis().SetLabelSize(19)
    Stack.GetYaxis().SetTitleOffset(1.3)
    Stack.GetYaxis().SetTitle("Events")

    if Debug: print "DEBUG: Draw legends"
    Legends.Draw("same")

    ROOT.gPad.RedrawAxis()

    # Show ATLAS legend
    if Debug: print "DEBUG: Show ATLAS legend"
    if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
    elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
    elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
    else:
      print "ERROR: ATLASlegend not recognized, exiting"
      sys.exit(0)
    ATLASBlock = ROOT.TLatex(0.45,0.8,atlas)
    ATLASBlock.SetNDC()
    ATLASBlock.Draw("same")

    # Show CME and luminosity
    if Debug: print "DEBUG: Show CME and luminosity"
    CME = "#scale[1.5]{13 TeV, "+str(round(Luminosity/1000,1))+" fb^{-1}}"
    CMEblock = ROOT.TLatex(0.45,0.7,CME)
    CMEblock.SetNDC()
    CMEblock.Draw("same")

    # Show channel
    if Debug: print "DEBUG: Show channel type"
    channel = "#scale[1.5]{"
    channel += "#mu" if Channel == "MU" else "e"
    channel += " channel}"
    TextBlock = ROOT.TLatex(0.45,0.6,channel)
    TextBlock.SetNDC()
    TextBlock.Draw("same")

    # Show case
    if Debug: print "DEBUG: Show final state"
    channel = "#scale[1.5]{"
    channel += "Z+b" if case == "Zb" else "Z+c"
    channel += "}"
    TextBlock = ROOT.TLatex(0.78,0.4,channel)
    TextBlock.SetNDC()
    TextBlock.Draw("same")
    
    # TPad for bottom plot
    if Debug: print "DEBUG: Create TPad for bottom panel"
    Canvas.cd()
    pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
    pad2.SetTopMargin(0.03)
    pad2.SetBottomMargin(0.32)
    pad2.Draw()
    pad2.cd()

    # Set log-y scale (if requested)
    if Debug: print "DEBUG: Set log-Y scale if requested"
    if histname in Logx: pad2.SetLogx()

    # Create ratio histograms
    if Debug: print "DEBUG: Create Method/Bayes histogram"
    minY    = 0.45
    maxY    = 1.55
    counter = 0
    RatioHists = dict()
    for key,hist in Histograms.iteritems():
      if key == 'Bayes': continue
      ratioHist = Histograms[key].Clone("ratioHist_"+key)
      ratioHist.Divide(Histograms['Bayes'])
      ratioHist.SetMinimum(minY)
      ratioHist.SetMaximum(maxY)
      if counter == 0: ratioHist.Draw("e0")
      else: ratioHist.Draw("e0 same")
      RatioHists[key] = ratioHist
      counter += 1

    # Draw line at 1
    if Debug: print "DEBUG: Draw line at 1"
    nbins = RatioHists['BinByBin'].GetNbinsX()
    if histname in XaxisRange:
      minX = XaxisRange[histname][0]
      maxX = XaxisRange[histname][1]
    else:
      minX = RatioHists['BinByBin'].GetXaxis().GetBinLowEdge(1)
      maxX = RatioHists['BinByBin'].GetXaxis().GetBinUpEdge(nbins)
    Line = ROOT.TLine(minX,1,maxX,1)
    Line.SetLineStyle(7)
    Line.Draw("same")

    # Set x-axis title
    for key,ratioHist in RatioHists.iteritems():
      if Debug: print "DEBUG: Set X-axis title"
      histkey = histname
      if histname == "lep_pt":
        histkey = "mu_pt" if Channel == "MU" else "el_pt"
      if XaxisTitles.has_key(histkey):
        ratioHist.GetXaxis().SetTitleSize(20)
        ratioHist.GetXaxis().SetTitleFont(43)
        ratioHist.GetXaxis().SetLabelFont(43)
        ratioHist.GetXaxis().SetLabelSize(19)
        ratioHist.GetXaxis().SetTitleOffset(3)
        ratioHist.GetXaxis().SetTitle(XaxisTitles[histkey])
        ratioHist.GetXaxis().SetNdivisions(510)
      ratioHist.GetYaxis().SetTitleSize(20)
      ratioHist.GetYaxis().SetTitleFont(43)
      ratioHist.GetYaxis().SetLabelFont(43)
      ratioHist.GetYaxis().SetLabelSize(19)
      ratioHist.GetYaxis().SetTitleOffset(1.3)
      ratioHist.GetYaxis().SetTitle("Method / Bayes")

    # Save PDF
    if Debug: print "DEBUG: Save/print PDF"
    Canvas.Print(outName)
    Canvas.Print(outName+"]")

    # Clear dict
    for key,hist in RatioHists.iteritems():
      hist.Delete()
    for key,hist in Histograms.iteritems():
      hist.Delete()
    Histograms.clear()
    RatioHists.clear()

InputFile.Close()
print ">>> ALL DONE <<<"
