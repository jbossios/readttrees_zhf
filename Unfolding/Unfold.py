##########################################################################
#                                                                         #
# Purpose: Unfold data, perform closure test and propagate systematics    #
# Author: Jona Bossio (jbossios@cern.ch)                                  #
#                                                                         #
##########################################################################

# Date of inputs
#Date  = "28062020" # Soft MET
#Date  = "03072020" # Final MET
#Date  = "06072020" # Final MET fixed
Date  = "08072020" # Final MET fixed (fixed Zpt/absy)

# Datasets
DataYears = ['15','16','17','18']

# Distributions to unfold
Observables = [
  "HFjet0_pt_Ti1",
  "HFjet0_absy_Ti1",
  "Zpt_Ti1",
  "Zabsy_Ti1",
  "deltaYZjHF_Ti1",
  "deltaPhiZjHF_Ti1",
  "deltaRZjHF_Ti1",
##  "Zpt_Zbb",
##  "pTbb",
##  "mbb",
##  "pTbbovermbb",
##  "deltaYbb",
##  "deltaPhibb",
##  "deltaRbb",
]

# Final states
FinalStates = [
  'Zb', # Z + at least one b-jet
  'Zc', # Z + at least one c-jet
]

# Unfolding methods
UnfoldingMethods = [
  'Bayes',
  'SVD',
  'BinByBin',
]

# Systematics
PropagateSystematics = True

# Closure test
doTrivialClosureTest = True

# Unfolding parameters scan
NumberOfIterations = [1,2,3,4,5]
kTerms             = [-1,2,3,4,5,6] # -1 means default which is numberBins/2

# Debugging
Debug = True

##############################################################################
## DO NOT MODIFY
##############################################################################

Systematics = [
  'fit',  # flavour fit non-closure
  'lumi', # luminosity
#  'SF',
]

import ROOT,os,sys
# RooUnfold
ROOT.gSystem.Load("RooUnfold/libRooUnfold")

# Import SF variations from Reader
PATH = "../"
sys.path.insert(1, PATH) # insert at 1, 0 is the script path
from Systematics import *

# Unfold data separately for data15+16, data17 and data18
DataDict = dict()
if '15' in DataYears and '16' in DataYears: DataDict['1516'] = ['15','16']
elif '15' in DataYears:                     DataDict['15']   = ['15']
elif '16' in DataYears:                     DataDict['16']   = ['16']
if '17' in DataYears:                       DataDict['17']   = ['17']
if '18' in DataYears:                       DataDict['18']   = ['18']
for key,DataPeriods in DataDict.iteritems():

  # Open input file
  InputFileName = "Inputs/unfoldingInputs_"+Date+"_data"+key+".root"
  File = ROOT.TFile.Open(InputFileName)
  if not File:
    print "ERROR: "+InputFileName+" not found, exiting"
    sys.exit(0)
  
  DataHists         = []
  UnfoldedDataHists = []
  UnfoldedRecoHists = []
  TruthHists        = []
  for case in FinalStates:
    # Loop over observables
    if Debug: print "DEBUG: Loop over observables"
    for obs in Observables:
      # Get Reco histogram
      RecoHistName = "Reco_"+case+"_"+obs
      hReco = File.Get(RecoHistName)
      if not hReco:
        print "ERROR: "+RecoHistName+" not found, exiting"
        sys.exit(0)
      # Get Truth histogram
      TruthHistName = "Truth_"+case+"_"+obs.replace('_Ti1','')
      hTruth = File.Get(TruthHistName)
      if not hTruth:
        print "ERROR: "+TruthHistName+" not found, exiting"
        sys.exit(0)
      TruthHists.append(hTruth)
      # Get Transfer matrix
      TMHistName = "TM_"+case+"_"+obs
      hTM = File.Get(TMHistName)
      if not hTM:
        print "ERROR: "+TMHistName+" not found, exiting"
        sys.exit(0)
  
      # Create RooUnfoldResponse
      Response = ROOT.RooUnfoldResponse(hReco,hTruth,hTM)
  
      ######################
      # Unfold nominal data
  
      # Get histogram
      DataHistName = "Data_"+case+"_"+obs
      hData        = File.Get(DataHistName)
      if not hData:
        print "ERROR: "+DataHistName+" not found, exiting"
        sys.exit(0)
      DataHists.append(hData)
  
      # Unfold
      for method in UnfoldingMethods:
        if method == 'Bayes':
          for ite in NumberOfIterations:
            Bayes = ROOT.RooUnfoldBayes(Response,hData,ite) # Response matrix, distribution to unfold, number of iterations
            UnfoldedData = Bayes.Hreco()
            UnfoldedData.SetName("UnfoldedData_"+case+"_"+obs+"_Bayes_iterations_"+str(ite))
            UnfoldedData.SetDirectory(0)
            UnfoldedDataHists.append(UnfoldedData)
        elif method == 'SVD':
          for k in kTerms:
            if k != -1: Svd = ROOT.RooUnfoldSvd(Response,hData,k) # Response matrix, distribution to unfold, kTerm
            else: Svd = ROOT.RooUnfoldSvd(Response,hData) # Response matrix, distribution to unfold, kTerm=numberBins/2
            UnfoldedData = Svd.Hreco()
            if k != -1: UnfoldedData.SetName("UnfoldedData_"+case+"_"+obs+"_SVD_kTerm_"+str(k))
            else: UnfoldedData.SetName("UnfoldedData_"+case+"_"+obs+"_SVD_kTerm_default")
            UnfoldedData.SetDirectory(0)
            UnfoldedDataHists.append(UnfoldedData)
        elif method == 'BinByBin':
          BinByBin = ROOT.RooUnfoldBinByBin(Response,hData) # Response matrix, distribution to unfold
          UnfoldedData = BinByBin.Hreco()
          UnfoldedData.SetName("UnfoldedData_"+case+"_"+obs+"_BinByBin")
          UnfoldedData.SetDirectory(0)
          UnfoldedDataHists.append(UnfoldedData)
  
      ###############################
      # Unfold systematic variations
      if PropagateSystematics and case == 'Zc':
        # Loop over nominal + systematics
        for source in Systematics:
          if source == 'fit' or source == 'lumi':
            # Loop over up/down
            for np in ['UP','DOWN']:
              # Get histogram
              DataHistName = "Data_"+case+"_"+obs+'_'+source+np
              hData        = File.Get(DataHistName)
              if not hData:
                print "ERROR: "+DataHistName+" not found, exiting"
                sys.exit(0)
              # Unfold
              Bayes = ROOT.RooUnfoldBayes(Response,hData,4) # Response matrix, distribution to unfold, number of iterations # FIXME (decide on #iterations)
              UnfoldedData = Bayes.Hreco()
              UnfoldedData.SetName("UnfoldedData_"+case+"_"+obs+"_Bayes_iterations_4_"+source+'_'+np)
              UnfoldedData.SetDirectory(0)
              UnfoldedDataHists.append(UnfoldedData)
          else: # SF
            for SFsource,NPs in SFsysts.itermitems(): # Loop over source of SF uncertainties
              if 'EL' in SFsource: continue # FIXME (only MU channel implemented)
              if SFsource == 'nominal': continue
              for inp in range(1,NPs+1):
                # Get variated TM
                extraSF = '_'+SFsource+'_'+str(inp)
                TMHistName = "TM_"+case+"_"+obs
                hTM = File.Get(TMHistName)
                if not hTM:
                  print "ERROR: "+TMHistName+" not found, exiting"
                  sys.exit(0)
                # Create variated RooUnfoldResponse
                VarResponse = ROOT.RooUnfoldResponse(hReco,hTruth,hTM)
                # Unfold
                Bayes = ROOT.RooUnfoldBayes(VarResponse,hData,4) # Response matrix, distribution to unfold, number of iterations # FIXME (decide on #iterations)
                UnfoldedData = Bayes.Hreco()
                UnfoldedData.SetName("UnfoldedData_"+case+"_"+obs+"_Bayes_iterations_4"+extraSF)
                UnfoldedData.SetDirectory(0)
                UnfoldedDataHists.append(UnfoldedData)
  
      #################################################################
      # Trivial closure test (unfold MC reco to compare with MC truth)
      if doTrivialClosureTest:
        Bayes = ROOT.RooUnfoldBayes(Response,hReco,4) # Response matrix, distribution to unfold, number of iterations
        UnfoldedReco = Bayes.Hreco()
        UnfoldedReco.SetName("UnfoldedReco_"+case+"_"+obs+"_Bayes_iterations_4")
        UnfoldedReco.SetDirectory(0)
        UnfoldedRecoHists.append(UnfoldedReco)
  
  # Output TFile
  OutFileName = "Outputs/unfoldingOutputs_"+Date+"_data"+key+".root"
  OutFile = ROOT.TFile(OutFileName,"RECREATE")
  for hist in UnfoldedDataHists: hist.Write()
  for hist in UnfoldedRecoHists: hist.Write()
  for hist in DataHists:         hist.Write()
  for hist in TruthHists:        hist.Write()
  OutFile.Close()

print ">>> ALL DONE <<<"

