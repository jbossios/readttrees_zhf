##########################################################################
#                                                                         #
# Purpose: Prepare data distribution to unfold                            #
# Author: Jona Bossio (jbossios@cern.ch)                                  #
#                                                                         #
# Procedure: subtract non-Z backgrounds from data and apply Z+HF fraction #
#            also create transfer matrices                                # 
#                                                                         #
##########################################################################

Channel = 'MU'
Tagger  = 'DL1r'
Debug   = False

DataYears = [
  '15',
  '16',
  '17',
  '18',
]

Backgrounds = [
  'Top',
  'Diboson',
  'Ztautau',
#  'Wmunu',
]

#Date = '28062020' # Soft MET
#Date = '03072020' # Final MET
#Date = '06072020' # Final MET fixed
#Date = '08072020' # Final MET fixed (fixed Zpt/absy)
Date = '05082020' # w/ PRW

FinalStates = [
  'Zb',
  'Zc',
]

PropagateFlavourFitNonClosureUncertainty = True
PropagateLuminosityUncertainty           = True
PropagateSFuncertainties                 = False
ApplyFlavourFitSF                        = False


# TODO: Prepare Z+bb and Z+cc

###########################################################################
## DO NOT MODIFY
###########################################################################

LumiUncert = 0.017 # (1.7%) taken from https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics#2018_13_TeV_proton_proton_Morion

# Distributions to be unfolded
HistNames = [
  "HFjet0_pt_Ti1",
  "HFjet0_absy_Ti1",
  "Zpt_Ti1",
  "Zabsy_Ti1",
  "deltaYZjHF_Ti1",
  "deltaPhiZjHF_Ti1",
  "deltaRZjHF_Ti1",
##  "Zpt_Zbb",
##  "pTbb",
##  "mbb",
##  "pTbbovermbb",
##  "deltaYbb",
##  "deltaPhibb",
##  "deltaRbb",
]

TruthFlavourClassification = {
  'Zb' : 'FlavA1B',
  'Zc' : 'FlavA1C',
}

import ROOT,os,sys

# Prepare unfolding inputs separately for data15+16, data17 and data18
DataDict = dict()
if '15' in DataYears and '16' in DataYears: DataDict['1516'] = ['15','16']
elif '15' in DataYears:                     DataDict['15']   = ['15']
elif '16' in DataYears:                     DataDict['16']   = ['16']
if '17' in DataYears:                       DataDict['17']   = ['17']
if '18' in DataYears:                       DataDict['18']   = ['18']
for key,DataPeriods in DataDict.iteritems():

  if Debug: print 'DEBUG: Prepare inputs for '+key+' period'

  # Retrieve flavour fractions
  FitterOutputFileName = '../Fitter/Fractions_vs_observables_data'+key+'.root'
  FractionsFile = ROOT.TFile.Open(FitterOutputFileName)
  if not FractionsFile:
    print "ERROR: Flavour fractions file not found, exiting"
    sys.exit(0)
  FlavourFractions = dict()
  for case in FinalStates:
    FlavourFractions[case] = dict()
    for observable in HistNames:
      HistName = '{0}Fractions_vs_{1}'.format(case,observable)
      Hist     = FractionsFile.Get(HistName)
      if not Hist:
        print "ERROR: "+HistName+" not found, exiting"
        sys.exit(0)
      Hist.SetDirectory(0)
      FlavourFractions[case][observable] = Hist

  if ApplyFlavourFitSF:
    # Retrieve scale factors
    ScaleFactors = dict()
    for case in FinalStates:
      ScaleFactors[case] = dict()
      for observable in HistNames:
        HistName = '{0}SF_vs_{1}'.format(case,observable)
        Hist     = FractionsFile.Get(HistName)
        if not Hist:
          print "ERROR: "+HistName+" not found, exiting"
          sys.exit(0)
        Hist.SetDirectory(0)
        ScaleFactors[case][observable] = Hist

  
  # Import input files from Plotter
  PATH = "../Plotter/"
  sys.path.insert(1, PATH) # insert at 1, 0 is the script path
  from InputFiles import InputFiles
  from Luminosities import *
  
  # Import SF variations from Reader
  PATH = "../"
  sys.path.insert(1, PATH) # insert at 1, 0 is the script path
  from Systematics import *

  ############################
  # Prepare data distribution
  ############################
  
  Campaigns = []
  Luminosities = dict()
  Luminosities['a'] = 0
  Luminosity = 0
  for period in DataPeriods:
    if period == '15':
      Campaigns.append('a')
      Luminosity += Luminosity_2015
      Luminosities['a'] += Luminosity_2015
    elif period == '16':
      Luminosity += Luminosity_2016
      if 'a' not in Campaigns: Campaigns.append('a')
      Luminosities['a'] += Luminosity_2016
    elif period == '17': 
      Luminosity += Luminosity_2017
      Campaigns.append('d')
      Luminosities['d'] = Luminosity_2017
    elif period == '18':
      Luminosity += Luminosity_2018
      Campaigns.append('e')
      Luminosities['e'] = Luminosity_2018

  if Debug: print 'DEBUG: Total luminosity: '+Luminosity
  if Debug: print 'DEBUG: Luminosities dict: '
  if Debug: print Luminosities
  
  # Create output TFile
  OutFileName = "Inputs/unfoldingInputs_"+Date+"_data"+key+'.root'
  OutFile = ROOT.TFile(OutFileName,"RECREATE")

  # Loop over distributions
  if Debug: print "DEBUG: Loop over distributions"
  for histname in HistNames:
  
    if Debug: print "DEBUG: Loop data-taking periods"
    # Loop over data-taking periods
    DataHists = []
    for period in DataPeriods:
      if Debug: 'ERROR: Get histogram('+histname+') for data'+period
      # Open input file
      Key      = "Signal_data"+period+"_"+Channel+"_SR_"+Tagger
      FileName = InputFiles[Key]
      File     = ROOT.TFile.Open(FileName)
      if not File:
        print "ERROR: "+FileName+" not found, exiting"
        sys.exit(0)
      # Get histogram
      Hist = File.Get(histname)
      if not Hist:
        print "ERROR: "+histname+" not found, exiting"
        sys.exit(0)
      Hist.SetDirectory(0)
      File.Close()
      DataHists.append(Hist)

    if Debug: 'DEBUG: DatHists dict:'
    if Debug: print DataHists

    if Debug:
      print 'DEBUG: GetBinContent(10) for each hist in DataHists:'
      for hist in DataHists:
        print 'DEBUG: GetBinContent(10): '+hist.GetBinContent(10)
  
    # Get total data histogram
    if Debug: print "DEBUG: Get total data histogram"
    hData = DataHists[0].Clone("Data_"+histname)
    hData.SetDirectory(0)
    for ihist in range(1,len(DataHists)):
      hData.Add(DataHists[ihist])

    if Debug:
      print 'DEBUG: GetBinContent(10) for final data hist: '+hData.GetBinContent(10)
  
    # Get background estimations
    if Debug: print "DEBUG: Get background estimations"
    hBkgs = []
    for campaign in Campaigns:
      for bkg in Backgrounds:
        # Open File
        Key      = bkg+"_MC16"+campaign+"_"+Channel+"_SR_"+Tagger
        FileName = InputFiles[Key]
        File     = ROOT.TFile.Open(FileName)
        if not File:
          print "ERROR: "+FileName+" not found, exiting"
          sys.exit(0)
        # Get histogram
        Hist = File.Get(histname)
        if not Hist:
          print "ERROR: "+histname+" not found, exiting"
          sys.exit(0)
        Hist.SetDirectory(0)
	File.Close()
        Hist.Scale(Luminosities[campaign])
        hBkgs.append(Hist)
  
    # Subtract non-Z backgrounds to the total data
    if Debug: print "DEBUG: Subtract non-Z backgrounds to the total data"
    for hist in hBkgs: hData.Add(hist,-1)
  
    # Loop over final states
    for case in FinalStates:
      hFinalData = hData.Clone('Data_'+case+'_'+histname) 
  
      # Get FitNonClosure up/down
      if PropagateFlavourFitNonClosureUncertainty and case == 'Zc':
        # Get uncertainty
        inTXTname  = '../Fitter/'+case+'NonClosureUncertainty_data'
        for period in DataPeriods: inTXTname += period
        inTXTname += '.txt'
        inTXT = open(inTXTname,'r')
        # Prepare final distributions
        hFinalDataFitUp   = hFinalData.Clone('Data_'+case+'_'+histname+'_fitUP')
        hFinalDataFitDown = hFinalData.Clone('Data_'+case+'_'+histname+'_fitDOWN')
        Uncert         = float(inTXT.read())
        inTXT.close()
        NominalFrac    = FlavourFractions[case][histname]
        hFinalDataFitUp.Multiply(NominalFrac+NominalFrac*Uncert)
        hFinalDataFitDown.Multiply(NominalFrac-NominalFrac*Uncert)

      if PropagateLuminosityUncertainty:
        # Prepare final distributions
        hFinalDataLumiUp   = hFinalData.Clone('Data_'+case+'_'+histname+'_lumiUP')
        hFinalDataLumiDown = hFinalData.Clone('Data_'+case+'_'+histname+'_lumiDOWN')
        NominalFrac        = FlavourFractions[case][histname]
        hFinalDataLumiUp.Multiply(NominalFrac*(1+LumiUncert))
        hFinalDataLumiDown.Multiply(NominalFrac*(1-LumiUncert))
  
      # Scale data to get desired final state
      if Debug: print "DEBUG: Scale data to get Z+HF"
  
      hFinalData.Multiply(FlavourFractions[case][histname])
      OutFile.cd()
      hFinalData.Write()
      if PropagateFlavourFitNonClosureUncertainty and case == 'Zc':
        hFinalDataFitUp.Write()
        hFinalDataFitDown.Write()
      if PropagateLuminosityUncertainty:
        hFinalDataLumiUp.Write()
        hFinalDataLumiDown.Write()

  ####################
  # Prepare MC inputs
  ####################
  
  if Debug: print "DEBUG: Prepare response matrices"
  
  # Loop over final states
  for case in FinalStates:
  
    # Loop over observables
    if Debug: print "DEBUG: Loop over observables"
    for histname in HistNames:
 
      if False:
        # Get TM for each campaign
        TMs = []
        for campaign in Campaigns:
          # Open TFile
          Key  = "Signal_MC16"+campaign+"_"+Channel+"_SR_"+Tagger
          File = ROOT.TFile.Open(InputFiles[Key])
          if not File:
            print "ERROR: "+FileName+" not found, exiting"
            sys.exit(0)
          # Get matrix
          HistName = "TM_"+TruthFlavourClassification[case]+"_"+histname
          Hist     = File.Get(HistName)
          if not Hist:
            print "ERROR: "+HistName+" not found, exiting"
            sys.exit(0)
          Hist.SetDirectory(0)
	  File.Close()
          Hist.Scale(Luminosities[campaign])
          TMs.append(Hist)
  
        # Get total TM for this observable
        TM = TMs[0].Clone("TM_"+case+"_"+histname)
	print 'TM.GetBinContent(100) (v1): '+str(TM.GetBinContent(100))
        TM.SetDirectory(0)
        #for tm in TMs: TM.Add(tm)
        for itm in range(1,len(TMs)): TM.Add(TMs[itm])
	print 'TM.GetBinContent(100) (v2): '+str(TM.GetBinContent(100))
  
      # Loop over SF variations
      if True:
        TMs = []
        for source,NPs in SFsysts.iteritems():
          if not PropagateSFuncertainties and source != 'nominal': continue # only get SF-variated TMs if requested
          if Channel == 'MU' and 'EL' in source: continue # skip EL SFs in MU channel
          if Channel == 'EL' and 'MU' in source: continue # skip MU SFs in EL channel
          # Loop over NPs
          for inp in range(1,NPs+1):
            extraSF = '' if source == 'nominal' else '_'+source+'_'+str(inp)
            # Get TM for each campaign
            TempTMs = []
            for campaign in Campaigns:
              # Open TFile
              Key  = "Signal_MC16"+campaign+"_"+Channel+"_SR_"+Tagger
              File = ROOT.TFile.Open(InputFiles[Key])
              if not File:
                print "ERROR: "+FileName+" not found, exiting"
                sys.exit(0)
              # Get matrix
              HistName = "TM_"+TruthFlavourClassification[case]+"_"+histname+extraSF
              Hist     = File.Get(HistName)
              if not Hist:
                print "ERROR: "+HistName+" not found, exiting"
                sys.exit(0)
              Hist.SetDirectory(0)
              File.Close()
              Hist.Scale(Luminosities[campaign])
              TempTMs.append(Hist)
            # Get total TM for this observable, SF source and NP
            TM = TempTMs[0].Clone("TM_"+case+"_"+histname+extraSF)
            TM.SetDirectory(0)
            #for tm in TempTMs: TM.Add(tm) # buggy but gives unfolded data / MC truth == 1 (Jona, Luis)
            for itm in range(1,len(TempTMs)): TM.Add(TempTMs[itm]) # correct but there must be a bug somewhere else (data/MC ~ 0.5)
            TMs.append(TM)
        #TM = TMs[0]

      # Get reco distribution for each campaign
      Recos = []
      for campaign in Campaigns:
        # Open TFile
        Key  = "Signal_MC16"+campaign+"_"+Channel+"_SR_"+Tagger
        File = ROOT.TFile.Open(InputFiles[Key])
        if not File:
          print "ERROR: "+FileName+" not found, exiting"
          sys.exit(0)
        # Get matrix
        HistName = TruthFlavourClassification[case]+"_"+histname
        Hist     = File.Get(HistName)
        if not Hist:
          print "ERROR: "+HistName+" not found, exiting"
          sys.exit(0)
        Hist.SetDirectory(0)
	File.Close()
        Hist.Scale(Luminosities[campaign])
        Recos.append(Hist)
  
      # Get total Reco for this observable
      Reco = Recos[0].Clone("Reco_"+case+"_"+histname)
      Reco.SetDirectory(0)
      for ireco in range(1,len(Recos)): Reco.Add(Recos[ireco])
      if ApplyFlavourFitSF:
        Reco.Multiply(ScaleFactors[case][histname])
  
      # Get truth distribution for each campaign
      Truths = []
      for campaign in Campaigns:
        # Open TFile
        Key  = "Signal_MC16"+campaign+"_"+Channel+"_SR_"+Tagger
        File = ROOT.TFile.Open(InputFiles[Key])
        if not File:
          print "ERROR: "+FileName+" not found, exiting"
          sys.exit(0)
        # Get matrix
        Observable = histname.replace('_Ti1','')
        HistName = TruthFlavourClassification[case]+"_"+Observable+"_truth"
        Hist     = File.Get(HistName)
        if not Hist:
          print "ERROR: "+HistName+" not found, exiting"
          sys.exit(0)
        Hist.SetDirectory(0)
	File.Close()
        Hist.Scale(Luminosities[campaign])
        Truths.append(Hist)
  
      # Get total Truth for this observable
      Truth = Truths[0].Clone("Truth_"+case+"_"+Observable)
      Truth.SetDirectory(0)
      for itruth in range(1,len(Truths)): Truth.Add(Truths[itruth])
  
      OutFile.cd()
      for tm in TMs: tm.Write()
      #TM.Write()
      Reco.Write()
      Truth.Write()
  
  OutFile.Close()
  
print ">>> ALL DONE <<<"






