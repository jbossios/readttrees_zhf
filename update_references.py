
import os
import sys

from run_reader import cases, samples


if __name__ == '__main__':
    commands = []
    for case in cases:
        # Prepare settings
        options = case.split('_')
        sample = options[0]
        channel = options[1]
        selection = options[2]
        sample_key = '{}_{}'.format(sample, options[3]) if 'Data' not in sample else sample
        input_path = samples[sample_key]
        nominal = True
        if 'Data' not in sample and options[3] == 'FullSysts':
            nominal = False
        fill_truth = True if nominal else False
        apply_sf_systs = True if nominal else False
        ttrees = 'nominal' if nominal else 'FullSysts'
        mc = True
        if 'Data' in sample:
            mc = False
        read_ami = True if mc else False
        # Common settings
        tagger = 'DL1r'
        jet_coll = 'PFlow'
        use_sfs = True
        use_sample_weights = True
        use_prw = True
        # Find input file
        input_file = os.listdir(input_path)[0]
        # Prepare commmand
        command = "python3 Reader.py --path "+input_path+" --outPATH /eos/user/j/jbossios/SM/WZgroup/ZHF/ReaderRefs/ --file "+input_file+" --jetcoll "+jet_coll+" --channel "+channel+" --dataset "+sample+" --selection "+selection+" --tagger "+tagger
        command += " --ttrees "+ttrees
        if not fill_truth:
            command += " --noTruth"
        if read_ami:
            command += " --readAMI"
        if not use_sfs:
            command += " --noSFs"
        if not use_sample_weights:
            command += " --noSampleWeights"
        if not use_prw:
            command += " --noPRW"
        if apply_sf_systs:
            command += " --applySFsysts"
        command += ' --localTest'
        command += ' --ref'
        commands.append(command)
    command = ' && '.join(commands) + ' &'
    os.system(command)
