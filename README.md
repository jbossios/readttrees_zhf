# INSTRUCTIONS (last update 24/05/2022)

Get BootstrapGenerator:

```
git clone ssh://git@gitlab.cern.ch:7999/atlas-physics/sm/StandardModelTools_BootstrapGenerator/BootstrapGenerator.git
```

## STEP 1 | Reader (loop over TTrees and produce histograms)

The main code (loop over events) is made in ```Reader.py```

There are scripts with helper classes, functions and dicts

The lists of inputs are declared in ```InputLists.py```

The different event selection options are declared in ```Selections.py```

```Dicts.py``` is used to define the list of variables, and which ones are observables, and when each are filled ['' : inclusive events,'Te1' : exactly one b-tagged jet, 'Ti2': at least two b-tagged jets]. The binning for each variable is defined therein.

### How to extend support to new inputs?

if you run over a sample that is not one that is currently being used, you would need to do the following things:

1. Modify getDSID() to be able to identify other DSIDs properly in HelperFunctions.py

2. Make sure you have the AMI info in ami_reader.txt script. If you don't, you can use the pyAMI script from here to get that info: https://gitlab.cern.ch/jbossios/MakeTTrees_ZHF/-/blob/master/scripts/GetAMIvalues.py (instructions here: https://gitlab.cern.ch/jbossios/MakeTTrees_ZHF/-/blob/master/scripts/Instructions)

3. Update the SamplesTypes array with new sample type in Arrays.py

4. Put the path to those new samples in InputLists.py

### Quick test on LXPLUS

**1. Take a look at ```LocalTest.py```**

Choose the channel (electron, muon or electron+muon) with ```Channel``` (EL, MU or ELMU, respectively)

Choose the dataset type with ```Dataset```. The options are: ZmumuMC16xSherpa, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, ZmumuMC16xSherpa2211, ZeeMC16xSherpa2211, Data15, Data16, Data17, Data18, MC16xTop, MC16xSingleTop, MC16xTTbar, MC16xWenu, MC16xWmunu, MC16xWtaunu, MC16xDiboson, MC16xVHcc, MC16xVHbb (x = a,d,e)

Choose a sample to run on from ```InputFiles```

If you want to use scale factors (SFs) set ```useSFs``` to ```True```

If you want to use sample weights set ```useSampleWeights``` to True```

If you want to fill reco distributions set ```fillReco``` to ```True```

If you want to fill truth distributions set ```fillTruth``` to ```True```

**2. Run the python script in the following way:**

```
python LocalTest.py
```

This script will automatically run in debug mode (i.e., will run over a single file and a limited number of events).

### Full test on LXPLUS (same as CI test)

Run the following:

```
python run_local_test.py
```

If changes are observed and they are expected, references can be updated with the following command:

```
python update_references.py
```

### HTConder submission

**Make sure to run in AFS, HTCondor is not available in EOS**

**0. [NEW] Define common running conditions**

Open CommonRunningDefs.py and:

Choose where outputs should be written with ```BasePATH``` (output path) and ```DATE``` (date or any other string to identify this submission)

Set ```useSFs```, ```useSampleWeights``` and ```usePRW``` accordingly

Choose ```fillReco``` and ```fillTruth``` accordingly (this will set which type of histograms will be filled: reco and/or truth)

Choose tagger to use with ```Tagger```

Choose to apply SF variations or not with ```ApplySFsysts``` (```False``` will produce nominal-only distributions)

Choose to produce distributions using alternative event weights with ```GetWgtSysts```

Choose to run over nominal only (```nominal```) or over all TTrees (```Full```, i.e. nominal + systematics) with ```TTrees```.

**1. Prepare submission scripts**

i)Take a look at ```Prepare_Reader_DAGMAN_SubmissionScripts.py```

Choose the desired channel+selection combinations with the ```Cases``` dict

Choose the desired datasets with the ```Datasets``` dict

ii) Run script in the following way:

```
python Prepare_Reader_DAGMAN_SubmissionScripts.py
```
This script will produce all HTCondor submission scripts, which will be located at ```DAGMANScripts/```

If you have problems with the schedd, you can try with another one. For example

```
export _condor_SCHEDD_HOST="bigbird16.cern.ch”
```

**2. Produce tarball with all needed files:**

```
source Tar.sh
```

This will compress the following files in a tar.gz file which will be sent during submission (```FilesForSubmission.tar.gz``` in the ```Submit/``` folder):

```
Reader.py
Arrays.py
Dicts.py
HelperClasses.py
HelperFunctions.py
Selections.py
InputLists.py
metadata_dict.py
ami_reader.txt
Systematics.py
```

If any of the above files are updated Tar.sh should be run again

**3. Submit condor jobs with DAGMAN**

To submit batch jobs, go to  Submit:

```
cd Submit/
```

i) Take a look at ```DAGMANSubmit.py```

Choose the desired channel+selection combination with ```Launch``` (options: ELMU_VR, ELMU_AltVR, EL_SR, MU_SR, EL_MU_SR)

Choose the list of datasets to run on with ```Datasets```

Set ```Test``` to ```True``` if you just want to check if there are any remaining job that has to be sent

ii) Run script:

```
python DAGMANSubmit.py
```

This script will check if there is already the corresponding output file for each job, and will send only those jobs that are needed (this allows to resend only those jobs which failed instead of sending all of them again, set ```Test``` to ```True``` if you want to know if any job needs to be sent) 

The logfiles will be automatically transferred to ```Submit/Logs/{DATE}```and the outputs to the choosen ```outhPATH```.

There is also a python script (```Submit/MergeOutputs.py```) to merge the output root files so we have a single file for each sample for the plotter (do ```source Submit/setup_for_merger.sh``` before running such a script).)

iii) Monitoring

You can monitor condor jobs with:

```
condor_q
```

You can kill a given job with:

```
condor_rm JOB_ID
```

You can kill all jobs with:

```
condor_rm -all
```

You can also monitor a single job with:

```
condor_wait -status Logs/{DATE}/NAME.log
```

You can find out why a job is on HOLD state with:

```
condor_q -analyze JOB_ID
```

iv) Retry (if jobs are in HOLD state)

Kill HOLD jobs and remove output files of HOLD (broken) batch jobs:

```
find *.root -size 0 | xargs rm
```

Run ```DAGMANSubmit.py``` script again (will send jobs only for the files w/o output files)

v) Final checks

Once all jobs are finished, run ```DAGMANSubmit.py``` script again with ```Test``` as ```False``` to ensure no more jobs need to be sent

Run ```RemoveBrokenOutputFiles.py``` to check if there are any outputs that are broken, if output files are removed, resend jobs with ```DAGMANSubmit.py```

Finally, check all .err and .out logs. From Logs/{DATE}/ once could do the following:

```
cat */*.err > Err &
cat */*.out > Out &
```

Make sure ```Err``` is empty, and that there is no line containing ```SYSTEM_PERIODIC_REMOVE``` in ```Out```. If that is found, it means a condor job was killed since it lasted longer than it is supposed to run. If there is an output for that job, it needs to be removed and the job needs to be sent again with a longer job flavour (defined in ```Prepare_Reader_DAGMAN_SubmissionScripts.py```

## STEP 2 | Produce reco-level MC/data plots (mandatory!)

```
cd Plotter
```

Set ```InputFiles.py``` accordingly (choose corresponding date of jobs)

**If you do not want to produce reco-level MC/data plots:**

Run ```ExpandInputFiles.py``` such that it produces the needed ```_expanded.root``` files (this script adds Te1 and Ti2 histograms to produce Ti1 histograms). Run this script only if you will not produce reco-level MC/data plots, since the plotter below already produce those files (will produce them only if those were not produced though).

Configure ```Run_Data_vs_MC.py``` and run it: 

```
python Run_Data_vs_MC.py
```

## STEP 3 | Perform flavour fitting

```
cd ../Fitter
```

Configure ```TemplateFits.py``` and run it:

```
python TemplateFits.py
```

## STEP 5 | Unfolding

First we need to prepare the data (subtract backgrounds and apply measured fractions)

```
cd ../Unfolding
```

Configure ```PrepareInputs.py``` and run it

```
python PrepareInputs.py
```

Then, we unfold the data. Please take a look at the instructions available in the ```Instructions``` file

Then, configure ```PrepareInputs.py``` and run it:

```
python Unfold.py
```

## STEP 6 | Compare unfolded data with MC truth

```
cd ../Plotter
```

Configure ```UnfoldedData_vs_MC.py``` and run it:

```
python UnfoldedData_vs_MC.py
```


## OPTIONAL | Perform data-driven ttbar estimation

Follow instructions in ```DataDrivenEstimation_ttbar/README.md```

## OPTIONAL | Derive ttbar theory uncertainties

Follow instructions in ```DeriveTTbarTheoryUncertainties/README.md```
