
# Common definitions
MUisoWP  = 'FCTight_FixedRad'
ELisoWP  = 'Tight'
minLepPt = 27  # GeV
UseJVTineffSFs = False  # for now do not use JVT SF from jets not passing JVT (to agree with CxAOD approach)

# SR: Signal region
class PFlowSRselection:
  def __init__(self):
    self.excludeTransitionRegion4Electrons = True     # exclude 1.37<|eta|<1.52
    self.lep_n                             = 2        # number of leptons (-1 means no requirement)
    self.muon_minPt                        = minLepPt # GeV (must be at least 1.05 * online threshold)
    self.muon_maxEta                       = 2.5      # absolute eta
    self.el_minPt                          = minLepPt # GeV (must be at least 1 GeV larger than online threshold)
    self.el_maxEta                         = 2.47     # absolute eta
    self.jet_atLeastOne                    = False    # At least one jet?
    self.jet_minPt                         = 20       # GeV
    self.jet_maxRapidity                   = 2.5      # |y|
    self.jet_cleaning                      = True     # remove events not passing jet cleaning (MC only, not-clean events in data already removed)
    self.jet_minDRJetLepton                = 0.4      # minimum DeltaR (jet, any signal lepton)
    self.jet_JVTcut                        = True     # select only jets passing JVT
    self.jet_JVTwp                         = 'Tight'
    self.maxMET                            = 60       # GeV
    self.maxZPt4METcut                     = 150      # GeV
    self.minZmass                          = [76]     # GeV
    self.maxZmass                          = [106]    # GeV
    self.triggerMatching                   = True

# SR2: Alternative SR with wider Zmass window
class PFlowSR2selection:
  def __init__(self):
    self.excludeTransitionRegion4Electrons = True     # exclude 1.37<|eta|<1.52
    self.lep_n                             = 2        # number of leptons (-1 means no requirement)
    self.muon_minPt                        = minLepPt # GeV (must be at least 1.05 * online threshold)
    self.muon_maxEta                       = 2.5      # absolute eta
    self.el_minPt                          = minLepPt # GeV (must be at least 1 GeV larger than online threshold)
    self.el_maxEta                         = 2.47     # absolute eta
    self.jet_atLeastOne                    = False    # At least one jet?
    self.jet_minPt                         = 20       # GeV
    self.jet_maxRapidity                   = 2.5      # |y|
    self.jet_cleaning                      = True     # remove events not passing jet cleaning (MC only, not-clean events in data already removed)
    self.jet_minDRJetLepton                = 0.4      # minimum DeltaR (jet, any signal lepton)
    self.jet_JVTcut                        = True     # select only jets passing JVT
    self.jet_JVTwp                         = 'Tight'
    self.maxMET                            = 60       # GeV
    self.maxZPt4METcut                     = 150      # GeV
    self.minZmass                          = [71]     # GeV
    self.maxZmass                          = [111]    # GeV
    self.triggerMatching                   = True

# VR: Validation region enhanced on ttbar
# Same as SR but opposite MET cut
class PFlowVRselection:
  def __init__(self):
    self.excludeTransitionRegion4Electrons = True     # exclude 1.37<|eta|<1.52
    self.lep_n                             = 2        # number of leptons (-1 means no requirement)
    self.muon_minPt                        = minLepPt # GeV (must be at least 1.05 * online threshold)
    self.muon_maxEta                       = 2.5      # absolute eta
    self.el_minPt                          = minLepPt # GeV (must be at least 1 GeV larger than online threshold)
    self.el_maxEta                         = 2.47     # absolute eta
    self.jet_atLeastOne                    = False    # At least one jet?
    self.jet_minPt                         = 20       # GeV
    self.jet_maxRapidity                   = 2.5      # |y|
    self.jet_cleaning                      = True     # remove events not passing jet cleaning (MC only, not-clean events in data already removed)
    self.jet_minDRJetLepton                = 0.4      # minimum DeltaR (jet, any signal lepton)
    self.jet_JVTcut                        = True     # select only jets passing JVT
    self.jet_JVTwp                         = 'Tight'
    self.minMET                            = 60       # GeV
    self.minZmass                          = [71, 106]  # GeV
    self.maxZmass                          = [76, 111]  # GeV
    self.triggerMatching                   = True

# SRa1jet: SR + at least one jet
class PFlowSRa1jetselection(PFlowSRselection):
  def __init__(self):
    PFlowSRselection.__init__(self)
    self.jet_atLeastOne                    = True    # At least one jet?

# CR: Control region (enhanced in ttbar for e+mu channel)
class PFlowCRselection:
  def __init__(self):
    self.excludeTransitionRegion4Electrons = True     # exclude 1.37<|eta|<1.52
    self.lep_n                             = 2        # number of leptons (-1 means no requirement)
    self.muon_minPt                        = minLepPt # GeV (must be at least 1.05 * online threshold)
    self.muon_maxEta                       = 2.5      # absolute eta
    self.el_minPt                          = minLepPt # GeV (must be at least 1 GeV larger than online threshold)
    self.el_maxEta                         = 2.47     # absolute eta
    self.jet_atLeastOne                    = False    # At least one jet?
    self.jet_minPt                         = 20       # GeV
    self.jet_maxRapidity                   = 2.5      # |y|
    self.jet_cleaning                      = True     # remove events not passing jet cleaning (MC only, not-clean events in data already removed)
    self.jet_minDRJetLepton                = 0.4      # minimum DeltaR (jet, any signal lepton)
    self.jet_JVTcut                        = True     # select only jets passing JVT
    self.jet_JVTwp                         = 'Tight'
    self.maxMET                            = 60       # GeV
    self.maxZPt4METcut                     = 150      # GeV
    self.minZmass                          = [76]     # GeV
    self.maxZmass                          = [106]    # GeV
    self.triggerMatching                   = True

# AltCR: Alternative CR (same as CR but in Zmass sidebands)
class PFlowAltCRselection(PFlowVRselection): # Alternative VR for ttbar validation
  def __init__(self):
    PFlowVRselection.__init__(self)
    self.minZmass                          = [71,106] # GeV
    self.maxZmass                          = [76,111] # GeV

