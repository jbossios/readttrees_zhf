###########################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    09/08/2019
## Purpose: Read TTrees produced with MakeTTrees_ZHF and fill histograms
###########################################################################################

###################################################
## TODO
###################################################
# Add a TLatex kind of class with the description of the ROOT file
# Add separated cuts for truth (jet pT, lepton pT/eta, etc)
###################################################

# Python modules
import os
import sys
import argparse
import time
from math import *

# ROOT
from ROOT import *

# Import things from reader
from HelperClasses import *
from HelperFunctions import *
from Arrays import *
from Dicts import *
from Systematics import *
from protections import *


def run_reader(args):

  # Protections
  apply_protections(args)

  # Read user settings
  settings = configure(args)

  # Timing and memory usage
  if settings['Perf']:
    start = time.perf_counter()

  # Print INFO
  if settings['useSFs']:
    print("INFO: Using scale factors")
  if settings['useSampleWeights']:
    print("INFO: Using sample weights")
  if settings['usePRW']:
    print("INFO: Using PRW")

  # Read AMI info (if requested)
  if settings['readAMI'] and settings['MC']:
    ami_xss, ami_eff, ami_k = read_ami_info()

  # Output file name
  OutName = get_output_file_name(settings)

  ####################################
  # Output file to compare with CxAOD
  ####################################
  if settings['cxaod_validation']:
    cxaod_val_output_file_name = '{}_{}_CxAOD_validation'.format(settings['Dataset'], settings['DSID'] if settings['MC'] else settings['dataPeriod'])
    try:
      cxaod_val_output_file = open(cxaod_val_output_file_name, 'w')
    except IOError:
      print('ERROR: {} can not be opened, exiting...'.format(cxaod_val_output_file_name))
      sys.exit(1)
    if not settings['MC']:
      evt_numbers_2_check = {
        #280673 : ['1023459146', '1823260099'],
        #279813 : ['1232041764'],
        #281411 : ['429262800'],
        #284213 : ['2024600413'],
        #280500 : ['13147639'],
        #279598 : ['265217844'],
        #281070 : ['26110215'],
        #278912 : ['480630466'],
        #283780 : ['1534675486', '1603695290', '41689298'],
        #281385 : ['1422751785', '1835401016', '768885972'],
        #280319 : ['756349330'],
        #279284 : ['584618993'],
        #284006 : ['99019668'],
        #279984 : ['522739244'],
        #280950 : ['2390242622'],
        #280862 : ['2234522836'],
        #282992 : ['1891093653', '1909753445'],
      }
    else: # MC
      evt_numbers_2_check = ['2']

  # Print selection definitions
  print('############################################')
  print('Selection definitions:')
  print('MUisoWP = {}'.format(settings['MUisoWP']))
  print('ELisoWP = {}'.format(settings['ELisoWP']))
  print('minLepPt [GeV] = {}'.format(settings['minLepPt']))
  print(vars(settings['SelectionClass']))
  print('############################################')

  # Get sumWeights (MC only)
  sumWeights = get_sum_of_weights(settings)

  #############################################
  # Open TFile/TDirectory and loop over TTrees
  #############################################

  full_input_file_name = '{}{}{}'.format(get_xrootd_prefix(settings['inputPath']) if not settings['noxrootd'] else '', settings['inputPath'], settings['inputFile'])
  print("INFO: Opening {}".format(full_input_file_name))
  tfile = TFile.Open(full_input_file_name)
  if not tfile:
    print("ERROR: {} not found, exiting...".format(full_input_file_name))
    sys.exit(0)

  # Get TNamed object with weight names
  if settings['MC'] and settings['getWeightSysts']:
    if 'TTbar' not in settings['Dataset']:  # Temporary (remove for v12 TTrees)
      TNamedWeights = tfile.Get('WeightNames')
      if not TNamedWeights:
        print('WARNING: WeightNames TNamed object does not exist, will use index 0 of the nominal weight')
        NominalWeightIndex = 0
        WeightNamesArray = ['nominal']
      else:
        WeightNames = TNamedWeights.GetTitle()
        if 'SEP' in WeightNames: # new files
          WeightNamesArrayTmp = WeightNames.split('SEP')
        else: # old files
          WeightNamesArrayTmp = WeightNames.split(':')
        # Temporary (remove these lines)
        WeightNamesArray = [x.replace('.','_') for x in WeightNamesArrayTmp]
        WeightNamesArray = [x.replace('=','') for x in WeightNamesArray]
        WeightNamesArray = [x.replace(' ','') for x in WeightNamesArray]
        WeightNamesArray = [x.replace(',','') for x in WeightNamesArray]
        WeightNamesArray = [x.replace(':','') for x in WeightNamesArray]
        if ' nominal ' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index(' nominal ')
          WeightNamesArray[NominalWeightIndex] = 'nominal'
        elif 'nominal' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('nominal')
        elif 'Weight' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('Weight')
          WeightNamesArray[NominalWeightIndex] = 'nominal' # Weight -> nominal
        elif 'WeightMEWeight' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('WeightMEWeight')
          WeightNamesArray[NominalWeightIndex] = 'nominal' # WeightMEWeight -> nominal
        elif 'nominalmuR1_0muF2_0' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('nominalmuR1_0muF2_0')
          WeightNamesArray[NominalWeightIndex] = 'nominal' # WeightMEWeight -> nominal
        else:
          print('ERROR: nominal MC event weight not found, exiting...')
          sys.exit(1)
        print("INFO: Using index {} as nominal weight".format(NominalWeightIndex))
        # Protection to identify a sample where the nominal weight is not at index 0 (if that is the case, I need to modify the case where I don't use syst weights)
        if NominalWeightIndex != 0:
          print('ERROR: Nominal weight is not at index 0! Update code! exiting...')
          sys.exit(1)
    else:  # read hardcoded list of variations (needed for v11 TTrees due to a bug in maker)
      with open('weight_names.txt', 'r') as ifile:
        WeightNamesArray = [line.replace('\n', '') for line in ifile.readlines()]
        if ' nominal ' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index(' nominal ')
          WeightNamesArray[NominalWeightIndex] = 'nominal'
        elif 'nominal' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('nominal')
        elif 'Weight' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('Weight')
          WeightNamesArray[NominalWeightIndex] = 'nominal' # Weight -> nominal
        elif 'WeightMEWeight' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('WeightMEWeight')
          WeightNamesArray[NominalWeightIndex] = 'nominal' # WeightMEWeight -> nominal
        elif 'nominalmuR1_0muF2_0' in WeightNamesArray:
          NominalWeightIndex = WeightNamesArray.index('nominalmuR1_0muF2_0')
          WeightNamesArray[NominalWeightIndex] = 'nominal' # WeightMEWeight -> nominal
        else:
          print('ERROR: nominal MC event weight not found, exiting...')
          sys.exit(1)
        print("INFO: Using index {} as nominal weight".format(NominalWeightIndex))
        # Protection to identify a sample where the nominal weight is not at index 0 (if that is the case, I need to modify the case where I don't use syst weights)
        if NominalWeightIndex != 0:
          print('ERROR: Nominal weight is not at index 0! Update code! exiting...')
          sys.exit(1)
  elif settings['MC']:
    NominalWeightIndex = 0
    WeightNamesArray = ['nominal']

  # Get TDirectoryFile with all the TTrees
  tdir = TDirectoryFile()
  tdir = tfile.Get(settings['TDirectoryName'])
  if not tdir:
    print("ERROR: "+settings['TDirectoryName']+" not found, exiting...")
    sys.exit(0)

  # Get list of TTrees
  if settings['Perf']:
    printMemory(0)
  Keys = tdir.GetListOfKeys()
  keys = list(set([key.GetName() for key in Keys]))

  # Set list of requested TTrees
  if settings['TTrees'] == 'nominal':  # requested to run over nominal only
    if 'nominal' in keys:
      keys = ['nominal']
    else:
      print("ERROR: nominal TTree not found in input file, exiting...")
  elif 'Full' not in settings['TTrees']:  # requested to run over a subset of TTrees
    TTreesList = TTrees.split(',') # list of TTrees separated by comma
    # check if all requested TTrees exist
    for name in TTreesList:
      if name not in keys:
        print("ERROR: requested TTree ("+name+") not found, exiting...")
        sys.exit(0)
    # Set keys to the requested list of TTrees
    keys = TTreesList

  # Loop over TTrees
  for ttree_counter, TTreeName in enumerate(keys):
    if 'AF2' in TTreeName:
      continue
    print('INFO: TTreeName = {}'.format(TTreeName))

    nominal = True
    if TTreeName != 'nominal':  # systematic TTree(s)
      nominal = False
      fillTruth = False
      ApplySFsysts = False  # protection

    # Open TFile
    if settings['Debug']: print("DEBUG: Opening "+settings['inputPath'] + settings['inputFile'])
    tfile = TFile.Open('{}{}{}'.format(get_xrootd_prefix(settings['inputPath']) if not settings['noxrootd'] else '', settings['inputPath'], settings['inputFile']))
    if not tfile:
      print("ERROR: {} not found, exiting...".format(settings['inputPath'] + settings['inputFile']))
      sys.exit(0)

    # Open corresponding directory
    tdir = TDirectoryFile()
    tdir = tfile.Get(settings['TDirectoryName'])
    if not tdir:
      print("ERROR: " + settings['TDirectoryName'] + " not found, exiting...")
      sys.exit(0)

    # Get TTree
    if settings['Debug']: print("DEBUG: Get " + TTreeName + " TTree")
    tree = tdir.Get(TTreeName)
    if not tree:
      print("ERROR: "+TTreeName+" not found, exiting...")
      sys.exit(0)
    totalEvents = tree.GetEntries()
    print("INFO: TotalEvents in " + TTreeName + ": " + str(totalEvents))

    ###################
    # Book histograms
    ###################

    Histograms = dict()
    Histograms_2D = dict()
    h_btagWeight = dict() # quantile of leading b-tagged jet
    h_btagWeight_vsObs = dict() # quantile of leading b-tagged jet vs observable
    h_btagCombWeight = dict() # combination of quantiles from leading and subleading b-tagged jets
    h_btagCombWeight_vsObs = dict() # combination of quantiles from leading and subleading b-tagged jets vs observable

    # Cutflow histograms
    Types = ["reco"]
    if settings['MC'] and settings['fillTruth'] and settings['Channel'] != 'ELMU':
      Types.append("truth")
    if not settings['fillReco']:
      Types.remove("reco")
    for Type in Types:
      h_cutflow[Type] = TH1D("cutflow_"+Type,"", 1, 1, 2)
      h_cutflow[Type].SetCanExtend(TH1D.kAllAxes)
      h_cutflow_all[Type] = h_cutflow[Type].GetXaxis().FindBin("All")
      h_cutflow_trigger[Type] = h_cutflow[Type].GetXaxis().FindBin("Trigger")
      h_cutflow_jetCleaning[Type] = h_cutflow[Type].GetXaxis().FindBin("Jet cleaning")
      h_cutflow_xAHselections[Type] = h_cutflow[Type].GetXaxis().FindBin(Type+" Selections")
      h_cutflow_lepton[Type] = h_cutflow[Type].GetXaxis().FindBin("Lepton")
      h_cutflow_Zmass[Type] = h_cutflow[Type].GetXaxis().FindBin("Zmass")
      h_cutflow_met[Type] = h_cutflow[Type].GetXaxis().FindBin("MET")
      h_cutflow_jet[Type] = h_cutflow[Type].GetXaxis().FindBin("Jet")
      if Type == 'reco':
        h_cutflow_Te1[Type] = h_cutflow[Type].GetXaxis().FindBin("Te1")
        h_cutflow_Ti2[Type] = h_cutflow[Type].GetXaxis().FindBin("Ti2")

    # Distribution of weights/efficiencies
    if settings['MC']:
      h_mcEventWeight = TH1D("mcEvtWeight","",20000,-100,100)
      h_sampleWeight = TH1D("sampleWeight","",20000,-100,100)
      h_leptonSF = TH1D("leptonSF","",1000,-5,5)
      h_leptonRecoSF = TH1D("leptonRecoSF","",1000,-5,5)
      h_leptonIsoSF = TH1D("leptonIsoSF","",1000,-5,5)
      h_leptonPIDSF = TH1D("leptonPIDSF","",1000,-5,5)
      h_leptonTrigSF = TH1D("leptonTrigSF","",1000,-5,5)
      h_leptonTrigSF_vs_Zpt = TH2D("leptonTrigSF_vs_Zpt","",len(Binning['Zpt'])-1,Binning_array['Zpt'],1000,array.array('d',[-5+0.01*x for x in range(1001)]))
      h_leptonTrigMCEff = TH1D("leptonTrigMCEff","",1000,0,1)
      h_leptonTrigMCEff_vs_Zpt = TH2D("leptonTrigMCEff_vs_Zpt","",len(Binning['Zpt'])-1,Binning_array['Zpt'],1000,array.array('d',[0.001*x for x in range(1001)]))
      h_jvtSF = TH1D("jvtSF","",1000,-5,5)
      h_btaggingSF = TH1D("btaggingSF","",1000,-5,5)
      h_totalWeight_woBTagSF = TH1D("totalWeight_woBTagSF","",20000,-1,1)

    ##################################
    # Histograms for each observable

    # Number of interactions per bunch crossing (<mu>/actualMu)
    if nominal:
      h_correctedAverageMu = TH1D("correctedAverageMu","",100,0,100)
      h_correctedAverageMu.Sumw2()
      h_correctedAndScaledAverageMu = TH1D("correctedAndScaledAverageMu","",100,0,100)
      h_correctedAndScaledAverageMu.Sumw2()
      h_correctedActualMu = TH1D("correctedActualMu","",100,0,100)
      h_correctedActualMu.Sumw2()
      h_correctedAndScaledActualMu = TH1D("correctedAndScaledActualMu","",100,0,100)
      h_correctedAndScaledActualMu.Sumw2()
      h_actualInteractionsPerCrossing = TH1D("actualInteractionsPerCrossing","",100,0,100)
      h_actualInteractionsPerCrossing.Sumw2()
      h_averageInteractionsPerCrossing = TH1D("averageInteractionsPerCrossing","",100,0,100)
      h_averageInteractionsPerCrossing.Sumw2()

    # BTagWeight (actually b-tagging quantile) binning
    btagWeightBins = { # works for any tagger and jet collection
      'lead' : [1.5,2.5,3.5,4.5,5.5],
      'comb' : [5.5,6.5,7.5,8.5,9.5,10.5,11.5,12.5,13.5,14.5,15.5],
    }
    btagWeightBinning = {
      'lead' : array.array('d', btagWeightBins['lead']),
      'comb' : array.array('d', btagWeightBins['comb']),
    }

    # Get full list of variables
    variables = dict()
    if nominal and not settings['ApplySFsysts']:
      variables = copy.deepcopy(AllVariables)
    else:
      variables = copy.deepcopy(Observables)

    # Do not fill inclusive Z+jet distributions when doing SF variations
    Variables = dict()
    if settings['ApplySFsysts']:
      for key, value in variables.items():
        List = []
        for val in value:
          if val != '': List.append(val)
        Variables[key] = List
    else:
      Variables = variables

    # 1D histogram for new hf truth distribution
    if 'hf_jet0_pt' in ExtraTruthVariables:
      var = 'hf_jet0_pt'
      key = var + '_truth'
      hist_name = key
      if var in Binning:
        if len(Binning[var]) > 3: # binning given by array
          Histograms[key] = TH1D(hist_name, '', len(Binning[var])-1, Binning_array[var])
        else: # binning given by nbins, minVal and maxVal
          Histograms[key] = TH1D(hist_name, '', Binning[var][0], Binning[var][1], Binning[var][2])
      else:
        print('ERROR: no binning provided for this variable: '+var+', exiting...')
        sys.exit(0)
      Histograms[key].Sumw2()

    # 1D histograms for all variables
    for Type in Types: # loop over reco/truth
      # Loop over variables
      for var in Variables:
        # Loop over requested reco tag scenarios ('': inclusive and/or 'Te1' and/or 'Ti2')
        TagOptions = Variables[var] if Type == "reco" else ['']
        for tagType in TagOptions:
          # Loop over quark flavours
          for flavour in settings['Flavours'][Type]:

            if flavour != '': # skip non-inclusive flavour distributions for non observables and for tagTypes not listed for observables
              if var not in Observables: continue
              else:
                if tagType not in Observables[var] and Type != 'truth': continue
            if Type == "truth" and var not in Observables and var not in ExtraTruthVariables: continue
            #if Type == 'truth' and flavour == '': continue # Temporary (I want truth inclusive Z+jet)

            extraFlavour = '_'+flavour if flavour != '' else ''
            extraTag = ''
            if flavour == '' and tagType != '':
              extraTag = '_'+tagType
            elif flavour != '':
              extraTag = tagType

            # Hists with systematically-varied event weights
            obs_weight_variations = copy.deepcopy(Observables)
            obs_weight_variations['Zmass'] = ['Te1', 'Ti2']
            if settings['MC'] and settings['getWeightSysts'] and var in obs_weight_variations and tagType in obs_weight_variations[var] and Type == 'reco':
              for iSystWgt in range(len(WeightNamesArray)):  # loop over event weight indexes
                sysWgtName = WeightNamesArray[iSystWgt]

                # observable's distribution
                key4systWgtHist = var + extraFlavour + extraTag + '_EvtWgtVar_' + sysWgtName
                HistName = flavour + var + '_' + tagType + '_EvtWgtVar_' + sysWgtName
                if var in Binning:
                  if len(Binning[var]) > 3: # binning given by array
                    Histograms[key4systWgtHist] = TH1D(HistName, '', len(Binning[var]) - 1, Binning_array[var])
                  else: # binning given by nbins, minVal and maxVal
                    Histograms[key4systWgtHist] = TH1D(HistName, '', Binning[var][0], Binning[var][1], Binning[var][2])
                else:
                  print('ERROR: no binning provided for this variable: '+var+', exiting...')
                  sys.exit(0)
                Histograms[key4systWgtHist].Sumw2()

                # b-tagging quantile vs observables
                if tagType in obs_weight_variations[var]:
                  # Quantile from leading b-tagged jet
                  hist_name = "{0}{1}LeadQuantile_vs_{2}_{3}_EvtWgtVar_{4}".format(flavour, settings['Tagger'], var, tagType, sysWgtName)
                  key4systWgtHist  = var + extraFlavour + extraTag + '_EvtWgtVar_' + sysWgtName
                  if len(Binning[var]) > 3:  # binning given by array
                    h_btagWeight_vsObs[key4systWgtHist] = TH2D(hist_name, "", len(Binning[var]) - 1, Binning_array[var], len(btagWeightBinning['lead']) - 1, btagWeightBinning['lead'])
                  else:  # binning given by nbins, minVal and maxVal
                    h_btagWeight_vsObs[key4systWgtHist] = TH2D(hist_name, "", Binning[var][0], Binning[var][1], Binning[var][2], len(btagWeightBinning['lead']) - 1, btagWeightBinning['lead'])
                  h_btagWeight_vsObs[key4systWgtHist].Sumw2()
                  # Combination of quantiles from leading and subleading b-tagged jet
                  if tagType == 'Ti2':
                    hist_name = "{0}{1}CombQuantile_vs_{2}_{3}_EvtWgtVar_{4}".format(flavour, settings['Tagger'], var, tagType, sysWgtName)
                    key4systWgtHist  = var + extraFlavour + extraTag + '_EvtWgtVar_' + sysWgtName
                    if len(Binning[var]) > 3:  # binning given by array
                      h_btagCombWeight_vsObs[key4systWgtHist] = TH2D(hist_name, "", len(Binning[var]) - 1, Binning_array[var], len(btagWeightBinning['comb']) - 1, btagWeightBinning['comb'])
                    else:  # binning given by nbins, minVal and maxVal
                      h_btagCombWeight_vsObs[key4systWgtHist] = TH2D(hist_name, "", Binning[var][0], Binning[var][1], Binning[var][2], len(btagWeightBinning['comb']) - 1, btagWeightBinning['comb'])
                    h_btagCombWeight_vsObs[key4systWgtHist].Sumw2()

            # Loop over b-tagging SF, muon SFs and JVT SF systematics (MC nominal only) + nominal
            for source, nNP in SFsysts.items():
              # Protections
              if not settings['MC'] or not nominal:  # MC nominal or data
                if source != 'nominal':
                  continue
              if Type == "truth" and source != "nominal":
                  continue  # SFs systematics only in MC reco nominal
              if not settings['ApplySFsysts'] and source != 'nominal':
                  continue  # user did not request additional hists for each SF syst
              if settings['ApplySFsysts'] and source != 'nominal':  # variate only distributions for observables
                if var not in Observables:
                  continue
                else:
                  if tagType not in Observables[var]:
                    continue
              if flavour != '' and source != 'nominal':
                continue  # skip non-inclusive flavours for SF variations (I personally don't need this anymore)
              if tagType == '' and source == 'BTag':
                continue  # no BTag SF systematics in inclusive Z+jets events
              if 'EL' in source and settings['Channel'] == 'MU':
                continue  # skip EL systematics in MU
              if 'MU' in source and settings['Channel'] == 'EL':
                continue  # skip MU systematics in EL
              if source != 'nominal' and tagType == '':
                continue  # skip SF variations for inclusive case

              # Loop over SF NPs
              for inp in range(1, nNP+1):

                # Key for Histogram dict
                extraType = '_truth' if Type == 'truth' else ''
                extraSFsyst = '' if source == 'nominal' else '_' + source + '_' + str(inp)
                key = var + extraType + extraFlavour + extraTag + extraSFsyst

                # Create TH1D histogram(s) for all variables (except b-tagging quantiles)
                HistName = flavour+var+'_'+tagType+extraType+extraSFsyst if tagType != '' else flavour+var+extraType+extraSFsyst
                if var in Binning:
                  if len(Binning[var]) > 3: # binning given by array
                    Histograms[key] = TH1D(HistName,'',len(Binning[var])-1,Binning_array[var])
                  else: # binning given by nbins, minVal and maxVal
                    Histograms[key] = TH1D(HistName,'',Binning[var][0],Binning[var][1],Binning[var][2])
                else:
                  print('ERROR: no binning provided for this variable: '+var+', exiting...')
                  sys.exit(0)
                Histograms[key].Sumw2()

                #if var == 'jet_y' and Type == "reco":
                #  for ptbin in pTbins: # loop over pt bins for eta distribution
                #    PtBin = ptbin.split(":")
                #    HistName = flavour+var+'_'+tagType+"__pt_"+PtBin[0]+"_"+PtBin[1] if tagType != '' else flavour+var+"__pt_"+PtBin[0]+"_"+PtBin[1]
                #    Histograms[var+'__pt_'+PtBin[0]+'_'+PtBin[1]+extraFlavour+extraTag] = TH1D(HistName,"",Binning[var][0],Binning[var][1],Binning[var][2])

                # b-tagging quantile vs observables
                if var in Observables and Type == "reco" and tagType != '' and tagType in Observables[var]:
                  # Quantile from leading b-tagged jet
                  Name = "{0}{1}LeadQuantile_vs_{2}_{3}{4}".format(flavour, settings['Tagger'], var, tagType, extraSFsyst)
                  key  = var+extraFlavour+extraTag+extraSFsyst
                  if len(Binning[var]) > 3: # binning given by array
                    h_btagWeight_vsObs[key] = TH2D(Name,"",len(Binning[var])-1,Binning_array[var],len(btagWeightBinning['lead'])-1,btagWeightBinning['lead'])
                  else: # binning given by nbins, minVal and maxVal
                    h_btagWeight_vsObs[key] = TH2D(Name,"",Binning[var][0],Binning[var][1],Binning[var][2],len(btagWeightBinning['lead'])-1,btagWeightBinning['lead'])
                  h_btagWeight_vsObs[key].Sumw2()
                  # Combination of quantiles from leading and subleading b-tagged jet
                  if tagType == 'Ti2':
                    Name = "{0}{1}CombQuantile_vs_{2}_{3}{4}".format(flavour, settings['Tagger'], var, tagType, extraSFsyst)
                    key  = var+extraFlavour+extraTag+extraSFsyst
                    if len(Binning[var]) > 3: # binning given by array
                      h_btagCombWeight_vsObs[key] = TH2D(Name,"",len(Binning[var])-1,Binning_array[var],len(btagWeightBinning['comb'])-1,btagWeightBinning['comb'])
                    else: # binning given by nbins, minVal and maxVal
                      h_btagCombWeight_vsObs[key] = TH2D(Name,"",Binning[var][0],Binning[var][1],Binning[var][2],len(btagWeightBinning['comb'])-1,btagWeightBinning['comb'])
                    h_btagCombWeight_vsObs[key].Sumw2()

      # Loop over quark flavours
      for flavour in settings['Flavours'][Type]:
        # Inclusive B-tagging quantile distributions
        if Type == "reco":
          for tagType in ['Te1', 'Ti2']:
            # Quantile from leading b-tagged jet
            h_btagWeight[flavour+'_'+tagType] = TH1D(flavour + settings['Tagger'] + "LeadQuantile_{}".format(tagType), "", len(btagWeightBinning['lead']) - 1, btagWeightBinning['lead'])
            h_btagWeight[flavour+'_'+tagType].Sumw2()
          # Combination of quantiles from leading and subleading b-tagged jet
          h_btagCombWeight[flavour+'_Ti2'] = TH1D(flavour + settings['Tagger'] + "CombQuantile_Ti2", "", len(btagWeightBinning['comb']) - 1, btagWeightBinning['comb'])
          h_btagCombWeight[flavour+'_Ti2'].Sumw2()
          # TODO Add also SFsyst variations!

    # Book TMs for unfolding (only for observables)
    if settings['MC'] and settings['makeTMs']:
      print("INFO: Will make transfer matrices")
      for flavour in settings['Flavours']["truth"]: # loop over hadron flavours
        if flavour == '':
          continue
        if flavour == 'FlavL_':
          continue
        for obs in Observables: # loop over observables
          # Loop over requested tag scenarios ('': inclusive and/or 'Te1' and/or 'Ti2')
          for tagType in Observables[obs]:
            if tagType == '': continue
            # Loop over b-tagging SF, muon SFs and JVT SF systematics (MC nominal only) + nominal
            for source, nNP in SFsysts.items():
              if not nominal and source != 'nominal':
                continue  # SF systematics only for MC nominal
              #if not ApplySFsysts and source != 'nominal': continue # user did not request additional hists for each SF syst
              if source != 'nominal':
                continue  # skip non-nominal TMs because I personally don't need this
              if 'EL' in source and settings['Channel'] == 'MU':
                continue  # skip EL systematics in MU
              if 'MU' in source and settings['Channel'] == 'EL':
                continue  # skip MU systematics in EL
              # Loop over NPs
              for inp in range(1, nNP+1):
                extraSFsyst = '' if source == 'nominal' else '_' + source + '_' + str(inp)
                key = flavour+obs + '_' + tagType + extraSFsyst
                HistName = 'TM_' + key
                if len(Binning[obs])>3: # binning given by array
                  Histograms_2D[key] = TH2D(HistName,'',len(Binning[obs])-1,Binning_array[obs],len(Binning[obs])-1,Binning_array[obs])
                else: # binning given by nbins, minVal and maxVal
                  Histograms_2D[key] = TH2D(HistName,'',Binning[obs][0],Binning[obs][1],Binning[obs][2],Binning[obs][0],Binning[obs][1],Binning[obs][2])
                Histograms_2D[key].Sumw2()

    # Set branches
    Branches = dict()
    get_branches(tree, Branches, settings, nominal)

    if settings['Perf']:
      print("PERF: Time spent until event loop starts = "+str(time.perf_counter() - start))

    ###################
    # Loop over events
    ###################
    time_counter = 0
    sf_systs_sizes_checked = False  # protection done only once per input file
    for event_counter in range(0, totalEvents):

      # Get entry
      tree.GetEntry(event_counter)

      event_counter += 1
      if (settings['Debug'] or settings['Test'] or settings['Ref']) and event_counter > settings['DebugMAXevents']:
        break  # break event loop
      if settings['Debug']:
        print("DEBUG: EventNumber: " + str(Branches["eventNumber"][0]))
      if event_counter == 1:
        print("INFO: Running on event #" + str(event_counter) + " of " + str(totalEvents)+" events")
      if event_counter % 100000 == 0:
        print("INFO: Running on event #" + str(event_counter) + " of "+str(totalEvents)+" events")
      if settings['Perf'] and event_counter % 1000 == 0:
        printMemory(1)
        if not time_counter:
          t = time.perf_counter()
        else:
          t = time.perf_counter() - time_counter
        print("PERF: evts/second = " + str(1000/t))
        time_counter += t
 
      VariablesValues = dict()

      # Flag this event for CxAOD vs xAH comparison?
      cxaod_val_debug = False
      #if cxaod_validation and not MC and cxaod_check(evt_numbers_2_check, Branches["runNumber"][0], str(Branches["eventNumber"][0])):
      #  cxaod_val_debug = True
      #elif cxaod_validation and MC and str(Branches['eventNumber'][0]) in evt_numbers_2_check:
      if settings['cxaod_validation'] and settings['MC'] and str(Branches['eventNumber'][0]) in evt_numbers_2_check:
      #elif cxaod_validation and MC:
        cxaod_val_debug = True
      if settings['cxaod_validation'] and not settings['MC']:
        cxaod_val_debug = True

      ##################################
      # Fill mcEvtWeight for all events
      ##################################
      if settings['MC']:
        mcEventWeights = dict()
        if nominal:
          mcEventWeights['nominal'] = Branches["mcEventWeights"][NominalWeightIndex]
          h_mcEventWeight.Fill(mcEventWeights['nominal'])
          if settings['getWeightSysts']:
            for iSystWgt in range(len(WeightNamesArray)):  # loop over event weight indexes
              mcEventWeights['EvtWgtVar_' + WeightNamesArray[iSystWgt]] = Branches["mcEventWeights"][iSystWgt]
        else:
          mcEventWeights['nominal'] = Branches["mcEventWeight"][0]
          h_mcEventWeight.Fill(mcEventWeights['nominal'])

      ####################
      # Get event weights
      ####################
      SampleWeights = dict() # sample weights (nominal + systematically-varied event weights)
      SampleWeights['nominal'] = 1.0
      if settings['useSampleWeights']:
        mcChannelNumber = Branches["mcChannelNumber"][0]
        if mcChannelNumber == 700100: # Sherpa 2.2.10
          if '700128' in settings['inputPath']: mcChannelNumber = 700128
          elif '700129' in settings['inputPath']: mcChannelNumber = 700129
          elif '700130' in settings['inputPath']: mcChannelNumber = 700130
        if not settings['readAMI']: SampleWeights['nominal'] *= Branches["weight"][0] / sumWeights[mcChannelNumber]
        else: # read AMI info again
          for iSystWgt in range(len(WeightNamesArray)):  # loop over event weight indexes
            if not settings['getWeightSysts']:
              if iSystWgt != NominalWeightIndex: continue
              SampleWeights['nominal']  = mcEventWeights['nominal']
              SampleWeights['nominal'] *= ami_xss[mcChannelNumber]
              SampleWeights['nominal'] *= ami_eff[mcChannelNumber]
              SampleWeights['nominal'] *= ami_k[mcChannelNumber]
              SampleWeights['nominal'] /= sumWeights[mcChannelNumber]
            else:
              SystWgtName = 'EvtWgtVar_' + WeightNamesArray[iSystWgt]
              SampleWeights[SystWgtName]  = mcEventWeights[SystWgtName]
              SampleWeights[SystWgtName] *= ami_xss[mcChannelNumber]
              SampleWeights[SystWgtName] *= ami_eff[mcChannelNumber]
              SampleWeights[SystWgtName] *= ami_k[mcChannelNumber]
              SampleWeights[SystWgtName] /= sumWeights[mcChannelNumber]
              if iSystWgt == NominalWeightIndex:
                SampleWeights['nominal'] = mcEventWeights['nominal']
                SampleWeights['nominal'] *= ami_xss[mcChannelNumber]
                SampleWeights['nominal'] *= ami_eff[mcChannelNumber]
                SampleWeights['nominal'] *= ami_k[mcChannelNumber]
                SampleWeights['nominal'] /= sumWeights[mcChannelNumber]
          h_sampleWeight.Fill(SampleWeights['nominal'])
        if settings['Debug']:
          print("DEBUG: mcEventWeight: "+str(mcEventWeights['nominal']))
          print("DEBUG: mcChannelNumber: "+str(mcChannelNumber))
          print("DEBUG: sumWeights: "+str(sumWeights[mcChannelNumber]))
          print("DEBUG: sampleWeight: "+str(SampleWeights['nominal']))
        if settings['usePRW']:
          if settings['ApplySFsysts']:
            SampleWeights['weight_pileup_1'] = SampleWeights['nominal'] * Branches["weight_pileup_up"][0]
            SampleWeights['weight_pileup_2'] = SampleWeights['nominal'] * Branches["weight_pileup_down"][0]
          for wgtName in SampleWeights:
            SampleWeights[wgtName] *= Branches["weight_pileup"][0]
        h_totalWeight_woBTagSF.Fill(SampleWeights['nominal'])  # total except for b-tagging SF

      # Create array of weights (needed to get alternative weights due to SF systematics)
      preFinalWeight = dict()
      if settings['MC'] and nominal and settings['ApplySFsysts']:
        for source, NPs in SFsysts.items():  # Loop over sources of SF systematics
          if settings['Channel'] == 'MU' and 'EL' in source: continue  # skip electron SF systematics in muon channel
          elif settings['Channel'] == 'EL' and 'MU' in source: continue  # skip muon SF systematics in electron channel
          if source == 'nominal': preFinalWeight[source] = SampleWeights['nominal']
          else:  # systematic
            for inp in range(1, NPs+1):  # Loop over NPs
              preFinalWeight[source+'_'+str(inp)] = SampleWeights['nominal']
      if settings['MC'] and len(SampleWeights.keys()) > 1:  # This can happen if getWeightSysts is True and/or if ApplySFsysts is True due to weight_pileup_x
        for wgtName in SampleWeights:
          if wgtName not in preFinalWeight: preFinalWeight[wgtName] = SampleWeights[wgtName]
      if 'nominal' not in preFinalWeight:
        preFinalWeight['nominal'] = SampleWeights['nominal']

      ##########################################
      # Get number of interactions per crossing
      ##########################################
      if nominal:
        correctedAndScaledAverageMu = Branches["correctedAndScaledAverageMu"][0]
        averageInteractionsPerCrossing = Branches["averageInteractionsPerCrossing"][0]
        actualInteractionsPerCrossing = Branches["actualInteractionsPerCrossing"][0]
        correctedAndScaledActualMu = Branches["correctedAndScaledActualMu"][0]
        correctedAverageMu = Branches["correctedAverageMu"][0]
        correctedActualMu = Branches["correctedActualMu"][0]
    
      if settings['Debug'] and nominal: print("DEBUG: averageInteractionsPerCrossing: " + str(averageInteractionsPerCrossing))

      ##################################################################
      # Apply event/object selections and fill reco/truth distributions
      ##################################################################
      passReco = False
      passTruth = False
      ToFillRecoFlavours = ['']
      ToFillTruthFlavours = ['']
      ToFillRecoTagCases = ['']
      ToFillTruthTagCases = ['']
      # Loop over reco/truth events
      for Type in Types:

        ##############################
        # Event and object selections
        ##############################
        if settings['Debug']: print("DEBUG: Apply " + Type + " event/object selections")

        h_cutflow[Type].Fill(h_cutflow_all[Type],1)

        ########################
        # Early rejection
        ########################
        if settings['MC'] and settings['usePRW'] and not settings['ApplySFsysts'] and Branches["weight_pileup"][0] == 0:
          if settings['Debug']: print("DEBUG: Early rejection of event (eventNumber={}) due to PRW".format(Branches['eventNumber'][0]))
          continue  # skip event for MC

        if settings['Debug'] and settings['MC']: print("DEBUG: RandomRunNumber: " + str(Branches["RandomRunNumber"][0]))

        ########################
        # C0: Trigger Selection
        ########################
        if Type == "reco": # MC and data
          TriggerList = get_trigger_list(Branches, settings)  # set list of triggers to be checked in this event
          passTrigger = False
          if settings['Debug']:
            print('Fired triggers:')
            print(Branches["passedTriggers"])
          for trigger in TriggerList:
            if settings['Debug']: print('Checking if {} trigger was fired'.format(trigger))
            if trigger in Branches["passedTriggers"]:
              if settings['Debug']: print(' {} was fired'.format(trigger))
              passTrigger = True
              break
            else:
              if settings['Debug']: print(' {} was NOT fired'.format(trigger))
          if not passTrigger:
            if settings['Debug']: print("DEBUG: Event not passed trigger selection")
            if cxaod_val_debug:
              print("DEBUG: Event {} from {} not passed trigger selection".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
            continue # skip event for reco
          if settings['Debug']: print("DEBUG: Event passed trigger selection")
          h_cutflow["reco"].Fill(h_cutflow_trigger["reco"],1)

        ###########################################################
        # C1: Jet Cleaning (MC reco only, in data already applied)
        ###########################################################
        if settings['MC'] and Type == "reco" and settings['SelectionClass'].jet_cleaning and settings['isMCsignal'] and nominal and not eventIsClean(Branches):
          if settings['Debug']: print("DEBUG: Event not passed jet cleaning")
          continue  # skip event for MC reco
        if settings['Debug']: print("DEBUG: Event passed jet cleaning")
        if Type == "reco": h_cutflow["reco"].Fill(h_cutflow_jetCleaning["reco"], 1)

        ###################################################################################
        # C2: Check decoration with truth/reco event selection decision and select leptons
        ###################################################################################

        # Check truth/reco event selection decoration
        passEventSelections = True
        if settings['MC']:  # for data only events passing reco selections are stored
          if Type == "reco":
            #if not nominal: # syst ttree (removed since it is not needed, a protection is added in case that is not true)
            #  if not getLeptonPassRecoSelection(Branches):
            #    passEventSelections = False
            #elif isMCsignal: # nominal tree for MC signal
            if settings['isMCsignal'] and nominal:  # nominal tree for MC signal
              if not getLeptonPassRecoSelection(Branches):
                passEventSelections = False
          else: # truth
            if not getLeptonPassTruthSelection(Branches):
              passEventSelections = False
        if not passEventSelections:
          if settings['Debug'] and settings['MC']: print("DEBUG: Event not passed passed xAH " + Type + " selections")
          if cxaod_val_debug:
            print("DEBUG: Event {} from {} not passed the NLepton selection".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
          continue  # skip event
        if settings['Debug'] and settings['MC']: print("DEBUG: Event passed xAH " + Type + " selections")
        if settings['MC'] and nominal:
          h_cutflow[Type].Fill(h_cutflow_xAHselections[Type], 1)
        # Select leptons
        SelectedLeptons = []
        nLeptons = getNLepton(Branches, settings['Channel']) if Type == "reco" else getNLeptonTruth(Branches, settings['Channel'])
        if settings['Channel'] == 'ELMU':
          nElectrons = getNLepton(Branches, 'EL')
        for ilep in range(0, nLeptons):  # loop over leptons
          if settings['Channel'] == 'ELMU':
            isMuon = False if ilep < nElectrons else True
          else:  # EL or MU
            isMuon = True if settings['Channel'] == 'MU' else False
          leptonPt = getLeptonPt (Branches, ilep, settings['Channel']) if Type == "reco" else getLeptonPtTruth(Branches, ilep, settings['Channel'])
          leptonEta = getLeptonEta(Branches, ilep, settings['Channel']) if Type == "reco" else getLeptonEtaTruth(Branches, ilep, settings['Channel'])
          leptonPhi = getLeptonPhi(Branches, ilep, settings['Channel']) if Type == "reco" else getLeptonPhiTruth(Branches, ilep, settings['Channel'])
          if Type == "reco" and not nominal:
            leptonCharge = getLeptonCharge(Branches, ilep, settings['Channel'])
          if Type == "reco":
            leptonM = getLeptonM(Branches, ilep, settings['Channel'])
          else:  # truth
            leptonE = getLeptonETruth(Branches, ilep, settings['Channel'])
          # kinematic selection
          minPt = settings['SelectionClass'].muon_minPt if isMuon else settings['SelectionClass'].el_minPt
          maxEta = settings['SelectionClass'].muon_maxEta if isMuon else settings['SelectionClass'].el_maxEta
          if Type == 'reco':
            lepton_eta_for_eta_cuts = leptonEta if isMuon else get_electron_calo_cluster_eta(Branches, ilep)
          else:
            lepton_eta_for_eta_cuts = leptonEta
          if leptonPt > minPt and abs(lepton_eta_for_eta_cuts) < maxEta:
            passExcludeTransitionRegion4Electrons = False if Type == 'reco' and not isMuon else True
            if Type == 'reco' and not isMuon:  # reco electron
              if settings['SelectionClass'].excludeTransitionRegion4Electrons:
                if abs(lepton_eta_for_eta_cuts) < 1.37 or abs(lepton_eta_for_eta_cuts) > 1.52:  # Outside transition region
                  passExcludeTransitionRegion4Electrons = True
              else:
                passExcludeTransitionRegion4Electrons = True
            if passExcludeTransitionRegion4Electrons:
              TLVLepton = iLepton()
              if Type == "reco":
                TLVLepton.SetPtEtaPhiM(leptonPt, leptonEta, leptonPhi, leptonM)
                if not nominal:
                  TLVLepton.charge = leptonCharge
                if settings['Channel'] == 'ELMU': TLVLepton.isMuon = isMuon
                TLVLepton.isTrigMatched, TLVLepton.matchedChains = isLeptonTrigMatched(Branches, ilep, settings['Channel'], TriggerList)
                if settings['Debug']:
                  print("DEBUG: Matched chains (lep pt = {}):".format(leptonPt))
                  print(TLVLepton.matchedChains)
                if settings['useSFs']:  # if not then SF is 1 (see HelperClasses)
                  # Is it an electron or a muon?
                  if settings['Channel'] == 'ELMU':
                    LeptonType = 'MU' if TLVLepton.isMuon else 'EL'
                  else:
                    LeptonType = settings['Channel']
                  # get array of Reco/Iso/PID/Trig SFs and TrigEff
                  RecoSF = getLeptonRecoSF(Branches, ilep, settings['Channel'])
                  IsoSF = getLeptonIsoSF(Branches, ilep, settings['Channel'])
                  if settings['Channel'] != 'ELMU':
                    PIDSF = getMuonTTVASF(Branches, ilep) if settings['Channel'] == 'MU' else getElectronPIDSF(Branches, ilep)
                    TrigSF = getMuonTrigEffSF(Branches, ilep, settings['campaign']) if settings['Channel'] == 'MU' else getElectronTrigEffSF(Branches, ilep)
                    TrigEff = getMuonTrigEff(Branches, ilep, settings['campaign']) if settings['Channel'] == 'MU' else getElectronTrigEff(Branches, ilep)
                  else:  # ELMU
                    PIDSF = getMuonTTVASF(Branches, ilep - nElectrons) if TLVLepton.isMuon else getElectronPIDSF(Branches, ilep)
                    TrigSF = getMuonTrigEffSF(Branches, ilep - nElectrons, settings['campaign']) if TLVLepton.isMuon else getElectronTrigEffSF(Branches, ilep)
                    TrigEff = getMuonTrigEff(Branches, ilep - nElectrons, settings['campaign']) if TLVLepton.isMuon else getElectronTrigEff(Branches, ilep)
                  # Use arrays to fill dicts
                  TLVLepton.RecoSF = {(LeptonType + 'Reco_' + str(isf) if isf else 'nominal'): value for isf, value in enumerate(RecoSF)}
                  TLVLepton.IsoSF = {(LeptonType + 'Iso_' + str(isf) if isf else 'nominal'): value for isf, value in enumerate(IsoSF)}
                  TLVLepton.PIDSF = {(LeptonType + 'PID_' + str(isf) if isf else 'nominal'): value for isf, value in enumerate(PIDSF)}
                  TLVLepton.TrigSF = {(LeptonType + 'Trig_' + str(isf) if isf else 'nominal'): value for isf, value in enumerate(TrigSF)}
                  if settings['Channel'] == 'ELMU':
                    TLVLepton.TrigEff = {(LeptonType + 'Trig_' + str(isf) if isf else 'nominal'): value for isf, value in enumerate(TrigEff)}
                  else:  # EL or MU
                    TLVLepton.TrigEff['nominal'] = TrigEff[0]
                  # Protection (performend only once per input file)
                  if settings['ApplySFsysts'] and not sf_systs_sizes_checked:
                    nRecoSFNPs = len(TLVLepton.RecoSF) - 1
                    nIsoSFNPs = len(TLVLepton.IsoSF) - 1
                    nPIDSFNPs = len(TLVLepton.PIDSF) - 1
                    nTrigNPs = len(TLVLepton.TrigSF) - 1
                    if (nRecoSFNPs != SFsysts[LeptonType + 'Reco'] or nIsoSFNPs != SFsysts[LeptonType+'Iso'] or nPIDSFNPs != SFsysts[LeptonType+'PID'] or nTrigNPs != SFsysts[LeptonType+'Trig']):
                      print("ERROR: Asked for SF variations but the provided number of NPs does not match with what is found in the TTree, exiting...")
                      sys.exit(0)
                    else:
                      sf_systs_sizes_checked = True
                  # Construct array of SFs
                  # Nominal SF (zero component): multiply all zero components
                  # Each SF variation: use all zero components except one (size equal to totalNPs+nom)
                  TLVLepton.SF['nominal'] = TLVLepton.RecoSF['nominal'] * TLVLepton.IsoSF['nominal'] * TLVLepton.PIDSF['nominal']  # trigger SF is event-level and is applied afterwards
              else:  # truth
                TLVLepton.SetPtEtaPhiE(leptonPt, leptonEta, leptonPhi, leptonE)
              # Save lepton
              SelectedLeptons.append(TLVLepton)

        ################################################
        # C3: NLepton and trigger matching requirements
        ################################################
        passNLeptonSelection = True
        if settings['SelectionClass'].lep_n != -1:
          if len(SelectedLeptons) != settings['SelectionClass'].lep_n: passNLeptonSelection = False
        if settings['Channel'] == 'ELMU':  # make sure one is an electron and the oher one is a muon
          nMuon = 0
          for lepton in SelectedLeptons:
            if lepton.isMuon: nMuon += 1
          if nMuon != 1: passNLeptonSelection = False
        if not passNLeptonSelection:
          if settings['Debug']: print("DEBUG: Event not passed NLepton selection")
          if cxaod_val_debug:
            print("DEBUG: Event {} from {} not passed the NLepton selection".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
          continue  # skip event
        if settings['Debug']: print("DEBUG: Event passed NLepton selection")

        # For systematic TTrees, make sure leptons are of oppposite charge
        if Type == "reco" and not nominal:
          if (SelectedLeptons[0].charge * SelectedLeptons[1].charge) != -1:
            continue  # skip event
          if settings['Debug']: print("DEBUG: Event passed lepton charge selection")

        passTriggerMatching = True
        if Type == "reco" and settings['SelectionClass'].triggerMatching:
          if settings['Debug']: print("DEBUG: SelectedLeptons[0].isTrigMatched = " + str(SelectedLeptons[0].isTrigMatched))
          if settings['Debug']: print("DEBUG: SelectedLeptons[1].isTrigMatched = " + str(SelectedLeptons[1].isTrigMatched))
          # request at least one signal lepton to be trigger matched
          if not SelectedLeptons[0].isTrigMatched and not SelectedLeptons[1].isTrigMatched: passTriggerMatching = False  # no signal lepton is matched
        if not passTriggerMatching:
          if settings['Debug']: print("DEBUG: Event not passed trigger matching requirement")
          if cxaod_val_debug:
            print("DEBUG: Event {} from {} not passed the trigger matching requirement".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
          continue # skip event
        if Type == "reco" and settings['SelectionClass'].triggerMatching and settings['Debug']: print("DEBUG: Event passed trigger matching requirement")
        h_cutflow[Type].Fill(h_cutflow_lepton[Type],1)

        ###############################
        # C4: Z mass requirement
        ###############################
        passZmassCut = False
        Zboson = SelectedLeptons[0] + SelectedLeptons[1]
        #if Type == "reco" and not nominal:  # protection (if I ever use truth in syst TTrees, I have to make this an evt selection and not a protection)
        #  if (SelectedLeptons[0].charge * SelectedLeptons[1].charge) != -1:
        #    raise RuntimeError('Same charge leptons were selected!')
        VariablesValues['Zmass_'+Type] = [Zboson.M()]
        VariablesValues['Zpt_'+Type] = [Zboson.Pt()]
        VariablesValues['Zabsy_'+Type] = [abs(Zboson.Rapidity())]
        ZmassCutResults = []
        for icut in range(0, len(settings['SelectionClass'].minZmass)):
          if Zboson.M() > settings['SelectionClass'].minZmass[icut] and Zboson.M() < settings['SelectionClass'].maxZmass[icut]: ZmassCutResults.append(True)
        if True in ZmassCutResults: passZmassCut = True
        if not passZmassCut:
          if settings['Debug']: print("DEBUG: Event not passed Zmass cut (Zmass == "+str(Zboson.M())+" )")
          if cxaod_val_debug:
            print("DEBUG: Event {} from {} not passed the Zmass cut (Zmass == {})".format(Branches["eventNumber"][0], Branches["runNumber"][0], Zboson.M()))
          continue  # skip event
        if settings['Debug']: print("DEBUG: Event passed Zmass cut")
        h_cutflow[Type].Fill(h_cutflow_Zmass[Type],1)

        ##############
        # C5: MET cut
        ##############
        if Type == "reco":
          MET = getMET(Branches)
          if hasattr(settings['SelectionClass'], 'maxMET'):  # maxMET cut exists in the chosen selection class
            if MET > settings['SelectionClass'].maxMET:
              if hasattr(settings['SelectionClass'], 'maxZPt4METcut'):  # maxMET cut only applied if ZPt < maxZPt4METcut
                if Zboson.Pt() < settings['SelectionClass'].maxZPt4METcut:  # MET cut not passed
                  if settings['Debug']: print("DEBUG: Event not passed MET cut (MET = {})".format(MET))
                  if cxaod_val_debug:
                    print("DEBUG: Event {} from {} not passed the MET cut".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
                  continue  # skip event for reco
              else:  # MET cut not passed
                if settings['Debug']: print("DEBUG: Event not passed MET cut (MET = {})".format(MET))
                if cxaod_val_debug:
                  print("DEBUG: Event {} from {} not passed the MET cut".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
                continue  # skip event for reco
          if hasattr(settings['SelectionClass'], 'minMET'):  # minMET cut exists in the chosen selection class
            if MET < settings['SelectionClass'].minMET:
              if settings['Debug']: print("DEBUG: Event not passed minMET cut (MET = {})".format(MET))
              continue  # skip event for reco

        if settings['Debug']: print("DEBUG: Event passed MET cut")
        h_cutflow[Type].Fill(h_cutflow_met[Type],1)

        ###############################
        # C6: At least one jet
        ###############################
        SelectedJets = []  # array of TLorentzVector jets
        bJets = []  # array of TLorentzVector reco b-tagged jets or truth b-jets
        cJets = []  # array of TLorentzVector truth c-jets
        hf_jet0_truth = None  # leading heavy flavour truth jet
        nJets = getNJet(Branches) if Type == "reco" else getNJetTruth(Branches)
        SumPt_jets = 0
        for var in ['pt', 'y', 'phi']:
          VariablesValues['jet_' + var + '_' + Type] = []
          for ijet in range(0, 2):
            VariablesValues['jet' + str(ijet) + '_' + var+ '_' + Type]  = []
        # Loop over jets
        for ijet in range(0, nJets):

          # Get jet's kinematics
          jetEta = getRecoJetEta(Branches, ijet) if Type == "reco" else getJetEtaTruth(Branches, ijet)
          jetPhi = getRecoJetPhi(Branches, ijet) if Type == "reco" else getJetPhiTruth(Branches, ijet)
          jetPt = getRecoJetPt(Branches, ijet) if Type == "reco" else getJetPtTruth(Branches, ijet)
          jetE = getRecoJetE(Branches, ijet) if Type == "reco" else getJetETruth(Branches, ijet)

          # Create an instance of the expanded TLorentzVector class
          TLVjet = iJet()
          TLVjet.SetPtEtaPhiE(jetPt, jetEta, jetPhi, jetE)
          jetRapidity = TLVjet.Rapidity()

          # Identify pileup jets (if requested)
          if Type == "reco" and settings['SelectionClass'].jet_JVTcut:
            if not jetPassJVT(Branches, ijet, settings['SelectionClass'].jet_JVTwp):
              TLVjet.isPileup = True
              if not settings['UseJVTineffSFs']:
                continue  # skip pileup jets already now so JVT inefficiency SFs are not used (when requested)

          # Apply kinematic selection
          if jetPt < settings['SelectionClass'].jet_minPt or abs(jetRapidity) > settings['SelectionClass'].jet_maxRapidity:
            continue  # skip jet

          # Apply minDR(jet, leptons) cut
          passMinDRcut = True
          for lepton in SelectedLeptons: # loop over selected leptons
            dr_lepton_jet = myDeltaR(lepton, TLVjet)  # equiv. to lepton.DeltaR(TLVjet, True) in newer ROOT versions
            if dr_lepton_jet < settings['SelectionClass'].jet_minDRJetLepton:
              passMinDRcut = False
          if not passMinDRcut:
            continue  # skip jet

          # Identify b-tagged jets and heavy flavour truth jets
          if Type == "reco":
            if settings['MC']:
              TLVjet.hadronTruthLabelID = getJetHadronTruthLabelID(Branches, ijet)
            if settings['useSFs']:  # SF for b-tagged jets
              BTaggingSF = getJetBTagSF(Branches, ijet, settings['Tagger'])
              if abs(jetEta) > 2.5:  # protection (all jets outside tracker should have b-tagging SF = 1)
                BTaggingSF = [1] * len(BTaggingSF)
              TLVjet.BTaggingSF = {('BTag_'+str(isf) if isf else 'nominal'): value for isf, value in enumerate(BTaggingSF)}
              if settings['SelectionClass'].jet_JVTcut:
                JvtSF = getJVTSF(Branches, ijet, settings['SelectionClass'].jet_JVTwp)
                TLVjet.JvtSF = {('JVT_'+str(isf) if isf else 'nominal'): value for isf, value in enumerate(JvtSF)}
            if getJetBTag(Branches, ijet, settings['Tagger'], settings['btagWP']) and (not TLVjet.isPileup or not settings['SelectionClass'].jet_JVTcut):
              TLVjet.btagQuantile = getBTagQuantile(Branches, ijet, settings['Tagger'])
              bJets.append(TLVjet)
          else: # truth
            TLVjet.hadronTruthLabelID = getTruthJetHadronTruthLabelID(Branches, ijet)
            if TLVjet.hadronTruthLabelID == 5:
              bJets.append(TLVjet)
            elif TLVjet.hadronTruthLabelID == 4:
              cJets.append(TLVjet)
            if hf_jet0_truth is None and (TLVjet.hadronTruthLabelID==5 or TLVjet.hadronTruthLabelID==4):
              hf_jet0_truth = TLVjet
          # Save variables
          SumPt_jets += jetPt
          VariablesValues['jet_pt_'+Type].append( jetPt )
          VariablesValues['jet_y_'+Type].append( jetRapidity )
          VariablesValues['jet_phi_'+Type].append( jetPhi )
          if len(SelectedJets) < 2:
              VariablesValues['jet' + str(len(SelectedJets)) + '_pt_' + Type].append( jetPt )
              VariablesValues['jet' + str(len(SelectedJets)) + '_y_' + Type].append( jetRapidity )
              VariablesValues['jet' + str(len(SelectedJets)) + '_phi_' + Type].append( jetPhi )
          # Save jets
          SelectedJets.append(TLVjet)

        # Select events with at least one non-pileup jet (if requested)
        njet = len(SelectedJets)
        njet_no_pileup = sum([1 if not jet.isPileup else 0 for jet in SelectedJets])
        if settings['SelectionClass'].jet_atLeastOne and not njet_no_pileup:
          if settings['Debug']: print("DEBUG: Event not passed the at least one jet selection")
          if cxaod_val_debug:
            print("DEBUG: Event {} from {} not passed the at least one jet selection".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
          continue  # skip event
        if settings['Debug']: print("DEBUG: Event passed the at least one jet selection")
        h_cutflow[Type].Fill(h_cutflow_jet[Type],1)

        if Type == 'truth' and settings['cxaod_validation'] :  # Temporary
          if not settings['MC']: cxaod_val_output_file.write('{} {}\n'.format(Branches["runNumber"][0], Branches["eventNumber"][0]))
          else: cxaod_val_output_file.write('{} {}\n'.format(Branches["mcChannelNumber"][0], Branches["eventNumber"][0]))

        #######################################
        # nTag classification ('Te1' or 'Ti2')
        #######################################
        nbJets = len(bJets)
        if Type == "reco":
          if nbJets == 1:
            ToFillRecoTagCases.append('Te1')  # exactly 1 b-tagged jet
            h_cutflow[Type].Fill(h_cutflow_Te1[Type], 1)
          elif nbJets > 1:
            ToFillRecoTagCases.append('Ti2')  # at least 2 b-tagged jets
            h_cutflow[Type].Fill(h_cutflow_Ti2[Type], 1)

        #if cxaod_val_debug:
          #print("DEBUG: Event {} from {} not passed the at least one b-jet selection".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
          #print("DEBUG: Jet info for this event:")
          #for ijet, jet in enumerate(SelectedJets): # loop over selected jets
          #  print('DEBUG: Jet({}) pt = {}'.format(ijet, jet.Pt()))
          #  print('DEBUG: Jet({}) eta = {}'.format(ijet, jet.Eta()))
          #  print('DEBUG: Jet({}) y = {}'.format(ijet, jet.Rapidity()))
          #  print('DEBUG: Jet({}) phi = {}'.format(ijet, jet.Phi()))
          #  for jjet in range(0, nJets): # loop over all jets
          #    # Find corresponding jet and get btagging quantile
          #    if abs((getRecoJetPt(Branches,jjet) - jet.Pt())/jet.Pt()) < 0.001:
          #      print('DEBUG: Jet({}) btag quantile = {}'.format(ijet, getBTagQuantile(Branches,jjet,Tagger)))
          ## Now loop over all jets again to see if there is at least one jet that is btagged but didn't pass other kinematic+JVT selections
          #print('DEBUG: Looking at all jets:')
          #for jjet in range(0, nJets): # loop over all jets
          #  if getBTagQuantile(Branches, jjet, Tagger) != -1 or getJetBTag(Branches, jjet, Tagger, btagWP):
          #    print('DEBUG: Is jet({}) btagged? = {}'.format(jjet, getJetBTag(Branches, jjet, Tagger, btagWP)))
          #    print('DEBUG: Jet({}) pt = {}'.format(jjet, getRecoJetPt(Branches, jjet)))
          #    print('DEBUG: Jet({}) eta = {}'.format(jjet, getRecoJetEta(Branches, jjet)))
          #    print('DEBUG: Jet({}) quantile = {}'.format(jjet, getBTagQuantile(Branches, jjet, Tagger)))
          #    print('DEBUG: Pass jet({}) JVT? = {}'.format(jjet, jetPassJVT(Branches, jjet, SelectionClass.jet_JVTwp)))
          #    TLVjet = iJet()
          #    TLVjet.SetPtEtaPhiE(getRecoJetPt(Branches, jjet), getRecoJetEta(Branches, jjet), getRecoJetPhi(Branches, jjet), getRecoJetE(Branches, jjet))
          #    passMinDRcut = True
          #    dr_min = 1E10
          #    for lepton in SelectedLeptons: # loop over selected leptons
          #      dr_lepton_jet = lepton.DeltaR(TLVjet, True) # useRapidity=True
          #      if dr_lepton_jet < SelectionClass.jet_minDRJetLepton:
          #        passMinDRcut = False
          #      if dr_lepton_jet < dr_min:
          #        dr_min = dr_lepton_jet
          #    if passMinDRcut:
          #      print('DEBUG: Pass jet({}) deltaR cut? = True (deltaR = {})'.format(jjet, dr_min))
          #    else:
          #      print('DEBUG: Pass jet({}) deltaR cut? = False (deltaR = {})'.format(jjet, dr_min))
          #      print('DEBUG: deltaR values for each lepton for jet index {}'.format(jjet))
          #      for lepton in SelectedLeptons: # loop over selected leptons
          #        print('DEBUG: lepton(pt={}).DeltaR(jet, True) = {}'.format(lepton.Pt(), lepton.DeltaR(TLVjet, True)))
          #        print('DEBUG: lepton eta = {}'.format(lepton.Eta()))
          #        print('DEBUG: lepton phi = {}'.format(lepton.Phi()))
          #      print('DEBUG: Jet info for jet index {}:'.format(jjet))
          #      print('DEBUG: pt = {}'.format(TLVjet.Pt()))
          #      print('DEBUG: eta = {}'.format(TLVjet.Eta()))
          #      print('DEBUG: phi = {}'.format(TLVjet.Phi()))
          #      print('DEBUG: m = {}'.format(TLVjet.M()))
          #      print('DEBUG: quantile = {}'.format(getBTagQuantile(Branches, jjet, Tagger)))

        ############
        # Apply SFs
        ############
        # Create weight dict for Z+HFjets (w/ BTag SF systematics)
        weightBTagEvents = copy.deepcopy(preFinalWeight)
        # Create weight dict for inclusive Z+jets (w/o BTag SF systematics)
        weight = {key: value for key, value in preFinalWeight.items() if 'BTag' not in key}
        if Type == "reco" and settings['useSFs']:
          # Dictionary with total lepton SF for each variation
          LeptonSF = {
            'nominal' : 1.0,
          }
          RecoKeys     = set(list(SelectedLeptons[0].RecoSF.keys())+list(SelectedLeptons[1].RecoSF.keys()))
          LeptonRecoSF = {key: 1.0 for key in RecoKeys}
          IsoKeys      = set(list(SelectedLeptons[0].IsoSF.keys())+list(SelectedLeptons[1].IsoSF.keys()))
          LeptonIsoSF  = {key: 1.0 for key in IsoKeys}
          PIDKeys      = set(list(SelectedLeptons[0].PIDSF.keys())+list(SelectedLeptons[1].PIDSF.keys()))
          LeptonPIDSF  = {key: 1.0 for key in PIDKeys}
          TrigKeys     = set(list(SelectedLeptons[0].TrigSF.keys())+list(SelectedLeptons[1].TrigSF.keys()))
          LeptonTrigSF = {key: 1.0 for key in TrigKeys}
          MCEff_1 = SelectedLeptons[0].TrigEff['nominal']
          MCEff_2 = SelectedLeptons[1].TrigEff['nominal']
          LeptonTrigMCEff = {
            'nominal' : MCEff_1 + ( (1-MCEff_1) * MCEff_2 )
          }
          if settings['Channel'] == 'ELMU':  # Calculate final trigger SF
            for var in LeptonTrigSF:
              VarKey1           = var if var in SelectedLeptons[0].TrigEff else 'nominal'
              VarKey2           = var if var in SelectedLeptons[1].TrigEff else 'nominal'
              MCEff_1           = SelectedLeptons[0].TrigEff[VarKey1]
              MCEff_2           = SelectedLeptons[1].TrigEff[VarKey2]
              DataEff_1         = SelectedLeptons[0].TrigSF[VarKey1]*MCEff_1
              DataEff_2         = SelectedLeptons[1].TrigSF[VarKey2]*MCEff_2
              FinalTrigSF       = ( DataEff_1 + ( (1-DataEff_1) * DataEff_2 ) ) / ( MCEff_1 + ( (1-MCEff_1) * MCEff_2 ) )
              LeptonTrigSF[var] = FinalTrigSF
          else:  # EL or MU
            for var in LeptonTrigSF:
              LeptonTrigSF[var] = SelectedLeptons[0].TrigSF[var]
          # Multiply trigger SF to nominal total SF
          LeptonSF['nominal'] *= LeptonTrigSF['nominal']
          # Multiply remaining SFs from each lepton to the total SF for each variation
          for lep in SelectedLeptons: # loop over leptons
            LeptonSF['nominal'] *= lep.SF['nominal']
            for variation in LeptonRecoSF:
              LeptonRecoSF[variation] *= lep.RecoSF[variation] if variation in lep.RecoSF else lep.RecoSF['nominal']
            for variation in LeptonIsoSF:
              LeptonIsoSF[variation]  *= lep.IsoSF[variation] if variation in lep.IsoSF else lep.IsoSF['nominal']
            for variation in LeptonPIDSF:
              LeptonPIDSF[variation]  *= lep.PIDSF[variation] if variation in lep.PIDSF else lep.PIDSF['nominal']
          h_leptonSF    .Fill(LeptonSF['nominal'])
          h_leptonRecoSF.Fill(LeptonRecoSF['nominal'])
          h_leptonIsoSF .Fill(LeptonIsoSF['nominal'])
          h_leptonPIDSF .Fill(LeptonPIDSF['nominal'])
          h_leptonTrigSF.Fill(LeptonTrigSF['nominal'])
          h_leptonTrigSF_vs_Zpt.Fill(VariablesValues['Zpt_' + Type][0], LeptonTrigSF['nominal'])
          h_leptonTrigMCEff.Fill(LeptonTrigMCEff['nominal'])
          h_leptonTrigMCEff_vs_Zpt.Fill(VariablesValues['Zpt_' + Type][0], LeptonTrigMCEff['nominal'])
          # Jet SFs
          if njet > 0:
            JvtSF = copy.deepcopy(SelectedJets[0].JvtSF)
            BTagSF = copy.deepcopy(SelectedJets[0].BTaggingSF)
          for jet in SelectedJets[1:]: # loop over jets (skip leading jet which was already considered)
            for variation in JvtSF: JvtSF[variation] *= jet.JvtSF[variation]
            for variation in BTagSF: BTagSF[variation] *= jet.BTaggingSF[variation]
          if njet:
            h_jvtSF.Fill(JvtSF['nominal'])
            h_btaggingSF.Fill(BTagSF['nominal'])
          for wgtName in weight:  # apply nominal SFs to variated weights due to event weight variations
            if wgtName == 'nominal' or 'EvtWgtVar_' in wgtName or 'weight_pileup' in wgtName:
              if njet > 0:
                weight[wgtName]           *= LeptonSF['nominal'] * JvtSF['nominal']  # inclusive Z+jets events
                weightBTagEvents[wgtName] *= LeptonSF['nominal'] * JvtSF['nominal'] * BTagSF['nominal']
              else:
                weight[wgtName]           *= LeptonSF['nominal']  # inclusive Z+jets events
                weightBTagEvents[wgtName] *= LeptonSF['nominal']
          if settings['ApplySFsysts']:
            for source, NPs in SFsysts.items():  # loop over sources of SF systematics
              if njet == 0 and (source == 'JVT' or source == 'BTag'): continue  # skip jet SFs in no jet events
              if   source == 'nominal': continue  # already computed
              if   settings['Channel'] == 'MU' and 'EL' in source: continue  # skip electron SF systematics in muon channel
              elif settings['Channel'] == 'EL' and 'MU' in source: continue  # skip muon SF systematics in electron channel
              for inp in range(1, NPs + 1): # loop over NPs
                SF = 1
                if 'JVT' not in SFsysts: SF *= JvtSF['nominal']  # when JVT not in SFsysts, we need to multiply nominal Jvt SF
                for sf in SFsysts:  # loop over SFs
                  if   sf == 'nominal':                continue  # not applicable
                  if   sf == 'BTag':                   continue  # added separately
                  if   sf == 'weight_pileup':          continue  # added separately
                  if   settings['Channel'] == 'MU' and 'EL' in sf: continue  # skip electron SF systematics in muon channel
                  elif settings['Channel'] == 'EL' and 'MU' in sf: continue  # skip muon SF systematics in electron channel
                  if njet == 0 and sf == 'JVT': continue  # skip JVT SF in no jet events
                  if settings['Channel'] != 'ELMU':
                    SFvar = 'nominal' if sf != source else sf+'_'+str(inp)
                    if sf == 'JVT':    SF *= JvtSF[SFvar]
                    elif 'Reco' in sf: SF *= LeptonRecoSF[SFvar]
                    elif 'Iso'  in sf: SF *= LeptonIsoSF[SFvar]
                    elif 'PID'  in sf: SF *= LeptonPIDSF[SFvar]
                    elif 'Trig' in sf: SF *= LeptonTrigSF[SFvar]
                if settings['Channel'] == 'ELMU':
                  SFvar        = 'nominal' if sf != source else sf+'_'+str(inp)
                  leptonRecoSF = LeptonRecoSF['nominal' if 'Reco' not in source else source+'_'+str(inp)]
                  leptonIsoSF  = LeptonIsoSF['nominal' if 'Iso' not in source else source+'_'+str(inp)]
                  leptonPIDSF  = LeptonPIDSF['nominal' if 'PID' not in source else source+'_'+str(inp)]
                  leptonTrigSF = LeptonTrigSF['nominal' if 'Trig' not in source else source+'_'+str(inp)]
                  leptonSF     = leptonRecoSF * leptonIsoSF * leptonPIDSF * leptonTrigSF
                  if njet:
                    SF *= JvtSF['nominal' if source != 'JVT' else source+'_'+str(inp)] * leptonSF
                  else:
                    SF *= leptonSF
                if source != 'BTag':
                  weight[source+'_'+str(inp)] *= SF
                if njet:
                  weightBTagEvents[source+'_'+str(inp)] *= SF*BTagSF['nominal'] if source != 'BTag' else SF*BTagSF['BTag_'+str(inp)]
                else:
                  weightBTagEvents[source+'_'+str(inp)] *= SF

        # Remove pileup jets (if requested)
        if Type == "reco" and settings['SelectionClass'].jet_JVTcut:
          selected_pileup_jets = [jet for jet in SelectedJets if jet.isPileup]
          for pileup_jet in selected_pileup_jets:
            SelectedJets.remove(pileup_jet)

        #########################
        # Flavour classification
        #########################
        if settings['MC'] and settings['doFlavourClassification'] and njet > 0:
          if settings['Debug']: print("DEBUG: Performing flavour classification")
          evtFlav = getEventHadronFlavour(SelectedJets)  # event flavour (see HelperFunctions.py)
          if settings['Debug']: print("DEBUG: event flavour = " + evtFlav)
          if "B" in evtFlav:   # exactly one B or more than one B (regardless of # of C hadrons)
            if Type == "reco":  ToFillRecoFlavours.append("FlavA1B_")
            if Type == "truth": ToFillTruthFlavours.append("FlavA1B_")
          elif "C" in evtFlav: # exactly one C or more than one C (no B hadron)
            if Type == "reco":  ToFillRecoFlavours.append("FlavA1C_")
            if Type == "truth": ToFillTruthFlavours.append("FlavA1C_")
          if Type == "reco"  and evtFlav in Flavours["reco"]: ToFillRecoFlavours.append(evtFlav)
          if Type == "truth" and evtFlav in Flavours["truth"]: ToFillTruthFlavours.append(evtFlav)
          if settings['Debug']:
            if Type == "reco":
              print("ToFillRecoFlavours:")
              for flav in ToFillRecoFlavours: print(flav)
            else:
              print("ToFillTruthFlavours:")
              for flav in ToFillTruthFlavours: print(flav)

        # Sort truth leptons (reco leptons already sorted)
        if Type == 'truth':
          if SelectedLeptons[0].Pt() < SelectedLeptons[1].Pt():  # not sorted
            LeadLepton         = SelectedLeptons[1]
            SelectedLeptons[1] = SelectedLeptons[0]
            SelectedLeptons[0] = LeadLepton

        ###################################
        # Print event info (if requested)
        ###################################
        if settings['PrintRecoInfo'] and Type == 'reco':
          print("#################################################################################")
          print("Reco-level info")
          print("EVENTINFO: EventNumber: "+str(Branches["eventNumber"][0]))
          if settings['MC']: print("EVENTINFO: RandomRunNumber: "+str(Branches["RandomRunNumber"][0]))
          print("EVENTINFO: nJets: "+str(njet))
          for ijet in range(0,len(SelectedJets)): # Loop over jets
            print("EVENTINFO: jet("+str(ijet)+") pt: "+str(round(SelectedJets[ijet].Pt(),2)))
            print("EVENTINFO: jet("+str(ijet)+") eta: "+str(round(SelectedJets[ijet].Eta(),2)))
            if settings['MC']:
              print("EVENTINFO: jet("+str(ijet)+") jvtSF: "+str(SelectedJets[ijet].JvtSF['nominal']))
              print("EVENTINFO: jet("+str(ijet)+") btagSF: "+str(SelectedJets[ijet].BTaggingSF['nominal']))
          for ilep in range(0,len(SelectedLeptons)): # Loop over signal leptons
            print("EVENTINFO: lep("+str(ilep)+") pt: "+str(round(SelectedLeptons[ilep].Pt(),2)))
            print("EVENTINFO: lep("+str(ilep)+") eta: "+str(round(SelectedLeptons[ilep].Eta(),2)))
            if settings['MC']:
              print("EVENTINFO: lep("+str(ilep)+") reco SF: "+str(SelectedLeptons[ilep].RecoSF['nominal']))
              print("EVENTINFO: lep("+str(ilep)+") iso SF: "+str(SelectedLeptons[ilep].IsoSF['nominal']))
              print("EVENTINFO: lep("+str(ilep)+") TTVA/PID SF: "+str(SelectedLeptons[ilep].PIDSF['nominal']))
              print("EVENTINFO: lep("+str(ilep)+") Trig SF: "+str(SelectedLeptons[ilep].TrigSF['nominal']))
            #print "EVENTINFO: Matched chains ("+str(ilep)+"):"
            #print SelectedLeptons[ilep].matchedChains
          print("EVENTINFO: Zmass: "+str(round(VariablesValues['Zmass_'+Type][0],2)))
          print("EVENTINFO: Zpt: "+str(round(VariablesValues['Zpt_'+Type][0],2)))
          print("EVENTINFO: Zabsy: "+str(round(VariablesValues['Zabsy_'+Type][0],2)))
          for ibjet in range(0,len(bJets)):
            print("EVENTINFO: b-tagged jet("+str(ibjet)+") pt: "+str(round(bJets[ibjet].Pt(),2)))
            print("EVENTINFO: b-tagged jet("+str(ibjet)+") eta: "+str(round(bJets[ibjet].Eta(),2)))
          if settings['MC']:
            print("EVENTINFO: mcEventWeight: "+str(mcEventWeights['nominal']))
            print("EVENTINFO: mcChannelNumber: "+str(mcChannelNumber))
            print("EVENTINFO: sumWeights: "+str(sumWeights[mcChannelNumber]))
            print("EVENTINFO: ami_xss: "+str(ami_xss[mcChannelNumber]))
            print("EVENTINFO: ami_eff: "+str(ami_eff[mcChannelNumber]))
            print("EVENTINFO: ami_k: "+str(ami_k[mcChannelNumber]))
            print("EVENTINFO: weight_pileup: "+str(Branches["weight_pileup"][0]))
            print("EVENTINFO: LeptonSF: "+str(LeptonSF['nominal']))
            if njet > 0:
              print("EVENTINFO: JvtSF: "+str(JvtSF['nominal']))
              print("EVENTINFO: BTagSF: "+str(BTagSF['nominal']))
          print("EVENTINFO: weight: "+str(weight['nominal']))
          print("EVENTINFO: MET: "+str(round(MET,2)))
          #if nbJets > 0: print "EVENTINFO: Passed Z+>=1b selection (eventNumber = {})".format(Branches["eventNumber"][0]) # Temporary

        if settings['PrintTruthInfo'] and settings['MC'] and Type == 'truth':
          print("#################################################################################")
          print("Truth-level info")
          print("EVENTINFO: EventNumber: "+str(Branches["eventNumber"][0]))
          print("EVENTINFO: RandomRunNumber: "+str(Branches["RandomRunNumber"][0]))
          print("EVENTINFO: nJets: "+str(njet))
          for ijet in range(0,len(SelectedJets)): # Loop over jets
            print("EVENTINFO: jet("+str(ijet)+") pt: "+str(round(SelectedJets[ijet].Pt(),2)))
            print("EVENTINFO: jet("+str(ijet)+") eta: "+str(round(SelectedJets[ijet].Eta(),2)))
          for ilep in range(0,len(SelectedLeptons)): # Loop over signal leptons
            print("EVENTINFO: lep("+str(ilep)+") pt: "+str(round(SelectedLeptons[ilep].Pt(),2)))
            print("EVENTINFO: lep("+str(ilep)+") eta: "+str(round(SelectedLeptons[ilep].Eta(),2)))
          print("EVENTINFO: Zmass: "+str(round(VariablesValues['Zmass_'+Type][0],2)))
          print("EVENTINFO: Zpt: "+str(round(VariablesValues['Zpt_'+Type][0],2)))
          print("EVENTINFO: Zabsy: "+str(round(VariablesValues['Zabsy_'+Type][0],2)))
          for ibjet in range(0,len(bJets)):
            print("EVENTINFO: truth b-jet("+str(ibjet)+") pt: "+str(round(bJets[ibjet].Pt(),2)))
            print("EVENTINFO: truth b-jet("+str(ibjet)+") eta: "+str(round(bJets[ibjet].Eta(),2)))
          print("EVENTINFO: mcEventWeight: "+str(mcEventWeights['nominal']))
          print("EVENTINFO: mcChannelNumber: "+str(mcChannelNumber))
          print("EVENTINFO: sumWeights: "+str(sumWeights[mcChannelNumber]))
          print("EVENTINFO: ami_xss: "+str(ami_xss[mcChannelNumber]))
          print("EVENTINFO: ami_eff: "+str(ami_eff[mcChannelNumber]))
          print("EVENTINFO: ami_k: "+str(ami_k[mcChannelNumber]))
          print("EVENTINFO: weight_pileup: "+str(Branches["weight_pileup"][0]))
          print("EVENTINFO: weight: "+str(weight['nominal']))


        ##############################
        # Fill mu distributions
        ##############################
        if settings['Debug']: print("DEBUG: weight = "+str(weight))
        if settings['Debug']: print("DEBUG: weight['nominal'] = "+str(weight['nominal']))
        if settings['Debug']: print("DEBUG: Filling mu histograms")
        if Type == "reco":  passReco  = True
        if Type == "truth": passTruth = True
        if Type == "reco" and nominal:  # Fill once !
          h_correctedAndScaledAverageMu.Fill( correctedAndScaledAverageMu, weight['nominal'] )
          h_averageInteractionsPerCrossing.Fill( averageInteractionsPerCrossing, weight['nominal'] )
          h_actualInteractionsPerCrossing.Fill( actualInteractionsPerCrossing, weight['nominal'] )
          h_correctedAndScaledActualMu.Fill( correctedAndScaledActualMu, weight['nominal'] )
          h_correctedAverageMu.Fill( correctedAverageMu, weight['nominal'] )
          h_correctedActualMu.Fill( correctedActualMu, weight['nominal'] )

        ####################
        # Compute variables
        ####################
        if settings['Debug']: print("DEBUG: Compute variables")

        # Save njet
        if 'njet' in Variables: VariablesValues['njet_'+Type] = [njet]

        # Save lepton variables
        SumPt_lep = 0
        VariablesValues['lep_pt_'+Type]  = []
        VariablesValues['lep_eta_'+Type] = []
        VariablesValues['lep_phi_'+Type] = []
        for ijet in range(0, 2):
          VariablesValues['lep'+str(ijet)+'_pt_'+Type]  = []
          VariablesValues['lep'+str(ijet)+'_eta_'+Type] = []
          VariablesValues['lep'+str(ijet)+'_phi_'+Type] = []
        #VariablesValues['deltaRll_'+Type] = [SelectedLeptons[0].DeltaR(SelectedLeptons[1],True)] # useRapidity=True
        #if len(SelectedJets): VariablesValues['deltaRlep0jet0_'+Type] = [SelectedLeptons[0].DeltaR(SelectedJets[0], True)] # useRapidity=True
        VariablesValues['deltaRll_'+Type] = [myDeltaR(SelectedLeptons[0], SelectedLeptons[1])]
        if len(SelectedJets): VariablesValues['deltaRlep0jet0_'+Type] = [myDeltaR(SelectedLeptons[0], SelectedJets[0])]
        for ilep, lep in enumerate(SelectedLeptons):
          leptonPt   = lep.Pt()
          leptonEta  = lep.Eta()
          leptonPhi  = lep.Phi()
          SumPt_lep += leptonPt
          VariablesValues['lep_pt_'+Type] .append( leptonPt )
          VariablesValues['lep_eta_'+Type].append( leptonEta )
          VariablesValues['lep_phi_'+Type].append( leptonPhi )
          if ilep < 2:
            VariablesValues['lep'+str(ilep)+'_pt_'+Type] .append( leptonPt )
            VariablesValues['lep'+str(ilep)+'_eta_'+Type].append( leptonEta )
            VariablesValues['lep'+str(ilep)+'_phi_'+Type].append( leptonPhi )

        # Store MET
        if Type == 'reco' and 'met' in Variables: VariablesValues['met_'+Type] = [MET]

        # Store HT
        if 'ht' in Variables: VariablesValues['ht_'+Type] = [SumPt_jets + SumPt_lep]

        # Calculate b-tagged observables in reco and, b-jet and c-jet truth-level observables

        # Calculate b-jet observables
        if nbJets: # at least one b-jet
          # Print run_number and event_number to output file to compare with CxAOD
          #if cxaod_validation : # Temporary
          #  if not MC: cxaod_val_output_file.write('{} {}\n'.format(Branches["runNumber"][0], Branches["eventNumber"][0]))
          #  else: cxaod_val_output_file.write('{} {}\n'.format(Branches["mcChannelNumber"][0], Branches["eventNumber"][0]))
          bjet = bJets[0]
          VariablesValues['bjet0_XF_'+Type]   = [2*bjet.Pt()*abs(sinh(bjet.Eta()))/13000]
          VariablesValues['bjet0_pt_'+Type]   = [bjet.Pt()]
          VariablesValues['bjet0_absy_'+Type] = [abs(bjet.Rapidity())]
          #VariablesValues['deltaRZb_'+Type]   = [bjet.DeltaR(Zboson,True)] # useRapidity=True
          VariablesValues['deltaRZb_'+Type]   = [myDeltaR(bjet, Zboson)]
          VariablesValues['deltaPhiZb_'+Type] = [abs(bjet.DeltaPhi(Zboson))]
          VariablesValues['deltaYZb_'+Type]   = [abs(bjet.Rapidity()-Zboson.Rapidity())]
        if nbJets > 1: # at least two b-jet
          bjet = bJets[1]
          VariablesValues['bjet1_pt_'+Type]   = [bjet.Pt()]
          VariablesValues['bjet1_absy_'+Type] = [abs(bjet.Rapidity())]
          VariablesValues['bjet1_XF_'+Type]   = [2*bjet.Pt()*abs(sinh(bjet.Eta()))/13000]

        # Calculate di-b-jet observables
        if nbJets > 1:
          bb                                          = bJets[0]+bJets[1]
          VariablesValues['pTbb_'+Type]               = [bb.Pt()]
          VariablesValues['pTbbsum_'+Type]            = [bJets[0].Pt()+bJets[1].Pt()]
          VariablesValues['mbb_'+Type]                = [bb.M()]
          VariablesValues['pTbbovermbb_'+Type]        = [bb.Pt()/bb.M()]
          VariablesValues['bjet1pToverpTbb_'+Type]    = [bJets[1].Pt()/bb.Pt()]
          VariablesValues['bjet1pToverpTbbsum_'+Type] = [bJets[1].Pt()/(bJets[0].Pt()+bJets[1].Pt())]
          VariablesValues['deltaPhibb_'+Type]         = [abs(bJets[0].DeltaPhi(bJets[1]))]
          VariablesValues['deltaYbb_'+Type]           = [abs(bJets[0].Rapidity()-bJets[1].Rapidity())]
          #VariablesValues['deltaRbb_'+Type]           = [bJets[0].DeltaR(bJets[1],True)] # useRapidity=True
          VariablesValues['deltaRbb_'+Type]           = [myDeltaR(bJets[0], bJets[1])]

        # Calculate observables in Z+c and Z+cc events (truth only)
        if Type == "truth":
          ncJets = len(cJets)
          if ncJets: # at least one c-jet
            cjet = cJets[0]
            VariablesValues['cjet0_XF_'+Type]   = [2*cjet.Pt()*abs(sinh(cjet.Eta()))/13000]
            VariablesValues['cjet0_pt_'+Type]   = [cjet.Pt()]
            VariablesValues['cjet0_absy_'+Type] = [abs(cjet.Rapidity())]
            #VariablesValues['deltaRZc_'+Type]   = [cjet.DeltaR(Zboson,True)] # useRapidity=True
            VariablesValues['deltaRZc_'+Type]   = [myDeltaR(cjet, Zboson)]
            VariablesValues['deltaPhiZc_'+Type] = [abs(cjet.DeltaPhi(Zboson))]
            VariablesValues['deltaYZc_'+Type]   = [abs(cjet.Rapidity()-Zboson.Rapidity())]
          if ncJets > 1: # at least two b-tagged jets
            cc                                          = cJets[0]+cJets[1]
            VariablesValues['pTcc_'+Type]               = [cc.Pt()]
            VariablesValues['pTccsum_'+Type]            = [cJets[0].Pt()+cJets[1].Pt()]
            VariablesValues['mcc_'+Type]                = [cc.M()]
            VariablesValues['pTccovermcc_'+Type]        = [cc.Pt()/cc.M()]
            VariablesValues['cjet0pToverpTcc_'+Type]    = [cJets[1].Pt()/cc.Pt()]
            VariablesValues['cjet0pToverpTccsum_'+Type] = [cJets[1].Pt()/(cJets[0].Pt()+cJets[1].Pt())]
            VariablesValues['deltaPhicc_'+Type]         = [abs(cJets[0].DeltaPhi(cJets[1]))]
            VariablesValues['deltaYcc_'+Type]           = [abs(cJets[0].Rapidity()-cJets[1].Rapidity())]
            #VariablesValues['deltaRcc_'+Type]           = [cJets[0].DeltaR(cJets[1],True)] # useRapidity=True
            VariablesValues['deltaRcc_'+Type]           = [myDeltaR(cJets[0], cJets[1])]
            VariablesValues['cjet1_pt_'+Type]           = [cJets[1].Pt()]
            VariablesValues['cjet1_absy_'+Type]         = [abs(cJets[1].Rapidity())]
            VariablesValues['cjet1_XF_'+Type]           = [2*cJets[1].Pt()*abs(sinh(cJets[1].Eta()))/13000]
          if hf_jet0_truth is not None:
            VariablesValues['hf_jet0_pt_truth'] = [hf_jet0_truth.Pt()]

        #if cxaod_val_debug and Type == 'reco' and nbJets:
          #print("DEBUG: Event {} from {} passed all selections".format(Branches["eventNumber"][0], Branches["runNumber"][0]))
          #print("DEBUG: Info for this event:")
          #print('DEBUG: DeltaR(Z, lead b-tagged jet) = {}'.format(VariablesValues['deltaRZb_reco'][0]))
          #print('DEBUG: Zmass = {}'.format(VariablesValues['Zmass_reco'][0]))
          #print('DEBUG: Zpt = {}'.format(VariablesValues['Zpt_reco'][0]))
          #print('DEBUG: Lead b-tagged jet pt = {}'.format(bJets[0].Pt()))
          #print('DEBUG: Lead b-tagged jet eta = {}'.format(bJets[0].Eta()))
          #for ijet, jet in enumerate(SelectedJets): # loop over selected jets
          #  print('DEBUG: Jet({}) pt = {}'.format(ijet, jet.Pt()))
          #  print('DEBUG: Jet({}) eta = {}'.format(ijet, jet.Eta()))
          #  print('DEBUG: Jet({}) y = {}'.format(ijet, jet.Rapidity()))
          #  print('DEBUG: Jet({}) phi = {}'.format(ijet, jet.Phi()))
          #  for jjet in range(0, nJets): # loop over all jets
          #    # Find corresponding jet and get btagging quantile
          #    if abs((getRecoJetPt(Branches,jjet) - jet.Pt())/jet.Pt()) < 0.001:
          #      print('DEBUG: Jet({}) btag quantile = {}'.format(ijet, getBTagQuantile(Branches,jjet,Tagger)))
          ## Now loop over all jets again to see if there is at least one jet that is btagged but didn't pass other kinematic+JVT selections
          #print('DEBUG: Looking at all jets:')
          #for jjet in range(0, nJets): # loop over all jets
          #  if getBTagQuantile(Branches, jjet, Tagger) != -1 or getJetBTag(Branches, jjet, Tagger, btagWP):
          #    print('DEBUG: Is jet({}) btagged? = {}'.format(jjet, getJetBTag(Branches, jjet, Tagger, btagWP)))
          #    print('DEBUG: Jet({}) pt = {}'.format(jjet, getRecoJetPt(Branches, jjet)))
          #    print('DEBUG: Jet({}) eta = {}'.format(jjet, getRecoJetEta(Branches, jjet)))
          #    print('DEBUG: Jet({}) quantile = {}'.format(jjet, getBTagQuantile(Branches, jjet, Tagger)))
          #    print('DEBUG: Pass jet({}) JVT? = {}'.format(jjet, jetPassJVT(Branches, jjet, SelectionClass.jet_JVTwp)))
          #    TLVjet = iJet()
          #    TLVjet.SetPtEtaPhiE(getRecoJetPt(Branches, jjet), getRecoJetEta(Branches, jjet), getRecoJetPhi(Branches, jjet), getRecoJetE(Branches, jjet))
          #    passMinDRcut = True
          #    dr_min = 1E10
          #    for lepton in SelectedLeptons: # loop over selected leptons
          #      dr_lepton_jet = lepton.DeltaR(TLVjet, True) # useRapidity=True
          #      if dr_lepton_jet < SelectionClass.jet_minDRJetLepton:
          #        passMinDRcut = False
          #      if dr_lepton_jet < dr_min:
          #        dr_min = dr_lepton_jet
          #    if passMinDRcut:
          #      print('DEBUG: Pass jet({}) deltaR cut? = True (deltaR = {})'.format(jjet, dr_min))
          #    else:
          #      print('DEBUG: Pass jet({}) deltaR cut? = False (deltaR = {})'.format(jjet, dr_min))
          #      print('DEBUG: deltaR values for each lepton for jet index {}'.format(jjet))
          #      for lepton in SelectedLeptons: # loop over selected leptons
          #        print('DEBUG: lepton(pt={}).DeltaR(jet, True) = {}'.format(lepton.Pt(), lepton.DeltaR(TLVjet, True)))
          #        print('DEBUG: lepton eta = {}'.format(lepton.Eta()))
          #        print('DEBUG: lepton phi = {}'.format(lepton.Phi()))
          #      print('DEBUG: Jet info for jet index {}:'.format(jjet))
          #      print('DEBUG: pt = {}'.format(TLVjet.Pt()))
          #      print('DEBUG: eta = {}'.format(TLVjet.Eta()))
          #      print('DEBUG: phi = {}'.format(TLVjet.Phi()))
          #      print('DEBUG: m = {}'.format(TLVjet.M()))
          #      print('DEBUG: quantile = {}'.format(getBTagQuantile(Branches, jjet, Tagger)))


        #####################
        # Fill distributions
        #####################
        # In the reco case, I have to fill each distribution for all the requested ntag cases
        # but only for those which are in ToFillRecoTagCases
        if settings['Debug']: print("DEBUG: Filling 1D histograms")
        ToFillFlavours = ToFillRecoFlavours if Type == "reco" else ToFillTruthFlavours
        ToFillTagCases = ToFillRecoTagCases if Type == 'reco' else ToFillTruthTagCases
        extraType = '_truth' if Type == 'truth' else ''

        for flavour in ToFillFlavours: # loop over truth-based flavours
          extraFlavour = '_'+flavour if flavour != ''   else ''
          for tagCase in ToFillTagCases: # loop over nbtag cases
            # create key for histogram
            extraTag     = ''
            if   flavour == '' and tagCase != '': extraTag = '_'+tagCase
            elif flavour != '':                   extraTag = tagCase
            key = extraType + extraFlavour + extraTag
            # compute final weight
            finalWeight = copy.deepcopy(weight)
            if tagCase != '': finalWeight = copy.deepcopy(weightBTagEvents)
            # Fill distributions
            for var in Variables:
              if flavour != '': # skip non-inclusive flavour distributions for non observables
                if var not in Observables: continue
                else:
                  if tagCase not in Observables[var] and Type == 'reco': continue
              if Type == "truth" and var not in Observables and var not in ExtraTruthVariables: continue # do not fill distributions which are not meant to be filled at truth level
              if tagCase in Variables[var] or Type == 'truth':
                safeFill(var, key, Type, flavour, tagCase, Histograms, VariablesValues, finalWeight, settings['Debug'])

          # Fill b-tagging quantile distributions
          if Type == "reco" and nbJets > 0:
            for tagCase in ToFillTagCases:
              if tagCase == '': continue # we don't need b-tagging discriminant distributions in inclusive Z+jets
              # Fill with quantile of leading pt b-tagged jet and with the corresponding quantile combination bin (Ti2 only)
              quantile = bJets[0].btagQuantile
              h_btagWeight[flavour+'_'+tagCase].Fill(quantile, weightBTagEvents['nominal'])  # fill b-tagging quantile of leading pt b-tagged jet
              if tagCase == 'Ti2':
                combQuantile = getQuantileBin(bJets)
                if combQuantile == -1:
                  print("ERROR: unexpected quantile combination (-1) this shouldn't be possible, exiting")
                  sys.exit(1)
                h_btagCombWeight[flavour + '_Ti2'].Fill(combQuantile, weightBTagEvents['nominal'])  # fill combined quantile
              for obs in Observables: # loop over observables
                if tagCase not in Observables[obs]: continue
                if 'jjHF' in obs:
                  if nbJets > 1:
                    obsRef = obs.replace('jjHF','bb')
                    obsRef = obsRef.replace('HFjet','bjet')
                    ObsValue = VariablesValues[obsRef.replace('jj','bb')+'_reco'][0]
                elif 'jHF' in obs:
                  ObsValue = VariablesValues[obs.replace('jHF','b')+'_reco'][0]
                elif 'HF' in obs:
                  ObsValue = VariablesValues[obs.replace('HF','b')+'_reco'][0]
                else: ObsValue = VariablesValues[obs+'_reco'][0]
                safeFillTH2(h_btagWeight_vsObs, obs + extraFlavour + extraTag, ObsValue, quantile, weightBTagEvents)
                if tagCase == 'Ti2':
                  safeFillTH2(h_btagCombWeight_vsObs, obs + extraFlavour + extraTag, ObsValue, combQuantile, weightBTagEvents)

        # Save new hf truth distribution
        if Type == 'truth' and 'hf_jet0_pt' in ExtraTruthVariables and 'hf_jet0_pt_truth' in VariablesValues:
          key = extraType
          safeFill('hf_jet0_pt', key, Type, '', '', Histograms, VariablesValues, finalWeight, settings['Debug'])

      ########################
      # Fill 2D distributions
      ########################
      # How do I match the Te1 and Ti2 to truth? Do I only care about Flav flag?
      if settings['MC'] and settings['makeTMs']:
        if passReco and passTruth:
          if settings['PrintMatchedInfo'] and settings['MC']:
            print("#################################################################################")
            print("Event passes reco and truth selections")
            print("EVENTINFO: EventNumber: " + str(Branches["eventNumber"][0]))
          if settings['Debug']: print("DEBUG: Filling 2D histograms")
          for flavour in ToFillTruthFlavours:
            if flavour in ToFillRecoFlavours:
              for tagCase in ToFillRecoTagCases:
                if tagCase == '': continue  # TMs are not needed for inclusive Z+jets
                extraTag = '_'+tagCase
                for obs in Observables:
                  if tagCase not in Observables[obs]: continue
                  # Non-HF observable
                  if 'F' not in obs:
                    if obs+'_reco' in VariablesValues and obs+'_truth' in VariablesValues:
                      # protection
                      if len(VariablesValues[obs+'_reco']) != len(VariablesValues[obs+'_truth']):
                        print('ERROR: number of values between reco and truth do not match for '+obs+', exiting...')
                        sys.exit(0)
                      if flavour != '' and flavour != 'FlavL_':
                        for ivalue in range(0,len(VariablesValues[obs+'_reco'])):
                          safeFillTH2(Histograms_2D, flavour + obs + extraTag, VariablesValues[obs+'_reco'][ivalue], VariablesValues[obs+'_truth'][ivalue], weightBTagEvents, True)
                  else:  # HF-based observable
                    if 'jjHF' in obs:
                      varb = obs.replace('jjHF','bb')
                      varc = obs.replace('jjHF','cc')
                      if 'jj' in obs:
                        varb = varb.replace('jj','bb')
                        varc = varc.replace('jj','cc')
                    elif 'jHF' in obs:
                      varb = obs.replace('jHF','b')
                      varc = obs.replace('jHF','c')
                    elif 'HFj' in obs:
                      varb = obs.replace('HFj','bj')
                      varc = obs.replace('HFj','cj')
                    if flavour == "FlavA1B_":
                      if varb+'_reco' in VariablesValues and varb+'_truth' in VariablesValues:
                        # protection
                        if len(VariablesValues[varb+'_reco']) != len(VariablesValues[varb+'_truth']):
                          print('ERROR: number of value between reco and truth do not match for '+varb+', exiting...')
                          sys.exit(0)
                        if flavour != '' and flavour != 'FlavL_':
                          for ivalue in range(0, len(VariablesValues[varb + '_reco'])):
                            safeFillTH2(Histograms_2D, flavour + obs + extraTag, VariablesValues[varb+'_reco'][ivalue], VariablesValues[varb+'_truth'][ivalue], weightBTagEvents, True)
                    elif flavour == "FlavA1C_":
                      if varb+'_reco' in VariablesValues and varc+'_truth' in VariablesValues:
                        # protection
                        if len(VariablesValues[varb+'_reco']) != len(VariablesValues[varc+'_truth']):
                          print('ERROR: number of value between reco and truth do not match for '+varb+' and '+varc+', exiting...')
                          sys.exit(0)
                        if flavour != '' and flavour != 'FlavL_':
                          for ivalue in range(0, len(VariablesValues[varb + '_reco'])):
                            safeFillTH2(Histograms_2D, flavour + obs + extraTag, VariablesValues[varb+'_reco'][ivalue], VariablesValues[varc+'_truth'][ivalue], weightBTagEvents, True)
        else:
          if settings['Debug']: print("DEBUG: Event did not passed simultaneuously reco and truth selections")
          if settings['Debug'] and passReco: print("DEBUG: Event passed reco selections")
          if settings['Debug'] and passTruth: print("DEBUG: Event passed truth selections")

    ##################################
    # Write histograms to a ROOT file
    ##################################
    if not ttree_counter:
      outFile = TFile('{}{}'.format(get_xrootd_prefix(OutName) if not settings['noxrootd'] else '', OutName), "RECREATE")
    else:
      outFile = TFile('{}{}'.format(get_xrootd_prefix(OutName) if not settings['noxrootd'] else '', OutName), "UPDATE")
      if not outFile:
        print('ERROR: {} can not be found, exiting...'.format(OutName))
        sys.exit(1)
    TTreeNames = settings['TTrees'].replace(',','_') # only used if TTrees != ['nominal','Full']
    FolderName = TTreeName if settings['TTrees'] != 'Full' and settings['TTrees'] != 'nominal' else TTreeNames
    outFile.mkdir(FolderName)
    TDir = outFile.Get(FolderName)
    if settings['MC']:
      TDir.WriteTObject(h_mcEventWeight)
      TDir.WriteTObject(h_sampleWeight)
      TDir.WriteTObject(h_totalWeight_woBTagSF)
      TDir.WriteTObject(h_leptonSF)
      TDir.WriteTObject(h_leptonRecoSF)
      TDir.WriteTObject(h_leptonIsoSF)
      TDir.WriteTObject(h_leptonPIDSF)
      TDir.WriteTObject(h_leptonTrigSF)
      TDir.WriteTObject(h_leptonTrigSF_vs_Zpt)
      TDir.WriteTObject(h_leptonTrigMCEff)
      TDir.WriteTObject(h_leptonTrigMCEff_vs_Zpt)
      TDir.WriteTObject(h_jvtSF)
      TDir.WriteTObject(h_btaggingSF)
    for Type in Types:
      TDir.WriteTObject(h_cutflow[Type])
      if Type == "reco":
        if nominal:
           TDir.WriteTObject(h_correctedAndScaledAverageMu)
           TDir.WriteTObject(h_averageInteractionsPerCrossing)
           TDir.WriteTObject(h_actualInteractionsPerCrossing)
           TDir.WriteTObject(h_correctedAndScaledActualMu)
           TDir.WriteTObject(h_correctedAverageMu)
           TDir.WriteTObject(h_correctedActualMu)
        for key in h_btagWeight:           TDir.WriteTObject(h_btagWeight[key])
        for key in h_btagCombWeight:       TDir.WriteTObject(h_btagCombWeight[key])
        for key in h_btagWeight_vsObs:     TDir.WriteTObject(h_btagWeight_vsObs[key])
        for key in h_btagCombWeight_vsObs: TDir.WriteTObject(h_btagCombWeight_vsObs[key])
      for key in Histograms: TDir.WriteTObject(Histograms[key])
    if settings['MC'] and settings['makeTMs']:
      for key in Histograms_2D: TDir.WriteTObject(Histograms_2D[key])
    outFile.Close()
    if settings['Perf']: printMemory(2)
    print("Histograms saved to " + OutName)
  print(">>> DONE <<<")
  if settings['Perf']:
    end = time.perf_counter()
    print("PERF: Time (s): "+str(end-start))
  if settings['cxaod_validation']:
    cxaod_val_output_file.close()
    print('File ({}) for comparing with CxAOD was produced'.format(cxaod_val_output_file_name))


if __name__ == '__main__':

  # Read arguments
  parser = argparse.ArgumentParser()
  parser.add_argument('--path',            action='store',      dest="path")
  parser.add_argument('--outPATH',         action='store',      dest="outPATH")
  parser.add_argument('--file',            action='store',      dest="inputFile")
  parser.add_argument('--jetcoll',         action='store',      dest="jetcoll",        default='PFlow')
  parser.add_argument('--channel',         action='store',      dest="channel")
  parser.add_argument('--dataset',         action='store',      dest="dataset")
  parser.add_argument('--selection',       action='store',      dest="selection")
  parser.add_argument('--tagger',          action='store',      dest="tagger",         default='DL1r')
  parser.add_argument('--debug',           action='store_true', dest="debug",          default=False)
  parser.add_argument('--test',            action='store_true', dest="test",           default=False)
  parser.add_argument('--ref',             action='store_true', dest="ref",            default=False)
  parser.add_argument('--noxrootd',        action='store_true', dest="noxrootd",       default=False)
  parser.add_argument('--perf',            action='store_true', dest="perf",           default=False)
  parser.add_argument('--noReco',          action='store_true', dest="noReco")
  parser.add_argument('--noTruth',         action='store_true', dest="noTruth")
  parser.add_argument('--noSFs',           action='store_true', dest="noSFs")
  parser.add_argument('--noSampleWeights', action='store_true', dest="noSampleWeights")
  parser.add_argument('--noPRW',           action='store_true', dest="noPRW")
  parser.add_argument('--readAMI',         action='store_true', dest="readAMI",        default=True)
  parser.add_argument('--localTest',       action='store_true', dest="localTest",      default=False)
  parser.add_argument('--CItest',          action='store_true', dest="ci_test",        default=False)
  parser.add_argument('--ttrees',          action='store',      dest="ttrees",         default='nominal')
  parser.add_argument('--printRecoInfo',   action='store_true', dest="printRecoInfo",  default=False)
  parser.add_argument('--printTruthInfo',  action='store_true', dest="printTruthInfo", default=False)
  parser.add_argument('--printMatchedInfo',action='store_true', dest="printMatchedInfo", default=False)
  parser.add_argument('--applySFsysts',    action='store_true', dest="applySFsysts",   default=False)
  parser.add_argument('--getWeightSysts',  action='store_true', dest="getWeightSysts", default=False)
  parser.add_argument('--cxaodval',        action='store_true', dest="cxaodval",       default=False)
  parser.add_argument('--useBootstraps',   action='store_true', dest="useBootstraps",  default=False)

  args = parser.parse_args()

  run_reader(args)
