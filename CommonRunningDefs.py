
# Location where the output ROOT files will be written
#BasePATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/"
#BasePATH = "/eos/user/j/jbossios/SM/WZgroup/ZHF/TreetoHists_outputs/"
BasePATH = '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TreetoHists_outputs/'
#DATE     = "17022022/"
#DATE     = "19022022/"
#DATE     = "01032022/"
#DATE     = "27042022/"
#DATE     = "29042022/"
#DATE     = "06052022/"
#DATE     = "08052022/"
#DATE     = "09052022/"
#DATE     = "19052022/"
#DATE     = "20052022/"
#DATE     = "22052022/"
#DATE     = "23052022/"
#DATE     = "04062022/"
#DATE     = "07062022/"
#DATE     = "21062022/"
#DATE     = "22062022/"
#DATE     = "23062022/"
#DATE     = "24062022/"
#DATE     = "28062022/"
#DATE     = "29062022/"
#DATE     = "01072022/"
#DATE     = "02072022/"
#DATE     = "12072022/"
#DATE     = "13072022/"
#DATE     = "14072022/"
#DATE     = "15072022/"
#DATE     = "26072022/"
#DATE     = "27072022/"
#DATE     = "20092022/"
#DATE     = "08112022/"
#DATE     = "09112022/"
#DATE     = "28112022/"
#DATE     = "01122022/"
#DATE     = "02122022/"
#DATE     = "15122022/"
DATE     = "16122022/"

# Fill Distributions (only meaningful for MC)
fillReco  = True
fillTruth = False

# Apply SF variations?
ApplySFsysts = False

# Produce distributions using alternative event weights
GetWgtSysts = False

# Systematics [options: 'nominal', 'Full', subset of TTrees separated by comman or any other single TTree]
TTrees = 'nominal'
#TTrees = 'FullSysts'

#####################################################
# DO NOT MODIFY, unless oyu know what you are doing
#####################################################

# Re-calculate sample weights
redoWeights = True

# Jet collection
JetColl = 'PFlow' # options: PFlow, EMTopo

# Flavour Jet Tagger
Tagger = "DL1r"

# MC Weights (only meaningful for MC, not used at all when running on data)
useSFs           = True  # reco, ID, isolation and trigger lepton SFs
useSampleWeights = True  # eventWeight * eff * xs / totalevents (sumWeights for Sherpa)
usePRW           = True  # pileup reweighting
