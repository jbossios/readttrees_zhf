
# pT bins for jet eta distribution
pTbins = ["30:40","70:100","100:150","200:300","500:700","700:1500"]

##################################
# Expected datasets
##################################
MC16campaigns  = ["a","d","e"]
SamplesTypes   = [
  "ZmumuMC16xSherpa",
  "ZmumuMC16xSherpa2210",
  "ZmumuMC16xSherpa2211",
  "ZeeMC16xSherpa",
  "ZeeMC16xSherpa2211",
  "ZmumuMC16xMG",
  "ZmumuMC16xFxFx",
  "ZeeMC16xMG",
  "ZeeMC16xFxFx",
  "Data15",
  "Data16",
  "Data17",
  "Data18",
  "MC16xTop",
  "MC16xTTbarNonAllHad",
  "MC16xTTbarDilepton",
  "MC16xSingleTop",
  "MC16xWenu",
  "MC16xWmunu",
  "MC16xDiboson",
  "MC16xZtautau",
  "MC16xVH",
  "MC16xWtaunu",
]
DatasetOptions = []
for dataset in SamplesTypes:
  if "Data" not in dataset:
    for campaign in MC16campaigns:
      sample = dataset.replace("MC16x","MC16"+campaign)
      DatasetOptions.append(sample)
  else:
    DatasetOptions.append(dataset)

##################################
# Hadron flavour classification
##################################
Flavours = {
  "reco"  : ["","FlavA1B_","FlavA1C_","FlavL_"], # Inclusive, exactly 1 B/C, at least 1 B/C, light
  "truth" : ["","FlavA1B_","FlavA1C_"], # Inclusive and at least 1 B/C
}

##################################
# Flavour Jet Taggers
##################################
Taggers = ["DL1r", "DL1", "MV2c10"]
