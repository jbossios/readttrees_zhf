import array

# Cutflow histograms
h_cutflow               = dict()
h_cutflow_all           = dict()
h_cutflow_trigger       = dict()
h_cutflow_jetCleaning   = dict()
h_cutflow_met           = dict()
h_cutflow_xAHselections = dict()
h_cutflow_lepton        = dict()
h_cutflow_Zmass         = dict()
h_cutflow_jet           = dict()
h_cutflow_Te1           = dict()
h_cutflow_Ti2           = dict()

##################################
# Observables and other variables
##################################

import copy

# List of observables for each type of b-tagging requirements
# This are the only variables where we produce TMs for them
Observables = {
  'Zpt'                  : ['','Te1','Ti2'],
  'deltaRZjHF'           : ['Te1','Ti2'],
  'HFjet0_pt'            : ['Te1','Ti2'],  # pt of the leading truth jet with same flavour as event flavour (b for FlavA1B, c for FlavA1C)
  'HFjet0_XF'            : ['Te1','Ti2'],
  'mjjHF'                : ['Ti2'],
  'deltaPhijjHF'         : ['Ti2'],
}

# Variables that will be used also at truth level, despite not being present in Observables dictionary
ExtraTruthVariables = {
  'Zmass'          : ['','Te1','Ti2'],
  'jet0_pt'        : ['','Te1','Ti2'],
  'jet0_y'         : ['','Te1','Ti2'],
  'lep0_pt'        : ['','Te1','Ti2'],
  'lep0_eta'       : ['','Te1','Ti2'],
  'lep1_pt'        : ['','Te1','Ti2'],
  'lep1_eta'       : ['','Te1','Ti2'],
  'deltaRll'       : ['','Te1','Ti2'],
  'deltaRlep0jet0' : ['','Te1','Ti2'],
  'hf_jet0_pt'     : [''],  # pt of the leading hf truth jet (w/o checking event flavour)
}

# List of all variables for each type of b-tagging requirements
# This also contains variables where we don't produce TMs for them
AllVariables = dict()
AllVariables = copy.deepcopy(Observables)
AllVariables['Zmass']                 = ['','Te1','Ti2']
AllVariables['Zabsy']                 = ['','Te1','Ti2']
AllVariables['deltaYZjHF']            = ['Te1','Ti2']
AllVariables['deltaPhiZjHF']          = ['Te1','Ti2']
AllVariables['pTjjHF']                = ['Ti2']
AllVariables['pTjjHFsum']             = ['Ti2']
AllVariables['pTjjovermjjHF']         = ['Ti2']
AllVariables['HFjet1pToverpTjjHF']    = ['Ti2']
AllVariables['HFjet1pToverpTjjHFsum'] = ['Ti2']
AllVariables['deltaYjjHF']            = ['Ti2']
AllVariables['deltaRjjHF']            = ['Ti2']
AllVariables['HFjet0_absy']           = ['Te1','Ti2']
AllVariables['HFjet1_pt']             = ['Ti2']
AllVariables['HFjet1_absy']           = ['Ti2']
AllVariables['HFjet1_XF']             = ['Ti2']
AllVariables['ht']                    = ['','Te1','Ti2']
AllVariables['met']                   = ['','Te1','Ti2']
AllVariables['njet']                  = ['','Te1','Ti2']
AllVariables['jet_pt']                = ['','Te1','Ti2']
AllVariables['jet_y']                 = ['','Te1','Ti2']
AllVariables['jet_phi']               = ['','Te1','Ti2']
AllVariables['jet0_pt']               = ['']
AllVariables['jet0_y']                = ['']
#AllVariables['jet0_phi']              = ['']
AllVariables['jet1_pt']               = ['']
AllVariables['jet1_y']                = ['']
#AllVariables['jet1_phi']              = ['']
#AllVariables['jet2_pt']    = ['']
#AllVariables['jet2_y']     = ['']
#AllVariables['jet2_phi']   = ['']
#AllVariables['deltaPhiZj'] = ['','Te1','Ti2']
#AllVariables['deltaYZj']   = ['','Te1','Ti2']
#AllVariables['deltaRZj']   = ['','Te1','Ti2']
#AllVariables['pTjj']       = ['','Ti2']
#AllVariables['mjj']        = ['','Ti2']
#AllVariables['pTjjovermjj']= ['','Ti2']
#AllVariables['deltaPhijj'] = ['','Ti2']
#AllVariables['deltaYjj']   = ['','Ti2']
#AllVariables['deltaRjj']   = ['','Ti2']
AllVariables['deltaRlep0jet0'] = ['','Te1','Ti2']
AllVariables['deltaRll']       = ['','Te1','Ti2']
AllVariables['lep_pt']         = ['','Te1','Ti2']
AllVariables['lep_eta']        = ['','Te1','Ti2']
AllVariables['lep_phi']        = ['','Te1','Ti2']
AllVariables['lep0_pt']        = ['','Te1','Ti2']
AllVariables['lep0_eta']       = ['','Te1','Ti2']
#AllVariables['lep0_phi']       = ['','Te1','Ti2']
AllVariables['lep1_pt']        = ['','Te1','Ti2']
AllVariables['lep1_eta']       = ['','Te1','Ti2']
#AllVariables['lep1_phi']       = ['','Te1','Ti2']

################################
# Binning of observables
################################

# Binning given by array
Binning       = dict()
Binning_array = dict()
# XF
#Binning['HFjet0_XF']             = [0,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.12,0.14,0.16,0.18,0.2,0.25,0.4]
#Binning['HFjet0_XF']             = [0,0.01,0.02,0.03,0.04,0.05,0.06,0.08,0.1,0.14,0.18,0.22,0.3,0.5] # to match CxAOD
Binning['HFjet0_XF']             = [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.08, 0.1, 0.14, 0.18, 0.22, 0.3, 0.5, 1.0] # to match CxAOD v2
Binning_array['HFjet0_XF']       = array.array('d',Binning['HFjet0_XF'])
Binning['HFjet1_XF']             = Binning['HFjet0_XF']
Binning_array['HFjet1_XF']       = Binning_array['HFjet0_XF']
# Zpt and Zpt_ZjjHF
Binning['Zpt']                   = [0, 15, 30, 40, 50, 60, 75, 90, 110, 130, 180, 240, 350, 500, 1000]
Binning_array['Zpt']             = array.array('d',Binning['Zpt'])
Binning['Zpt_ZjjHF']             = Binning['Zpt']
Binning_array['Zpt_ZjjHF']       = Binning_array['Zpt']
# pTjjHF
Binning['pTjjHF']                = [0, 5, 25, 40, 55, 70, 90, 110, 140, 190, 260, 400, 700]
Binning_array['pTjjHF']          = array.array('d',Binning['pTjjHF'])
Binning['pTjjHFsum']             = Binning['pTjjHF']
Binning_array['pTjjHFsum']       = Binning_array['pTjjHF']
Binning['pTjj']                  = Binning['pTjjHF']
Binning_array['pTjj']            = Binning_array['pTjjHF']
# mjjHF
Binning['mjjHF']                 = [0, 10, 30, 55, 80, 105, 130, 160, 190, 220, 260, 310, 400, 600, 900, 1200]
Binning_array['mjjHF']           = array.array('d',Binning['mjjHF'])
Binning['mjj']                   = Binning['mjjHF']
Binning_array['mjj'  ]           = Binning_array['mjjHF']
# pTjjHF/mjjHF
Binning['pTjjovermjjHF']         = [0, 0.05, 0.15, 0.3, 0.45, 0.65, 0.85, 1.2, 1.6, 2.2, 2.8, 3.8, 5.5]
Binning_array['pTjjovermjjHF']   = array.array('d',Binning['pTjjovermjjHF'])
Binning['pTjjovermjj']           = Binning['pTjjovermjjHF']
Binning_array['pTjjovermjj']     = Binning_array['pTjjovermjjHF']
# pT1HF/pTjjHF
Binning['HFjet1pToverpTjjHF']          = [0, 0.05, 0.10, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
Binning_array['HFjet1pToverpTjjHF']    = array.array('d',Binning['HFjet1pToverpTjjHF'])
Binning['HFjet1pToverpTjjHFsum']       = Binning['HFjet1pToverpTjjHF']
Binning_array['HFjet1pToverpTjjHFsum'] = Binning_array['HFjet1pToverpTjjHF']
# deltaRZjHF, deltaRjj, deltaYZjHF and deltaYjj
Binning['deltaRZjHF']            = [0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0]
Binning_array['deltaRZjHF']      = array.array('d',Binning['deltaRZjHF'])
Binning['deltaRjjHF']            = Binning['deltaRZjHF']
Binning_array['deltaRjjHF']      = Binning_array['deltaRZjHF']
Binning['deltaRjj']              = Binning['deltaRZjHF']
Binning_array['deltaRjj']        = Binning_array['deltaRZjHF']
Binning['deltaYZjHF']            = Binning['deltaRZjHF']
Binning_array['deltaYZjHF']      = Binning_array['deltaRZjHF']
Binning['deltaYjjHF']            = Binning['deltaRZjHF']
Binning_array['deltaYjjHF']      = Binning_array['deltaRZjHF']
Binning['deltaYjj']              = Binning['deltaRZjHF']
Binning_array['deltaYjj']        = Binning_array['deltaRZjHF']
Binning['deltaRZj']              = Binning['deltaRZjHF']
Binning_array['deltaRZj']        = Binning_array['deltaRZjHF']
Binning['deltaYZj']              = Binning['deltaRZjHF']
Binning_array['deltaYZj']        = Binning_array['deltaRZjHF']
Binning['deltaRll']              = Binning['deltaRZjHF']
Binning_array['deltaRll']        = Binning_array['deltaRZjHF']
Binning['deltaRlep0jet0']        = Binning['deltaRZjHF']
Binning_array['deltaRlep0jet0']  = Binning_array['deltaRZjHF']
# (HF-)jet pT
Binning['jet_pt']                = [17, 20, 24, 30, 36, 43, 60, 80, 100, 130, 170, 230, 330, 500, 750, 1200]
Binning_array['jet_pt']          = array.array('d',Binning['jet_pt'])
Binning['jet0_pt']               = Binning['jet_pt']
Binning_array['jet0_pt']         = Binning_array['jet_pt']
Binning['jet1_pt']               = Binning['jet_pt']
Binning_array['jet1_pt']         = Binning_array['jet_pt']
Binning['jet2_pt']               = Binning['jet_pt']
Binning_array['jet2_pt']         = Binning_array['jet_pt']
Binning['HFjet0_pt']             = Binning['jet_pt']
Binning_array['HFjet0_pt']       = Binning_array['jet_pt']
Binning['HFjet1_pt']             = Binning['jet_pt']
Binning_array['HFjet1_pt']       = Binning_array['jet_pt']
Binning['hf_jet0_pt']            = Binning['jet_pt']
Binning_array['hf_jet0_pt']      = Binning_array['jet_pt']
# Binning given by nBins, minVal and maxVal
# HT
Binning['ht']                    = [500,0,5000]
# MET
Binning['met']                   = [5000,0,5000]
# Zmass
#Binning['Zmass']                 = [20,70,110]
Binning['Zmass']                 = [120,40,160]
# Zabsy and HFjet0_absy
Binning['Zabsy']                 = [10,0,2.5]
Binning['HFjet0_absy']           = Binning['Zabsy']
Binning['HFjet1_absy']           = Binning['Zabsy']
# deltaPhiZjHF and deltaPhijj
Binning['deltaPhiZjHF']          = [12,0,3.15]
Binning['deltaPhijjHF']          = Binning['deltaPhiZjHF']
Binning['deltaPhijj']            = Binning['deltaPhiZjHF']
Binning['deltaPhiZj']            = Binning['deltaPhiZjHF']
# njet
Binning['njet']                  = [10,0,10]
# Jet eta / rapidity
Binning['jet_eta']               = [500,-4.5,4.5]
Binning['jet0_eta']              = Binning['jet_eta']
Binning['jet1_eta']              = Binning['jet_eta']
Binning['jet2_eta']              = Binning['jet_eta']
Binning['jet_y']                 = Binning['jet_eta']
Binning['jet0_y']                = Binning['jet_eta']
Binning['jet1_y']                = Binning['jet_eta']
Binning['jet2_y']                = Binning['jet_eta']
# Jet phi
Binning['jet_phi']               = [500,-3.25,3.25]
Binning['jet0_phi']              = Binning['jet_phi']
Binning['jet1_phi']              = Binning['jet_phi']
Binning['jet2_phi']              = Binning['jet_phi']
# mjj
Binning['mjj']                   = [500, 0, 5000]
# Lepton pT
Binning['lep_pt']                = [900, 0, 900]
Binning['lep0_pt']               = Binning['lep_pt']
Binning['lep1_pt']               = Binning['lep_pt']
# Lepton eta
Binning['lep_eta']               = [60,-3,3]
Binning['lep0_eta']              = Binning['lep_eta']
Binning['lep1_eta']              = Binning['lep_eta']
# Lepton phi
Binning['lep_phi']               = [500,-3.25,3.25]
Binning['lep0_phi']              = Binning['lep_phi']
Binning['lep1_phi']              = Binning['lep_phi']
