
import os
from checker import compare_to_ref
from run_reader import cases


def test():
    commands = []
    for case in cases:
	# Compare to reference
        compare_to_ref(case)


if __name__ == '__main__':
    test()
