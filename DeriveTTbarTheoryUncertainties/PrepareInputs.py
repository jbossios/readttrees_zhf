import ROOT
import sys
import os

# Import things from Plotter
sys.path.insert(1, '../Plotter') # insert at 1, 0 is the script path
from Luminosities import *

def prepare_input(version, ch, sel, campaign):
    luminosity = dict()
    luminosity['a'] = Luminosity_2015 + Luminosity_2016
    luminosity['d'] = Luminosity_2017
    luminosity['e'] = Luminosity_2018
    input_path = '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/'
    extra = '' if version == '29062022' else 'NonAllHad'
    if campaign != 'all':
        # Open input file
        input_file_name = input_path + "MC16{}TTbar{}_{}_{}_DL1rTagger_useSFs_useSampleWeights_usePRW_nominalOnly_All_{}_expanded.root".format(campaign, extra, ch, sel, version)
        input_file = ROOT.TFile.Open(input_file_name)
        print('Opening {}'.format(input_file_name))
        if not input_file:
            print('{} not found, exiting'.format(input_file_name))
            sys.exit(1)
        ## Get TDirectoryFile
        tdir = input_file.Get('nominal')
        # Get histograms
        hists_names = [key.GetName() for key in tdir.GetListOfKeys()]
        #hists_names = [key.GetName() for key in input_file.GetListOfKeys()]
        hists = []
        for name in hists_names:
            hist = tdir.Get(name)
            #hist = input_file.Get(name)
            if not hist:
                print('{} not found in {}, exiting'.format(name, input_file_name))
                sys.exit(1)
            hist.SetDirectory(0)
            hist.Scale(luminosity[campaign])
            hists.append(hist)
        # Open output file
        output_file_name = input_path
        output_file_name += "MC16{}TTbar{}_{}_{}_DL1rTagger_useSFs_useSampleWeights_usePRW_nominalOnly_All_{}_expanded_4PMGtool.root".format(campaign, extra, ch, sel, version)
        output_file = ROOT.TFile(output_file_name, 'RECREATE')
        output_file.mkdir('nominal')
        output_file.cd('nominal')
        for hist in hists:
            hist.Write()
        output_file.Close()
    else:  # MC16all
        # Merge ROOT files from each campaign
        input_files = [input_path + "MC16{}TTbar{}_{}_{}_DL1rTagger_useSFs_useSampleWeights_usePRW_nominalOnly_All_{}_expanded_4PMGtool.root".format(cpg, extra, ch, sel, version) for cpg in ['a', 'd', 'e']]
        output_file = input_path + "MC16allTTbar{}_{}_{}_DL1rTagger_useSFs_useSampleWeights_usePRW_nominalOnly_All_{}_expanded_4PMGtool.root".format(extra, ch, sel, version)
        command = 'hadd ' + output_file + ' ' + ' '.join(input_files)
        os.system(command)

if __name__ == '__main__':
    campaigns = ('a', 'd', 'e', 'all')  # options: 'a', 'd', 'e', 'all'
    channels = ['EL', 'MU']
    selection = 'SR'  # options: SR or SR2
    version = {
      'SR': '29062022',
      'SR2': '0222022',
    }[selection]
    for channel in channels:
        for campaign in campaigns:
            print('INFO: preparing inputs for MC16{}...'.format(campaign))
            prepare_input(version, channel, selection, campaign)
    print('>>> ALL DONE <<<')
