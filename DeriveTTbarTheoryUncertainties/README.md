# Derive ttbar theory uncertainties

## Setup

### First time

```
git clone ssh://git@gitlab.cern.ch:7999/atlas-physics/pmg/tools/systematics-tools.git
cd systematics-tools
source systematics-tools-bootstrap.sh
cd data/
source setupLHAPDF.sh
cd ../../
```

### Second time and beyond

```
cd systematics-tools/
source setupSystematicsTool.sh
cd data/
source setupLHAPDF.sh
cd ../../
```

## Derive uncertainties and make plots

Choose MC16 campaign and run ```derive_ttbar_theory_uncertainties.py```

Outputs will be located in the ```output_dir``` folder.

In particular, the uncertainty plot will be named ```xAHOutput_MC16x_OBS_EvtWgtVar.pdf``` with x = ['a', 'd', 'e', 'all'] and OBS the name of the observable.
