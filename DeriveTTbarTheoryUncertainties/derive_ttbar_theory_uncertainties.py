#!/usr/bin/env python

import systematicsTool as st  # import the systematicl tool
import readDatabase as rdb  # import the systematic database

# Import Python modules
import os
import sys
import copy

# Import things from DataDrivenEstimation_ttbar
sys.path.insert(1, '../DataDrivenEstimation_ttbar/') # insert at 1, 0 is the script path
from CommonDefs import Options as opt

def process_campaign(campaign, selection, channel):

    # Input folder
    indir = '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs'

    # Choose appropriate version version
    version = '29062022' if selection == 'SR' else '02122022'

    # Choose TTbar sample
    extra = '' if version == '29062022' else 'NonAllHad'

    # Set input file
    input_file = "MC16{}TTbar{}_{}_{}_DL1rTagger_useSFs_useSampleWeights_usePRW_nominalOnly_All_{}_expanded_4PMGtool.root".format(campaign, extra, channel, selection, version)

    # Loop over observables
    if selection == 'SR2':
      obs_weight_variations = {'Zmass': ['Ti1', 'Ti2']}
    else:
      Observables = opt['Observables']
      obs_weight_variations = copy.deepcopy(Observables)
    for obs, cases in obs_weight_variations.items():
        for case in cases:
            final_observable = '{}_{}'.format(obs, case)

            # Output folder
            outdir = 'output_dir/{}_{}_{}_{}'.format(selection, channel, obs, case)

            # Create output folder if it doesn't exist
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            # You can filter for only certain histogram names
            regexFilter = "nominal/{}_EvtWgtVar.*".format(final_observable)

            # You can veto any histogram names you with to skip
            regexVeto = None

            # Retrieve weights and combination recipes for ttbar nonallhad PPy8 sample
            weightList = rdb.getWeights('410470')[0]

            # Define the schema
            # i.e. the naming convention of how you stored the analysis objects in your files(s)
            schema = "!INDIR/!INFILE.root:nominal/!AONAME_!NSFWEIGHTNAME"

            # Derive uncertainties by combining per-weight histograms into per-variation analysis objects
            result = st.combineAllVariations(
                weightList,
                indir,
                outdir,
                regexFilter = regexFilter,
                regexVeto = regexVeto,
                returnOnlyVariationsInComination = True,
                schema = schema,
                inFile = input_file,
            )

            # Make plots
            plots = st.makeSystematicsPlotsWithROOT(
                result,
                outdir,
                nominalName = '_Nominal',
                label = "xAHOutput_MC16{}_".format(campaign),
                ratioZoom = None,
                regexFilter = regexFilter,
                regexVeto = regexVeto,
            )


if __name__ == '__main__':

    #campaigns = ['a', 'd', 'e', 'all']
    campaigns = ['all']  # individual campaigns suffer from low stats
    selection = 'SR'  # options: SR2 and SR
    channel = 'MU'  # options: EL and MU
    for campaign in campaigns:
        process_campaign(campaign, selection, channel)
    print('>>> ALL DONE <<<')
