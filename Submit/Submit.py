####################################################
#                                                  #
# Author: Jona Bossio (jbossios@cern.ch)           #
# Date:   30 November 2019                         #
#                                                  #
####################################################

Channel = "EL" # options: EL, MU and ELMU
Dataset = "ZeeMC16aMG" # options: ZmumuMC16xSherpa/MG/Sherpa2210/Sherpa2211, ZeeMC16xSherpa/MG, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16xZtautau, MC16xVH, MC16xWtaunu (where x = "a", "d" or "e")
Test    = False

# MC Weights (only meaningful for MC, not used at all when running on data)
useSFs           = True # reco, ID, isolation and trigger lepton SFs
useSampleWeights = True # eventWeight * eff * xs / totalevents (sumWeights for Sherpa)
usePRW           = True # pileup reweighting

# Fill Distributions (only meaningful for MC)
fillReco         = True
fillTruth        = True

# Flavour Jet Tagger
Tagger = "DL1r"

# Selection
Selection = "SR" # options: SR, SR20, SRnoMET, SRa1jet
#Selection = "VR" # options: SR, SR20, SRnoMET, SRa1jet
#Selection = "SRa1jet" # options: SR, SR20, SRnoMET, SRa1jet

# Jet collection
JetColl = "PFlow" # options: PFlow, EMTopo

# output path
BasePATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/"
DATE     = "20092021/" # w/ PRW and DL1r, using v6 TTrees

# Systematics
TTrees = 'nominal' # options: nominal, Full, subset of ttrees separated by comma

######################################################################
## DO NOT MODIFY
######################################################################

PATH = BasePATH + DATE

# old
#PATH = "/eos/user/j/jbossios/SM/WZgroup/ZHF/TreetoHists_outputs/25062020/"
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/25062020/" # soft met
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/03072020/" # final met
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/06072020/" # final met fixed
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/08072020/" # fixed Zpt and Zabs
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/13072020/" # derive SF uncertainties
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/05082020/" # w/ PRW
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/10082020/" # w/ PRW and DL1 (instead of DL1r)
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/16092020/" # w/ PRW and DL1r, extended truth and XF
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/17092020/" # w/ PRW and DL1r, extended truth and *fixed* XF
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/02102020/" # w/ PRW and DL1r, extended truth and *fixed* XF + now looking at zero jet events too
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/21102020/" # w/ PRW and DL1r, extended truth and *fixed* XF + now looking at zero jet events too (bugfixes and Sherpa2210)
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/10022021/" # w/ PRW and DL1r, using v4 TTrees
#PATH = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TreeToHists_outputs/25042021/" # w/ PRW and DL1r, using v4 TTrees (SR1jet)

import os,sys

# Create corresponding folder for logs
os.system('mkdir -p Logs/{}'.format(DATE))

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../')
from Arrays import *

# MC?
MC = False
if "MC" in Dataset:
  MC = True

# Protections
if "EL" not in Channel and "MU" not in Channel:
  print "ERROR: Channel not recognised, exiting"
  sys.exit(0)
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)
if JetColl != "PFlow" and JetColl != "EMTopo":
  print "ERROR: Jet collection not recognised, exiting"
  sys.exit(0)

# Find all the local Reader's outputs
ROOTfiles = []
AllFiles = os.listdir(PATH)
for File in AllFiles:
  if ".root" in File:
    ROOTfiles.append(File)

counter      = 0
path         = "../SubmissionScripts/"
localCommand = ""
for File in os.listdir(path): # Loop over submission scripts files
  if ".sub" not in File:
    continue
  if Dataset+'_' not in File:
    continue
  if Channel not in File:
    continue
  if JetColl not in File:
    continue
  if Selection+'Selection' not in File:
    continue
  if Tagger+"Tagger" not in File:
    continue
  if MC:
    if not fillReco and "noReco" not in File:
      continue
    if not fillTruth and "noTruth" not in File:
      continue
    if useSFs and "useSFs" not in File:
      continue
    if not useSFs and "noSFs" not in File:
      continue
    if useSampleWeights and "useSampleWeights" not in File:
      continue
    if not useSampleWeights and "noSampleWeights" not in File:
      continue
    if usePRW and "usePRW" not in File:
      continue
    if not usePRW and "noPRW" not in File:
      continue
    if TTrees == 'Full':
      if "fullSysts" not in File: continue
    elif TTrees == 'nominal':
      if "nominalOnly" not in File: continue
    else:
      if "partialSysts" not in File: continue

  # Check if there is an output already for this job
  ROOTfileFound = False
  FileName      = File.replace("Selection","")
  FileName      = FileName.replace(".sub","")
  FileName      = FileName.replace("noTruth","")
  for rootFile in ROOTfiles: # look at reader's outputs
    if FileName in rootFile: # there is already an output for this submission script
      ROOTfileFound = True
      break
  if ROOTfileFound:
    continue
  counter += 1
  command = "condor_submit "+path+File+" &"
  if not Test: os.system(command)
  else: # Print commands to run locally
    FileName = path+File.replace(".sub",".sh")
    File     = open(FileName,"r")
    for line in File:
      if "python" in line: localCommand += line + " && "
if Test: print localCommand[:-2]
if counter == 0:
  print "No need to send jobs"
else:
  if not Test: print str(counter)+" jobs will be sent"
  else: print str(counter)+" jobs need to be sent"
