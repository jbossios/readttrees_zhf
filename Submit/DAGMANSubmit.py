####################################################
#                                                  #
# Author: Jona Bossio (jbossios@cern.ch)           #
# Date:   30 November 2019                         #
#                                                  #
####################################################

# Channel + Selection [options: ELMU_VR, ELMU_AltVR, EL_SR, MU_SR, EL_MU_SR]
#Launch = 'EL_SRa1jet'
#Launch = 'MU_SRa1jet'
#Launch = 'EL_SR'
#Launch = 'MU_SR'
#Launch = 'ELMU_VR'
#Launch = 'ELMU_AltVR'
#Launch = 'EL_CR'
#Launch = 'MU_CR'
#Launch = 'EL_SR2'
Launch = 'MU_SR2'

# Test does not make the submission
Test = True
SendAllJobs = False # if there are no outputs to check, avoid wasting time on that
look_for_broken_outputs = True # this only works if SendAllJobs is False (runs only if remove_broken_output_files is False)
remove_broken_output_files = False # this only works if SendAllJobs is False
check_n_processed_events = False

datasets = [
#  "Data15",
#  "Data16",
#  "Data17",
#  "Data18",
#  "ZmumuMC16aSherpa2211",
#  "ZmumuMC16aMG",
#  "ZmumuMC16aFxFx",
#  "ZmumuMC16dSherpa2211",
#  "ZmumuMC16dMG",
#  "ZmumuMC16dFxFx",
#  "ZmumuMC16eSherpa2211",
#  "ZmumuMC16eMG",
#  "ZmumuMC16eFxFx",
#  "ZeeMC16aSherpa2211",
#  "ZeeMC16aMG",
#  "ZeeMC16aFxFx",
#  "ZeeMC16dSherpa2211",
#  "ZeeMC16dMG",
#  "ZeeMC16dFxFx",
#  "ZeeMC16eSherpa2211",
#  "ZeeMC16eMG",
#  "ZeeMC16eFxFx",
#  "MC16aTop",
#  "MC16dTop",
#  "MC16eTop",
  "MC16aTTbarNonAllHad",
  "MC16dTTbarNonAllHad",
  "MC16eTTbarNonAllHad",
#  "MC16aTTbarDilepton",
#  "MC16dTTbarDilepton",
#  "MC16eTTbarDilepton",
#  "MC16aSingleTop",
#  "MC16dSingleTop",
#  "MC16eSingleTop",
#  "MC16aWmunu",
#  "MC16dWmunu",
#  "MC16eWmunu",
#  "MC16aWenu",
#  "MC16dWenu",
#  "MC16eWenu",
#  "MC16aZtautau",
#  "MC16dZtautau",
#  "MC16eZtautau",
#  "MC16aDiboson",
#  "MC16dDiboson",
#  "MC16eDiboson",
#  "MC16aVH",
#  "MC16dVH",
#  "MC16eVH",
#  "MC16aWtaunu",
#  "MC16dWtaunu",
#  "MC16eWtaunu",
]

######################################################################
## DO NOT MODIFY
######################################################################

Debug = False

import logging
logging.basicConfig(level = 'INFO' if not Debug else 'DEBUG', format = '%(levelname)s: %(message)s')
log = logging.getLogger()

from check_root_files import check_files

launch_options = {
  'ELMU_VR': {'Channels':['ELMU'], 'Selection':'VR'},
  'ELMU_AltVR': {'Channels':['ELMU'], 'Selection':'AltVR'},
  'EL_SR': {'Channels':['EL'], 'Selection':'SR'},
  'MU_SR': {'Channels':['MU'], 'Selection':'SR'},
  'EL_SRa1jet': {'Channels':['EL'], 'Selection':'SRa1jet'},
  'MU_SRa1jet': {'Channels':['MU'], 'Selection':'SRa1jet'},
  'EL_CR': {'Channels':['EL'], 'Selection':'CR'},
  'MU_CR': {'Channels':['MU'], 'Selection':'CR'},
  'EL_SR2': {'Channels':['EL'], 'Selection':'SR2'},
  'MU_SR2': {'Channels':['MU'], 'Selection':'SR2'},
}[Launch]

Channels = launch_options['Channels']
Selection = launch_options['Selection']

import os,sys,ROOT

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../')
from Arrays import *
from SystematicTTrees  import *
from CommonRunningDefs import *

PATH = BasePATH + DATE

if JetColl != "PFlow":
  log.fatal("ERROR: Jet collection not recognised, exiting")
  sys.exit(0)

# Get TTree names
if TTrees == 'nominal':
  ttree_names = ['nominal']
elif TTrees == 'Full': # nominal + systs TTrees
  ttree_names = ['nominal'] + SystTTrees
elif TTrees == 'FullSysts': # nominal + systs TTrees
  ttree_names = ['FullSysts']
elif TTrees == 'AllSysts':
  ttree_names = SystTTrees
else:
  log.fatal('TTrees option ({}) not recognized, exiting'.format(TTrees))
  sys.exit(1)

# Protection
if len(Channels) > 1:
  print('ERROR: Can not handle submission of two channels simultaneously, exiting')
  sys.exit(1)

# Remove all .dag files
os.system('rm ../DAGMANScripts/*_{}_{}_{}{}/*dag*'.format(Channels[0], Tagger, Selection, '_FullSysts' if TTrees == 'FullSysts' else ''))

if not SendAllJobs:

  # Find all the Reader's outputs and remove broken root files
  all_folders = os.listdir(PATH)
  root_files = {Folder: os.listdir(PATH+Folder) for Folder in all_folders if Folder.split('_')[0] in datasets and Channels[0]+'_'+Tagger+'_'+Selection in Folder}
  
  if remove_broken_output_files:
    check_files(PATH, root_files, log)
  elif look_for_broken_outputs:
    check_files(PATH, root_files, log, False)
  
  # For debugging
  log.debug('all_folders = {}'.format(all_folders))
  log.debug('root_files  = {}'.format(root_files))

  # Get number of output files per job
  n_root_files = {key: len(list_files) for key, list_files in root_files.items()}

command = ''
total_jobs = 0
total_subjobs = 0
# Loop over channels
for channel in Channels:

  # Protections
  if "EL" not in channel and "MU" not in channel:
    log.fatal("ERROR: channel=={} not recognised, exiting".format(channel))
    sys.exit(0)

  # Loop over datasets
  for dataset in datasets:

    # Protections
    if dataset not in DatasetOptions:
      log.fatal("ERROR: dataset not recognised, exiting")
      sys.exit(0)
    if channel == 'EL' and 'Zmumu' in dataset:
      continue # skip undesired combination
    if channel == 'EL' and 'Wmunu' in dataset:
      continue # skip undesired combination
    if channel == 'MU' and 'Zee' in dataset:
      continue # skip undesired combination
    if channel == 'MU' and 'Wenu' in dataset:
      continue # skip undesired combination

    # MC?
    MC = False
    if "MC" in dataset:
      MC = True

    # Create .dag file
    counter = 0 # count subjobs for this dataset
    folder_name = "{}_{}_{}_{}".format(dataset, channel, Tagger, Selection)
    dag_folder_name = folder_name
    if TTrees == 'FullSysts':
      dag_folder_name += '_FullSysts'
    if not os.path.exists('../DAGMANScripts/{}'.format(dag_folder_name)):
      os.makedirs('../DAGMANScripts/{}'.format(dag_folder_name))
    dag_file_name = "../DAGMANScripts/{0}/{0}.dag".format(dag_folder_name)
    try:
      output_file = open(dag_file_name, "w")
    except IOError:
      log.fatal('ERROR: {} can not be opened, exiting'.format(dag_file_name))
      sys.exit(1)

    # Loop over TTree names
    for ttree_name in ttree_names:

      if 'Data' in dataset and ttree_name != 'nominal': continue # skip syst TTrees for data

      # Create corresponding folder for logs
      Extra = ''
      if   ttree_name == 'Full': Extra = "_Full"
      if   ttree_name == 'FullSysts': Extra = "_FullSysts"
      elif ttree_name == 'nominal': Extra = ""
      elif ',' not in ttree_name: Extra = "_"+ttree_name
      else: Extra = "_PartialSysts"
      log_folder_name = "{}{}".format(folder_name, Extra)
      if not os.path.exists('Logs/{}{}'.format(DATE, log_folder_name)):
        os.system('mkdir -p Logs/{}{}'.format(DATE, log_folder_name))

      # Path to submission scripts
      path = "../DAGMANScripts/{}/".format(log_folder_name)

      # Get list of submission files and count them
      sub_files = [file_name for file_name in os.listdir(path) if '.sub' in file_name]
      n_sub_files = len(sub_files)

      # Get name of the folder where outputs are located (stripping out PATH)
      folder_name_root_file = folder_name+'_'+ttree_name if ttree_name != 'nominal' else folder_name

      ## Check if I already have all the outputs for this main job
      # NOT DOING THIS ANYMORE SINCE I COULD HAVE A FILE FOR EACH JOB BUT A FILE COULD NOT HAVE ALL NEEDED DIRECTORIES
      #if not SendAllJobs:
      #  if n_sub_files == n_root_files[folder_name_root_file]: # the number of output root files matches the number of submission scripts
      #    continue # no need to check anything and no job needs to be sent

      # Send missing jobs
      send_all_jobs = True if n_sub_files == 0 else False
      if SendAllJobs: send_all_jobs = True
      for File in sub_files: # Loop over submission scripts files
        # Check if there is an output already for this job
        if not send_all_jobs:
          root_file_found = False
          sh_file_name = File.replace(".sub", ".sh")
          file_name = File.replace("Selection","")
          file_name = file_name.replace(".sub","")
          file_name = file_name.replace("noTruth","")
          # Make a list of labels that need to be on output file
          #file_labels = [label if label != 'VR' else '_VR' for label in file_name.split('_')] # take into account that VR is in AltVR
          file_labels = [label for label in file_name.split('_')]
          if MC:
            if ttree_name == 'nominal':
              output_file_name = '{}_{}_{}_{}_{}Tagger_useSFs_useSampleWeights_usePRW_nominalOnly_user.jbossios.{}_{}.tree.root'.format(file_labels[0], file_labels[1], file_labels[2], file_labels[3], Tagger, file_labels[4], file_labels[5])
            else: # systematic TTree
              output_file_name = '{}_{}_{}_{}_{}Tagger_useSFs_useSampleWeights_usePRW_{}_user.jbossios.{}_{}.tree.root'.format(file_labels[0], file_labels[1], file_labels[2], file_labels[3], Tagger, ttree_name, file_labels[4], file_labels[5])
          else: # datai
            output_file_name = '{}_{}_{}_{}_{}Tagger_user.jbossios.{}_{}.tree.root'.format(file_labels[0], file_labels[1], file_labels[2], file_labels[3], Tagger, file_labels[4], file_labels[5])
          output_file_name = output_file_name.replace('FullSysts', 'fullSysts')
          if output_file_name in root_files[folder_name_root_file]:
            # Check if output ROOT file has all the expected directories
            if TTrees == 'FullSysts':
              # Check if root file has all the expected directories
	      # open input file and count number of keys
              with open(f'../DAGMANScripts/{log_folder_name}/{sh_file_name}') as sh:
                for line in sh:
                  if 'python3' in line:
                    in_path = line.split(' ')[3]
                    in_file = line.split(' ')[7]
              in_full_root_file_name = in_path + in_file
              in_file = ROOT.TFile.Open(in_full_root_file_name)
              in_dir = in_file.Get("ZHFTreeAlgo")
              in_nkeys = len(set([inkey.GetName() for inkey in in_dir.GetListOfKeys()]))
	      # open output file and count number of keys
              out_full_root_file_name = PATH + folder_name_root_file + '/' + output_file_name
              out_file = ROOT.TFile.Open(out_full_root_file_name)
              out_nkeys = len(out_file.GetListOfKeys())
              if in_nkeys - 2  == out_nkeys: # do not consider AF2 systs
                root_file_found = True
              else: # output file needs to be removed
                log.warning('This output file is missing some directories, it will be removed: {}'.format(out_full_root_file_name))
                os.system('rm {}'.format(out_full_root_file_name))
              # check if all events were processed # Temporary
              if check_n_processed_events:
                for key in list(set([inkey.GetName() for inkey in in_dir.GetListOfKeys() if 'AF2' not in inkey.GetName()])):
                  # Get number of processed events
                  Dir = out_file.Get(key)
                  if not Dir:
                    log.error("{} not found in {}".format(key, out_full_root_file_name))
                  cutflow = Dir.Get('cutflow_reco')
                  if not cutflow:
                    log.error("cutflow_reco not found in Dir {} in {}".format(key, out_full_root_file_name))
                  nProcessed = cutflow.GetBinContent(1)
                  # Get number of input events
                  TTree = in_dir.Get(key)
                  nAll  = TTree.GetEntries()
                  if nAll != nProcessed:
                    log.error("This output file didn't process all input events ({}/{}), this file should be removed: {}".format(nProcessed, nAll, out_full_root_file_name))
            else:
              root_file_found = True
          else:
            log.debug('{} not found in {}'.format(output_file_name, root_files[folder_name_root_file]))
        send_job = False
        if send_all_jobs: send_job = True
        else:
          if not root_file_found: send_job = True
        if send_job:
          output_file.write("JOB A{} {}{}\n".format(counter, path, File))
          output_file.write("RETRY A{} 2\n".format(counter))
          counter += 1

    # Close .dag file
    output_file.close()
  
    if counter > 0:
      command += "condor_submit_dag "+dag_file_name+" && "
      total_jobs += 1
    total_subjobs += counter

command = command[:-2]
if not Test: os.system(command)
elif command: log.info(command)
if total_jobs == 0:
  log.info("No need to send jobs")
else:
  if not Test: log.info("{} jobs comprising {} subjobs will be sent".format(total_jobs,total_subjobs))
  else: log.info("{} jobs comprising {} subjobs need to be sent".format(total_jobs,total_subjobs))
