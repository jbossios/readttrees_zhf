from ROOT import TFile, TH1D
from multiprocessing import Pool
from functools import partial
import os
import logging

def check_file(root_file_name: str, path: str, key: str, log: logging.basicConfig, rm: bool = True) -> str:
    """Try to open ROOT file"""
    full_root_file_name = path+key+'/'+root_file_name
    try:
        _ = TFile.Open(full_root_file_name)
        _.Close()
    except IOError:
        # Remove broken file
        if rm:
            log.warning('This output file is broken, it will be removed: {}'.format(full_root_file_name))
            os.system('rm {}'.format(full_root_file_name))
        else:
            log.warning('This output file is broken, it should be removed but not done: {}'.format(full_root_file_name))
        return root_file_name
    except OSError:
        # Remove broken file
        if rm:
            log.warning('This output file is broken, it will be removed: {}'.format(full_root_file_name))
            os.system('rm {}'.format(full_root_file_name))
        else:
            log.warning('This output file is broken, it should be removed but not done: {}'.format(full_root_file_name))
        return root_file_name
    return 'None'

def check_files(path: str, root_files: str, log: logging.basicConfig, rm: bool = True):
    """Loop over root files in the root_files dictionary and check every one to see if it's broken"""
    for key, list_root_files in root_files.items():
        log.debug(f'Checking root files for key:{key}')
        check_file_defaults = partial(check_file, path = path, key = key, log = log, rm = rm)
        with Pool(6) as p:
            files_to_remove = p.map(check_file_defaults, list_root_files)
        files_to_remove_not_none = [root_file for root_file in files_to_remove if root_file != 'None']
        log.debug(f'Files to remove for {key}: {files_to_remove_not_none}')
        for root_file_name in files_to_remove_not_none:
            root_files[key].remove(root_file_name)

if __name__ == '__main__':

    import time
    start = time.perf_counter()

    # Create logger
    logging.basicConfig(level = 'DEBUG', format = '%(levelname)s: %(message)s') # Temporary
    log = logging.getLogger()

    # Find all ROOT files and flag those that need to be removed
    all_folders = os.listdir(path)
    all_files = {folder: os.listdir(path+folder) for folder in all_folders}

    check_files(path, root_files, log)
    end = time.perf_counter()
    log.debug(f'Total spent time = {end-start} s')
    log.info('>>> ALL DONE <<<')
