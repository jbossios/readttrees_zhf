
import argparse, sys

parser = argparse.ArgumentParser()
parser.add_argument('--channel', action = 'store', default = '', help = 'options: EL, MU, ELMU')
parser.add_argument('--selection', action = 'store', default = '', help = 'options: SR, VR, AltVR, SRa1jet, CR')
args = parser.parse_args()

if args.channel == '':
  print('ERROR: channel not provided, exiting')
  parser.print_help()
  sys.exit(1)
if args.selection == '':
  print('ERROR: selection not provided, exiting')
  parser.print_help()
  sys.exit(1)

Selection = args.selection
Channel = args.channel

Datasets      = [ # options: ZmumuMC16xSherpa, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16aZtautau, MC16xVH (where x = "a", "d" or "e")
###  "ZmumuMC16aSherpa",
###  "ZmumuMC16aSherpa2210",
###  "ZmumuMC16dSherpa",
###  "ZmumuMC16dSherpa2210",
###  "ZmumuMC16eSherpa",
###  "ZmumuMC16eSherpa2210",
###  "ZeeMC16aSherpa",
###  "ZeeMC16dSherpa",
###  "ZeeMC16eSherpa",
  "ZmumuMC16aSherpa2211",
#  "ZmumuMC16aMG",
  "ZmumuMC16aFxFx",
  "ZmumuMC16dSherpa2211",
#  "ZmumuMC16dMG",
  "ZmumuMC16dFxFx",
  "ZmumuMC16eSherpa2211",
#  "ZmumuMC16eMG",
  "ZmumuMC16eFxFx",
  "ZeeMC16aSherpa2211",
#  "ZeeMC16aMG",
  "ZeeMC16aFxFx",
  "ZeeMC16dSherpa2211",
#  "ZeeMC16dMG",
  "ZeeMC16dFxFx",
  "ZeeMC16eSherpa2211",
#  "ZeeMC16eMG",
  "ZeeMC16eFxFx",
  "Data15",
  "Data16",
  "Data17",
  "Data18",
  "MC16aTTbarNonAllHad",
  "MC16dTTbarNonAllHad",
  "MC16eTTbarNonAllHad",
  "MC16aTTbarDilepton",
  "MC16dTTbarDilepton",
  "MC16eTTbarDilepton",
  "MC16aSingleTop",
  "MC16dSingleTop",
  "MC16eSingleTop",
#  "MC16aTop",
#  "MC16dTop",
#  "MC16eTop",
  "MC16aDiboson",
  "MC16dDiboson",
  "MC16eDiboson",
  "MC16aVH",
  "MC16dVH",
  "MC16eVH",
  "MC16aWtaunu",
  "MC16dWtaunu",
  "MC16eWtaunu",
  "MC16aZtautau",
  "MC16dZtautau",
  "MC16eZtautau",
  "MC16aWmunu",
  "MC16dWmunu",
  "MC16eWmunu",
  "MC16aWenu",
  "MC16dWenu",
  "MC16eWenu",
]

######################################################################
## DO NOT MODIFY
######################################################################

import os

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../')
from SystematicTTrees  import *
from CommonRunningDefs import *

PATH = BasePATH+DATE

# Get TTree names
if TTrees == 'nominal':
  TTreeNames = ['nominal']
elif TTrees == 'Full': # nominal + systs TTrees
  TTreeNames = ['nominal']+SystTTrees
elif TTrees == 'AllSysts':
  TTreeNames = SystTTrees
elif TTrees == 'FullSysts':
  TTreeNames = ['FullSysts']
else:
  print('TTrees option ({}) not recognized, exiting'.format(TTrees))
  sys.exit(1)

commands = []
# Loop over TTree names
for TTreeName in TTreeNames:
  # Loop over datasets
  for Dataset in Datasets:
    # MC?
    MC = False
    if "MC" in Dataset:
      MC = True

    # Protections
    if Channel == 'EL' and 'Zmumu' in Dataset:
      continue # skip undesired combination
    if Channel == 'EL' and 'Wmunu' in Dataset:
      continue # skip undesired combination
    if Channel == 'MU' and 'Zee' in Dataset:
      continue # skip undesired combination
    if Channel == 'MU' and 'Wenu' in Dataset:
      continue # skip undesired combination
    if not MC and TTreeName == 'FullSysts':
      continue # skip systematic variations on data

    folder_name = '{}_{}_{}_{}{}/'.format(Dataset, Channel, Tagger, Selection, '_'+TTreeName if TTreeName!='nominal' else '')

    #command  = "hadd /eos/atlas/user/j/jbossios/SM/ZHFRun2/PlotterInputs/"
    command  = "hadd /eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/"
    command += Dataset+"_"+Channel+"_"+Selection+"_"+Tagger+"Tagger"
    if MC:
      command += "_"
      if useSFs:
        command += "useSFs"
      else:
        command += "noSFs"
      command += "_"
      if useSampleWeights:
        command += "useSampleWeights"
      else:
        command += "noSampleWeights"
      command += "_"
      if usePRW:
        command += "usePRW"
      else:
        command += "noPRW"
      if TTreeName == 'nominal':
        command += '_nominalOnly'
      else:
        command += '_{}'.format(TTreeName)
    command += "_All_"+DATE.replace('/','')+".root "+PATH
    command += folder_name
    command += Dataset+"_*"
    command += "_"+Channel
    command += "_"+Selection
    command += "_"+Tagger+"Tagger"
    if MC:
      command += "_"
      if useSFs:
        command += "useSFs"
      else:
        command += "noSFs"
      command += "_"
      if useSampleWeights:
        command += "useSampleWeights"
      else:
        command += "noSampleWeights"
      command += "_"
      if usePRW:
        command += "usePRW"
      else:
        command += "noPRW"
      if TTreeName == 'nominal':
        command += '_nominalOnly'
      else:
        command += '_{}'.format(TTreeName.replace('FullSysts', 'fullSysts'))
    command += "_*.root"
    commands.append(command)

# Execute all commands sequentially
FullCommand = ''
for command in commands:
  FullCommand += '{} && '.format(command)
FullCommand = FullCommand[:-2]
#print(FullCommand)
os.system(FullCommand)
