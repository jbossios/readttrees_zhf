
######################################################################
# DO NOT MODIFY (below this line)
######################################################################

import os,sys,ROOT

# Read arguments
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--removeFiles', action='store_true', dest='remove', default=False, help='If broken files are found, they will be deleted')
args   = parser.parse_args()
Remove = args.remove

if Remove:
  print('INFO: if broken files are found, they will be deleted')
else:
  print('INFO: if broken files are found, they will NOT be deleted (run with --removeFiles to delete them)')

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../')
from CommonRunningDefs import *

FullPATH = '{}/{}'.format(BasePATH,DATE)

Files = []

# Loop over files
for File in os.listdir(FullPATH):
  # try to open file
  try:
    iFile = ROOT.TFile.Open(FullPATH+File)
  except:
    Files.append(File)

# Remove files
commands = []
for File in Files:
  commands.append('rm {}'.format(FullPATH+File))
if len(commands) > 0:
  command = ' && '.join(commands)
  command += ' &'
  print(command)
  if Remove:
    os.system(command)
    print('{} files were deleted'.format(len(commands)))
  else:
    print('{} files should be deleted'.format(len(commands)))
else:
  print('There are no broken files')
