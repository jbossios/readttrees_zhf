#!/usr/bin/env python

####################################################################
#                                                                  #
# Purpose: Make ttbar validation plots                             #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

import ROOT,os,sys,resource,psutil,argparse
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--histname',  action='store',      dest="histname")
parser.add_argument('--selection', action='store',      dest="selection")
parser.add_argument('--tagger',    action='store',      dest="tagger")
parser.add_argument('--atlas',     action='store',      dest="atlas")
parser.add_argument('--samples',   action='store',      dest="samples")
parser.add_argument('--format',    action='store',      dest="outputFormat", default='PDF')
parser.add_argument('--debug',     action='store_true', dest="debug", default=False)
args = parser.parse_args()
# Protections
if args.histname is None:
  print 'ERROR: no histname provided, exiting'
  sys.exit(0)
if args.selection is None:
  print 'ERROR: selection not provided, exiting'
  sys.exit(0)
if args.tagger is None:
  print 'ERROR: tagger not provided, exiting'
  sys.exit(0)
if args.atlas is None:
  print 'ERROR: ATLAS legend not selected, exiting'
  sys.exit(0)
if args.samples is None:
  print 'ERROR: You have selected no sample, exiting'
  sys.exit(0)
HistName    = args.histname
Debug       = args.debug
Channel     = 'ELMU'
Selection   = args.selection
Tagger      = args.tagger
ATLASlegend = args.atlas
Samples     = args.samples.split(',')
Format      = {'PDF': 'pdf', 'PNG': 'png', 'JPG': 'jpg'}[args.outputFormat]

# Luminosity
from Luminosities import *

# Total luminosity and needed campaigns
Luminosity         = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"]= 0
Luminosity["MC16d"]= 0
Luminosity["MC16e"]= 0
Campaigns          = []
DataSamples        = [] # List of data samples
MCSignalSamples    = [] # List of MC ttbar samples
BackgroundSamples  = dict() # List of MC Background samples for each background type
DataPeriod         = 'Data'
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    Luminosity["MC16a"] += Luminosity_2015
    Campaigns.append("MC16a")
    MCSignalSamples.append("TTbar_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data16" in sample:
    DataPeriod += '16'
    Luminosity["MC16a"] += Luminosity_2016
    if "MC16a" not in Campaigns:
      Campaigns.append("MC16a")
      MCSignalSamples.append("TTbar_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data17" in sample:
    DataPeriod += '17'
    Luminosity["MC16d"] += Luminosity_2017
    Campaigns.append("MC16d")
    MCSignalSamples.append("TTbar_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data18" in sample:
    DataPeriod += '18'
    Luminosity["MC16e"] += Luminosity_2018
    Campaigns.append("MC16e")
    MCSignalSamples.append("TTbar_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
for sample in Samples:
  if "TTbar" not in sample and 'data' not in sample:
    BackgroundSamples[sample] = []
    for campaign in Campaigns:
      BackgroundSamples[sample].append(sample+"_"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)

# Calculate total luminosity
TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi
if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

Campaign   = "MC16"
for campaign in Campaigns:
  if Campaign != "MC16": Campaign += "+"
  if campaign == "MC16a": Campaign += "a"
  elif campaign == "MC16d": Campaign += "d"
  elif campaign == "MC16e": Campaign += "e"

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Loop over histograms
HistNames = [HistName] # Temporary
for histname in HistNames:
  Hist_MC_Backgrounds = dict()

  if "Weight" in histname: histname = Tagger+histname # pseudo-continuous b-tagging weight

  if Debug: print "###########################################################"
  if Debug: print "DEBUG: Producing PDF for '"+histname+"' histogram"
  if Debug: print "DEBUG: Memory usage = {0} (MB)".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024)
  if Debug: pid = os.getpid()
  if Debug: py  = psutil.Process(pid)
  if Debug: print "DEBUG: CPU[0] = {0} (%)".format(py.cpu_percent())

  ############################
  # Get Histograms
  ############################

  # Total data histogram
  if Debug: print "DEBUG: Get Data histogram"
  Hist_Data,msg = GetTotalHist(DataSamples,histname,Debug)
  if msg != 'OK':
    print msg
    print histname+" not found, exiting"
    sys.exit(0)
  if Debug: print "DEBUG: Data histogram retrieved"
  if Debug: print "DEBUG: Set line and fill color for data histogram"
  Hist_Data.SetLineColor(ROOT.kBlack)
  Hist_Data.SetFillColor(ROOT.kBlack)

  # Total MC Signal histogram
  if Debug: print "DEBUG: Get MC Signal histograms"
  Hist_MC_Signal,msg = GetTotalHist(MCSignalSamples,histname,Debug,Luminosity)
  if msg != 'OK':
    print msg
    print histname+" not found, exiting"
    sys.exit(0)
  Hist_MC_Signal.SetLineColor(ROOT.kRed+1)

  # Histogram for each background sample
  if Debug: print "DEBUG: Get MC Background histograms"
  for key,value in BackgroundSamples.iteritems():
    hist,msg = GetTotalHist(value,histname,Debug,Luminosity)
    if msg != 'OK':
      print msg
      sys.exit(0)
    else: Hist_MC_Backgrounds[key] = hist
  # Set line color and scale background histograms
  counter = 0
  for key,hist in Hist_MC_Backgrounds.iteritems():
    hist.SetLineColor(Colors[counter])
    hist.SetFillColor(Colors[counter])
    counter += 1

  ########################
  # Make Plot
  ########################

  # TCanvas
  if Debug: print "DEBUG: Create TCanvas"
  Canvas  = ROOT.TCanvas()
  outName = "Plots/{0}/{1}/{2}/{3}_{4}_{5}_{6}.{7}".format(Selection,Tagger,DataPeriod,Channel,Selection,Tagger,histname,Format)
  Canvas.Print(outName+"[")

  # TPad for upper panel
  if Debug: print "DEBUG: Create TPad for upper panel"
  pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
  pad1.SetTopMargin(0.08)
  pad1.SetBottomMargin(0.03)
  pad1.Draw()
  pad1.cd()

  # Set log scales (if requested)
  if Debug: print "DEBUG: Set log scales if requested"
  histnameBase = histname.replace('_Te1','')
  histnameBase = histnameBase.replace('_Ti2','')
  histnameBase = histnameBase.replace('_Ti1','')
  if histnameBase in Logx:
    pad1.SetLogx()
  if histnameBase in Logy:
    pad1.SetLogy()
  if "Weight" in histname: pad1.SetLogy()

  # Add histograms to THStack and draw legends
  Legends = ROOT.TLegend(0.7,0.43,0.92,0.9)
  Legends.SetTextFont(42)

  Hist_Data.Draw("E P")
  Hist_Data.GetXaxis().SetLabelSize(0.)
  Hist_Data.GetXaxis().SetTitleSize(0.)
  Hist_Data.GetYaxis().SetTitleSize(20)
  Hist_Data.GetYaxis().SetTitleFont(43)
  Hist_Data.GetYaxis().SetLabelFont(43)
  Hist_Data.GetYaxis().SetLabelSize(19)
  Hist_Data.GetYaxis().SetTitleOffset(1.3)
  Hist_Data.GetYaxis().SetTitle("Events / bin-width")
  Hist_Data.SetMinimum(1)
  Legends.AddEntry(Hist_Data,"Data","p")

  # THStack for MC samples
  StackMC = ROOT.THStack()

  # Add MC backgrounds to StackMC
  for key,hist in Hist_MC_Backgrounds.iteritems():
    StackMC.Add(hist,"HIST][")
    if key == "SingleTop":     legend = "Single-top"
    elif key == "Wmunu":       legend = "W(#rightarrow#mu#nu)+jets"
    elif key == "Wenu":        legend = "W(#rightarrow e#nu)+jets"
    elif key == "Wtaunu":      legend = "W(#rightarrow#tau#nu)+jets"
    elif key == "ZeeSH2211":   legend = "Z(#rightarrow e e)+jets Sherpa 2.2.11"
    elif key == "ZeeMG":       legend = "Z(#rightarrow e e)+jets MG"
    elif key == "ZmumuSH2211": legend = "Z(#rightarrow#mu#mu)+jets Sherpa 2.2.11"
    elif key == "ZmumuMG":     legend = "Z(#rightarrow#mu#mu)+jets MG"
    elif key == "Ztautau":     legend = "Z(#rightarrow#tau#tau)+jets"
    else: legend = key
    Legends.AddEntry(hist,legend,"f")

  StackMC.Add(Hist_MC_Signal,"HIST][")
  Legends.AddEntry(Hist_MC_Signal,"t#bar{t}","l")
  
  if Debug: print "DEBUG: Draw THStack with MC histograms"
  StackMC.Draw("same")

  if Debug: print "DEBUG: Draw data histogram"
  Hist_Data.Draw("same")
  
  if XaxisRange.has_key(histnameBase):
    StackMC.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
    Hist_Data.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
  maxY = Hist_Data.GetMaximum()
  if StackMC.GetMaximum() > maxY: maxY = StackMC.GetMaximum()
  maxYscaling = 1E5
  if "absy" in histname or "phi" in histname or "eta" in histname or "deltaR" in histname or "deltaPhi" in histname or "Weight" in histname: maxYscaling = 2E7
  if "Weight" in histname: maxYscaling = 1E9
  StackMC.GetYaxis().SetRangeUser(1,maxY*maxYscaling)
  Hist_Data.GetYaxis().SetRangeUser(1,maxY*maxYscaling)

  StackMC.GetXaxis().SetLabelSize(0.)
  StackMC.GetXaxis().SetTitleSize(0.)
  StackMC.GetYaxis().SetTitleSize(20)
  StackMC.GetYaxis().SetTitleFont(43)
  StackMC.GetYaxis().SetLabelFont(43)
  StackMC.GetYaxis().SetLabelSize(19)
  StackMC.GetYaxis().SetTitleOffset(1.3)
  StackMC.GetYaxis().SetTitle("Events / bin-width")

  if Debug: print "DEBUG: Draw legends"
  Legends.Draw("same")

  ROOT.gPad.RedrawAxis()

  # Show ATLAS legend
  if Debug: print "DEBUG: Show ATLAS legend"
  if ATLASlegend != 'NONE':
    if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
    elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
    elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
    else:
      print "ERROR: ATLASlegend not recognized, exiting"
      sys.exit(0)
    ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
    ATLASBlock.SetNDC()
    ATLASBlock.Draw("same")

  # Show CME and luminosity
  if Debug: print "DEBUG: Show CME and luminosity"
  CME = "#scale[1.5]{13 TeV, "+str(round(TotalLumi/1000,1))+" fb^{-1}}"
  topY = 0.7 if ATLASlegend != 'NONE' else 0.8
  CMEblock = ROOT.TLatex(0.2,topY,CME)
  CMEblock.SetNDC()
  CMEblock.Draw("same")

  # Show channel
  if Debug: print "DEBUG: Show channel type"
  channel = "#scale[1.5]{VR e^{#pm}#mu^{#mp}}"
  topY = 0.6 if ATLASlegend != 'NONE' else 0.7
  TextBlock = ROOT.TLatex(0.2,topY,channel)
  TextBlock.SetNDC()
  TextBlock.Draw("same")
  
  # TPad for bottom plot
  if Debug: print "DEBUG: Create TPad for bottom panel"
  Canvas.cd()
  pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
  pad2.SetTopMargin(0.03)
  pad2.SetBottomMargin(0.32)
  pad2.Draw()
  pad2.cd()

  # Set log-y scale (if requested)
  if Debug: print "DEBUG: Set log-Y scale if requested"
  if histnameBase in Logx:
    pad2.SetLogx()

  # Create data/MC histogram
  if Debug: print "DEBUG: Create MC/data histogram"
  ratioHist = Hist_MC_Signal.Clone("ratioHist")
  ratioHist.SetLineColor(ROOT.kRed+1)
  ratioHist.SetMarkerColor(ROOT.kRed+1)
  for key,hist in Hist_MC_Backgrounds.iteritems():
    ratioHist.Add(hist)
  ratioHist.Divide(Hist_Data)

  # Set y-axis range of ratio panel
  minY = 0.35
  maxY = 1.65
  if "Weight" in histname:
    minY = -0.05
    maxY = 3.05
  ratioHist.SetMinimum(minY)
  ratioHist.SetMaximum(maxY)

  # Set x-axis range of ratio panel
  nbins = ratioHist.GetNbinsX()
  if histnameBase in XaxisRange:
    minX = XaxisRange[histnameBase][0]
    maxX = XaxisRange[histnameBase][1]
  else:
    minX = ratioHist.GetXaxis().GetBinLowEdge(1)
    maxX = ratioHist.GetXaxis().GetBinUpEdge(nbins)
  ratioHist.GetXaxis().SetRangeUser(minX,maxX)
 
  # Draw data/MC ratio
  if Debug: print "DEBUG: Draw MC/data ratio"
  ratioHist.Draw("e0")

  # Draw line at data/MC==1
  if Debug: print "DEBUG: Draw line at MC/data==1"
  Line = ROOT.TLine(minX,1,maxX,1)
  Line.SetLineStyle(7)
  Line.Draw("same")

  # Set x-axis title
  if Debug: print "DEBUG: Set X-axis title"
  histkey = histnameBase
  if XaxisTitles.has_key(histkey):
    ratioHist.GetXaxis().SetTitleSize(20)
    ratioHist.GetXaxis().SetTitleFont(43)
    ratioHist.GetXaxis().SetLabelFont(43)
    ratioHist.GetXaxis().SetLabelSize(19)
    ratioHist.GetXaxis().SetTitleOffset(3)
    ratioHist.GetXaxis().SetTitle(XaxisTitles[histkey])
    ratioHist.GetXaxis().SetNdivisions(510)

  ratioHist.GetYaxis().SetTitleSize(20)
  ratioHist.GetYaxis().SetTitleFont(43)
  ratioHist.GetYaxis().SetLabelFont(43)
  ratioHist.GetYaxis().SetLabelSize(19)
  ratioHist.GetYaxis().SetTitleOffset(1.3)
  ratioHist.GetYaxis().SetTitle("MC / Data")

  # Save PDF
  if Debug: print "DEBUG: Save/print PDF"
  Canvas.Print(outName)
  Canvas.Print(outName+"]")

  # Save ratio hists into a ROOT file
  outROOTFile = ROOT.TFile('RatioHists/'+Selection+'/'+Tagger+'/'+DataPeriod+'/'+histname+'.root','RECREATE')
  outROOTFile.cd()
  ratioHist.Write()
  outROOTFile.Close()

  ratioHist.Delete()
  # Clear dict
  for key,hist in Hist_MC_Backgrounds.iteritems():
    hist.Delete()
  Hist_MC_Backgrounds.clear()
  Hist_Data.Delete()
  Hist_MC_Signal.Delete()

print '>>> DONE <<<'
