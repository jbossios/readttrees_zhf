# Luminosity
Luminosity_2015 = 3219.56 # [pb^-1] The luminosity of data15: 3.21956 fb-1 (checked on May 4 2020, using OflLumi-13TeV-010 and L1_EM12)
Luminosity_2016 = 32995.4 # [pb^-1] The luminosity of data16: 32.9954 fb-1 (updated on May 4 2020, using OflLumi-13TeV-010 and L1_EM12)
Luminosity_2017 = 44307.4 # [pb^-1] The luminosity of data17: 44.3074 fb-1 (checked on May 4 2020, using OflLumi-13TeV-010 and L1_EM24VHI)
Luminosity_2018 = 58450.1 # [pb^-1] The luminosity of data18: 58.4501 fb-1 (checked on May 4 2020, using OflLumi-13TeV-010 and L1_EM24VHI)

