
####################################################################
#                                                                  #
# Purpose: Run Data_vs_MC.py and MCplots.py                        #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

DataSamples = [
  'Signal_data15',
  'Signal_data16',
#  'Signal_data17',
#  'Signal_data18',
]

Backgrounds= [
  'SingleTop',
  'Diboson',
  'ZeeSH2211',
  #'ZeeMG',
  'ZmumuSH2211',
  #'ZmumuMG',
  'Ztautau',
  'Wenu',
  'Wmunu',
  'Wtaunu',
  'VH',
]

Selection    = "VR" # options: VR and AltVR
Tagger       = "DL1r"
ATLASlegend  = "Internal" # Options: Internal, Preliminary, ATLAS and NONE
OutputFormat = 'PDF' # Options: PDF, PNG, JPG

###########################################################################
# DO NOT MODIFY
###########################################################################

#######################################
# Add Ti1 distributions to input files
#######################################

# Make full list of samples
import copy
ExtendedSamples = copy.deepcopy(DataSamples)
# add MC signal samples
for sample in DataSamples:
  if '15' in sample or '16' in sample:
    if 'TTbarNonAllHad_MC16a' not in ExtendedSamples:
      ExtendedSamples.append('TTbarNonAllHad_MC16a')
  elif '17' in sample:
    ExtendedSamples.append('TTbarNonAllHad_MC16d')
  elif '18' in sample:
    ExtendedSamples.append('TTbarNonAllHad_MC16e')
# add MC background samples
for bkg in Backgrounds:
  if 'TTbarNonAllHad_MC16a' in ExtendedSamples: ExtendedSamples.append(bkg+'_MC16a')
  if 'TTbarNonAllHad_MC16d' in ExtendedSamples: ExtendedSamples.append(bkg+'_MC16d')
  if 'TTbarNonAllHad_MC16e' in ExtendedSamples: ExtendedSamples.append(bkg+'_MC16e')

from InputFiles import *
from ROOT       import *
import os,sys

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *

Flavours = ['']

# Loop over samples
for sample in ExtendedSamples:
  key = sample + '_ELMU_' + Selection + '_' + Tagger
  # Check if expanded file exists
  checkFile = TFile.Open(InputFiles[key])
  if checkFile: continue # skip sample (already has an expanded root file)
  # Open input file
  InputFileName = InputFiles[key].replace('_expanded','')
  File = TFile.Open(InputFileName)
  if not File:
    print 'ERROR: '+InputFileName+' not found, exiting'
    sys.exit(0)
  Hists = [] # new histograms
  # Loop over flavours
  for flavour in Flavours:
    # Loop over variables
    for var in AllVariables:
      if 'Te1' not in AllVariables[var]: continue # skip two-b-tagged-jet variables
      if 'lep_' in var: continue # skip lepton kinematic plots in ELMU channel
      
      # Sum Te1 and Ti2 1D histograms

      # Open input hists
      Ti2_histname = flavour + '_' + var + '_Ti2' if flavour != '' else var + '_Ti2'
      Ti2_hist = File.Get('nominal/'+Ti2_histname)
      if not Ti2_hist:
        print 'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting'
	sys.exit(0)
      Ti2_hist.SetDirectory(0)
      Te1_histname = flavour + '_' + var + '_Te1' if flavour != '' else var + '_Te1'
      Te1_hist = File.Get('nominal/'+Te1_histname)
      if not Te1_hist:
        print 'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting'
        sys.exit(0)
      Te1_hist.SetDirectory(0)
      Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
      Hist.Add(Te1_hist)
      Hists.append(Hist)

  # Open output file
  Dir  = File.Get('nominal')
  keys = Dir.GetListOfKeys()
  outFile = TFile(InputFiles[key],'RECREATE')
  for key in keys:
    Hist = key.ReadObj()
    outFile.cd()
    Hist.Write()
  for hist in Hists:
    outFile.cd()
    hist.Write()
  outFile.Close()
  File.Close()

###############
# Run plotters
###############

Samples = ["TTbarNonAllHad"]
for sample in DataSamples:          Samples.append(sample)
for sample in Backgrounds:          Samples.append(sample)

# Create output folder
os.system('mkdir Plots')
os.system('mkdir Plots/'+Selection)
os.system('mkdir Plots/'+Selection+'/'+Tagger)
os.system('mkdir RatioHists')
os.system('mkdir RatioHists/'+Selection)
os.system('mkdir RatioHists/'+Selection+'/'+Tagger)
DataPeriod = 'Data'
for sample in Samples:
  if 'data15' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data15')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data15')
    DataPeriod += '15'
  elif 'data16' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data16')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data16')
    DataPeriod += '16'
  elif 'data17' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data17')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data17')
    DataPeriod += '17'
  elif 'data18' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data18')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data18')
    DataPeriod += '18'
if 'Signal_data15' in DataSamples and 'Signal_data15' in DataSamples:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data1516')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data1516')
os.system('mkdir Plots/'+Selection+'/'+Tagger+'/'+DataPeriod)
os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/'+DataPeriod)

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *

# Get list of histogram names from ReadTTrees_ZHF
HistNames = []
for var,cases in AllVariables.iteritems():
  for case in cases:
    if case != '': HistNames.append(var+'_'+case)
    else:          HistNames.append(var)
  if '' in cases or 'Te1' in cases:
    HistNames.append(var+'_Ti1')

# Construct strings with selected samples (each period alone and all together)
samples = []
# Add Full Run 2
if len(DataSamples) > 1:
  Full    = ''
  for sample in Samples: Full += sample+','
  Full = Full[:-1]
  samples.append(Full)
# Add data15+16 if both available
if 'Signal_data15' in DataSamples and 'Signal_data16' in DataSamples:
  String = 'Signal_data15,Signal_data16'
  for sample in Samples:
    if 'data' not in sample: String += ','+sample
  samples.append(String)
import copy
# Get list of MC samples
MCsamples   = copy.deepcopy(Samples)
for sample in Samples:
  if 'data' in sample: MCsamples.remove(sample)
# Get list of data samples
Datasamples = copy.deepcopy(Samples)
for sample in Samples:
  if 'data' not in sample: Datasamples.remove(sample)
for sample in Datasamples:
  string = sample+','
  for MCsample in MCsamples: string += MCsample+','
  string = string[:-1]
  samples.append(string)

Campaign = "MC16"
for sample in Samples:
  if '15' in sample or '16' in sample:
    if 'a' not in Campaign: Campaign += "a"
  elif '17' in sample:
    if Campaign != 'MC16': Campaign += "+"
    Campaign += "d"
  elif '18' in sample:
    if Campaign != 'MC16': Campaign += "+"
    Campaign += "e"

# Loop over set of samples
counter = 0
for sample in samples:
  command = ''
  # Run Data_vs_MC.py for each histogram
  for name in HistNames: # loop over histograms
    command += 'python ttbarValidation.py --histname '+name
    command += ' --selection '+Selection
    command += ' --tagger '+Tagger
    command += ' --atlas '+ATLASlegend
    command += ' --samples '+sample
    command += ' --format '+OutputFormat
    command += ' && '
  command = command[:-2]
  os.system(command)
  counter += 1

