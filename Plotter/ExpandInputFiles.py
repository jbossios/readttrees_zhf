
####################################################################
#                                                                  #
# Purpose: Prepare _expanded ROOT files containing Ti2 hists       #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

import argparse
import sys
import copy
from InputFiles             import *
from ROOT                   import *
from PlotterHelperFunctions import *
import os

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts       import *
from Systematics import *
from SystematicTTrees import *

if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--channel', action='store', default = '', help = 'options: EL, MU, ELMU')
  parser.add_argument('--selection', action='store', default = '', help = 'options: SR, VR, AltVR, SRa1jet, CR and SR2')
  parser.add_argument('--ttrees', action='store', default = '', help = 'options: nominal, FullSysts, MCSysts, truth')
  parser.add_argument('--SFvariations', action='store', default = True, help = 'merge SF-based variatied histograms (only TTbar hists)')
  parser.add_argument('--debug', action='store_true', default = False, help = 'Increase verbosity')
  args = parser.parse_args()

  if not args.channel:
    print('ERROR: channel not set, exiting')
    parser.print_help()
    sys.exit(0)
  if not args.selection:
    print('ERROR: selection not set, exiting')
    parser.print_help()
    sys.exit(0)
  if not args.ttrees:
    print('ERROR: ttrees not set, exiting')
    parser.print_help()
    sys.exit(0)

  Channel = args.channel
  Selection = args.selection
  conf = '{}_{}'.format(Channel, Selection)
  TTrees = args.ttrees
  MergeVariatedHists = args.SFvariations if TTrees == 'nominal' else False  # only needed for TTbar EL/MU SR and ELMU VR/AltVR (nominal TTrees)
  if args.selection == 'CR' or args.selection == 'SR2':
    MergeVariatedHists = False

  DataSamples = [
    'Signal_data15',
    'Signal_data16',
    'Signal_data17',
    'Signal_data18',
  ]

  def get_bkg_samples(conf, ttrees):
    if ttrees != 'MCSysts':
      if 'SR2' in conf:
        return [
          'TTbarNonAllHad',
        ]
      elif 'SR' in conf:
        return [
          'Top',
          'TTbarNonAllHad',
          'TTbarDilepton',
          'Diboson',
          'Ztautau',
          'Wjets',
          'VH',
          'Wtaunu',
        ]
      elif 'CR' in conf:
        return [
          'TTbarNonAllHad',
          'TTbarDilepton',
          'SingleTop',
          'Diboson',
          'Ztautau',
          'Wjets',
          'VH',
          'Wtaunu',
        ]
      else:  # VR/AltVR
        return [
          'TTbarNonAllHad',
          'TTbarDilepton',
          'SingleTop',
          'Diboson',
          'Ztautau',
          'VH',
          'Wtaunu',
          'ZeeFxFx',
          'ZeeSH2211',
          'ZmumuFxFx',
          'ZmumuSH2211',
          'Wenu',
          'Wmunu',
        ]
    return [
      'TTbarNonAllHad',
    ]

  def get_alt_signals(conf, ttrees):
    if ttrees != 'MCSysts':
      if 'SR2' in conf:
        return []
      elif 'SR' in conf:
        return ['SH2211', 'FxFx']
      elif 'CR' in conf:
        return ['SH2211', 'FxFx']
      else: # VR/AltVR
        return []
    else:
      return []

  BackgroundSamples = get_bkg_samples(conf, TTrees)
  if TTrees == 'FullSysts' and 'Top' in BackgroundSamples:
    BackgroundSamples.remove('Top')
  AlternativeSignalMCs = get_alt_signals(conf, TTrees)

  Tagger      = "DL1r"
  ATLASlegend = "Internal" # Options: Internal, Preliminary, ATLAS and NONE
  OutputFormat = 'PDF' # Options: PDF, PNG, JPG

  SkipData = False if TTrees == 'nominal' else True
  if TTrees == 'truth': SkipData = False  # truth Data TTrees do not have truth but flavour hists which nominal don't have
  if args.selection == 'SR2': SkipData = True
  SkipMCSignal = True  # skips Signal Sherpa 2.2.1

  # Replace Wjets by Wenu/Wmunu
  if Channel != 'ELMU':
    WjetsSample = 'Wenu' if Channel == 'EL' else 'Wmunu'
    Backgrounds = [WjetsSample if x=='Wjets' else x for x in BackgroundSamples]
  else:
    Backgrounds = [x for x in BackgroundSamples if 'Wjets' not in x]
    if TTrees != 'MCSysts':
      Backgrounds += ['Wenu']
      Backgrounds += ['Wmunu']

  #######################################
  # Add Ti1 distributions to input files
  #######################################

  # Make full list of samples
  ExtendedSamples = copy.deepcopy(DataSamples)
  # add MC signal samples
  for sample in DataSamples:
    if '15' in sample or '16' in sample:
      if 'Signal_MC16a' not in ExtendedSamples:
        ExtendedSamples.append('Signal_MC16a')
      for alt in AlternativeSignalMCs:
        altSample = 'Signal{}_MC16a'.format(alt)
        if altSample not in ExtendedSamples:
          ExtendedSamples.append(altSample)
    elif '17' in sample:
      ExtendedSamples.append('Signal_MC16d')
      for alt in AlternativeSignalMCs:
        ExtendedSamples.append('Signal{}_MC16d'.format(alt))
    elif '18' in sample:
      ExtendedSamples.append('Signal_MC16e')
      for alt in AlternativeSignalMCs:
        ExtendedSamples.append('Signal{}_MC16e'.format(alt))
  # add MC background samples
  for bkg in Backgrounds:
    if 'Signal_MC16a' in ExtendedSamples: ExtendedSamples.append(bkg+'_MC16a')
    if 'Signal_MC16d' in ExtendedSamples: ExtendedSamples.append(bkg+'_MC16d')
    if 'Signal_MC16e' in ExtendedSamples: ExtendedSamples.append(bkg+'_MC16e')

  # Get TTree names
  if TTrees == 'nominal' or TTrees == 'truth' or TTrees == 'MCSysts':
    TTreeNames = ['nominal']
  elif TTrees == 'FullSysts':
    TTreeNames = SystTTrees
  else:
    FATAL('TTrees option ({}) not recognized, exiting'.format(TTrees))

  #Flavours = ['','FlavA1B','FlavA1C','FlavE1B','FlavE1C','FlavL']
  Flavours = ['', 'FlavA1B', 'FlavA1C', 'FlavL']

  # Loop over samples
  for sample in ExtendedSamples:
    print('Processing sample={}...'.format(sample))
    # Check if expanded file exists
    #if SkipSignal and 'Signal' in sample: continue # skip data/MC signal samples
    if SkipData and 'data' in sample: continue # skip data sample
    if SkipMCSignal and 'Signal_MC' in sample: continue # skip MC signal samples
    extra_key = ''
    if TTrees == 'FullSysts':
      extra_key = '_FullSysts'
    elif TTrees == 'truth':
      extra_key = '_truth'
    elif TTrees == 'MCSysts':
      extra_key = '_mcsysts'
    key = sample + '_' + Channel + '_' + Selection + '_' + Tagger + extra_key
    InputFileName = InputFiles[key]
    if TTrees == 'FullSysts':
      InputFileName = InputFileName.replace('_nominalOnly', '_FullSysts')
    if os.path.isfile(InputFileName):
      continue  # skip sample (already has an expanded root file)
    # Open input file
    InputFileName = InputFileName.replace('_expanded','')
    File = TFile.Open(InputFileName)
    CHECKObj(File,'ERROR: '+InputFileName+' not found, exiting')
    # Collect histograms for each TTree name
    Hists = dict()
    # Loop over TTree names
    for TTreeName in TTreeNames:
      if args.debug: print('TTreeName = {}'.format(TTreeName))
      Hists[TTreeName] = []
      # Loop over nominal+NPs
      for source, nNPs in SFsysts.items():
        if args.debug: print('source = {}'.format(source))
        for iNP in range(1, nNPs+1):
          if args.debug: print('iNP = {}'.format(iNP))
          if not MergeVariatedHists and source != 'nominal': continue # skip NPs
          if MergeVariatedHists and source != 'nominal' and TTreeName != 'nominal': continue  # skip case w/o sense
          #if MergeVariatedHists and source != 'nominal' and 'TTbar' not in sample: continue
          if 'data' in sample and source != 'nominal': continue
          if Channel == 'EL' and 'MU' in source: continue  # skip EL systematics in MU channel
          if Channel == 'MU' and 'EL' in source: continue  # skip MU systematics in EL channel
          extra = '_{}_{}'.format(source, iNP) if source != 'nominal' else ''
          # Loop over flavours
          if TTrees != 'truth':
            Flavours = ['']
          for flavour in Flavours:
          #for flavour in ['']: # Temporary
            if args.debug: print('Flavour = {}'.format(flavour))
            if flavour != '':
              if not ('Signal' in sample and 'MC' in sample): continue # skip non-signal MC samples
              if TTreeName != 'nominal': continue # only inclusive distributions are considered for systematics
            # Loop over variables
            Variables = AllVariables if not MergeVariatedHists else Observables
            if args.debug: print('Inside flavour loop')
            for var in Variables:
              if 'Te1' not in Variables[var]: continue  # skip two-b-tagged-jet variables
              if TTreeName != 'nominal' and var not in Observables: continue  # only observables are considered for systematics
              if var not in Observables and flavour != '': continue # non-inclusive-flavour distributions only for observables

              # Sum Te1 and Ti2 1D histograms
              if args.debug: print('Sum Te1 and Ti2 1D histograms...')

              # Open input hists
              Ti2_histname = flavour + '_' + var + '_Ti2' + extra if flavour != '' else var + '_Ti2' + extra
              Ti2_hist = File.Get('{}/{}'.format(TTreeName, Ti2_histname))
              CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
              Ti2_hist.SetDirectory(0)
              Te1_histname = flavour + '_' + var + '_Te1' + extra if flavour != '' else var + '_Te1' + extra
              Te1_hist = File.Get('{}/{}'.format(TTreeName, Te1_histname))
              CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
              Te1_hist.SetDirectory(0)
              Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2', 'Ti1'))
              Hist.Add(Te1_hist)
              Hists[TTreeName].append(Hist)

              # Sum Te1 and Ti2 for LeadQuantile 1D histograms
              if source == 'nominal':
                HistNameBase = flavour + '_' + Tagger + 'LeadQuantile' if flavour != '' else Tagger+'LeadQuantile'
                Te1_histname = HistNameBase + '_Te1' + extra
                Te1_hist = File.Get(TTreeName+'/'+Te1_histname)
                CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
                Te1_hist.SetDirectory(0)
                Ti2_histname = HistNameBase + '_Ti2' + extra
                Ti2_hist = File.Get(TTreeName+'/'+Ti2_histname)
                CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
                Ti2_hist.SetDirectory(0)
                Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
                Hist.Add(Te1_hist)
                Hists[TTreeName].append(Hist)

              if var not in Observables: continue # 2D histograms only for observables
              if 'Te1' not in Observables[var]: continue

              # Sum Te1 and Ti2 for LeadQuantile 2D histograms
              HistNameBase = flavour + '_' + Tagger + 'LeadQuantile' if flavour != '' else Tagger+'LeadQuantile'
              Te1_histname = HistNameBase + '_vs_' + var + '_Te1' + extra
              Te1_hist     = File.Get('{}/{}'.format(TTreeName, Te1_histname))
              CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
              Te1_hist.SetDirectory(0)
              Ti2_histname = HistNameBase + '_vs_' + var + '_Ti2' + extra
              Ti2_hist     = File.Get('{}/{}'.format(TTreeName, Ti2_histname))
              CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
              Ti2_hist.SetDirectory(0)
              Hist         = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
              Hist.Add(Te1_hist)
              Hists[TTreeName].append(Hist)

              # Temporary
              if source != 'nominal': continue

              # Sum TM histograms
              if ( ('Signal' in sample and 'MC' in sample) or ('Zee' in sample or 'Zmumu' in sample) ) and 'FlavA' in flavour:
                Ti2_histname = 'TM_' + flavour + '_' + var + '_Ti2'
                Ti2_hist = File.Get(TTreeName+'/'+Ti2_histname)
                CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
                Ti2_hist.SetDirectory(0)
                Te1_histname = 'TM_' + flavour + '_' + var + '_Te1'
                Te1_hist = File.Get(TTreeName+'/'+Te1_histname)
                CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
                Te1_hist.SetDirectory(0)
                Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
                Hist.Add(Te1_hist)
                Hists[TTreeName].append(Hist)

      # Loop over MC theoretical variations
      if TTrees != 'MCSysts' and args.selection != 'SR2':
        continue
      if 'data' in sample: continue
      # Find out list of variation names
      tdir = File.Get('nominal')
      histogram_names = [ikey.GetName() for ikey in tdir.GetListOfKeys()]
      evtwgt_variations = ['EvtWgtVar' + name.split('EvtWgtVar')[1] for name in histogram_names if 'mjjHF_Ti2_EvtWgtVar' in name]
      for source in evtwgt_variations:
        if args.debug: print('source = {}'.format(source))
        extra = '_{}'.format(source)
        if args.debug: print('Flavour = {}'.format(flavour))
        if flavour != '':
          if not ('Signal' in sample and 'MC' in sample): continue # skip non-signal MC samples
          if TTreeName != 'nominal': continue # only inclusive distributions are considered for systematics
        # Loop over variables
        Variables = AllVariables if not MergeVariatedHists else Observables
        if args.debug: print('Inside flavour loop')
        obs_weight_variations = copy.deepcopy(Observables)
        obs_weight_variations['Zmass'] = ['Te1', 'Ti2']
        for var in obs_weight_variations:
          if 'Te1' not in obs_weight_variations[var]: continue  # skip two-b-tagged-jet variables

          # Sum Te1 and Ti2 1D histograms
          if args.debug: print('Sum Te1 and Ti2 1D histograms for var = {}...'.format(var))

          # Open input hists
          Ti2_histname = var + '_Ti2' + extra
          Ti2_hist = File.Get('{}/{}'.format(TTreeName, Ti2_histname))
          CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
          Ti2_hist.SetDirectory(0)
          Te1_histname = var + '_Te1' + extra
          Te1_hist = File.Get('{}/{}'.format(TTreeName, Te1_histname))
          CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
          Te1_hist.SetDirectory(0)
          Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2', 'Ti1'))
          Hist.Add(Te1_hist)
          Hists[TTreeName].append(Hist)

          # Sum Te1 and Ti2 for LeadQuantile 1D histograms
          #if args.debug: print('Sum Te1 and Ti2 LeadQuantile 1D histograms...')
          #HistNameBase = Tagger+'LeadQuantile'
          #Te1_histname = HistNameBase + '_Te1' + extra
          #Te1_hist = File.Get(TTreeName+'/'+Te1_histname)
          #CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
          #Te1_hist.SetDirectory(0)
          #Ti2_histname = HistNameBase + '_Ti2' + extra
          #Ti2_hist = File.Get(TTreeName+'/'+Ti2_histname)
          #CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
          #Ti2_hist.SetDirectory(0)
          #Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
          #Hist.Add(Te1_hist)
          #Hists[TTreeName].append(Hist)

          # Sum Te1 and Ti2 for LeadQuantile 2D histograms
          if args.debug: print('Sum Te1 and Ti2 LeadQuantile 2D histograms...')
          HistNameBase = Tagger+'LeadQuantile'
          Te1_histname = HistNameBase + '_vs_' + var + '_Te1' + extra
          Te1_hist     = File.Get('{}/{}'.format(TTreeName, Te1_histname))
          CHECKObj(Te1_hist,'ERROR: '+Te1_histname+' not found in '+InputFileName+', exiting')
          Te1_hist.SetDirectory(0)
          Ti2_histname = HistNameBase + '_vs_' + var + '_Ti2' + extra
          Ti2_hist     = File.Get('{}/{}'.format(TTreeName, Ti2_histname))
          CHECKObj(Ti2_hist,'ERROR: '+Ti2_histname+' not found in '+InputFileName+', exiting')
          Ti2_hist.SetDirectory(0)
          Hist         = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
          Hist.Add(Te1_hist)
          Hists[TTreeName].append(Hist)

    # Open output file
    outFileName = InputFiles[key]
    outFile = TFile(outFileName, 'RECREATE')
    # Get histograms from input file
    for Dir, hists in Hists.items():
      keys = File.Get(Dir).GetListOfKeys() # list of hist names
      if TTrees == 'nominal' or TTrees == 'truth':
        outFile.cd()
      else:
        outFile.mkdir(Dir)
        outFile.cd(Dir)
      for key in keys:
        Hist = key.ReadObj()
        Hist.Write()
      for hist in hists:
        hist.Write()
    outFile.Close()
    File.Close()

  print('>>> ALL DONE <<<')
