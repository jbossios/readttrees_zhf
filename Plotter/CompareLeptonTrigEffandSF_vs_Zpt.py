Sample = 'TTbar_MC16a'

Compare = [
  'ELMU_VR',
  'MU_SR',
  'EL_SR',
]

############################################################
# DO NOT MODIFY (below this line)
############################################################

from InputFiles import *
import ROOT,os,sys
from Style import *

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Create output folder
os.system('mkdir Plots')
os.system('mkdir Plots/CompareLeptonTrigEffandSF_vs_Zpt/')

# Loop over variables
for case in ['SF','MCEff']:

  # TCanvas
  Canvas   = ROOT.TCanvas()
  outName  = 'Plots/CompareLeptonTrigEffandSF_vs_Zpt/{}_{}_'.format(case,Sample)
  outName += '_vs_'.join(Compare)
  outName += '.pdf'
  Canvas.Print(outName+']')
  
  # TPad for upper panel
  pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
  pad1.SetTopMargin(0.08)
  pad1.SetBottomMargin(0.03)
  pad1.Draw()
  pad1.cd()
  
  # Legend
  Legends = ROOT.TLegend(0.8,0.73,0.92,0.9)
  Legends.SetTextFont(42)
  
  # Styles
  Colors  = [ROOT.kBlack,ROOT.kRed,ROOT.kMagenta]
  Markers = [20,21,22]

  Stack   = ROOT.THStack()
  Hists   = []
  counter = 0
  for opt in Compare:
    # Open input file
    fName  = InputFiles[Sample+'_'+opt+'_DL1r'] 
    inFile = ROOT.TFile.Open(fName)
    if not inFile:
      print('ERROR: {} not found, exiting'.format(fName))
      sys.exit(1)
    # Get histogram
    hName = 'leptonTrig{}_vs_Zpt'.format(case)
    hist  = inFile.Get(hName)
    if not hist:
      print('ERROR: {} not found in {}, exiting'.format(hName,fName))
      sys.exit(1)
    hist.SetDirectory(0)
    inFile.Close()
    # Take profile
    hist = hist.ProfileX(hist.GetName()+'_{}_profileX'.format(opt))
    hist.SetLineColor(Colors[counter])
    hist.SetMarkerColor(Colors[counter])
    hist.SetMarkerStyle(Markers[counter])
    Legends.AddEntry(hist,opt,"p")
    Hists.append(hist)
    Stack.Add(hist,"p")
    counter += 1

  # Draw
  Stack.Draw('nostack')

  Stack.GetXaxis().SetLabelSize(0.)
  Stack.GetXaxis().SetTitleSize(0.)
  Stack.GetYaxis().SetTitleSize(20)
  Stack.GetYaxis().SetTitleFont(43)
  Stack.GetYaxis().SetLabelFont(43)
  Stack.GetYaxis().SetLabelSize(19)
  Stack.GetYaxis().SetTitleOffset(1.3)
  Stack.GetYaxis().SetTitle('MC Trigger Efficiency' if case=='MCEff' else 'MC Trigger SF')
  Stack.SetMinimum(0.94)
  Stack.SetMaximum(1.05)

  ROOT.gPad.RedrawAxis()
  
  Legends.Draw('same')

  # Draw line at data/MC==1
  minX = Hists[0].GetXaxis().GetBinLowEdge(1)
  maxX = Hists[0].GetXaxis().GetBinUpEdge(Hists[0].GetNbinsX())
  Line = ROOT.TLine(minX,1,maxX,1)
  Line.SetLineStyle(7)
  Line.Draw("same")
  
  # TPad for bottom plot
  Canvas.cd()
  pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
  pad2.SetTopMargin(0.03)
  pad2.SetBottomMargin(0.32)
  pad2.Draw()
  pad2.cd()
  
  # Compute ratios
  ratioHists = []
  for ihist in range(1,len(Hists)):
    ratioHist = Hists[ihist].Clone("ratioHist_{}".format(ihist))
    ratioHist.SetLineColor(ihist)
    ratioHist.Divide(Hists[0])
    ratioHists.append(ratioHist)
  
  # Draw ratios
  ratioHists[0].Draw('histp')
  for ihist in range(1,len(ratioHists)):
    ratioHists[ihist].Draw('phistsame')
  
  for hist in ratioHists:
    hist.GetYaxis().SetRangeUser(0.95,1.05)
  
  # Set x-axis title
  for ratioHist in ratioHists:
    ratioHist.GetXaxis().SetTitleSize(20)
    ratioHist.GetXaxis().SetTitleFont(43)
    ratioHist.GetXaxis().SetLabelFont(43)
    ratioHist.GetXaxis().SetLabelSize(19)
    ratioHist.GetXaxis().SetTitleOffset(3)
    ratioHist.GetXaxis().SetTitle(XaxisTitles['Zpt'])
    ratioHist.GetXaxis().SetNdivisions(510)
    ratioHist.GetYaxis().SetTitleSize(20)
    ratioHist.GetYaxis().SetTitleFont(43)
    ratioHist.GetYaxis().SetLabelFont(43)
    ratioHist.GetYaxis().SetLabelSize(19)
    ratioHist.GetYaxis().SetTitleOffset(1.3)
    ratioHist.GetYaxis().SetTitle("SR / VR")
  
  # Save PDF
  Canvas.Print(outName)
  Canvas.Print(outName+']')
  
  # Delete stuff
  for hist in Hists:
    del hist
  for hist in ratioHists:
    del hist

print('>>> ALL DONE <<<')
