#!/usr/bin/env python

####################################################################
#                                                                  #
# Purpose: Compare unfolded MC reco with MC truth                  #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

import ROOT,os,sys,resource,psutil,argparse,math
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

# List of data samples
Samples = [
  'data15',
  'data16',
  'data18',
]

FinalStates = ['Zc']

# Date of unfolding inputs
Date = '08072020'  # Final MET fixed

# List of observables
Observables = [
  'HFjet0_pt_Ti1',
  'HFjet0_absy_Ti1',
  'Zpt_Ti1',
  'Zabsy_Ti1',
  'deltaYZjHF_Ti1',
  'deltaPhiZjHF_Ti1',
  'deltaRZjHF_Ti1',
]

ATLASlegend = 'Internal' # Options: Internal, NONE, Preliminary, ATLAS
Debug = False

################################################################################
# Do not modify
################################################################################

os.system('mkdir Plots/')
os.system('mkdir Plots/TrivialUnfoldingClosureTest')

# Luminosity
from Luminosities import *

# Total luminosity and needed campaigns
Luminosity         = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"]= 0
Luminosity["MC16d"]= 0
Luminosity["MC16e"]= 0
DataPeriod         = 'data'
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    Luminosity["MC16a"] += Luminosity_2015
  elif "data16" in sample:
    DataPeriod += '16'
    Luminosity["MC16a"] += Luminosity_2016
  elif "data17" in sample:
    DataPeriod += '17'
    Luminosity["MC16d"] += Luminosity_2017
  elif "data18" in sample:
    DataPeriod += '18'
    Luminosity["MC16e"] += Luminosity_2018

TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Open input file
FileName = '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_reader/readttrees_zhf/Unfolding/Outputs/unfoldingOutputs_'+Date+'_'+DataPeriod+'.root'
inFile   = ROOT.TFile.Open(FileName)
if not inFile:
  print 'ERROR: input file not found, exiting'
  sys.exit(0)

# Dict with histograms for each observable and uncertainty source
SystematicUncertainties = dict()

# Loop over histograms
Cases = [(x,y) for x in FinalStates for y in Observables]
for case in Cases:
  finalState = case[0] 
  obs        = case[1]
  if Debug: print "###########################################################"
  if Debug: print "DEBUG: Producing PDF for '"+obs+"' observable"

  ############################
  # Get Histograms
  ############################

  # Get unfolded MC reco histogram
  if Debug: print "DEBUG: Get Unfolded Data histogram"
  Hist_Data = inFile.Get('UnfoldedReco_'+finalState+'_'+obs+'_Bayes_iterations_4') # FIXME (check which number of iterations we want to use)
  if not Hist_Data:
    print "ERROR: "+"UnfoldedReco_"+finalState+"_"+obs+"_Bayes_iterations_4 not found, exiting"
    sys.exit(0)
  if Debug: print "DEBUG: Unfolded Data histogram retrieved"
  if Debug: print "DEBUG: Set style for data histogram"
  Hist_Data.SetFillColor(ROOT.kBlack)
  Hist_Data.SetFillStyle(3004)
  Hist_Data.SetLineWidth(0)

  # Divide by bin width
  if Debug: print "DEBUG: (Hist_Data) divide by bin-width"
  # Data w/o errors
  for binX in range(1,Hist_Data.GetNbinsX()+1):
    if Hist_Data.GetBinWidth(binX)!=0:
      Hist_Data.SetBinContent(binX, Hist_Data.GetBinContent(binX)/Hist_Data.GetBinWidth(binX))
      Hist_Data.SetBinError(binX, 0) # only used in MC/data ratio
    else:
      Hist_Data.SetBinContent(binX, 0)
      Hist_Data.SetBinError(binX, 0)

  # MC true histogram
  if Debug: print "DEBUG: Get MC Signal histogram"
  Hist_MC_Signal = inFile.Get('Truth_'+finalState+'_'+obs.replace('_Ti1',''))
  if not Hist_MC_Signal:
    print "ERROR: Truth_"+finalState+"_"+obs.replace('_Ti1','')+" not found, exiting"
    sys.exit(0)
  if Debug: print "DEBUG: (GetHist) divide by bin-width"
  for binX in range(1,Hist_MC_Signal.GetNbinsX()+1):
    if Hist_MC_Signal.GetBinWidth(binX)!=0:
      Hist_MC_Signal.SetBinContent(binX, Hist_MC_Signal.GetBinContent(binX)/Hist_MC_Signal.GetBinWidth(binX))
      Hist_MC_Signal.SetBinError(binX, Hist_MC_Signal.GetBinError(binX)/Hist_MC_Signal.GetBinWidth(binX))
    else:
      Hist_MC_Signal.SetBinContent(binX, 0)
      Hist_MC_Signal.SetBinError(binX, 0)
  Hist_MC_Signal.SetMarkerColor(ROOT.kRed+1)

  ########################
  # Make Plot
  ########################

  # TCanvas
  if Debug: print "DEBUG: Create TCanvas"
  Canvas  = ROOT.TCanvas()
  outName = "Plots/TrivialUnfoldingClosureTest/{0}_{1}_{2}.pdf".format(DataPeriod,finalState,obs)
  Canvas.Print(outName+"[")

  # TPad for upper panel
  if Debug: print "DEBUG: Create TPad for upper panel"
  pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
  pad1.SetTopMargin(0.08)
  pad1.SetBottomMargin(0.03)
  pad1.Draw()
  pad1.cd()

  # Set log scales (if requested)
  if Debug: print "DEBUG: Set log scales if requested"
  histnameBase = obs.replace('_Te1','')
  histnameBase = histnameBase.replace('_Ti2','')
  histnameBase = histnameBase.replace('_Ti1','')
  if histnameBase in Logx:
    pad1.SetLogx()
  if histnameBase in Logy:
    pad1.SetLogy()

  # Legends
  Legends = ROOT.TLegend(0.78,0.7,0.92,0.9)
  Legends.SetTextFont(42)

  Hist_Data.Draw("P")
  Hist_Data.GetXaxis().SetLabelSize(0.)
  Hist_Data.GetXaxis().SetTitleSize(0.)
  Hist_Data.GetYaxis().SetTitleSize(20)
  Hist_Data.GetYaxis().SetTitleFont(43)
  Hist_Data.GetYaxis().SetLabelFont(43)
  Hist_Data.GetYaxis().SetLabelSize(19)
  Hist_Data.GetYaxis().SetTitleOffset(1.3)
  YaxisTitle  = XaxisUnfoldTitle[finalState+'_'+histnameBase]
  if '[GeV]' in YaxisTitle:
    YaxisTitle = YaxisTitle.replace('[GeV]','[pb/GeV]')
  else:
    YaxisTitle += ' [pb]'
  Hist_Data.GetYaxis().SetTitle("d#sigma/d"+YaxisTitle)
  Hist_Data.SetMinimum(1)
  Legends.AddEntry(Hist_Data,"Unfolded reco","p")

  Hist_MC_Signal.Draw("P")
  Legends.AddEntry(Hist_MC_Signal,"Truth","p")

  if Debug: print "DEBUG: Draw data histogram"
  Hist_Data.Draw("P same")
  
  if XaxisRange.has_key(histnameBase):
    Hist_MC_Signal.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
    Hist_Data.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
  maxY = Hist_Data.GetMaximum()
  if Hist_MC_Signal.GetMaximum() > maxY: maxY = Hist_MC_Signal.GetMaximum()
  maxYscaling = 1E5
  if "absy" in obs or "phi" in obs or "eta" in obs or "deltaR" in obs or "deltaPhi" in obs: maxYscaling = 2E7
  Hist_MC_Signal.GetYaxis().SetRangeUser(1,maxY*maxYscaling)
  Hist_Data.GetYaxis().SetRangeUser(1,maxY*maxYscaling)

  Hist_MC_Signal.GetXaxis().SetLabelSize(0.)
  Hist_MC_Signal.GetXaxis().SetTitleSize(0.)
  Hist_MC_Signal.GetYaxis().SetTitleSize(20)
  Hist_MC_Signal.GetYaxis().SetTitleFont(43)
  Hist_MC_Signal.GetYaxis().SetLabelFont(43)
  Hist_MC_Signal.GetYaxis().SetLabelSize(19)
  Hist_MC_Signal.GetYaxis().SetTitleOffset(1.3)
  Hist_MC_Signal.GetYaxis().SetTitle("d#sigma/d"+YaxisTitle)

  if Debug: print "DEBUG: Draw legends"
  Legends.Draw("same")

  ROOT.gPad.RedrawAxis()

  # Show ATLAS legend
  if Debug: print "DEBUG: Show ATLAS legend"
  if ATLASlegend != 'NONE':
    if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
    elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
    elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
    else:
      print "ERROR: ATLASlegend not recognized, exiting"
      sys.exit(0)
    ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
    ATLASBlock.SetNDC()
    ATLASBlock.Draw("same")

  # Show CME and luminosity
  if Debug: print "DEBUG: Show CME and luminosity"
  CME = "#scale[1.5]{13 TeV, "+str(round(TotalLumi/1000,1))+" fb^{-1}}"
  topY = 0.7 if ATLASlegend != 'NONE' else 0.8
  CMEblock = ROOT.TLatex(0.2,topY,CME)
  CMEblock.SetNDC()
  CMEblock.Draw("same")

  # Show channel
  #if Debug: print "DEBUG: Show channel type"
  #channel = "#scale[1.5]{"
  #channel += "#mu" if Channel == "MU" else "e"
  #channel += " channel}"
  #topY = 0.6 if ATLASlegend != 'NONE' else 0.7
  #TextBlock = ROOT.TLatex(0.2,topY,channel)
  #TextBlock.SetNDC()
  #TextBlock.Draw("same")
  
  # TPad for bottom plot
  if Debug: print "DEBUG: Create TPad for bottom panel"
  Canvas.cd()
  pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
  pad2.SetTopMargin(0.03)
  pad2.SetBottomMargin(0.32)
  pad2.Draw()
  pad2.cd()

  # Set log-y scale (if requested)
  if Debug: print "DEBUG: Set log-Y scale if requested"
  if histnameBase in Logx:
    pad2.SetLogx()

  # Create data/MC histogram
  if Debug: print "DEBUG: Create MC/data histogram"
  ratioHist = Hist_Data.Clone("ratioHist")
  ratioHist.SetLineColor(ROOT.kRed+1)
  ratioHist.SetMarkerColor(ROOT.kRed+1)
  ratioHist.Divide(Hist_MC_Signal)

  # Set y-axis range of ratio panel
  minY = -0.05
  maxY = 2.05
  ratioHist.SetMinimum(minY)
  ratioHist.SetMaximum(maxY)

  # Set x-axis range of ratio panel
  nbins = ratioHist.GetNbinsX()
  if histnameBase in XaxisRange:
    minX = XaxisRange[histnameBase][0]
    maxX = XaxisRange[histnameBase][1]
  else:
    minX = ratioHist.GetXaxis().GetBinLowEdge(1)
    maxX = ratioHist.GetXaxis().GetBinUpEdge(nbins)
  ratioHist.GetXaxis().SetRangeUser(minX,maxX)
 
  # Draw data/MC ratio
  if Debug: print "DEBUG: Draw MC/data ratio"
  ratioHist.Draw("e0")

  # Draw line at data/MC==1
  if Debug: print "DEBUG: Draw line at MC/data==1"
  Line = ROOT.TLine(minX,1,maxX,1)
  Line.SetLineStyle(7)
  Line.Draw("same")

  # Set x-axis title
  if Debug: print "DEBUG: Set X-axis title"
  histkey = finalState + '_' + histnameBase
  if XaxisUnfoldTitle.has_key(histkey):
    ratioHist.GetXaxis().SetTitleSize(20)
    ratioHist.GetXaxis().SetTitleFont(43)
    ratioHist.GetXaxis().SetLabelFont(43)
    ratioHist.GetXaxis().SetLabelSize(19)
    ratioHist.GetXaxis().SetTitleOffset(3)
    ratioHist.GetXaxis().SetTitle(XaxisUnfoldTitle[histkey])
    ratioHist.GetXaxis().SetNdivisions(510)

  ratioHist.GetYaxis().SetTitleSize(20)
  ratioHist.GetYaxis().SetTitleFont(43)
  ratioHist.GetYaxis().SetLabelFont(43)
  ratioHist.GetYaxis().SetLabelSize(19)
  ratioHist.GetYaxis().SetTitleOffset(1.3)
  ratioHist.GetYaxis().SetTitle("Unfolded reco / truth")

  # Save PDF
  if Debug: print "DEBUG: Save/print PDF"
  Canvas.Print(outName)
  Canvas.Print(outName+"]")

  ratioHist.Delete()
  Hist_Data.Delete()
  Hist_MC_Signal.Delete()

print '>>> DONE <<<'
