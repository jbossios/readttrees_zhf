import copy
import os
import sys
import math
import copy

from InputFiles import *
from PlotterHelperFunctions import *

from ROOT import *

# FIXME/TODO
# I can't simply sum all MC16a/d/e root files, I need to multiply first by lumi and then sum

# AtlasStyle
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

# Avoid pop-up windows
gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *
from SystematicTTrees import *  # name of non-SF uncertainty sources
from Systematics import *  # name of SF-based uncertainty sources


def get_hist(hist_name, input_file_name):
    '''Get nominal histogram'''
    input_file = TFile.Open(input_file_name)
    CHECKObj(input_file, '{} not found, exiting'.format(input_file_name))
    hist = input_file.Get('{}'.format(hist_name))
    CHECKObj(hist, '{} not found in {}, exiting'.format(hist_name, input_file_name))
    hist.SetDirectory(0)
    input_file.Close()
    return hist


def main(**kargs):

    sample = kargs['sample']
    campaign = kargs['campaign']
    log = kargs['logger']
    channel = 'EL' if 'ee' in sample else 'MU'
    selection = "SR"
    tagger = "DL1r"
    output_format = 'PDF'  # options: PDF, PNG, JPG
    sign = 'UP'  # options: UP, DOWN
         
    # Get full list of campaigns
    #campaigns = [campaign] if campaign != 'MC16all' else ['MC16a', 'MC16d', 'MC16e']
    campaigns = [campaign]
    
    # Define final list of samples (one set for each campaign)
    all_samples = []
    for campaign in campaigns:
        generator = sample.split('Zmumu')[1] if 'Zmumu' in sample else sample.split('Zee')[1]
        all_samples.append('Signal{}_{}_{}_{}'.format(generator, campaign, channel, selection))
                
    # Output format
    Format = 'pdf' if output_format == 'PDF' else 'png'
    
    # Colors
    colors = [kBlack, kRed+1, kBlue, kGreen, kMagenta, kCyan, kOrange, kOrange+10, kGray]

    # Get histograms for non-SF systematic uncertainties
    non_sf_sources = ['EG', 'MUON', 'JET', 'MET']  # sources of non-SF systematics
    TTreeNames = SystTTrees  # TTree names for non-SF systematics
    for sample in all_samples:  # loop over samples
        cpg = ''
        if 'MC16all' in sample: cpg = 'MC16all'
        elif 'MC16a' in sample: cpg = 'MC16a'
        elif 'MC16d' in sample: cpg = 'MC16d'
        elif 'MC16e' in sample: cpg = 'MC16e'
        non_sf_hists = dict()
        # Loop over observables
        for obs in Observables:
            non_sf_hists[obs] = dict()
            # Loop over cases
            for case in ['Ti1', 'Ti2']:
                if case == 'Ti1' and 'Te1' not in Observables[obs]: continue  # Ti1 is not possible
                non_sf_hists[obs][case] = dict()
                histname = '{}_{}'.format(obs, case)
                key = sample + '_' + tagger
                input_file_name = InputFiles[key]
                nom_hist = get_hist(histname, input_file_name)
                # Collect quadrature sum of uncertainties for each source
                for source in non_sf_sources:
                    non_sf_hists[obs][case][source] = nom_hist.Clone('Total_{}_{}_{}_{}'.format(obs, case, source, sign))
                sum_dict = {source: {xbin: 0 for xbin in range(1, nom_hist.GetNbinsX()+1)} for source in non_sf_sources}
                # Loop over TTrees
                for TTreeName in TTreeNames:
                    # Get variated histogram
                    file_name = InputFiles[key+'_FullSysts']
                    input_file = TFile.Open(file_name)
                    CHECKObj(input_file, '{} not found, exiting'.format(file_name))
                    hist = input_file.Get('{}'.format(TTreeName+'/'+histname))
                    CHECKObj(hist, '{} not found in {}, exiting'.format(TTreeName+'/'+histname, file_name))
                    hist.SetDirectory(0)
                    input_file.Close()
                    # Derive uncertainty
                    SystHist = hist.Clone('{}_{}'.format(histname, TTreeName))
                    SystHist.Add(nom_hist, -1)
                    SystHist.Divide(nom_hist)
                    # Add to appropriate sum
                    for source in non_sf_sources:
                        if source in TTreeName:
                            source_type = source
                            break
                    for xbin in range(1, SystHist.GetNbinsX()+1):  # loop over bins
                        uncert = SystHist.GetBinContent(xbin)
                        if uncert >= 0 and sign == 'UP':
                            sum_dict[source_type][xbin] += uncert * uncert
                        elif sign == 'DOWN':
                            sum_dict[source_type][xbin] += uncert * uncert
                # Fill histograms with quadrature sums
                for counter, source in enumerate(non_sf_sources):
                    for xbin in range(1, non_sf_hists[obs][case][source].GetNbinsX()+1):
                      non_sf_hists[obs][case][source].SetBinContent(xbin, math.sqrt(sum_dict[source][xbin]))
                    non_sf_hists[obs][case][source].SetLineColor(colors[counter])

    # Get histograms for SF-based systematic uncertainties
    cr = 'VR'
    # remove undesired sources
    other_channel = 'EL' if channel == 'MU' else 'MU'
    SF_systs = copy.deepcopy(SFsysts)
    for source in SFsysts.keys():
        if other_channel in source:
            SF_systs.pop(source)
    for sample in all_samples:  # loop over samples
        cpg = ''
        if 'MC16all' in sample: cpg = 'MC16all'
        elif 'MC16a' in sample: cpg = 'MC16a'
        elif 'MC16d' in sample: cpg = 'MC16d'
        elif 'MC16e' in sample: cpg = 'MC16e'
        # Loop over observables
        sf_hists = dict()
        for obs in Observables:
            sf_hists[obs] = dict()
            # Loop over cases
            for case in ['Ti1', 'Ti2']:
                if case == 'Ti1' and 'Te1' not in Observables[obs]: continue  # Ti1 is not possible
                sf_hists[obs][case] = dict()
                histname = '{}_{}'.format(obs, case)
                key = sample + '_' + tagger
                input_file_name = InputFiles[key]
                nom_hist = get_hist(histname, input_file_name)
                # Collect quadrature sum of uncertainties for each source
                sf_sources = ['BTag', 'JVT', channel, 'weight_pileup']
                for source in sf_sources:
                    sf_hists[obs][case][source] = nom_hist.Clone('Total_{}_{}_{}_{}'.format(obs, case, source, sign))
                sum_dict = {source: {xbin: 0 for xbin in range(1, nom_hist.GetNbinsX()+1)} for source in sf_sources}
                # Get variated histograms
                for source, n_nps in SF_systs.items():
                    if source == 'nominal': continue
                    for i_np in range(1, n_nps + 1):
                        hist_name = '{}_{}_{}'.format(histname, source, i_np)
                        hist = get_hist(hist_name, input_file_name)
                        # Derive uncertainty
                        syst_hist = hist.Clone('uncert_{}'.format(hist_name))
                        syst_hist.Add(nom_hist, -1)
                        syst_hist.Divide(nom_hist)
                        # Add to appropriate sum
                        for source_type in sf_sources:
                            if source_type in source:
                                break
                        for xbin in range(1, syst_hist.GetNbinsX()+1):  # loop over bins
                            uncert = syst_hist.GetBinContent(xbin)
                            if uncert >= 0 and sign == 'UP':
                                sum_dict[source_type][xbin] += uncert * uncert
                            elif sign == 'DOWN':
                                sum_dict[source_type][xbin] += uncert * uncert
                # Fill histograms with quadrature sums
                for new_counter, source in enumerate(sf_sources, counter + 1):
                    for xbin in range(1, sf_hists[obs][case][source].GetNbinsX()+1):
                        sf_hists[obs][case][source].SetBinContent(xbin, math.sqrt(sum_dict[source][xbin]))
                    sf_hists[obs][case][source].SetLineColor(colors[new_counter])

    # Sum MU+MUON, EL+EG, JVT+JET
    final_sources = ['JET', 'MET', 'PRW', 'FT']
    final_sources += ['MUON'] if channel == 'MU' else ['EG']
    final_hists = dict()
    # Loop over observables
    for obs in Observables:
        final_hists[obs] = dict()
        # Loop over cases
        for case in ['Ti1', 'Ti2']:
            if case == 'Ti1' and 'Te1' not in Observables[obs]: continue  # Ti1 is not possible
            final_hists[obs][case] = dict()
            for counter, source in enumerate(final_sources):
                if source == 'PRW' or source == 'FT':
                    final_hists[obs][case][source] = sf_hists[obs][case]['weight_pileup' if source == 'PRW' else 'BTag']
                    final_hists[obs][case][source].SetLineColor(colors[counter])
                if source == 'MET':
                    final_hists[obs][case][source] = non_sf_hists[obs][case][source]
                    final_hists[obs][case][source].SetLineColor(colors[counter])
                elif source == 'JET':
                    final_hists[obs][case][source] = sf_hists[obs][case]['JVT'].Clone('Final_{}_{}_{}_{}'.format(obs, case, source, sign))
                    final_hists[obs][case][source].SetLineColor(colors[counter])
                    hist_1 = sf_hists[obs][case]['JVT']
                    hist_2 = non_sf_hists[obs][case]['JET']
                    for xbin in range(1, final_hists[obs][case][source].GetNbinsX()+1):  # loop over bins
                        uncert_1 = hist_1.GetBinContent(xbin)
                        uncert_2 = hist_2.GetBinContent(xbin)
                        final_uncert = math.sqrt(uncert_1 * uncert_1 + uncert_2 * uncert_2)
                        final_hists[obs][case][source].SetBinContent(xbin, final_uncert)
                elif source == 'MUON':
                     final_hists[obs][case][source] = sf_hists[obs][case]['MU'].Clone('Final_{}_{}_{}_{}'.format(obs, case, source, sign))
                     final_hists[obs][case][source].SetLineColor(colors[counter])
                     hist_1 = sf_hists[obs][case]['MU']
                     hist_2 = non_sf_hists[obs][case]['MUON']
                     for xbin in range(1, final_hists[obs][case][source].GetNbinsX()+1):  # loop over bins
                         uncert_1 = hist_1.GetBinContent(xbin)
                         uncert_2 = hist_2.GetBinContent(xbin)
                         final_uncert = math.sqrt(uncert_1 * uncert_1 + uncert_2 * uncert_2)
                         final_hists[obs][case][source].SetBinContent(xbin, final_uncert)
                elif source == 'EG':
                     final_hists[obs][case][source] = sf_hists[obs][case]['EL'].Clone('Final_{}_{}_{}_{}'.format(obs, case, source, sign))
                     final_hists[obs][case][source].SetLineColor(colors[counter])
                     hist_1 = sf_hists[obs][case]['EL']
                     hist_2 = non_sf_hists[obs][case]['EG']
                     for xbin in range(1, final_hists[obs][case][source].GetNbinsX()+1):  # loop over bins
                         uncert_1 = hist_1.GetBinContent(xbin)
                         uncert_2 = hist_2.GetBinContent(xbin)
                         final_uncert = math.sqrt(uncert_1 * uncert_1 + uncert_2 * uncert_2)
                         final_hists[obs][case][source].SetBinContent(xbin, final_uncert)

    # Create output folder
    os.system('mkdir -p Plots/{0}/{1}/{2}/'.format(selection, tagger, campaign))
    # Loop over observables
    for obs in Observables:
        # Loop over cases
        for case in ['Ti1','Ti2']:
            if case == 'Ti1' and 'Te1' not in Observables[obs]: continue # Ti1 is not possible
            # TCanvas
            Canvas  = TCanvas()
            outName = "Plots/{0}/{1}/{2}/BreakdownUncertainties_{3}_{4}_{5}_{6}{7}.{8}".format(selection, tagger, campaign, channel, selection, obs, case, '_DOWN' if sign == 'DOWN' else '', Format)
            Canvas.Print(outName+"[")
            # TLegend
            #Legends = TLegend(0.7, 0.43, 0.92, 0.9)
            Legends = TLegend(0.75, 0.6, 0.92, 0.9)
            Legends.SetTextFont(42)
            # THStack
            Stack = THStack()
            for source, hist in final_hists[obs][case].items():
                Stack.Add(hist, 'HIST')
                Legends.AddEntry(hist, source,'l')
            Stack.Draw('nostack')
            Stack.GetYaxis().SetTitle('Systematic uncertainty')
            Stack.SetMinimum(0)  # Temporary
            Stack.SetMaximum(0.3)  # Temporary
            if obs in XaxisTitles:
                if channel == 'EL' and obs == 'Zpt':
                    Stack.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
                else:
                    Stack.GetXaxis().SetTitle(XaxisTitles[obs])
            Legends.Draw('same')
            Canvas.Print(outName)
            Canvas.Print(outName+']')
    
 
if __name__ == '__main__':

    campaign = 'MC16a'  # options: ['MC16a', 'MC16d', 'MC16e', 'MC16all']  # MC16all not supported yet
    
    samples = [
        'ZmumuSH2211',
        'ZeeSH2211',
    ]

    # Logger
    import logging
    logging.basicConfig(level = 'INFO', format = '%(levelname)s: %(message)s')
    log = logging.getLogger()

    for sample in samples:
        main(sample = sample, campaign = campaign, logger = log)

    log.info('>>> ALL DONE <<<')
