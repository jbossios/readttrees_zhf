from ROOT import *

Colors = [
  kGreen-8,
  kMagenta,
  kBlue+1,
  kOrange,
  kPink-7,
  kCyan,
  kBlue-6,
  kRed-2,
  kCyan-8,
]

AltColors = [
  kGreen+2,
  kMagenta+2,
  kCyan+3,
  kOrange+10,
  kGray,
  kAzure,
]

AltMarkers = [21,22,23,33,34]

# List of histograms in which Logx will be applied
Logx = [
#  "jet_pt",
#  "bjet0_pt",
#  "mu_pt",
]

# List of histograms in which Logy will be applied
Logy = [
  "correctedAndScaledAverageMu",
  "correctedAndScaledActualMu",
  "HFjet0_pt",
  "HFjet1_pt",
  "HFjet0_absy",
  "HFjet1_absy",
  "njet",
  "jet_pt",
  "jet_eta",
  "jet_y",
  "jet_phi",
  "jet_e",
  "jet0_pt",
  "jet0_eta",
  "jet0_y",
  "jet0_phi",
  "jet0_e",
  "jet1_pt",
  "jet1_eta",
  "jet1_y",
  "jet1_phi",
  "jet1_e",
  "jet2_pt",
  "jet2_eta",
  "jet2_y",
  "jet2_phi",
  "jet2_e",
  "lep_pt",
  "lep0_pt",
  "lep1_pt",
  "lep_eta",
  "lep0_eta",
  "lep1_eta",
  "lep_phi",
  "Zmass",
  "Zpt",
  "Zpt_ZjjHF",
  "Zabsy",
  "deltaYjj",
  "deltaPhijj",
  "deltaRjj",
  "deltaYZj",
  "deltaYZjHF",
  "deltaPhiZj",
  "deltaPhiZjHF",
  "deltaRZj",
  "deltaRZjHF",
  "deltaYjjHF",
  "deltaPhijjHF",
  "deltaRjjHF",
  "pTjj",
  "pTjjHF",
  "mjj",
  "mjjHF",
  "pTjjovermjj",
  "pTjjHFovermjjHF",
  "ht",
  "met",
  "j0_eta__pt_30_40",
  "j1_eta__pt_30_40",
  "j0_eta__pt_100_150",
  "j1_eta__pt_100_150",
  "j0_eta__pt_500_600",
  "j1_eta__pt_500_600",
  "j0_eta__pt_1000_1200",
  "j1_eta__pt_1000_1200",
  "XF",
  "HFjet0_XF",
  "mcEvtWeight",
  "sampleWeight",
  "leptonSF",
  "jvtSF",
]

# Axis ranges
XaxisRange = dict()
XaxisRange["jet_pt"]	           = [20,900]
XaxisRange["HFjet0_pt"]	           = [20,500]
XaxisRange["HFjet1_pt"]	           = [20,900]
XaxisRange["HFjet0_absy"]          = [0,2.5]
XaxisRange["HFjet1_absy"]          = [0,2.5]
#XaxisRange["lep_pt"]	           = [27,900] # Temporary
XaxisRange["lep_pt"]	           = [28,500] # Temporary
XaxisRange["lep0_pt"]              = [28,500] # Temporary
XaxisRange["lep1_pt"]              = [28,500] # Temporary
XaxisRange["lep_eta"]              = [-2.5,2.5]
XaxisRange["lep0_eta"]             = [-2.5,2.5]
XaxisRange["lep1_eta"]             = [-2.5,2.5]
XaxisRange["lep_phi"]              = [-3.14,3.14]
XaxisRange["Zmass"]	           = [76,106]
XaxisRange["Zpt"]                  = [0,1000]
XaxisRange["Zpt_ZjjHF"]            = [0,1000]
XaxisRange["Zabsy"]	           = [0,2.5]
XaxisRange["njet"]	           = [0,10]
XaxisRange["jet0_pt"]	           = [20,900]
XaxisRange["jet0_y"]	           = [-3,3]
XaxisRange["jet0_eta"]             = [-3,3]
XaxisRange["jet0_phi"]             = [-4.5,4.5]
XaxisRange["jet0_e"]	           = [20,900]
XaxisRange["jet1_pt"]	           = [20,900]
XaxisRange["jet1_y"]	           = [-3,3]
XaxisRange["jet1_eta"]	           = [-3,3]
XaxisRange["jet1_phi"]	           = [-4.5,4.5]
XaxisRange["jet1_e"]	           = [20,900]
XaxisRange["jet2_pt"]	           = [20,900]
XaxisRange["jet2_y"]	           = [-3,3]
XaxisRange["jet2_eta"]	           = [-3,3]
XaxisRange["jet2_phi"]	           = [-4.5,4.5]
XaxisRange["jet2_e"]	           = [20,900]
XaxisRange["mjj"]	           = [20,2000]
XaxisRange["deltaYjj"]	           = [0,5.0]
XaxisRange["deltaYZj"]             = [0,5.0]
XaxisRange["deltaYZjHF"]           = [0,5.0]
XaxisRange["deltaYjjHF"]	   = [0,5.0]
XaxisRange["deltaPhijj"]           = [0,3.15]
XaxisRange["deltaPhiZj"]           = [0,3.15]
XaxisRange["deltaPhiZjHF"]         = [0,3.15]
XaxisRange["deltaPhijjHF"]         = [0,3.15]
XaxisRange["deltaRjj"]	           = [0,5.0]
XaxisRange["deltaRZj"]	           = [0,5.0]
XaxisRange["deltaRZjHF"]	   = [0,5.0]
XaxisRange["deltaRjjHF"]	   = [0,5.0]
XaxisRange["jet_eta"]              = [-2.5,2.5]
XaxisRange["jet_y"]                = [-2.5,2.5]
XaxisRange["jet_phi"]              = [-3.14,3.14]
XaxisRange["jet_e"]                = [20,900]
XaxisRange["met"]                  = [0,200]
XaxisRange["ht"]                   = [63,900]
XaxisRange["pTjj"]                 = [0,700]
XaxisRange["pTjjHF"]               = [0,700]
XaxisRange["mjjHF"]                = [0,2000]
XaxisRange["pTjjovermjj"]          = [0,5.5]
XaxisRange["pTjjovermjjHF"]        = [0,5.5]
XaxisRange["pTjjHFovermjjHF"]      = [0,5.5]
XaxisRange["j0_eta__pt_30_40"]     = [-2.5,2.5]
XaxisRange["j1_eta__pt_30_40"]     = [-2.5,2.5]
XaxisRange["j0_eta__pt_100_150"]   = [-2.5,2.5]
XaxisRange["j1_eta__pt_100_150"]   = [-2.5,2.5]
XaxisRange["j0_eta__pt_500_600"]   = [-2.5,2.5]
XaxisRange["j1_eta__pt_500_600"]   = [-2.5,2.5]
XaxisRange["j0_eta__pt_1000_1200"] = [-2.5,2.5]
XaxisRange["j1_eta__pt_1000_1200"] = [-2.5,2.5]
XaxisRange["mu_pt"]                = XaxisRange["lep_pt"]
XaxisRange["mu_eta"]               = XaxisRange["lep_eta"]
XaxisRange["mu_phi"]               = XaxisRange["lep_phi"]
XaxisRange["XF"]                   = [0.02,0.4]

# Axis titles
XaxisTitles = dict()
XaxisTitles["correctedAndScaledAverageMu"]= "<#mu>"
XaxisTitles["correctedAndScaledActualMu"] = "<#mu>"
XaxisTitles["jet_pt"]                     = "#it{p}_{T}^{jet} [GeV]"
XaxisTitles["HFjet0_pt"]                  = "#it{p}_{T}^{lead tagged-jet} [GeV]"
XaxisTitles["HFjet1_pt"]                  = "#it{p}_{T}^{sublead tagged-jet} [GeV]"
XaxisTitles["HFjet0_absy"]                = "|y|^{lead tagged-jet}"
XaxisTitles["HFjet1_absy"]                = "|y|^{sublead tagged-jet}"
XaxisTitles["Zmass"]                      = "#it{m} (#mu^{+}#mu^{-}) [GeV]"
XaxisTitles["Zpt"]                        = "#it{p}_{T} (#mu^{+}#mu^{-}) [GeV]"
XaxisTitles["Zpt_ZjjHF"]                  = XaxisTitles["Zpt"]
XaxisTitles["Zabsy"]                      = "|y| (#mu^{+}#mu^{-})"
XaxisTitles["mu_pt"]                      = "#it{p}_{T}^{#mu} [GeV]"
XaxisTitles["mu_phi"]                     = "#it{#phi}^{#mu}"
XaxisTitles["mu_eta"]                     = "#it{#eta}^{#mu}"
XaxisTitles["njet"]                       = "N_{jets}"
XaxisTitles["jet0_pt"]                    = "#it{p}_{T}^{lead jet} [GeV]"
XaxisTitles["jet0_y"]                     = "#it{y}^{lead jet}"
XaxisTitles["jet0_phi"]                   = "#phi^{lead jet}"
XaxisTitles["jet0_eta"]                   = "#it{#eta}^{lead jet}"
XaxisTitles["jet0_e"]                     = "#it{E}^{lead jet} [GeV]"
XaxisTitles["jet1_pt"]                    = "#it{p}_{T}^{sublead jet} [GeV]"
XaxisTitles["jet1_y"]                     = "#it{y}^{sublead jet}"
XaxisTitles["jet1_phi"]                   = "#phi^{jet1}"
XaxisTitles["jet1_eta"]                   = "#it{#eta}^{jet1}"
XaxisTitles["jet1_e"]                     = "#it{E}^{jet1} [GeV]"
XaxisTitles["jet2_pt"]                    = "#it{p}_{T}^{sub-sublead jet} [GeV]"
XaxisTitles["jet2_y"]                     = "#it{y}^{sub-sublead jet}"
XaxisTitles["jet2_phi"]                   = "#phi^{jet2}"
XaxisTitles["jet2_eta"]                   = "#it{#eta}^{jet2}"
XaxisTitles["jet2_e"]                     = "#it{E}^{jet2} [GeV]"
XaxisTitles["jet_phi"]                    = "#phi^{jet}"
XaxisTitles["jet_eta"]                    = "#it{#eta}^{jet}"
XaxisTitles["jet_y"]                      = "#it{y}^{jet}"
XaxisTitles["jet_e"]                      = "#it{E}^{jet} [GeV]"
XaxisTitles["mjj"]                        = "#it{m}_{jj} [GeV]"
XaxisTitles["deltaYjj"]                   = "#Delta y^{jj}"
XaxisTitles["deltaPhijj"]                 = "#Delta #Phi^{jj}"
XaxisTitles["deltaRjj"]                   = "#Delta R^{jj}"
XaxisTitles["deltaYZj"]                   = "#Delta y(Z,jet)"
XaxisTitles["deltaYZjHF"]                 = "#Delta y(Z,tagged-jet)"
XaxisTitles["deltaPhiZj"]                 = "#Delta #Phi(Z,jet)"
XaxisTitles["deltaPhiZjHF"]               = "#Delta #Phi(Z,tagged-jet)"
XaxisTitles["deltaRZj"]                   = "#Delta R(Z,jet)"
XaxisTitles["deltaRZjHF"]                 = "#Delta R(Z,tagged-jet)"
XaxisTitles["deltaYjjHF"]                 = "#Delta y(tagged-jet,tagged-jet)"
XaxisTitles["deltaPhijjHF"]               = "#Delta #Phi(tagged-jet,tagged-jet)"
XaxisTitles["deltaRjjHF"]                 = "#Delta R(tagged-jet,tagged-jet)"
XaxisTitles["ht"]                         = "H_{T} [GeV]"
XaxisTitles["pTjj"]                       = "#it{p}_{T}^{jet,jet} [GeV]"
XaxisTitles["pTjjHF"]                     = "#it{p}_{T}^{tagged-jet,tagged-jet} [GeV]"
XaxisTitles["mjjHF"]                      = "#it{m}^{tagged-jet,tagged-jet} [GeV]"
XaxisTitles["pTjjovermjj"]                = "#it{p}_{T}^{jet,jet}/#it{m}^{jet,jet} [GeV]"
XaxisTitles["pTjjHFovermjjHF"]            = "#it{p}_{T}^{tagged-jet,tagged-jet}/#it{m}^{tagged-jet,tagged-jet} [GeV]"
XaxisTitles["j0_eta__pt_30_40"]           = "#it{#eta}^{lead jet}_{30,40}"
XaxisTitles["j1_eta__pt_30_40"]           = "#it{#eta}^{sublead jet}_{30,40}"
XaxisTitles["j0_eta__pt_100_150"]         = "#it{#eta}^{lead jet}_{100,150}"
XaxisTitles["j1_eta__pt_100_150"]         = "#it{#eta}^{sublead jet}_{100,150}"
XaxisTitles["j0_eta__pt_500_600"]         = "#it{#eta}^{lead jet}_{500,600}"
XaxisTitles["j1_eta__pt_500_600"]         = "#it{#eta}^{sublead jet}_{500,600}"
XaxisTitles["j0_eta__pt_1000_1200"]       = "#it{#eta}^{lead jet}_{1000,1200}"
XaxisTitles["j1_eta__pt_1000_1200"]       = "#it{#eta}^{sublead jet}_{1000,1200}"
XaxisTitles["MV2c10Weight"]               = "b-tagging discriminant"
XaxisTitles["MV2c10Weight__pt_30_40"]     = "b-tagging discriminant"
XaxisTitles["MV2c10Weight__pt_100_150"]   = "b-tagging discriminant"
XaxisTitles["MV2c10Weight__pt_500_600"]   = "b-tagging discriminant"
XaxisTitles["MV2c10Weight__pt_1000_1200"] = "b-tagging discriminant"
XaxisTitles["DL1Weight"]                  = "b-tagging discriminant"
XaxisTitles["DL1Weight__pt_30_40"]        = "b-tagging discriminant"
XaxisTitles["DL1Weight__pt_100_150"]      = "b-tagging discriminant"
XaxisTitles["DL1Weight__pt_500_600"]      = "b-tagging discriminant"
XaxisTitles["DL1Weight__pt_1000_1200"]    = "b-tagging discriminant"
XaxisTitles["DL1rWeight"]                 = "b-tagging discriminant"
XaxisTitles["DL1rWeight__pt_30_40"]       = "b-tagging discriminant"
XaxisTitles["DL1rWeight__pt_100_150"]     = "b-tagging discriminant"
XaxisTitles["DL1rWeight__pt_500_600"]     = "b-tagging discriminant"
XaxisTitles["DL1rWeight__pt_1000_1200"]   = "b-tagging discriminant"
XaxisTitles["met"]                        = "MET"
XaxisTitles["lep_phi"]                    = "#it{#phi}^{lepton}"
XaxisTitles["lep_eta"]                    = "#it{#eta}^{lepton}"
XaxisTitles["lep0_eta"]                   = "#it{#eta}^{lead lepton}"
XaxisTitles["lep1_eta"]                   = "#it{#eta}^{sublead lepton}"
XaxisTitles["lep_pt"]                     = "#it{p}_{T}^{lepton} [GeV]"
XaxisTitles["lep0_pt"]                    = "#it{p}_{T}^{lead lepton} [GeV]"
XaxisTitles["lep1_pt"]                    = "#it{p}_{T}^{sublead lepton} [GeV]"
XaxisTitles["HFjet0_XF"]                  = "X_{F}"
XaxisTitles["XF"]                         = "X_{F}"
XaxisTitles["mcEvtWeight"]                = "mcEvtWeight"
XaxisTitles["sampleWeight"]               = "sample eight"
XaxisTitles["leptonSF"]                   = "lepton SF"
XaxisTitles["jvtSF"]                      = "jvt SF"

# Unfolding axis titles
XaxisUnfoldTitle = dict()
XaxisUnfoldTitle["Zc_HFjet0_pt"]          = "#it{p}_{T}^{lead c-jet} [GeV]"
XaxisUnfoldTitle["Zc_HFjet0_absy"]        = "|y|^{lead c-jet}"
XaxisUnfoldTitle["Zc_Zpt"]                = XaxisTitles["Zpt"]
XaxisUnfoldTitle["Zc_Zabsy"]              = XaxisTitles["Zabsy"]
XaxisUnfoldTitle["Zc_deltaYZjHF"]         = "#Delta y(Z,c-jet)"
XaxisUnfoldTitle["Zc_deltaPhiZjHF"]       = "#Delta #Phi(Z,c-jet)"
XaxisUnfoldTitle["Zc_deltaRZjHF"]         = "#Delta R(Z,c-jet)"
XaxisUnfoldTitle["Zc_XF"]                 = "{X}_{F}(c-jet)"
XaxisUnfoldTitle["Zb_HFjet0_pt"]          = "#it{p}_{T}^{lead b-jet} [GeV]"
XaxisUnfoldTitle["Zb_HFjet0_absy"]        = "|y|^{lead b-jet}"
XaxisUnfoldTitle["Zb_Zpt"]                = XaxisTitles["Zpt"]
XaxisUnfoldTitle["Zb_Zabsy"]              = XaxisTitles["Zabsy"]
XaxisUnfoldTitle["Zb_deltaYZjHF"]         = "#Delta y(Z,b-jet)"
XaxisUnfoldTitle["Zb_deltaPhiZjHF"]       = "#Delta #Phi(Z,b-jet)"
XaxisUnfoldTitle["Zb_deltaRZjHF"]         = "#Delta R(Z,b-jet)"
XaxisUnfoldTitle["Zb_XF"]                 = "X_{F}(b-jet)"






