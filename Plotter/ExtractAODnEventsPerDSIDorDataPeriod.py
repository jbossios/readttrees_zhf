# Version of the outputs
Date = '07122021'

Debug = False

###################################
# DO NOT MODIFY (below this line)
###################################

def getDSID(path):
  if path.find("3611") != -1:
    return path[path.find("3611"):path.find("3611")+6]
  elif path.find("7001") != -1:
    return path[path.find("7001"):path.find("7001")+6]
  elif path.find("7003") != -1:
    return path[path.find("7003"):path.find("7003")+6]
  elif path.find("3641") != -1:
    return path[path.find("3641"):path.find("3641")+6]
  elif path.find("4104") != -1:
    return path[path.find("4104"):path.find("4104")+6]
  elif path.find("3616") != -1:
    return path[path.find("3616"):path.find("3616")+6]
  elif path.find("4106") != -1:
    return path[path.find("4106"):path.find("4106")+6]
  elif path.find("3457") != -1:
    return path[path.find("3457"):path.find("3457")+6]
  elif path.find("3450") != -1:
    return path[path.find("3450"):path.find("3450")+6]
  elif path.find("3642") != -1:
    return path[path.find("3642"):path.find("3642")+6]
  elif path.find("3636") != -1:
    return path[path.find("3636"):path.find("3636")+6]
  elif path.find("3631") != -1:
    return path[path.find("3631"):path.find("3631")+6]
  elif path.find("3633") != -1:
    return path[path.find("3633"):path.find("3633")+6]
  elif path.find("3634") != -1:
    return path[path.find("3634"):path.find("3634")+6]
  elif path.find("3451") != -1:
    return path[path.find("3451"):path.find("3451")+6]

def getPeriod(path):
  return path[path.find("period"):path.find("period")+7]

JetColl = 'PFlow'

from ROOT import *
import os,sys

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from InputLists import Inputs

# Collect data
Nevents = dict()
for sample in Inputs[JetColl]:
  if Debug: print('DEBUG: Collect number of events for sample = {}'.format(sample))
  MC    = False if 'Data' in sample else True
  PATHs = Inputs[JetColl][sample]
  for path in PATHs:
    if Debug: print('DEBUG: Path assigned to {} = {}'.format(sample,path))
    for folder in os.listdir(path):
      if Debug: print('DEBUG: folder in {} assigned to {} = {}'.format(path,sample,folder))
      if '_metadata.root' not in folder: continue
      if MC:
        dsid = getDSID(folder)
	MetadataFileName = '{}_metadata.root'.format(dsid)
      else: # data
        period = getPeriod(folder)
	MetadataFileName = '{}_metadata.root'.format(period)
      if Debug:
        if MC: 
          print('DEBUG: "period" = {}'.format(dsid))
	else:
          print('DEBUG: "period" = {}'.format(period))
      if Debug: print('DEBUG: MedataFileName = {}'.format(MetadataFileName))
      # Open metadata file
      iFile = TFile.Open(path+folder+'/'+MetadataFileName)
      # Get histogram and number of events
      hist         = iFile.Get("MetaData_EventCount")
      nAODEvents   = int(hist.GetBinContent(1))
      nDAODEvents  = int(hist.GetBinContent(2))
      key          = '{}_{}'.format(sample,dsid) if MC else '{}_{}'.format(sample,period)
      Nevents[key] = {'SampleName' : sample, 'nEventsAOD' : nAODEvents, 'nEventsDAOD' : nDAODEvents}

# Write output file
os.system('mkdir InitialAODEvents')
os.system('mkdir InitialAODEvents/{}'.format(Date))
outputFile = open("InitialAODEvents/{}/Nevents.txt".format(Date),"w")
outputFile.write('SampleName\tnEventsAOD\tnEventsDAOD\n')
for key in Nevents:
  #if 'Data' in key:
  #  outputFile.write("{}\t{}\t{}\t{}\n".format(Nevents[key]['SampleName'],key.split('_')[1],Nevents[key]['nEventsAOD'],Nevents[key]['nEventsDAOD']))
  #else:
  #  outputFile.write("{}\t{}\t{}\t{}\n".format(Nevents[key]['SampleName'],key,Nevents[key]['nEventsAOD'],Nevents[key]['nEventsDAOD']))
  outputFile.write("{}\t{}\t{}\t{}\n".format(Nevents[key]['SampleName'],key.split('_')[1],Nevents[key]['nEventsAOD'],Nevents[key]['nEventsDAOD']))
outputFile.close()

print('>>> ALL DONE <<<')
