
DataSamples = [
  'Signal_data15',
  'Signal_data16',
#  'Signal_data17',
#  'Signal_data18',
]

Backgrounds = [
  'Top',
#  'Diboson',
#  'Ztautau',
]

Debug       = False
Channel     = "MU"
Selection   = "SRnoMET"
Tagger      = "DL1r"
ATLASlegend = "NONE" # Options: Internal, Preliminary, ATLAS and NONE


###########################################################################
# DO NOT MODIFY
###########################################################################

if len(Backgrounds)>1:
  print "ERROR: script not prepared to run with more than one background (ratio would not be correct), exiting"
  sys.exit(0)

import ROOT,os,sys,resource,psutil,argparse,math
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

Samples = ["Signal_MC"]
for sample in DataSamples: Samples.append(sample)
for sample in Backgrounds: Samples.append(sample)

# Luminosity
from Luminosities import *

# Total luminosity and needed campaigns
Luminosity         = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"]= 0
Luminosity["MC16d"]= 0
Luminosity["MC16e"]= 0
Campaigns          = []
DataSamples        = [] # List of data samples
MCSignalSamples    = [] # List of MC signal samples
BackgroundSamples  = [] # List of MC Background samples for each background type
DataPeriod         = 'Data'
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    Luminosity["MC16a"] += Luminosity_2015
    Campaigns.append("MC16a")
    MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
    BackgroundSamples.append("Top_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data16" in sample:
    DataPeriod += '16'
    Luminosity["MC16a"] += Luminosity_2016
    if "MC16a" not in Campaigns:
      Campaigns.append("MC16a")
      MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
      BackgroundSamples.append("Top_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data17" in sample:
    DataPeriod += '17'
    Luminosity["MC16d"] += Luminosity_2017
    Campaigns.append("MC16d")
    MCSignalSamples.append("Signal_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
    BackgroundSamples.append("Top_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data18" in sample:
    DataPeriod += '18'
    Luminosity["MC16e"] += Luminosity_2018
    Campaigns.append("MC16e")
    MCSignalSamples.append("Signal_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
    BackgroundSamples.append("Top_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
#for sample in Samples:
#  if "Signal" not in sample:
#    BackgroundSamples[sample] = []
#    for campaign in Campaigns:
#      BackgroundSamples[sample].append(sample+"_"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)

TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

Campaign   = "MC16"
for campaign in Campaigns:
  if Campaign != "MC16": Campaign += "+"
  if campaign == "MC16a": Campaign += "a"
  elif campaign == "MC16d": Campaign += "d"
  elif campaign == "MC16e": Campaign += "e"

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

#histname = 'nominal/met_Te1'
if Selection == 'SRnoMET':
  #histname = 'nominal/njet'
  histname = 'nominal/met'
else:
  histname = 'met'
  #histname = 'njet'
Hist_MC_Backgrounds = dict()

if Debug: print "###########################################################"
if Debug: print "DEBUG: Producing PDF for '"+histname+"' histogram"
if Debug: print "DEBUG: Memory usage = {0} (MB)".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024)
if Debug: pid = os.getpid()
if Debug: py  = psutil.Process(pid)
if Debug: print "DEBUG: CPU[0] = {0} (%)".format(py.cpu_percent())

############################
# Get Histograms
############################

# Total MC Signal histogram
if Debug: print "DEBUG: Get MC Signal histograms"
Hist_MC_Signal,msg = GetTotalHist(MCSignalSamples,histname,Debug,Luminosity)
if msg != 'OK':
  print msg
  print histname+" not found, exiting"
  sys.exit(0)
Hist_MC_Signal.SetLineColor(ROOT.kRed+1)
Hist_MC_Signal.SetMarkerColor(ROOT.kRed+1)
#Hist_MC_Signal.Scale(1./Hist_MC_Signal.Integral())


# Top MC histogram
if Debug: print "DEBUG: Top MC Signal histograms"
Hist_MC_Top,msg = GetTotalHist(BackgroundSamples,histname,Debug,Luminosity)
if msg != 'OK':
  print msg
  print histname+" not found, exiting"
  sys.exit(0)
Hist_MC_Top.SetLineColor(ROOT.kBlue+1)
Hist_MC_Top.SetMarkerColor(ROOT.kBlue+1)


## Histogram for each background sample
#if Debug: print "DEBUG: Get MC Background histograms"
#for key,value in BackgroundSamples.iteritems():
#  hist,msg = GetTotalHist(value,histname,Debug,Luminosity)
#  if msg != 'OK':
#    print msg
#    sys.exit(0)
  #hist.Scale(1./hist.Integral())
#  Hist_MC_Backgrounds[key] = hist
#  if key == 'Top':
 #   Hist_MC_Top = Hist_MC_Backgrounds[key].Clone("TopBkg")
# Set line color and scale background histograms
#counter = 0
#for key,hist in Hist_MC_Backgrounds.iteritems():
#  hist.SetLineColor(Colors[counter])
#  hist.SetFillColor(Colors[counter])
#  counter += 1

########################
# Make Plot
########################

os.system('mkdir Plots')
os.system('mkdir Plots/'+Selection)
os.system('mkdir Plots/'+Selection+'/TopVsSignal')

# TCanvas
if Debug: print "DEBUG: Create TCanvas"
Canvas  = ROOT.TCanvas()
observable = histname if 'nominal/' not in histname else histname.replace('nominal/','')
outName = "Plots/{0}/TopVsSignal/{1}_{2}_{3}_{4}.pdf".format(Selection,Channel,Selection,Tagger,observable)
Canvas.Print(outName+"[")

# Set log scales (if requested)
histbase = observable.replace('_Te1','')
histbase = histbase.replace('_Ti1','')
histbase = histbase.replace('_Te2','')
if histbase in Logx:
  Canvas.SetLogx()
#if histbase in Logy:
#  Canvas.SetLogy()


# Divide histograms
ratioHist = Hist_MC_Top.Clone('ratioHist')
ratioHist.Divide(Hist_MC_Signal)

## Loop over bins and fill
#nbins = ratioHist.GetNbinsX()
#for ibin in range(1, nbins+1):
#  signal = Hist_MC_Signal.GetBinContent(ibin)
#  bkg = Hist_MC_Top.GetBinContent(ibin)
#  #print bkg
#  if signal > 0:
#    ratioHist.SetBinContent(ibin, bkg/signal)
#  else:
#    ratioHist.SetBinContent(ibin, 0)
#  ratioHist.SetBinError(ibin, 0)


# Draw histogram
ratioHist.Draw("pe0")

# Add histograms to THStack and draw legends
Legends = ROOT.TLegend(0.7,0.7,0.92,0.9)
Legends.SetTextFont(42)

if XaxisRange.has_key(observable):
  ratioHist.GetXaxis().SetRangeUser(XaxisRange[observable][0],XaxisRange[observable][1])
maxY = ratioHist.GetMaximum()
minY = ratioHist.GetMinimum()
ratioHist.GetYaxis().SetRangeUser(minY,maxY)

#ratioHist.GetXaxis().SetLabelSize(0.)
#ratioHist.GetXaxis().SetTitleSize(0.)
ratioHist.GetYaxis().SetTitleSize(20)
ratioHist.GetYaxis().SetTitleFont(43)
ratioHist.GetYaxis().SetLabelFont(43)
ratioHist.GetYaxis().SetLabelSize(19)
ratioHist.GetYaxis().SetTitleOffset(1.3)
ratioHist.GetYaxis().SetTitle("Top bkg/Signal")
ratioHist.SetMinimum(1)
 

if XaxisTitles.has_key(histbase):
  ratioHist.GetXaxis().SetTitleSize(20)
  ratioHist.GetXaxis().SetTitleFont(43)
  ratioHist.GetXaxis().SetLabelFont(43)
  ratioHist.GetXaxis().SetLabelSize(19)
  ratioHist.GetXaxis().SetTitleOffset(1.3)
  ratioHist.GetXaxis().SetTitle(XaxisTitles[histbase])
  ratioHist.GetXaxis().SetNdivisions(510)


if Debug: print "DEBUG: Draw legends"
Legends.Draw("same")

ROOT.gPad.RedrawAxis()

# Show ATLAS legend
if Debug: print "DEBUG: Show ATLAS legend"
if ATLASlegend != 'NONE':
  if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
  elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
  elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
  else:
    print "ERROR: ATLASlegend not recognized, exiting"
    sys.exit(0)
  ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
  ATLASBlock.SetNDC()
  ATLASBlock.Draw("same")

# Show CME and luminosity
if Debug: print "DEBUG: Show CME and luminosity"
CME = "#scale[1.5]{13 TeV, "+str(round(TotalLumi/1000,1))+" fb^{-1}}"
topY = 0.7 if ATLASlegend != 'NONE' else 0.8
CMEblock = ROOT.TLatex(0.2,topY,CME)
CMEblock.SetNDC()
CMEblock.Draw("same")

# # Show channel
# if Debug: print "DEBUG: Show channel type"
# channel = "#scale[1.5]{"
# channel += "#mu" if Channel == "MU" else "e"
# channel += " channel}"
# topY = 0.6 if ATLASlegend != 'NONE' else 0.7
# TextBlock = ROOT.TLatex(0.2,topY,channel)
# TextBlock.SetNDC()
# TextBlock.Draw("same")


# Save PDF
if Debug: print "DEBUG: Save/print PDF"
Canvas.Print(outName)
Canvas.Print(outName+"]")

# # Save ratio hists into a ROOT file
# outROOTFile = ROOT.TFile('RatioHists/'+DataPeriod+'/'+histname+'.root','recreate')
# outROOTFile.cd()
# # ratioHist.Write()

# ratioHist.Delete()
# # Clear dict
# for key,hist in Hist_MC_Backgrounds.iteritems():
#   hist.Delete()
# Hist_MC_Backgrounds.clear()
# Hist_Data.Delete()
# Hist_MC_Signal.Delete()

print '>>> DONE <<<'
