# All possible input files

InputFiles = dict()  # key should match with one of the types in Samples (see below)

path_by_var = {
  #'nominal' : '/eos/atlas/user/j/jbossios/SM/ZHFRun2/PlotterInputs/',
  'nominal' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/',
  'systs' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/',
  'mcsysts' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/',
  'truth' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/',
  'VR' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/',
  #'SR2' : '/eos/user/j/jbossios/SM/WZgroup/ZHF/PlotterInputs/',
}

date_by_var = {
  #'nominal' : '19052022',
  #'systs' : '20052022',
  # First version using v11 TTrees
  #'nominal': '22052022',
  #'systs': '23052022',
  #'mcsysts': '04062022',
  #'truth': '07062022',
  # Second version using v11 TTrees (fixed list of observables, fixed xF and adding missing histograms)
  #'nominal': '21062022',  # nominal + SF variations
  #'mcsysts': '22062022',  # nominal + event weight variations
  #'systs': '23062022',  # systematic TTrees
  #'truth': '24062022',  # nominal + Flavour + truth
  #'mcsysts': '28062022',  # nominal + event weight variations (for SR)
  # TTbar now has dilepton
  #'nominal': '01072022',  # nominal + SF variations
  #'systs': '02072022',  # systematic TTrees
  # TTbar separated into NonAllHad and Dilepton, xF calculation fixed
  #'nominal': '14072022',  # nominal + SF variations
  #'systs': '15072022',  # systematic TTrees
  # Fixed PassRecoSelections for systematicTTrees (minor changes might appear on systematic jobs)
  #'nominal': '26072022',  # nominal + SF variations
  #'systs': '27072022',  # systematic TTrees
  'truth': '20092022',  # nominal + Flavour + truth (SRa1jet)
  #'VR' : '08112022',  # nominal + reco + VR only (all except Top)
  #'VR' : '09112022',  # nominal + reco + VR only (all except Top) (improved VR defs)
  #'SR2' : '28112022',  # same as mcsysts but for SR with larger Zmass window
  #'SR2' : '01122022',  # same as mcsysts but for SR with larger Zmass window
  #'mcsysts': '01122022',  # for SR2
  'mcsysts': '02122022',  # for SR2
  #'nominal': '16122022',  # nominal only [Jvt SF fixed, just for testing]
  'nominal': '04022023',  # nominal + SF variations [same as 26072022 but (Alt)VR->(Alt)CR and CR->VR]
  'systs': '05022023',  # systematic TTrees [same as 27072022 but (Alt)VR->(Alt)CR and CR->VR]
  'VR' : '06022023',  # nominal + reco + VR only (all except Top) (improved VR defs) [same as 09112022 but (Alt)VR->(Alt)CR and CR->VR]
}

###########################################################################################
# DO NOT MODIFY (below this line)
###########################################################################################

# SR for DL1r
#Date   = '25062020' # soft met
#Date   = '03072020'  # final met
#Date   = '06072020'  # final met fixed
#Date   = '08072020'  # final met fixed (fixed Zpt/absy)
#Date   = '05082020'  # w/ PRW DL1r
#Date   = '10082020'  # w/ PRW DL1
#Date   = '16092020' # w/ PRW, DL1r, XF and inclusive truth
#Date   = '17092020' # w/ PRW, DL1r, *fixed* XF and inclusive truth
#Date   = '02102020' # w/ PRW, DL1r, *fixed* XF and inclusive truth (now saving also zero jet events)
#Date   = '21102020' # w/ PRW, DL1r, *fixed* XF and inclusive truth (now saving also zero jet events) (bugfixes for muons and Sherpa 2.2.10)
#Date   = '10022021' # w/ PRW, DL1r, using v4 TTrees
#Date   = '25032021' # w/ PRW, DL1r, using v4 TTrees (used with SRa1jet)
#Date   = '09062021' # w/ PRW, DL1r, using v6 TTrees (used with SR)
#Date   = '27092021' # w/ PRW, DL1r, using v9 TTrees
#Date   = '13102021' # Fixed quantile distributions for Ti2
#Date   = '20102021'  # Added XF distributions for leading and subleading b-tagged jets
#Date   = '29102021'  # Extra jet pt bin
#Date   = '02112021'  # Running on v9 nominal+systematics (for now only SR MU ZmumuSh2211, SR EL TTbar and CR/ELMU TTbar)
#Date = '30112021'
#Date = '07122021'
tagger = 'DL1r'
PRW    = 'usePRW'

samples_by_channel = {
  'Common' : [
    'Signal_data15161718',
    'Signal_data1516',
    'Signal_data15',
    'Signal_data16',
    'Signal_data17',
    'Signal_data18',
    'TTbarNonAllHad_MC16a',
    'TTbarNonAllHad_MC16d',
    'TTbarNonAllHad_MC16e',
    'TTbarDilepton_MC16a',
    'TTbarDilepton_MC16d',
    'TTbarDilepton_MC16e',
    'Ztautau_MC16a',
    'Ztautau_MC16d',
    'Ztautau_MC16e',
    'Diboson_MC16a',
    'Diboson_MC16d',
    'Diboson_MC16e',
    'VH_MC16a',
    'VH_MC16d',
    'VH_MC16e',
    'Wtaunu_MC16a',
    'Wtaunu_MC16d',
    'Wtaunu_MC16e',
  ],
  'MU' : [
    'SignalSH2211_MC16all',
    'SignalSH2211_MC16a',
    'SignalSH2211_MC16d',
    'SignalSH2211_MC16e',
    'SignalMG_MC16a',
    'SignalMG_MC16d',
    'SignalMG_MC16e',
    'SignalFxFx_MC16all',
    'SignalFxFx_MC16a',
    'SignalFxFx_MC16d',
    'SignalFxFx_MC16e',
    'Top_MC16a',
    'Top_MC16d',
    'Top_MC16e',
    'Wmunu_MC16a',
    'Wmunu_MC16d',
    'Wmunu_MC16e',
  ],
  'EL' : [
    'SignalSH2211_MC16all',
    'SignalSH2211_MC16a',
    'SignalSH2211_MC16d',
    'SignalSH2211_MC16e',
    'SignalMG_MC16a',
    'SignalMG_MC16d',
    'SignalMG_MC16e',
    'SignalFxFx_MC16all',
    'SignalFxFx_MC16a',
    'SignalFxFx_MC16d',
    'SignalFxFx_MC16e',
    'Top_MC16a',
    'Top_MC16d',
    'Top_MC16e',
    'Wenu_MC16a',
    'Wenu_MC16d',
    'Wenu_MC16e',
  ],
  'ELMU' : [
    'SingleTop_MC16a',
    'SingleTop_MC16d',
    'SingleTop_MC16e',
    'ZmumuSH2211_MC16a',
    'ZmumuSH2211_MC16d',
    'ZmumuSH2211_MC16e',
    'ZmumuMG_MC16a',
    'ZmumuMG_MC16d',
    'ZmumuMG_MC16e',
    'ZmumuFxFx_MC16a',
    'ZmumuFxFx_MC16d',
    'ZmumuFxFx_MC16e',
    'ZeeSH2211_MC16a',
    'ZeeSH2211_MC16d',
    'ZeeSH2211_MC16e',
    'ZeeMG_MC16a',
    'ZeeMG_MC16d',
    'ZeeMG_MC16e',
    'ZeeFxFx_MC16a',
    'ZeeFxFx_MC16d',
    'ZeeFxFx_MC16e',
    'Wmunu_MC16a',
    'Wmunu_MC16d',
    'Wmunu_MC16e',
    'Wenu_MC16a',
    'Wenu_MC16d',
    'Wenu_MC16e',
  ],
}

def get_campaign(sample):
  if 'MC16all' in sample:
    return 'MC16all'
  elif 'MC16a' in sample:
    return 'MC16a'
  elif 'MC16d' in sample:
    return 'MC16d'
  elif 'MC16e' in sample:
    return 'MC16e'

def translate_sample(sample, channel):
  if 'data' in sample:
    return {'data1516': 'Data1516', 'data15161718': 'Data15161718', 'data15': 'Data15', 'data16': 'Data16', 'data17': 'Data17', 'data18': 'Data18'}[sample.split('_')[1]]
  elif 'Signal' in sample:
    if channel == 'MU':
      if 'SH2211' in sample:
        return 'Zmumu{}Sherpa2211'.format(get_campaign(sample))
      elif 'MG' in sample:
        return 'Zmumu{}MG'.format(get_campaign(sample))
      elif 'FxFx' in sample:
        return 'Zmumu{}FxFx'.format(get_campaign(sample))
    elif channel == 'EL':
      if 'SH2211' in sample:
        return 'Zee{}Sherpa2211'.format(get_campaign(sample))
      elif 'MG' in sample:
        return 'Zee{}MG'.format(get_campaign(sample))
      elif 'FxFx' in sample:
        return 'Zee{}FxFx'.format(get_campaign(sample))
  elif channel == 'ELMU' and ('Zee' in sample or 'Zmumu' in sample):
    if 'SH2211' in sample:
      if 'Zee' in sample:
        return 'Zee{}Sherpa2211'.format(get_campaign(sample))
      elif 'Zmumu' in sample:
        return 'Zmumu{}Sherpa2211'.format(get_campaign(sample))
    elif 'MG' in sample:
      if 'Zee' in sample:
        return 'Zee{}MG'.format(get_campaign(sample))
      elif 'Zmumu' in sample:
        return 'Zmumu{}MG'.format(get_campaign(sample))
    elif 'FxFx' in sample:
      if 'Zee' in sample:
        return 'Zee{}FxFx'.format(get_campaign(sample))
      elif 'Zmumu' in sample:
        return 'Zmumu{}FxFx'.format(get_campaign(sample))
  else:
    sample_type = sample.split('_')[0]
    return '{}{}'.format(get_campaign(sample), sample_type)
  return 'NotImplemented'

configurations = ['MU_SR', 'EL_SR', 'ELMU_CR', 'ELMU_AltCR', 'EL_SRa1jet', 'MU_SRa1jet', 'EL_VR', 'MU_VR', 'EL_SR2', 'MU_SR2']

for var in date_by_var:
  # extra string for dict key
  extra = ''
  if var == 'systs':
    extra = '_FullSysts'
  elif var == 'truth':
    extra = '_truth'
  elif var == 'mcsysts':
    extra = '_mcsysts'
  for conf in configurations:
    if var == 'VR' and 'VR' not in conf:
      continue
    elif var != 'VR' and 'VR' in conf:
      continue
    conf_channel = conf.split('_')[0]
    conf_selection = conf.split('_')[1]
    if conf_selection != 'SR2':
      for sample in samples_by_channel['Common']:
        if 'data' in sample and var != 'nominal' and var != 'truth' and var != 'VR': continue  # skip data for systematic variations (truth for data actually only have flavour dists)
        if 'data' in sample:
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, date_by_var[var])
        else: # MC
          extra_file_name = 'FullSysts' if var == 'systs' else 'nominalOnly'
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_useSFs_useSampleWeights_{}_{}_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, PRW, extra_file_name, date_by_var[var])
      extra_samples = samples_by_channel[conf_channel]
      if var == 'VR':
        extra_samples += [
          'SingleTop_MC16a',
          'SingleTop_MC16d',
          'SingleTop_MC16e',
        ]
      for sample in extra_samples:
        if 'data' in sample and var != 'nominal' and var != 'truth': continue  # skip data for systematic variations (truth for data actually only have flavour dists)
        if 'data' in sample:
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, date_by_var[var])
        else: # MC
          extra_file_name = 'FullSysts' if var == 'systs' else 'nominalOnly'
          InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_useSFs_useSampleWeights_{}_{}_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, PRW, extra_file_name, date_by_var[var])
    else:  # SR2
      extra_samples += [
        'TTbarNonAllHad_MC16a',
        'TTbarNonAllHad_MC16d',
        'TTbarNonAllHad_MC16e',
      ]
      for sample in extra_samples:
        extra_file_name = 'nominalOnly'
        InputFiles['{}_{}_{}_{}{}'.format(sample, conf_channel, conf_selection, tagger, extra)] = '{}{}_{}_{}_{}Tagger_useSFs_useSampleWeights_{}_{}_All_{}_expanded.root'.format(path_by_var[var], translate_sample(sample, conf_channel), conf_channel, conf_selection, tagger, PRW, extra_file_name, date_by_var[var])


if __name__ == '__main__':
  for key in InputFiles:
    print('InputFiles[{}] = {}'.format(key, InputFiles[key]))
