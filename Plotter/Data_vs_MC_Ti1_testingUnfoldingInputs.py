dataPeriods = [
  "1516",
  "17",
  "18",
]

finalStates = [
#  "Zb",
  "Zc",
]

Observables = [
  "HFjet0_pt_Ti1",
  "HFjet0_absy_Ti1",
  "Zpt_Ti1",
  "Zabsy_Ti1",
  "deltaYZjHF_Ti1",
  "deltaPhiZjHF_Ti1",
  "deltaRZjHF_Ti1",
]

import os,sys,ROOT
from Style import *

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

os.system("mkdir Plots/UnfoldingInputs")

# Loop over data-taking periods
for period in dataPeriods:
  # Get input file
  FileName = '../Unfolding/Inputs/unfoldingInputs_08072020_data{0}.root'.format(period)
  inFile   = ROOT.TFile.Open(FileName)
  if not inFile:
    print 'ERROR: '+FileName+' not found, exiting'
    sys.exit(0)
  # Loop over final states
  for case in finalStates:
    # Loop over observables
    for obs in Observables:
      #################
      # Get histograms

      # Get data hist
      inFile.cd()
      hDataName = 'Data_{0}_{1}'.format(case,obs)
      hData     = inFile.Get(hDataName)
      if not hData:
        print 'ERROR: '+hDataName+' not found, exiting'
	sys.exit(0)
      hData.SetDirectory(0)
      # Get MC hist
      hMCName = 'Reco_{0}_{1}'.format(case,obs)
      hMC     = inFile.Get(hMCName)
      if not hData:
        print 'ERROR: '+hMCName+' not found, exiting'
	sys.exit(0)
      hMC.SetDirectory(0)
      hMC.SetLineColor(ROOT.kRed)
      hMC.SetMarkerColor(ROOT.kRed)

      ############
      # Make plot

      # TCanvas
      Canvas  = ROOT.TCanvas()
      outName = 'Plots/UnfoldingInputs/Data_vs_MC_{0}_{1}_{2}.pdf'.format(period,case,obs)
      Canvas.Print(outName+'[')

      # TPad for upper panel
      pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
      pad1.SetTopMargin(0.08)
      pad1.SetBottomMargin(0.03)
      pad1.Draw()
      pad1.cd()

      # Set log scales (if requested)
      histnameBase = obs.replace('_Ti1','')
      if histnameBase in Logx: pad1.SetLogx()
      if histnameBase in Logy: pad1.SetLogy()

      # Legends
      Legends = ROOT.TLegend(0.78,0.7,0.92,0.9)
      Legends.SetTextFont(42)

      hData.Draw("P")
      hData.GetXaxis().SetLabelSize(0.)
      hData.GetXaxis().SetTitleSize(0.)
      hData.GetYaxis().SetTitleSize(20)
      hData.GetYaxis().SetTitleFont(43)
      hData.GetYaxis().SetLabelFont(43)
      hData.GetYaxis().SetLabelSize(19)
      hData.GetYaxis().SetTitleOffset(1.3)
      hData.GetYaxis().SetTitle("Events")
      hData.SetMinimum(1)
      Legends.AddEntry(hData,"Data","p")

      hMC.Draw("hist same")
      Legends.AddEntry(hMC,"MC","l")

      if XaxisRange.has_key(histnameBase):
        hMC.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
        hData.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
      maxY = hData.GetMaximum()
      if hMC.GetMaximum() > maxY: maxY = hMC.GetMaximum()
      maxYscaling = 1E5
      if "absy" in obs or "phi" in obs or "eta" in obs or "deltaR" in obs or "deltaPhi" in obs: maxYscaling = 2E7
      hMC.GetYaxis().SetRangeUser(1,maxY*maxYscaling)
      hData.GetYaxis().SetRangeUser(1,maxY*maxYscaling)

      hMC.GetXaxis().SetLabelSize(0.)
      hMC.GetXaxis().SetTitleSize(0.)
      hMC.GetYaxis().SetTitleSize(20)
      hMC.GetYaxis().SetTitleFont(43)
      hMC.GetYaxis().SetLabelFont(43)
      hMC.GetYaxis().SetLabelSize(19)
      hMC.GetYaxis().SetTitleOffset(1.3)
      hMC.GetYaxis().SetTitle("Events")

      Legends.Draw("same")

      ROOT.gPad.RedrawAxis()

      # TPad for bottom plot
      Canvas.cd()
      pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
      pad2.SetTopMargin(0.03)
      pad2.SetBottomMargin(0.32)
      pad2.Draw()
      pad2.cd()

      # Set log-y scale (if requested)
      if histnameBase in Logx: pad2.SetLogx()

      # Create data/MC histogram
      ratioHist = hMC.Clone("ratioHist")
      ratioHist.SetLineColor(ROOT.kRed+1)
      ratioHist.SetMarkerColor(ROOT.kRed+1)
      ratioHist.Divide(hData)

      # Set y-axis range of ratio panel
      minY = -0.05
      maxY = 2.05
      ratioHist.SetMinimum(minY)
      ratioHist.SetMaximum(maxY)

      # Set x-axis range of ratio panel
      nbins = ratioHist.GetNbinsX()
      if histnameBase in XaxisRange:
        minX = XaxisRange[histnameBase][0]
        maxX = XaxisRange[histnameBase][1]
      else:
        minX = ratioHist.GetXaxis().GetBinLowEdge(1)
        maxX = ratioHist.GetXaxis().GetBinUpEdge(nbins)
      ratioHist.GetXaxis().SetRangeUser(minX,maxX)

      # Draw data/MC ratio
      ratioHist.Draw("e0")

      # Draw line at data/MC==1
      Line = ROOT.TLine(minX,1,maxX,1)
      Line.SetLineStyle(7)
      Line.Draw("same")

      # Set x-axis title
      if XaxisTitles.has_key(histnameBase):
        ratioHist.GetXaxis().SetTitleSize(20)
        ratioHist.GetXaxis().SetTitleFont(43)
        ratioHist.GetXaxis().SetLabelFont(43)
        ratioHist.GetXaxis().SetLabelSize(19)
        ratioHist.GetXaxis().SetTitleOffset(3)
        ratioHist.GetXaxis().SetTitle(XaxisTitles[histnameBase])
        ratioHist.GetXaxis().SetNdivisions(510)

      ratioHist.GetYaxis().SetTitleSize(20)
      ratioHist.GetYaxis().SetTitleFont(43)
      ratioHist.GetYaxis().SetLabelFont(43)
      ratioHist.GetYaxis().SetLabelSize(19)
      ratioHist.GetYaxis().SetTitleOffset(1.3)
      ratioHist.GetYaxis().SetTitle("MC / Data")

      # Save PDF
      Canvas.Print(outName)
      Canvas.Print(outName+"]")

      ratioHist.Delete()
      hData.Delete()
      hMC.Delete()

  inFile.Close()

print ">>> ALL DONE <<<"
