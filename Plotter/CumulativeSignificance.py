##############################################################################
# Purpose: Calculate significance for each observable                        #
# Authors: Luis Pinto (lpintoca@cern.ch ) and Jona Bossio (jbossios@cern.ch) #
#                                                                            #
##############################################################################


DataSamples = [
  'Signal_data15',
  'Signal_data16',
#  'Signal_data17',
#  'Signal_data18',
]

Backgrounds = [
  'Top',
#  'Diboson',
#  'Ztautau',
]

Debug       = False
Channel     = "MU"
Selection   = "SR"
Tagger      = "DL1rTagger"
ATLASlegend = "NONE" # Options: Internal, Preliminary, ATLAS and NONE

UseApproxFormula = True # S/sqrt(S+B), if False then 2sqrt{(s+b)log(1+s/b)-2s}

###########################################################################
# DO NOT MODIFY
###########################################################################

if len(Backgrounds)>1:
  print "ERROR: script not prepared to run with more than one background (ratio would not be correct), exiting"
  sys.exit(0)

import ROOT,os,sys,resource,psutil,argparse,math
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

Samples = ["Signal_MC"]
for sample in DataSamples: Samples.append(sample)
for sample in Backgrounds: Samples.append(sample)

# Luminosity
from Luminosities import *

# Total luminosity and needed campaigns
Luminosity         = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"]= 0
Luminosity["MC16d"]= 0
Luminosity["MC16e"]= 0
Campaigns          = []
DataSamples        = [] # List of data samples
MCSignalSamples    = [] # List of MC signal samples
BackgroundSamples  = [] # List of MC Background samples for each background type
DataPeriod         = 'Data'
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    Luminosity["MC16a"] += Luminosity_2015
    Campaigns.append("MC16a")
    MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
    BackgroundSamples.append("Top_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data16" in sample:
    DataPeriod += '16'
    Luminosity["MC16a"] += Luminosity_2016
    if "MC16a" not in Campaigns:
      Campaigns.append("MC16a")
      MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
      BackgroundSamples.append("Top_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data17" in sample:
    DataPeriod += '17'
    Luminosity["MC16d"] += Luminosity_2017
    Campaigns.append("MC16d")
    MCSignalSamples.append("Signal_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
    BackgroundSamples.append("Top_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data18" in sample:
    DataPeriod += '18'
    Luminosity["MC16e"] += Luminosity_2018
    Campaigns.append("MC16e")
    MCSignalSamples.append("Signal_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
    BackgroundSamples.append("Top_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
#for sample in Samples:
#  if "Signal" not in sample:
#    BackgroundSamples[sample] = []
#    for campaign in Campaigns:
#      BackgroundSamples[sample].append(sample+"_"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)

TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

Campaign   = "MC16"
for campaign in Campaigns:
  if Campaign != "MC16": Campaign += "+"
  if campaign == "MC16a": Campaign += "a"
  elif campaign == "MC16d": Campaign += "d"
  elif campaign == "MC16e": Campaign += "e"

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

#histname = 'nominal/met_Te1'
if Selection == 'SRnoMET':
  #histname = 'nominal/njet'
  histname = 'nominal/met'
else:
  #histname = 'njet'
  histname = 'met_Ti1'


if Debug: print "###########################################################"
if Debug: print "DEBUG: Producing PDF for '"+histname+"' histogram"
if Debug: print "DEBUG: Memory usage = {0} (MB)".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024)
if Debug: pid = os.getpid()
if Debug: py  = psutil.Process(pid)
if Debug: print "DEBUG: CPU[0] = {0} (%)".format(py.cpu_percent())

############################
# Get Histograms
############################

# Total MC Signal histogram
if Debug: print "DEBUG: Get MC Signal histograms"
Hist_MC_Signal,msg = GetTotalHist(MCSignalSamples,histname,Debug,Luminosity)
if msg != 'OK':
  print msg
  print histname+" not found, exiting"
  sys.exit(0)
Hist_MC_Signal.SetLineColor(ROOT.kRed+1)
Hist_MC_Signal.SetMarkerColor(ROOT.kRed+1)


# Top MC histogram
if Debug: print "DEBUG: Top MC Signal histograms"
Hist_MC_Top,msg = GetTotalHist(BackgroundSamples,histname,Debug,Luminosity)
if msg != 'OK':
  print msg
  print histname+" not found, exiting"
  sys.exit(0)
Hist_MC_Top.SetLineColor(ROOT.kBlue+1)
Hist_MC_Top.SetMarkerColor(ROOT.kBlue+1)



########################
# Make Plot
########################

os.system('mkdir Plots')
os.system('mkdir Plots/'+Selection)
os.system('mkdir Plots/'+Selection+'/CumulativeSignificance')

# TCanvas
if Debug: print "DEBUG: Create TCanvas"
Canvas  = ROOT.TCanvas()
observable = histname if 'nominal/' not in histname else histname.replace('nominal/','')
outName = "Plots/{0}/CumulativeSignificance/{1}_{2}_{3}_{4}.pdf".format(Selection,Channel,Selection,Tagger,observable)
Canvas.Print(outName+"[")

# Set log scales (if requested)
histbase = observable.replace('_Te1','')
histbase = histbase.replace('_Ti1','')
histbase = histbase.replace('_Te2','')
if histbase in Logx:
  Canvas.SetLogx()
#if histbase in Logy:
#  Canvas.SetLogy()



# Create Cumulative Signal/Bkg histograms - Significance histogram
signalHist  = Hist_MC_Signal.Clone("Signal") # will be cumulative signal
bkgHist        = Hist_MC_Top.Clone("Bkg")       # Will be cumulative background
significanceHist  = Hist_MC_Signal.Clone("Significance") # will be Significance 

# Define variables
thresholds = []
sig_events = 0
bkg_events = 0
list_signal_events = []
list_bkg_events = []
list_significance = []

# Loop over different thresholds
nbins = significanceHist.GetNbinsX()
for ibin in range (1, nbins+1):
  thresholds.append(ibin)
  # Get and store signal event
  event = signalHist.GetBinContent(ibin)
  sig_events += event
  list_signal_events.append(sig_events)
  # Get and store bkg event
  event = bkgHist.GetBinContent(ibin)
  bkg_events += event
  list_bkg_events.append(bkg_events)

# Fill histograms
for x in range(1, len(list_signal_events)+1):
  signalHist.SetBinContent(x,list_signal_events[x-1])
  signalHist.SetBinError(x,0)
for x in range(1, len(list_bkg_events)+1):
  bkgHist.SetBinContent(x,list_bkg_events[x-1])
  bkgHist.SetBinError(x,0)

# Fill significance histogram
nbins = significanceHist.GetNbinsX()
for ibin in range(1, nbins+1):
  s = signalHist.GetBinContent(ibin)
  b = bkgHist.GetBinContent(ibin)
  n0 = s+b
  if s > 0 and b > 0:
    if UseApproxFormula:
      significance = s/math.sqrt(s+b)
    else:
      ln = math.log(1+ (s/b))
      significance = math.sqrt(2 * n0 * ln - 2*s)
  else:
    significance = 0
  significanceHist.SetBinContent(ibin, significance)
  significanceHist.SetBinError(ibin, 0)

# Set x-axis title
if XaxisTitles.has_key(histbase):
  significanceHist.GetXaxis().SetTitleSize(20)
  significanceHist.GetXaxis().SetTitleFont(43)
  significanceHist.GetXaxis().SetLabelFont(43)
  significanceHist.GetXaxis().SetLabelSize(19)
  significanceHist.GetXaxis().SetTitleOffset(1.3)
  significanceHist.GetXaxis().SetTitle(XaxisTitles[histbase])
  significanceHist.GetXaxis().SetNdivisions(510)

significanceHist.GetYaxis().SetTitleSize(20)
significanceHist.GetYaxis().SetTitleFont(43)
significanceHist.GetYaxis().SetLabelFont(43)
significanceHist.GetYaxis().SetLabelSize(19)
significanceHist.GetYaxis().SetTitleOffset(1.3)
significanceHist.GetYaxis().SetTitle("S_{cL}")

if UseApproxFormula:
  formula = "s/#sqrt{s+b}"
else:
  formula = "#sqrt{2n_{0}ln(1+s/b)-2s}"


# Draw histogram
significanceHist.Draw("pe0")

# Add histograms to THStack and draw legends
Legends = ROOT.TLegend(0.7,0.7,0.92,0.9)
Legends.SetTextFont(42)

#if XaxisRange.has_key(histbase):
#  ratioHist.GetXaxis().SetRangeUser(XaxisRange[histbase][0],XaxisRange[histbase][1])
maxY = significanceHist.GetMaximum()
minY = significanceHist.GetMinimum()
significanceHist.GetYaxis().SetRangeUser(minY,maxY)


if Debug: print "DEBUG: Draw legends"
Legends.Draw("same")

ROOT.gPad.RedrawAxis()

# Show ATLAS legend
if Debug: print "DEBUG: Show ATLAS legend"
if ATLASlegend != 'NONE':
  if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
  elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
  elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
  else:
    print "ERROR: ATLASlegend not recognized, exiting"
    sys.exit(0)
  ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
  ATLASBlock.SetNDC()
  ATLASBlock.Draw("same")

# Show CME and luminosity
if Debug: print "DEBUG: Show CME and luminosity"
CME = "#scale[1.5]{13 TeV, "+str(round(TotalLumi/1000,1))+" fb^{-1}}"
topY = 0.7 if ATLASlegend != 'NONE' else 0.8
CMEblock = ROOT.TLatex(0.2,topY,CME)
CMEblock.SetNDC()
CMEblock.Draw("same")

legend = "#scale[0.8]{S_{cL} = "
legend += formula
legend += "}"
Legend = ROOT.TLatex(0.4,0.25,legend)
Legend.SetNDC()
Legend.Draw("same")

# Save PDF
if Debug: print "DEBUG: Save/print PDF"
Canvas.Print(outName)
Canvas.Print(outName+"]")

print '>>> DONE <<<'
