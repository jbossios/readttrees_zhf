##############################################################################
# Purpose: Calculate significance for each observable                        #
# Authors: Luis Pinto (lpintoca@cern.ch ) and Jona Bossio (jbossios@cern.ch) #
#                                                                            #
##############################################################################

import ROOT,os,sys,math,resource,psutil

Channel   = "MU" # only works for the muon channel
Campaigns = ["a","e"] # only campaign 'a' is available
Selection = "SRnoMET" # Options: "Study_significance1", "SR", "SRnoMET_nobJetReq"
Tagger    = "MV2c10" # other taggers not available yet
Debug     = False

# List of samples from Samples which are backgrounds
Backgrounds = [
  "Top",
  "Diboson",
  "Wmunu",
  "Ztautau",
]

UseApproxFormula = True # S/sqrt(S+B), if False then 2sqrt{(s+b)log(1+s/b)-2s}

###########################################
## DO NOT MODIFY
###########################################

from InputFiles             import *
from PlotterHelperFunctions import *
from Style                  import *
from Luminosities           import *

# Import list of observables from reader
PATH = "../"
sys.path.insert(1, PATH) # insert at 1, 0 is the script path
from Arrays import *
Observables.append("met")

# Colors for backgrounds
Colors = [
  ROOT.kBlack,
  ROOT.kRed,
  ROOT.kGreen+2,
  ROOT.kMagenta+1
]


# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

Samples = ["Signal"]
for bkg in Backgrounds: Samples.append(bkg)
Luminosity   = 0
MCSignalSamples    = [] # List of MC singal samples + bkgs

if "Signal" in Samples:
  for campaign in Campaigns:
    if 'a' in campaign:
      Luminosity = Luminosity + Luminosity_2015 + Luminosity_2016
      MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)

    if 'd' in campaign:
      Luminosity += Luminosity_2017
      MCSignalSamples.append("Signal_MC16d_"+Channel+"_"+Selection+"_"+Tagger)

    if 'e' in campaign:
      Luminosity += Luminosity_2018
      MCSignalSamples.append("Signal_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
else:
  print "Missing signal in Samples lists"
  sys.exit(0)

BackgroundSamples = dict()
bkgSamples = list()
for sample in Samples:
  if "Signal" not in sample:
    BackgroundSamples[sample] = []
    for campaign in Campaigns:
      bkgSamples.append(sample+"_MC16"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)
      BackgroundSamples[sample].append(sample+"_MC16"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)

# Define Xrange for met distribution
maxMET = 500
XaxisRange["met"] = [0, maxMET]

# Loop over histograms
Hist_MC_Backgrounds = dict()
for histname in Observables:

  if Debug: print "DEBUG: Calculating significance for '"+histname+"' histogram"

  ############################
  # Get Histograms
  ############################

  # Total MC Signal histogram
  if Debug: print "DEBUG: Get MC Signal histograms"
  Hist_MC_Signal,msg = GetTotalHist(MCSignalSamples,histname,Debug)
  if msg != 'OK':
    print msg
    print histname+" not found, exiting"
    sys.exit(0)
  Hist_MC_Signal.SetLineColor(ROOT.kRed+1)
  Hist_MC_Signal.SetFillColor(ROOT.kRed+1)
  Hist_MC_Signal.Scale(Luminosity)

  # Total MC Bkg histogram
  if Debug: print "DEBUG: Get MC Signal histograms"
  Hist_MC_Bkg,msg = GetTotalHist(bkgSamples,histname,Debug)
  if msg != 'OK':
    print msg
    print histname+" not found, exiting"
    sys.exit(0)
  Hist_MC_Bkg.SetLineColor(ROOT.kRed+1)
  Hist_MC_Bkg.SetFillColor(ROOT.kRed+1)
  Hist_MC_Bkg.Scale(Luminosity)


  ########################
  # Make Plot
  ########################

  # TCanvas
  if Debug: print "DEBUG: Create TCanvas"
  Canvas  = ROOT.TCanvas()
  outName = "Plots/Significance/Individual/{0}_{1}_{2}_{3}.pdf".format(Channel, histname, Selection, Tagger)
  Canvas.Print(outName+"[")

  # Set log-y scale (if requested)
  if Debug: print "DEBUG: Set log-Y scale if requested"
  if histname in Logx: Canvas.SetLogx()

  # Create Cumulative Signal/Bkg histograms - Significance histogram
  signalHist  = Hist_MC_Signal.Clone("Signal") # will be signal
  bkgHist        = Hist_MC_Bkg.Clone("Bkg")       # Will be background
  significanceHist  = Hist_MC_Signal.Clone("Significance") # will be Significance

  # Define variables
  thresholds = []
  sig_event = 0
  bkg_event = 0
  list_significance = []

  # Loop over bins
  nbins = significanceHist.GetNbinsX()
  for ibin in range (1, nbins+1):
    # Get signal event
    s = signalHist.GetBinContent(ibin)
    # Get bkg event
    b = bkgHist.GetBinContent(ibin)
    # Fill significance histogram
    n0 = s+b
    if s > 0 and b > 0:
      if UseApproxFormula:
        significance = s/math.sqrt(s+b)
      else:
        ln = math.log(1+ (s/b))
        significance = math.sqrt(2 * n0 * ln - 2*s)
    else:
      significance = 0
    significanceHist.SetBinContent(ibin, significance)
    significanceHist.SetBinError(ibin, 0)

  # Set x-axis title
  if histname in XaxisTitles:
    significanceHist.GetXaxis().SetTitleSize(20)
    significanceHist.GetXaxis().SetTitleFont(43)
    significanceHist.GetXaxis().SetLabelFont(43)
    significanceHist.GetXaxis().SetLabelSize(19)
    significanceHist.GetXaxis().SetTitleOffset(1.3)
    significanceHist.GetXaxis().SetTitle(XaxisTitles[histname])
    significanceHist.GetXaxis().SetNdivisions(510)

  significanceHist.GetYaxis().SetTitleSize(20)
  significanceHist.GetYaxis().SetTitleFont(43)
  significanceHist.GetYaxis().SetLabelFont(43)
  significanceHist.GetYaxis().SetLabelSize(19)
  significanceHist.GetYaxis().SetTitleOffset(1.3)
  significanceHist.GetYaxis().SetTitle("S_{cL}")
  if UseApproxFormula:
    formula = "s/#sqrt{s+b}"
  else:
    formula = "#sqrt{2n_{0}ln(1+s/b)-2s}"

  # Draw significance
  significanceHist.Draw("pe0")

  # Show channel
  if Debug: print "DEBUG: Show channel type"
  channel = "#scale[1.5]{"
  channel += "#mu" if Channel == "MU" else "e"
  channel += " channel}"
  TextBlock = ROOT.TLatex(0.25,0.8,channel)
  TextBlock.SetNDC()
  TextBlock.Draw("same")

  legend = "#scale[0.8]{S_{cL} = "
  legend += formula
  legend += "}"
  Legend = ROOT.TLatex(0.4,0.25,legend)
  Legend.SetNDC()
  Legend.Draw("same")

  # Save PDF
  Canvas.Print(outName)
  Canvas.Print(outName+"]")

print ">>> ALL DONE <<<"

