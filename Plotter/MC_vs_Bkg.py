
DataSamples = [
  'Signal_data15',
  'Signal_data16',
#  'Signal_data17',
#  'Signal_data18',
]

Backgrounds = [
  'Top',
#  'Diboson',
#  'Ztautau',
]

Debug       = False
Channel     = "MU"
Selection   = "SRnoMET"
Tagger      = "DL1r"
ATLASlegend = "NONE" # Options: Internal, Preliminary, ATLAS and NONE

###########################################################################
# DO NOT MODIFY
###########################################################################

if len(Backgrounds)>1:
  print "ERROR: script not prepared to run with more than one background (ratio would not be correct), exiting"
  sys.exit(0)

import ROOT,os,sys,resource,psutil,argparse
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

Samples = ["Signal_MC"]
for sample in DataSamples: Samples.append(sample)
for sample in Backgrounds: Samples.append(sample)

# Luminosity
from Luminosities import *

# Total luminosity and needed campaigns
Luminosity         = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"]= 0
Luminosity["MC16d"]= 0
Luminosity["MC16e"]= 0
Campaigns          = []
DataSamples        = [] # List of data samples
MCSignalSamples    = [] # List of MC signal samples
BackgroundSamples  = dict() # List of MC Background samples for each background type
DataPeriod         = 'Data'
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    Luminosity["MC16a"] += Luminosity_2015
    Campaigns.append("MC16a")
    MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)

  elif "data16" in sample:
    DataPeriod += '16'
    Luminosity["MC16a"] += Luminosity_2016
    if "MC16a" not in Campaigns:
      Campaigns.append("MC16a")
      MCSignalSamples.append("Signal_MC16a_"+Channel+"_"+Selection+"_"+Tagger)

  elif "data17" in sample:
    DataPeriod += '17'
    Luminosity["MC16d"] += Luminosity_2017
    Campaigns.append("MC16d")
    MCSignalSamples.append("Signal_MC16d_"+Channel+"_"+Selection+"_"+Tagger)

  elif "data18" in sample:
    DataPeriod += '18'
    Luminosity["MC16e"] += Luminosity_2018
    Campaigns.append("MC16e")
    MCSignalSamples.append("Signal_MC16e_"+Channel+"_"+Selection+"_"+Tagger)

for sample in Samples:
  if "Signal" not in sample:
    BackgroundSamples[sample] = []
    for campaign in Campaigns:
      BackgroundSamples[sample].append(sample+"_"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)

TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

Campaign   = "MC16"
for campaign in Campaigns:
  if Campaign != "MC16": Campaign += "+"
  if campaign == "MC16a": Campaign += "a"
  elif campaign == "MC16d": Campaign += "d"
  elif campaign == "MC16e": Campaign += "e"

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

#histname = 'nominal/met_Te1'
if Selection == 'SRnoMET':
  #histname = 'nominal/njet'
  #histname = 'nominal/met'
  #histname = 'nominal/Zpt'
  histname = 'nominal/met_Te1'
  #histname = 'nominal/Zpt_Te1'
else:
  #histname = 'njet'
  #histname = 'met'
  #histname = 'Zpt'
  histname = 'met_Te1'
  #histname = 'Zpt_Te1'
Hist_MC_Backgrounds = dict()

if Debug: print "###########################################################"
if Debug: print "DEBUG: Producing PDF for '"+histname+"' histogram"
if Debug: print "DEBUG: Memory usage = {0} (MB)".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024)
if Debug: pid = os.getpid()
if Debug: py  = psutil.Process(pid)
if Debug: print "DEBUG: CPU[0] = {0} (%)".format(py.cpu_percent())

############################
# Get Histograms
############################

# Total MC Signal histogram
if Debug: print "DEBUG: Get MC Signal histograms"
Hist_MC_Signal,msg = GetTotalHist(MCSignalSamples,histname,Debug,Luminosity)
if msg != 'OK':
  print msg
  print histname+" not found, exiting"
  sys.exit(0)
Hist_MC_Signal.SetLineColor(ROOT.kRed+1)
Hist_MC_Signal.SetMarkerColor(ROOT.kRed+1)
#Hist_MC_Signal.Scale(1./Hist_MC_Signal.Integral())

# Histogram for each background sample
if Debug: print "DEBUG: Get MC Background histograms"
for key,value in BackgroundSamples.iteritems():
  hist,msg = GetTotalHist(value,histname,Debug,Luminosity)
  if msg != 'OK':
    print msg
    sys.exit(0)
  #hist.Scale(1./hist.Integral())
  Hist_MC_Backgrounds[key] = hist
# Set line color and scale background histograms
counter = 0
for key,hist in Hist_MC_Backgrounds.iteritems():
  hist.SetLineColor(Colors[counter])
  hist.SetFillColor(Colors[counter])
  counter += 1

########################
# Make Plot
########################

os.system('mkdir Plots')
os.system('mkdir Plots/'+Selection)
os.system('mkdir Plots/'+Selection+'/MC_vs_bkg')

# TCanvas
if Debug: print "DEBUG: Create TCanvas"
Canvas  = ROOT.TCanvas()
observable = histname if 'nominal/' not in histname else histname.replace('nominal/','')
outName = "Plots/{0}/MC_vs_bkg/{1}_{2}_{3}_{4}.pdf".format(Selection,Channel,Selection,Tagger,observable)
Canvas.Print(outName+"[")

# TPad for upper panel
if Debug: print "DEBUG: Create TPad for upper panel"
pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
pad1.SetTopMargin(0.08)
pad1.SetBottomMargin(0.03)
pad1.Draw()
pad1.cd()

# Set log scales (if requested)
histbase = observable.replace('_Te1','')
histbase = histbase.replace('_Ti1','')
histbase = histbase.replace('_Te2','')
if histbase in Logx:
  pad1.SetLogx()
if histbase in Logy:
  pad1.SetLogy()
# if Debug: print "DEBUG: Set log scales if requested"
# histnameBase = histname.replace('_Te1','')
# histnameBase = histnameBase.replace('_Ti2','')
# if histnameBase in Logx:
#   pad1.SetLogx()
# if histnameBase in Logy:
#   pad1.SetLogy()
# if "Weight" in histname: pad1.SetLogy()

# Add histograms to THStack and draw legends
Legends = ROOT.TLegend(0.7,0.7,0.92,0.9)
Legends.SetTextFont(42)

Hist_MC_Signal.Draw("E P")
Hist_MC_Signal.GetXaxis().SetLabelSize(0.)
Hist_MC_Signal.GetXaxis().SetTitleSize(0.)
Hist_MC_Signal.GetYaxis().SetTitleSize(20)
Hist_MC_Signal.GetYaxis().SetTitleFont(43)
Hist_MC_Signal.GetYaxis().SetLabelFont(43)
Hist_MC_Signal.GetYaxis().SetLabelSize(19)
Hist_MC_Signal.GetYaxis().SetTitleOffset(1.3)
Hist_MC_Signal.GetYaxis().SetTitle("Events")
Hist_MC_Signal.SetMinimum(1)
  #Legends.AddEntry(Hist_MC_Signal.,"Signal","p")

if XaxisTitles.has_key(histbase):
  Hist_MC_Signal.GetXaxis().SetTitle(XaxisTitles[histbase])
  Hist_MC_Signal.GetXaxis().SetNdivisions(510)

# THStack for MC samples
StackMC = ROOT.THStack()

# Add MC backgrounds
for key,hist in Hist_MC_Backgrounds.iteritems():
  StackMC.Add(hist,"HIST][")
  if key == "Top":         legend = "Top quark"
  elif key == "Wmunu":     legend = "W(#rightarrow#mu#nu)+jets"
  elif key == "Ztautau":   legend = "Z(#rightarrow#tau#tau)+jets"
  else: legend = key
  Legends.AddEntry(hist,legend,"f")

#StackMC.Add(Hist_MC_Signal,"HIST][")
if Channel == "MU": Legends.AddEntry(Hist_MC_Signal,"Z(#rightarrow#mu#mu)+jets","p")
else:               Legends.AddEntry(Hist_MC_Signal,"Z(#rightarrow e e)+jets","p")   # electron channel

if Debug: print "DEBUG: Draw THSTack with MC histograms"
StackMC.Draw("same")
Hist_MC_Signal.Draw("E P same")

#if XaxisRange.has_key(histname):
#  StackMC.GetXaxis().SetRangeUser(XaxisRange[histname][0],XaxisRange[histname][1])

if 'met' in histname:
  Hist_MC_Signal.GetXaxis().SetRangeUser(0,250)
  StackMC.GetXaxis().SetRangeUser(0,250)
else:
  Hist_MC_Signal.GetXaxis().SetRangeUser(0,1500)
  StackMC.GetXaxis().SetRangeUser(0,1500)

#if StackMC.GetMaximum() > maxY: maxY = StackMC.GetMaximum()
#maxYscaling = 1E5
#if "absy" in histname or "phi" in histname or "eta" in histname or "deltaR" in histname or "deltaPhi" in histname or "Weight" in histname: maxYscaling = 2E7
#if "Weight" in histname: maxYscaling = 1E9
#StackMC.GetYaxis().SetRangeUser(1,maxY*maxYscaling)

StackMC.GetXaxis().SetLabelSize(0.)
StackMC.GetXaxis().SetTitleSize(0.)
StackMC.GetYaxis().SetTitleSize(20)
StackMC.GetYaxis().SetTitleFont(43)
StackMC.GetYaxis().SetLabelFont(43)
StackMC.GetYaxis().SetLabelSize(19)
StackMC.GetYaxis().SetTitleOffset(1.3)
StackMC.GetYaxis().SetTitle("Events")

if Debug: print "DEBUG: Draw legends"
Legends.Draw("same")

#ROOT.gPad.RedrawAxis()

# Show ATLAS legend
if Debug: print "DEBUG: Show ATLAS legend"
if ATLASlegend != 'NONE':
  if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
  elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
  elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
  else:
    print "ERROR: ATLASlegend not recognized, exiting"
    sys.exit(0)
  ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
  ATLASBlock.SetNDC()
  ATLASBlock.Draw("same")

# Show CME and luminosity
if Debug: print "DEBUG: Show CME and luminosity"
CME = "#scale[1.5]{13 TeV, "+str(round(TotalLumi/1000,1))+" fb^{-1}}"
topY = 0.7 if ATLASlegend != 'NONE' else 0.8
CMEblock = ROOT.TLatex(0.2,topY,CME)
CMEblock.SetNDC()
CMEblock.Draw("same")

# Show channel
if Debug: print "DEBUG: Show channel type"
channel = "#scale[1.5]{"
channel += "#mu" if Channel == "MU" else "e"
channel += " channel}"
topY = 0.6 if ATLASlegend != 'NONE' else 0.7
TextBlock = ROOT.TLatex(0.2,topY,channel)
TextBlock.SetNDC()
TextBlock.Draw("same")

# TPad for bottom plot
if Debug: print "DEBUG: Create TPad for bottom panel"
Canvas.cd()
pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
pad2.SetTopMargin(0.03)
pad2.SetBottomMargin(0.32)
pad2.Draw()
pad2.cd()

# Set log-y scale (if requested)
if Debug: print "DEBUG: Set log-Y scale if requested"
if histbase in Logx:
  pad2.SetLogx()
pad2.SetLogy()

# Create data/MC histogram
if Debug: print "DEBUG: Create MC/data histogram"
ratioHist = Hist_MC_Signal.Clone("ratioHist")
ratioHist.SetLineColor(ROOT.kBlack)
for key,hist in Hist_MC_Backgrounds.iteritems():
  ratioHist.Divide(hist)

# # Draw line at data/MC==1
# if Debug: print "DEBUG: Draw line at MC/data==1"
# nbins = ratioHist.GetNbinsX()
# if histnameBase in XaxisRange:
#   minX = XaxisRange[histnameBase][0]
#   maxX = XaxisRange[histnameBase][1]
# else:
#   minX = ratioHist.GetXaxis().GetBinLowEdge(1)
#   maxX = ratioHist.GetXaxis().GetBinUpEdge(nbins)
# Line = ROOT.TLine(minX,1,maxX,1)
# Line.SetLineStyle(7)
# Line.Draw("same")

ratioHist.Draw()

if 'met' in histname:
  ratioHist.GetXaxis().SetRangeUser(0,250)
else:
  ratioHist.GetXaxis().SetRangeUser(0,1500)

# Set x-axis title
if Debug: print "DEBUG: Set X-axis title"
histkey = histbase
if histname == "lep_pt":
  histkey = "mu_pt" if Channel == "MU" else "el_pt"
if XaxisTitles.has_key(histkey):
  ratioHist.GetXaxis().SetTitleSize(20)
  ratioHist.GetXaxis().SetTitleFont(43)
  ratioHist.GetXaxis().SetLabelFont(43)
  ratioHist.GetXaxis().SetLabelSize(19)
  ratioHist.GetXaxis().SetTitleOffset(3)
  if histname == 'met':
    ratioHist.GetXaxis().SetTitle("#it{E}^{miss}_{T} [GeV]")
  ratioHist.GetXaxis().SetTitle(XaxisTitles[histkey])
  ratioHist.GetXaxis().SetNdivisions(510)

ratioHist.GetYaxis().SetTitleSize(20)
ratioHist.GetYaxis().SetTitleFont(43)
ratioHist.GetYaxis().SetLabelFont(43)
ratioHist.GetYaxis().SetLabelSize(19)
ratioHist.GetYaxis().SetTitleOffset(1.3)
ratioHist.GetYaxis().SetTitle("Significance")

# Save PDF
if Debug: print "DEBUG: Save/print PDF"
Canvas.Print(outName)
Canvas.Print(outName+"]")

# # Save ratio hists into a ROOT file
# outROOTFile = ROOT.TFile('RatioHists/'+DataPeriod+'/'+histname+'.root','recreate')
# outROOTFile.cd()
# # ratioHist.Write()

# ratioHist.Delete()
# # Clear dict
# for key,hist in Hist_MC_Backgrounds.iteritems():
#   hist.Delete()
# Hist_MC_Backgrounds.clear()
# Hist_Data.Delete()
# Hist_MC_Signal.Delete()

print '>>> DONE <<<'
