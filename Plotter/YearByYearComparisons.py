Years       = ['15', '16', '17', '18']
ATLASlegend = "Internal" # Options: Internal, Preliminary, ATLAS and NONE
Channel     = 'MU'
Selection   = 'SR'
Tagger      = 'DL1r'
Debug       = False

SignalSamples = ['ratioHist', 'SignalMG_MC_ratio', 'SignalFxFx_MC_ratio']  # ratioHist == Sherpa 2.2.11

############################################################################################################
# DO NOT MODIFY
############################################################################################################

import os,sys,ROOT
from   PlotterHelperFunctions import *
from   Style                  import *

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *

# List of histogram names from ReadTTrees_ZHF
HistNames = []
#for var, cases in AllVariables.iteritems():
for var, cases in Observables.iteritems():
  for case in cases:
    if case == '': continue  # not interested on this
    if case != '': HistNames.append(var+'_'+case)
    else:          HistNames.append(var)
    if case == 'Te1': HistNames.append(var+'_Ti1')

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

Colors = [ROOT.kMagenta,ROOT.kRed,ROOT.kBlue,ROOT.kCyan]

os.system('mkdir Plots/'+Selection)
os.system('mkdir Plots/'+Selection+'/'+Tagger)
os.system('mkdir Plots/'+Selection+'/'+Tagger+'/YearByYearComparisons')

# Loop over histograms
for signal in SignalSamples:
  for histname in HistNames:

    # TCanvas
    if Debug: print("DEBUG: Create TCanvas")
    Canvas  = ROOT.TCanvas()
    outName = "Plots/"+Selection+"/"+Tagger+"/YearByYearComparisons/{}_YearComparison_".format(signal)
    for year in Years: outName += year
    outName += "_{0}_{1}_{2}_{3}.pdf".format(Channel,Selection,Tagger,histname)
    Canvas.Print(outName+"[")

    Hists = dict()
    # Loop over years
    counter = 0
    for year in Years:
      file_name = 'RatioHists/'+Selection+'/'+Tagger+'/Data'+year+'/'+Channel+'_'+histname+'.root'
      File = ROOT.TFile.Open(file_name)
      if not File:
        print('ERROR: {} not found, exiting'.format(file_name))
        sys.exit(0)
      Hist = File.Get(signal)
      if not Hist:
        print('ERROR: {} not found in {}, exiting'.format(signal, file_name))
        sys.exit(0)
      Hist.SetDirectory(0)
      Hist.SetLineColor(Colors[counter])
      Hist.SetMarkerColor(Colors[counter])
      Hists[year] = Hist
      File.Close()
      counter += 1

    # Set log scales (if requested)
    if Debug: print("DEBUG: Set log scales if requested")
    histnameBase = histname.replace('_Te1','')
    histnameBase = histnameBase.replace('_Ti2','')
    histnameBase = histnameBase.replace('_Ti1','')
    if histnameBase in Logx:
      Canvas.SetLogx()

    # Add histograms to THStack and draw legends
    Legends = ROOT.TLegend(0.7,0.7,0.92,0.9)
    Legends.SetTextFont(42)

    # THStack for MC samples
    Stack = ROOT.THStack()
    if '15' in Hists:
      Stack.Add(Hists['15'],"E P")
      Legends.AddEntry(Hists['15'],"MC16a/Data15","p")
    if '16' in Hists:
      Stack.Add(Hists['16'],"E P")
      Legends.AddEntry(Hists['16'],"MC16a/Data16","p")
    if '17' in Hists:
      Stack.Add(Hists['17'],"E P")
      Legends.AddEntry(Hists['17'],"MC16d/Data17","p")
    if '18' in Hists:
      Stack.Add(Hists['18'],"E P")
      Legends.AddEntry(Hists['18'],"MC16e/Data18","p")

    if Debug: print("DEBUG: Draw THSTack")
    Stack.Draw('nostack')

    if XaxisRange.has_key(histnameBase):
      Stack.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
    Stack.GetYaxis().SetRangeUser(0.5,1.5)
    Stack.GetYaxis().SetTitleSize(20)
    Stack.GetYaxis().SetTitleFont(43)
    Stack.GetYaxis().SetLabelFont(43)
    Stack.GetYaxis().SetLabelSize(19)
    Stack.GetYaxis().SetTitleOffset(1.3)
    #Stack.GetYaxis().SetTitle("Events / bin-width")

    # Draw line at data/MC==1
    if Debug: print("DEBUG: Draw line at MC/data==1")
    nbins = Hists[Years[0]].GetNbinsX()
    if histnameBase in XaxisRange:
      minX = XaxisRange[histnameBase][0]
      maxX = XaxisRange[histnameBase][1]
    else:
      minX = Hists[Years[0]].GetXaxis().GetBinLowEdge(1)
      maxX = Hists[Years[0]].GetXaxis().GetBinUpEdge(nbins)
    Line = ROOT.TLine(minX,1,maxX,1)
    Line.SetLineStyle(7)
    Line.Draw("same")

    # Set x-axis title
    if Debug: print("DEBUG: Set X-axis title")
    histkey = histnameBase
    if histkey == "lep_pt":
      histkey = "mu_pt" if Channel == "MU" else "el_pt"
    if XaxisTitles.has_key(histkey):
      Stack.GetXaxis().SetTitleSize(20)
      Stack.GetXaxis().SetTitleFont(43)
      Stack.GetXaxis().SetLabelFont(43)
      Stack.GetXaxis().SetLabelSize(19)
      Stack.GetXaxis().SetTitleOffset(1.3)
      Stack.GetXaxis().SetTitle(XaxisTitles[histkey])
      #Stack.GetXaxis().SetNdivisions(510)

    if Debug: print("DEBUG: Draw legends")
    Legends.Draw("same")

    ROOT.gPad.RedrawAxis()

    # Show ATLAS legend
    if Debug: print("DEBUG: Show ATLAS legend")
    if ATLASlegend != 'NONE':
      if   ATLASlegend == "Internal":    atlas = "#scale[1]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
      elif ATLASlegend == "Preliminary": atlas = "#scale[1]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
      elif ATLASlegend == "ATLAS":       atlas = "#scale[1]{#scale[1.4]{#font[72]{ATLAS}}}";
      else:
        print("ERROR: ATLASlegend not recognized, exiting")
        sys.exit(0)
      ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
      ATLASBlock.SetNDC()
      ATLASBlock.Draw("same")

    # Show channel
    if Debug: print("DEBUG: Show channel type")
    channel = "#scale[1.5]{"
    channel += "#mu" if Channel == "MU" else "e"
    channel += " channel}"
    topY = 0.7 if ATLASlegend != 'NONE' else 0.8
    TextBlock = ROOT.TLatex(0.2,topY,channel)
    TextBlock.SetNDC()
    TextBlock.Draw("same")

    # Save PDF
    if Debug: print("DEBUG: Save/print PDF")
    Canvas.Print(outName)
    Canvas.Print(outName+"]")

    # Clear dict
    for key,hist in Hists.iteritems(): hist.Delete()
    Hists.clear()

