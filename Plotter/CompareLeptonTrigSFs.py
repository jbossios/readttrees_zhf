Sample = 'TTbar_MC16a'

Compare = [
  'ELMU_VR',
  'MU_SR',
  'EL_SR',
]

############################################################
# DO NOT MODIFY (below this line)
############################################################

from InputFiles import *
import ROOT,os,sys

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Create output folder
os.system('mkdir Plots')
os.system('mkdir Plots/CompareLeptonTrigSFs/')

# TCanvas
Canvas   = ROOT.TCanvas()
outName  = 'Plots/CompareLeptonTrigSFs/{}_'.format(Sample)
outName += '_vs_'.join(Compare)
outName += '.pdf'
Canvas.Print(outName+']')

# TPad for upper panel
pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
pad1.SetTopMargin(0.08)
pad1.SetBottomMargin(0.03)
pad1.Draw()
pad1.cd()
pad1.SetLogy()

# Legend
Legends = ROOT.TLegend(0.8,0.73,0.92,0.9)
Legends.SetTextFont(42)

# Styles
Colors  = [ROOT.kBlack,ROOT.kRed,ROOT.kMagenta]
Markers = [22,23,24]

Hists   = []
counter = 0
for opt in Compare:
  # Open input file
  fName  = InputFiles[Sample+'_'+opt+'_DL1r'] 
  inFile = ROOT.TFile.Open(fName)
  if not inFile:
    print('ERROR: {} not found, exiting'.format(fName))
    sys.exit(1)
  # Get histogram
  hName = 'leptonTrigSF'
  hist  = inFile.Get(hName)
  if not hist:
    print('ERROR: {} not found in {}, exiting'.format(hName,fName))
    sys.exit(1)
  hist.SetDirectory(0)
  inFile.Close()
  hist.SetLineColor(Colors[counter])
  hist.SetMarkerColor(Colors[counter])
  hist.SetMarkerStyle(Markers[counter])
  Legends.AddEntry(hist,opt,"p")
  Hists.append(hist)
  counter += 1

# Draw histograms
Hists[0].Draw('p')
for ihist in range(1,len(Hists)):
  Hists[ihist].Draw('psame')

for hist in Hists:
  hist.GetXaxis().SetRangeUser(0,2)

Legends.Draw('same')

for hist in Hists:
  hist.GetXaxis().SetLabelSize(0.)
  hist.GetXaxis().SetTitleSize(0.)
  hist.GetYaxis().SetTitleSize(20)
  hist.GetYaxis().SetTitleFont(43)
  hist.GetYaxis().SetLabelFont(43)
  hist.GetYaxis().SetLabelSize(19)
  hist.GetYaxis().SetTitleOffset(1.3)
  hist.GetYaxis().SetTitle("Events")

ROOT.gPad.RedrawAxis()

# TPad for bottom plot
Canvas.cd()
pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
pad2.SetTopMargin(0.03)
pad2.SetBottomMargin(0.32)
pad2.Draw()
pad2.cd()

# Compute ratios
ratioHists = []
for ihist in range(1,len(Hists)):
  ratioHist = Hists[ihist].Clone("ratioHist_ihist")
  ratioHist.SetLineColor(ihist)
  ratioHist.Divide(Hists[0])
  ratioHists.append(ratioHist)

# Draw ratios
ratioHists[0].Draw('p')
for ihist in range(1,len(ratioHists)):
  ratioHists[ihist].Draw('psame')

for hist in ratioHists:
  ratioHist.GetXaxis().SetRangeUser(0,2)

# Set x-axis title
for ratioHist in ratioHists:
  ratioHist.GetXaxis().SetTitleSize(20)
  ratioHist.GetXaxis().SetTitleFont(43)
  ratioHist.GetXaxis().SetLabelFont(43)
  ratioHist.GetXaxis().SetLabelSize(19)
  ratioHist.GetXaxis().SetTitleOffset(3)
  ratioHist.GetXaxis().SetTitle('Lepton Trigger SF')
  ratioHist.GetXaxis().SetNdivisions(510)
  ratioHist.GetYaxis().SetTitleSize(20)
  ratioHist.GetYaxis().SetTitleFont(43)
  ratioHist.GetYaxis().SetLabelFont(43)
  ratioHist.GetYaxis().SetLabelSize(19)
  ratioHist.GetYaxis().SetTitleOffset(1.3)
  ratioHist.GetYaxis().SetTitle("SR / VR")

# Save PDF
Canvas.Print(outName)
Canvas.Print(outName+']')

# Delete stuff
for hist in Hists:
  hist.Delete()
for hist in ratioHists:
  hist.Delete()

print('>>> ALL DONE <<<')
