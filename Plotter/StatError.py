##############################################################################
# Purpose: Calculate statError for each observable                           #
# Authors: Luis Pinto (lpintoca@cern.ch ) and Jona Bossio (jbossios@cern.ch) #
#                                                                            #
##############################################################################

import ROOT,os,sys,math,resource,psutil

Channel   = "MU"
Campaigns = ["a","d","e"]  # Options: "a", "d" and "e"
Selection = "SR"   # Options: "SR" and "VR"
Tagger    = "DL1r"
Debug     = False
Generator = 'MG' # options: MG, SH2211

###########################################
## DO NOT MODIFY
###########################################

from InputFiles             import *
from PlotterHelperFunctions import *
from Style                  import *
from Luminosities           import *

# Import list of observables from reader
PATH = "../"
sys.path.insert(1, PATH) # insert at 1, 0 is the script path
from Arrays import *
from Dicts  import *

# Colors for backgrounds
Colors = [
  ROOT.kBlack,
  ROOT.kRed,
  ROOT.kGreen+2,
  ROOT.kMagenta+1
]

# Create output folders
os.system('mkdir -p Plots/StatError')

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

Samples         = ["Signal"]
MCSignalSamples = [] # List of MC singal samples
Luminosity = dict()

if "Signal" in Samples:
  for campaign in Campaigns:
    if 'a' in campaign:
      MCSignalSamples.append("Signal"+Generator+"_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
      Luminosity['MC16a'] = Luminosity_2015 + Luminosity_2016
    if 'd' in campaign:
      MCSignalSamples.append("Signal"+Generator+"_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
      Luminosity['MC16d'] = Luminosity_2017
    if 'e' in campaign:
      MCSignalSamples.append("Signal"+Generator+"_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
      Luminosity['MC16e'] = Luminosity_2018
else:
  print("Missing signal in Samples lists")
  sys.exit(0)

Campaigns = 'MC16all' if Campaigns==['a','d','e'] else 'MC16'+Campaigns[0]

# Loop over histograms
for obs,cases in Observables.items():
  if 'Te1' in cases: cases.append('Ti1')
  for case in cases:

    if case == '': continue # skip inclusive Z+jets

    histname     = obs + '_' + case
    histnameBase = histname.replace('_Ti1','')
    histnameBase = histnameBase.replace('_Te1','')
    histnameBase = histnameBase.replace('_Ti2','')

    if Debug: print("DEBUG: Calculating significance for '"+histname+"' histogram")

    ############################
    # Get Histograms
    ############################

    # Total MC Signal histogram
    if Debug: print("DEBUG: Get MC Signal histograms")
    Hist_MC_Signal,msg = GetTotalHist(MCSignalSamples,histname,Debug,Luminosity)
    if msg != 'OK':
      print(msg)
      print(histname+" not found, exiting")
      sys.exit(0)
    Hist_MC_Signal.SetLineColor(ROOT.kRed+1)
    Hist_MC_Signal.SetFillColor(ROOT.kRed+1)

    ########################
    # Make Plot
    ########################

    # TCanvas
    if Debug: print("DEBUG: Create TCanvas")
    Canvas  = ROOT.TCanvas()
    outName = "Plots/StatError/{0}_{1}_{2}_{3}_{4}.pdf".format(Campaigns, Channel, histname, Selection, Tagger)
    Canvas.Print(outName+"[")

    # Set log-y scale (if requested)
    if Debug: print("DEBUG: Set log-Y scale if requested")
    if histnameBase in Logx: Canvas.SetLogx()

    # Create MCStatError histogram
    statHist = Hist_MC_Signal.Clone("MCStatError")

    # Loop over bins
    nbins = statHist.GetNbinsX()
    for ibin in range(1, nbins+1):
      # Get content
      s = Hist_MC_Signal.GetBinContent(ibin)
      # Fill stat histogram
      if s > 0:
        stat = math.sqrt(s)/s
      else:
        stat = 0
      statHist.SetBinContent(ibin, stat)
      statHist.SetBinError(ibin, 0)

    # Set x-axis title
    if histnameBase in XaxisTitles:
      statHist.GetXaxis().SetTitleSize(20)
      statHist.GetXaxis().SetTitleFont(43)
      statHist.GetXaxis().SetLabelFont(43)
      statHist.GetXaxis().SetLabelSize(19)
      statHist.GetXaxis().SetTitleOffset(1.3)
      statHist.GetXaxis().SetTitle(XaxisTitles[histnameBase])
      statHist.GetXaxis().SetNdivisions(510)

    statHist.GetYaxis().SetTitleSize(20)
    statHist.GetYaxis().SetTitleFont(43)
    statHist.GetYaxis().SetLabelFont(43)
    statHist.GetYaxis().SetLabelSize(19)
    statHist.GetYaxis().SetTitleOffset(1.3)
    statHist.GetYaxis().SetTitle("Statistical Error")

    # Draw stat
    statHist.Draw("pe0")

    # Draw threshold line at 0.1
    if histnameBase in XaxisRange:
      threshline = ROOT.TLine(XaxisRange[histnameBase][0] ,0.1 ,XaxisRange[histnameBase][1], 0.1)
    else:
      threshline = ROOT.TLine(statHist.GetXaxis().GetBinLowEdge(1) , 0.1, statHist.GetXaxis().GetBinLowEdge(statHist.GetNbinsX()+1), 0.1)
    threshline.SetLineColor(ROOT.kRed)
    threshline.SetLineStyle(9)
    threshline.Draw("same")

    # Show channel
    if Debug: print("DEBUG: Show channel type")
    channel = "#scale[1.5]{"
    channel += "#mu" if Channel == "MU" else "e"
    channel += " channel}"
    TextBlock = ROOT.TLatex(0.4,0.8,channel)
    TextBlock.SetNDC()
    TextBlock.Draw("same")

    # Save PDF
    Canvas.Print(outName)
    Canvas.Print(outName+"]")

print(">>> ALL DONE <<<")
