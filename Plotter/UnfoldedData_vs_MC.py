#!/usr/bin/env python

####################################################################
#                                                                  #
# Purpose: Compare unfolded data xss with MC predictions           #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

import ROOT,os,sys,resource,psutil,argparse,math
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

# List of data samples
Samples = [
  'data15',
  'data16',
  'data17',
  'data18',
]

FinalStates = ['Zc']

# Date of unfolding inputs
#Date = '28062020' # Soft MET
#Date = '03072020' # Final MET
#Date = '06072020'  # Final MET fixed
#Date = '08072020'  # Final MET fixed (fixed Zpt/absy)
Date = '05082020'  # w/ PRW

# List of observables
Observables = [
  'HFjet0_pt_Ti1',
  'HFjet0_absy_Ti1',
  'Zpt_Ti1',
  'Zabsy_Ti1',
  'deltaYZjHF_Ti1',
  'deltaPhiZjHF_Ti1',
  'deltaRZjHF_Ti1',
]

# Add systematic uncertainties
PlotSystematics = True
Systematics = [ # list of systematic uncertainties (luminosity applied automatically if PlotSystematics is True)
  'fit',       # flavour fit non-closure
  'lumi',      # luminosity
  'unfolding', # uncertainty on unfolding method
#  'SF',        # B-tagging, muon and JVT SF uncertainties
]

ATLASlegend = 'Internal' # Options: Internal, NONE, Preliminary, ATLAS
Debug = False

################################################################################
# Do not modify
################################################################################

os.system('mkdir Systematics')

# Luminosity
from Luminosities import *

# Import SF variations from Reader
PATH = "../"
sys.path.insert(1, PATH) # insert at 1, 0 is the script path
from Systematics import *

# Get unfolding results for data15+16, data17 and data18
DataDict = dict()
if 'data15' in Samples and 'data16' in Samples: DataDict['1516'] = ['15','16']
elif 'data15' in Samples:                       DataDict['15']   = ['15']
elif 'data16' in Samples:                       DataDict['16']   = ['16']
if 'data17' in Samples:                         DataDict['17']   = ['17']
if 'data18' in Samples:                         DataDict['18']   = ['18']

# Total luminosity and needed campaigns
Luminosity          = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"] = 0
Luminosity["MC16d"] = 0
Luminosity["MC16e"] = 0
DataPeriod          = 'data'
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    Luminosity["MC16a"] += Luminosity_2015
  elif "data16" in sample:
    DataPeriod += '16'
    Luminosity["MC16a"] += Luminosity_2016
  elif "data17" in sample:
    DataPeriod += '17'
    Luminosity["MC16d"] += Luminosity_2017
  elif "data18" in sample:
    DataPeriod += '18'
    Luminosity["MC16e"] += Luminosity_2018

TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Dict with histograms for each observable and uncertainty source
SystematicUncertainties = dict()

# Loop over histograms
Cases = [(x,y) for x in FinalStates for y in Observables]
for case in Cases:
  finalState = case[0] 
  obs        = case[1]
  if Debug: print "###########################################################"
  if Debug: print "DEBUG: Producing PDF for '"+obs+"' observable"

  ############################
  # Get Histograms
  ############################
  
  # Get unfolded data histogram
  
  # Get unfolded data results
  if Debug: print "DEBUG: Get Unfolded Data histogram"
  DataHists = []
  for key,DataPeriods in DataDict.iteritems():
    # Open input file
    FileName = '../Unfolding/Outputs/unfoldingOutputs_'+Date+'_data'+key+'.root'
    inFile   = ROOT.TFile.Open(FileName)
    if not inFile:
      print 'ERROR: input file not found, exiting'
      sys.exit(0)
    # Get Histogram
    Hist = inFile.Get('UnfoldedData_'+finalState+'_'+obs+'_Bayes_iterations_4') # FIXME (check which number of iterations we want to use)
    if not Hist:
      print "ERROR: "+"UnfoldedData_"+finalState+"_"+obs+"_Bayes_iterations_4 not found, exiting"
      sys.exit(0)
    if Debug: print "DEBUG: Unfolded Data histogram retrieved"
    Hist.SetDirectory(0)
    inFile.Close()
    # Set style
    Hist.SetFillColor(ROOT.kBlack)
    Hist.SetFillStyle(3004)
    Hist.SetLineWidth(0)
    DataHists.append(Hist)
  Hist_Data = DataHists[0]
  for ihist in range(1,len(DataHists)): Hist_Data.Add(DataHists[ihist])

  # Add stat+systematic uncertainties to data
  Hist_Data_Syst = Hist_Data.Clone('Data_Syst')
  SystematicUncertainties[obs+'_stat'] = Hist_Data_Syst.Clone(obs+'_stat') 
  for source in Systematics:
    if source != 'SF': SystematicUncertainties[obs+'_'+source] = Hist_Data_Syst.Clone(obs+'_'+source)
    else:
      for SFsource in SFsysts:
	if SFsource == 'nominal': continue
        if 'EL' in SFsource: continue # FIXME (only MU channel implemented)
        SystematicUncertainties[obs+'_'+SFsource] = Hist_Data_Syst.Clone(obs+'_'+SFsource)

  SystHistograms = dict()
  if PlotSystematics:
    # Get syst histograms
    for source in Systematics: # loop over uncertainty sources
      SystHistograms[source] = [] # collect up/down or NP variations for each source
      if source == 'fit' or source == 'lumi': # non-closure fit or luminosity
        for np in ['UP','DOWN']:
          TempHists = []
          for key,DataPeriods in DataDict.iteritems():
            # Open input file
            FileName = '../Unfolding/Outputs/unfoldingOutputs_'+Date+'_data'+key+'.root'
            inFile   = ROOT.TFile.Open(FileName)
            if not inFile:
              print 'ERROR: input file not found, exiting'
              sys.exit(0)
            hist = inFile.Get('UnfoldedData_'+finalState+'_'+obs+'_Bayes_iterations_4_'+source+'_'+np) # FIXME (check which number of iterations we want to use)
            if not hist:
              print "ERROR: UnfoldedData_"+finalState+"_"+obs+"_Bayes_iterations_4_"+source+"_"+np+" not found, exiting"
              sys.exit(0)
            hist.SetDirectory(0)
            inFile.Close()
	    TempHists.append(hist)
	  Hist = TempHists[0]
	  for ihist in range(1,len(TempHists)): Hist.Add(TempHists[ihist])
          SystHistograms[source].append(Hist)
      elif source == 'unfolding':
	for var in ['3','5']: # variations w.r.t. nominal for the number of iterations
          TempHists = []
          for key,DataPeriods in DataDict.iteritems():
            # Open input file
            FileName = '../Unfolding/Outputs/unfoldingOutputs_'+Date+'_data'+key+'.root'
            inFile   = ROOT.TFile.Open(FileName)
            if not inFile:
              print 'ERROR: input file not found, exiting'
              sys.exit(0)
            hist = inFile.Get('UnfoldedData_'+finalState+'_'+obs+'_Bayes_iterations_'+var)
            if not hist:
              print "ERROR: UnfoldedData_"+finalState+"_"+obs+"_Bayes_iterations_"+var+" not found, exiting"
              sys.exit(0)
            hist.SetDirectory(0)
            inFile.Close()
	    TempHists.append(hist)
	  Hist = TempHists[0]
	  for ihist in range(1,len(TempHists)): Hist.Add(TempHists[ihist])
          SystHistograms[source].append(Hist)
      elif source == 'SF':
        for SFsource,NPs in SFsysts.iteritems(): # loop over sources of SF uncertianties
          if SFsource == 'nominal': continue
          if 'EL' in SFsource: continue # FIXME (only MU channel implemented)
          for inp in xrange(1,NPs+1): # loop over NPs
            TempHists = []
            for key,DataPeriods in DataDict.iteritems():
              # Open input file
              FileName = '../Unfolding/Outputs/unfoldingOutputs_'+Date+'_data'+key+'.root'
              inFile   = ROOT.TFile.Open(FileName)
              if not inFile:
                print 'ERROR: input file not found, exiting'
                sys.exit(0)
              hist = inFile.Get('UnfoldedData_'+finalState+'_'+obs+'_Bayes_iterations_4_'+SFsource+'_'+str(inp)) # FIXME (check which number of iterations we want to use)
              if not hist:
                print "ERROR: UnfoldedData_"+finalState+"_"+obs+"_Bayes_iterations_4_"+SFsource+"_"+str(inp)+" not found, exiting"
                sys.exit(0)
              hist.SetDirectory(0)
              inFile.Close()
              TempHists.append(hist)
	    Hist = TempHists[0]
	    for ihist in range(1,len(TempHists)): Hist.Add(TempHists[ihist])
            SystHistograms[SFsource].append(Hist)

    # Construct systematic uncertainty histograms
    AbsoluteSystHists = [] # Histogram of absolute error for each source of systematic uncertainty
    for source in SystHistograms: # loop over systematic sources
      AbsSystHist = Hist_Data.Clone('AbsoluteSystematic_'+source)
      for ibin in xrange(1,Hist_Data.GetNbinsX()+1): # loop over data bins
        if source not in SFsysts:
          avgSyst = 0 # take average between up and down to reduce stat fluctuations (assume symmetric errors)
          counter = 0
          for hist in SystHistograms[source]:
            avgSyst += abs( hist.GetBinContent(ibin) - Hist_Data_Syst.GetBinContent(ibin) )
            counter += 1
          avgSyst /= counter # absolute error
          AbsSystHist.SetBinContent(ibin,avgSyst)
	  if Hist_Data_Syst.GetBinContent(ibin) != 0:
            SystematicUncertainties[obs+'_'+source].SetBinContent(ibin,avgSyst/Hist_Data_Syst.GetBinContent(ibin)) # relative error
	  else:
            SystematicUncertainties[obs+'_'+source].SetBinContent(ibin,0) # relative error
        else: # this is a SF uncertainty
          # Take envelope (sum in quadrature) | absolute error
          SumQua = 0
          for hist in SystHistograms: # loop over NPs
            NPsyst  = hist.GetBinContent(ibin) - Hist_Data_Syst.GetBinContent(ibin)
            SumQua += NPsyst * NPsyst
          SumQua = math.sqrt(SumQua)
          AbsSystHist.SetBinContent(ibin,SumQua)
	  if Hist_Data_Syst.GetBinContent(ibin) != 0:
            SystematicUncertainties[obs+'_'+source].SetBinContent(ibin,SumQua/Hist_Data_Syst.GetBinContent(ibin)) # relative error
          else:
            SystematicUncertainties[obs+'_'+source].SetBinContent(ibin,0) # relative error
      if source == 'unfolding' or source == 'fit' or source == 'lumi': AbsSystHist.Smooth()
      AbsoluteSystHists.append(AbsSystHist)
      if source == 'unfolding' or source == 'fit' or source == 'lumi': SystematicUncertainties[obs+'_'+source].Smooth()
    # Calculate total (systematics + stat) error
    systSqr = 0
    for ibin in xrange(1,Hist_Data.GetNbinsX()+1): # loop over data bins
      # get stat error
      stat = Hist_Data_Syst.GetBinError(ibin)
      if Hist_Data_Syst.GetBinContent(ibin) != 0:
        SystematicUncertainties[obs+'_stat'].SetBinContent(ibin,stat/Hist_Data_Syst.GetBinContent(ibin)) # relative error
      else:
        SystematicUncertainties[obs+'_stat'].SetBinContent(ibin,0) # relative error
      # get total syst error
      systSqr = 0
      for hist in AbsoluteSystHists:
        systSqr	+= hist.GetBinContent(ibin) * hist.GetBinContent(ibin)
      if Debug:
        print 'stat = '+str(stat)
        print 'totalSyst = '+str(math.sqrt(systSqr))
        print '######################################'
      # get total error
      error = math.sqrt(stat*stat + systSqr)
      Hist_Data_Syst.SetBinError(ibin,error)

  # Divide by bin width
  if Debug: print "DEBUG: (Hist_Data) divide by bin-width"
  # Data w/o errors
  for binX in range(1,Hist_Data.GetNbinsX()+1):
    if Hist_Data.GetBinWidth(binX)!=0:
      Hist_Data.SetBinContent(binX, Hist_Data.GetBinContent(binX)/Hist_Data.GetBinWidth(binX))
      Hist_Data.SetBinError(binX, 0) # only used in MC/data ratio
    else:
      Hist_Data.SetBinContent(binX, 0)
      Hist_Data.SetBinError(binX, 0)
  # Data with stat+syst errors
  for binX in range(1,Hist_Data_Syst.GetNbinsX()+1):
    if Hist_Data_Syst.GetBinWidth(binX)!=0:
      Hist_Data_Syst.SetBinContent(binX, Hist_Data_Syst.GetBinContent(binX)/Hist_Data_Syst.GetBinWidth(binX))
      Hist_Data_Syst.SetBinError(binX, Hist_Data_Syst.GetBinError(binX)/Hist_Data_Syst.GetBinWidth(binX))
    else:
      Hist_Data_Syst.SetBinContent(binX, 0)
      Hist_Data_Syst.SetBinError(binX, 0)

  # Prepare MC Signal histogram
  if Debug: print "DEBUG: Get MC Signal histogram"
  MCHists = []
  for key,DataPeriods in DataDict.iteritems():
    # Open input file
    FileName = '../Unfolding/Outputs/unfoldingOutputs_'+Date+'_data'+key+'.root'
    inFile   = ROOT.TFile.Open(FileName)
    if not inFile:
      print 'ERROR: input file not found, exiting'
      sys.exit(0)
    # Get Histogram
    HistName = 'Truth_'+finalState+'_'+obs.replace('_Ti1','') # FIXME
    hist = inFile.Get(HistName)
    if not hist:
      print "ERROR: "+HistName+" not found, exiting"
      sys.exit(0)
    hist.SetDirectory(0)
    inFile.Close()
    MCHists.append(hist)
  Hist_MC_Signal = MCHists[0]
  for ihist in range(1,len(MCHists)): Hist_MC_Signal.Add(MCHists[ihist])
  # Divid by bin width
  if Debug: print "DEBUG: (GetHist) divide by bin-width"
  for binX in range(1,Hist_MC_Signal.GetNbinsX()+1):
    if Hist_MC_Signal.GetBinWidth(binX)!=0:
      Hist_MC_Signal.SetBinContent(binX, Hist_MC_Signal.GetBinContent(binX)/Hist_MC_Signal.GetBinWidth(binX))
      Hist_MC_Signal.SetBinError(binX, Hist_MC_Signal.GetBinError(binX)/Hist_MC_Signal.GetBinWidth(binX))
    else:
      Hist_MC_Signal.SetBinContent(binX, 0)
      Hist_MC_Signal.SetBinError(binX, 0)
  Hist_MC_Signal.SetMarkerColor(ROOT.kRed+1)

  ########################
  # Make Plot
  ########################

  # TCanvas
  if Debug: print "DEBUG: Create TCanvas"
  Canvas  = ROOT.TCanvas()
  outName = "Plots/UnfoldedData/{0}_{1}_{2}.pdf".format(DataPeriod,finalState,obs)
  Canvas.Print(outName+"[")

  # TPad for upper panel
  if Debug: print "DEBUG: Create TPad for upper panel"
  pad1 = ROOT.TPad("pad1","pad1",0,0.4,1,1.0)
  pad1.SetTopMargin(0.08)
  pad1.SetBottomMargin(0.03)
  pad1.Draw()
  pad1.cd()

  # Set log scales (if requested)
  if Debug: print "DEBUG: Set log scales if requested"
  histnameBase = obs.replace('_Te1','')
  histnameBase = histnameBase.replace('_Ti2','')
  histnameBase = histnameBase.replace('_Ti1','')
  if histnameBase in Logx:
    pad1.SetLogx()
  if histnameBase in Logy:
    pad1.SetLogy()

  # Legends
  Legends = ROOT.TLegend(0.78,0.7,0.92,0.9)
  Legends.SetTextFont(42)

  Hist_Data_Syst.Draw("E3 P")
  Hist_Data_Syst.GetXaxis().SetLabelSize(0.)
  Hist_Data_Syst.GetXaxis().SetTitleSize(0.)
  Hist_Data_Syst.GetYaxis().SetTitleSize(20)
  Hist_Data_Syst.GetYaxis().SetTitleFont(43)
  Hist_Data_Syst.GetYaxis().SetLabelFont(43)
  Hist_Data_Syst.GetYaxis().SetLabelSize(19)
  Hist_Data_Syst.GetYaxis().SetTitleOffset(1.3)
  YaxisTitle  = XaxisUnfoldTitle[finalState+'_'+histnameBase]
  if '[GeV]' in YaxisTitle:
    YaxisTitle = YaxisTitle.replace('[GeV]','[pb/GeV]')
  else:
    YaxisTitle += ' [pb]'
  Hist_Data_Syst.GetYaxis().SetTitle("d#sigma/d"+YaxisTitle)
  Hist_Data_Syst.SetMinimum(1)
  Legends.AddEntry(Hist_Data_Syst,"Data","fp")

  Hist_MC_Signal.Draw("P")
  Legends.AddEntry(Hist_MC_Signal,"Sherpa","p")

  if Debug: print "DEBUG: Draw data histogram"
  Hist_Data_Syst.Draw("E3 P same")
  
  if XaxisRange.has_key(histnameBase):
    Hist_MC_Signal.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
    Hist_Data_Syst.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
  maxY = Hist_Data_Syst.GetMaximum()
  if Hist_MC_Signal.GetMaximum() > maxY: maxY = Hist_MC_Signal.GetMaximum()
  maxYscaling = 1E5
  if "absy" in obs or "phi" in obs or "eta" in obs or "deltaR" in obs or "deltaPhi" in obs: maxYscaling = 2E7
  Hist_MC_Signal.GetYaxis().SetRangeUser(1,maxY*maxYscaling)
  Hist_Data_Syst.GetYaxis().SetRangeUser(1,maxY*maxYscaling)

  Hist_MC_Signal.GetXaxis().SetLabelSize(0.)
  Hist_MC_Signal.GetXaxis().SetTitleSize(0.)
  Hist_MC_Signal.GetYaxis().SetTitleSize(20)
  Hist_MC_Signal.GetYaxis().SetTitleFont(43)
  Hist_MC_Signal.GetYaxis().SetLabelFont(43)
  Hist_MC_Signal.GetYaxis().SetLabelSize(19)
  Hist_MC_Signal.GetYaxis().SetTitleOffset(1.3)
  Hist_MC_Signal.GetYaxis().SetTitle("d#sigma/d"+YaxisTitle)

  if Debug: print "DEBUG: Draw legends"
  Legends.Draw("same")

  ROOT.gPad.RedrawAxis()

  # Show ATLAS legend
  if Debug: print "DEBUG: Show ATLAS legend"
  if ATLASlegend != 'NONE':
    if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
    elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
    elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
    else:
      print "ERROR: ATLASlegend not recognized, exiting"
      sys.exit(0)
    ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
    ATLASBlock.SetNDC()
    ATLASBlock.Draw("same")

  # Show CME and luminosity
  if Debug: print "DEBUG: Show CME and luminosity"
  CME = "#scale[1.5]{13 TeV, "+str(round(TotalLumi/1000,1))+" fb^{-1}}"
  topY = 0.7 if ATLASlegend != 'NONE' else 0.8
  CMEblock = ROOT.TLatex(0.2,topY,CME)
  CMEblock.SetNDC()
  CMEblock.Draw("same")

  # Show channel
  #if Debug: print "DEBUG: Show channel type"
  #channel = "#scale[1.5]{"
  #channel += "#mu" if Channel == "MU" else "e"
  #channel += " channel}"
  #topY = 0.6 if ATLASlegend != 'NONE' else 0.7
  #TextBlock = ROOT.TLatex(0.2,topY,channel)
  #TextBlock.SetNDC()
  #TextBlock.Draw("same")
  
  # TPad for bottom plot
  if Debug: print "DEBUG: Create TPad for bottom panel"
  Canvas.cd()
  pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.4)
  pad2.SetTopMargin(0.03)
  pad2.SetBottomMargin(0.32)
  pad2.Draw()
  pad2.cd()

  # Set log-y scale (if requested)
  if Debug: print "DEBUG: Set log-Y scale if requested"
  if histnameBase in Logx:
    pad2.SetLogx()

  # Create data/MC histogram
  if Debug: print "DEBUG: Create MC/data histogram"
  ratioHist = Hist_MC_Signal.Clone("ratioHist")
  ratioHist.SetLineColor(ROOT.kRed+1)
  ratioHist.SetMarkerColor(ROOT.kRed+1)
  ratioHist.Divide(Hist_Data)

  # Set y-axis range of ratio panel
  minY = -0.05
  maxY = 2.05
  ratioHist.SetMinimum(minY)
  ratioHist.SetMaximum(maxY)

  # Set x-axis range of ratio panel
  nbins = ratioHist.GetNbinsX()
  if histnameBase in XaxisRange:
    minX = XaxisRange[histnameBase][0]
    maxX = XaxisRange[histnameBase][1]
  else:
    minX = ratioHist.GetXaxis().GetBinLowEdge(1)
    maxX = ratioHist.GetXaxis().GetBinUpEdge(nbins)
  ratioHist.GetXaxis().SetRangeUser(minX,maxX)
 
  # Draw data/MC ratio
  if Debug: print "DEBUG: Draw MC/data ratio"
  ratioHist.Draw("e0")

  # Draw line at data/MC==1
  if Debug: print "DEBUG: Draw line at MC/data==1"
  Line = ROOT.TLine(minX,1,maxX,1)
  Line.SetLineStyle(7)
  Line.Draw("same")

  # Set x-axis title
  if Debug: print "DEBUG: Set X-axis title"
  histkey = finalState + '_' + histnameBase
  if XaxisUnfoldTitle.has_key(histkey):
    ratioHist.GetXaxis().SetTitleSize(20)
    ratioHist.GetXaxis().SetTitleFont(43)
    ratioHist.GetXaxis().SetLabelFont(43)
    ratioHist.GetXaxis().SetLabelSize(19)
    ratioHist.GetXaxis().SetTitleOffset(3)
    ratioHist.GetXaxis().SetTitle(XaxisUnfoldTitle[histkey])
    ratioHist.GetXaxis().SetNdivisions(510)

  ratioHist.GetYaxis().SetTitleSize(20)
  ratioHist.GetYaxis().SetTitleFont(43)
  ratioHist.GetYaxis().SetLabelFont(43)
  ratioHist.GetYaxis().SetLabelSize(19)
  ratioHist.GetYaxis().SetTitleOffset(1.3)
  ratioHist.GetYaxis().SetTitle("MC / Data")

  # Plot data errors
  Uncert = Hist_Data_Syst.Clone('Syst')
  for binX in range(1,Uncert.GetNbinsX()+1):
    if Uncert.GetBinContent(binX) != 0:
      Uncert.SetBinError(binX, Uncert.GetBinError(binX)/Uncert.GetBinContent(binX))
      Uncert.SetBinContent(binX, 1)
    else:
      Uncert.SetBinContent(binX, 0)
      Uncert.SetBinError(binX, 0)
  Uncert.SetMarkerColorAlpha(ROOT.kBlack, 0);
  Uncert.Draw("E3 same")

  # Save PDF
  if Debug: print "DEBUG: Save/print PDF"
  Canvas.Print(outName)
  Canvas.Print(outName+"]")

  ratioHist.Delete()
  Hist_Data.Delete()
  Hist_MC_Signal.Delete()

# Save systematic histograms into a ROOT file
outROOTFile = ROOT.TFile('Systematics/Systematics_'+DataPeriod+'_'+finalState+'.root','recreate')
outROOTFile.cd()
for key,hist in SystematicUncertainties.iteritems(): hist.Write()
outROOTFile.Close()

print '>>> DONE <<<'
