#!/usr/bin/env python

####################################################################
#                                                                  #
# Purpose: Compare systematic uncertainties                        #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

import ROOT,os,sys,resource,psutil,math

DataSamples = [
  '15',
  '16',
  '17',
  '18',
]

FinalStates = [
  'Zc',
]

# List of observables
Observables = [
  'HFjet0_pt_Ti1',
  'HFjet0_absy_Ti1',
  'Zpt_Ti1',
  'Zabsy_Ti1',
  'deltaYZjHF_Ti1',
  'deltaPhiZjHF_Ti1',
  'deltaRZjHF_Ti1',
]

Systematics = [
  'fit',       # flavour fit non-closure
  'unfolding', # unfolding method
  'lumi',      # luminosity
  'stat',      # luminosity
#  'SF',        # SF uncertainties
]

Debug = False

######################################################################
# DO NOT MODIFY
######################################################################

Colors = [
  ROOT.kBlue,
  ROOT.kRed+1,
  ROOT.kGreen+2,
  ROOT.kCyan,
  ROOT.kOrange,
  ROOT.kMagenta,
  ROOT.kYellow+1,
  ROOT.kMagenta+2,
  ROOT.kRed-9,
]

from Style import *

# Luminosity
from Luminosities import *

# Import SF variations from Reader
PATH = "../"
sys.path.insert(1, PATH) # insert at 1, 0 is the script path
from Systematics import *

# Total luminosity and needed campaigns
TotalLumi  = 0 # total luminosity
DataPeriod = 'data'
for sample in DataSamples:
  if "15" in sample:
    TotalLumi += Luminosity_2015
  elif "16" in sample:
    TotalLumi += Luminosity_2016
  elif "17" in sample:
    TotalLumi += Luminosity_2017
  elif "18" in sample:
    TotalLumi += Luminosity_2018
  DataPeriod += sample

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

os.system('mkdir Plots')
os.system('mkdir Plots/Systematics')

# Loop over final states
for case in FinalStates:
  # Open File
  inFileName = 'Systematics/Systematics_'+DataPeriod+'_'+case+'.root'
  inFile     = ROOT.TFile.Open(inFileName)
  if not inFile:
    print 'ERROR: '+inFileName+' not found, exiting'
    sys.exit(0)
  # Loop over observables
  for obs in Observables:
    Hists = dict()
    # Loop over systematics
    counter = 0
    for source in Systematics:
      if source != 'SF':
        hist = inFile.Get(obs+'_'+source)
        if not hist:
          print 'ERROR: '+obs+'_'+source+' not found, exiting'
          sys.exit(0)
        hist.SetLineColor(Colors[counter])
        Hists[source] = hist
        counter += 1
      else: # SF
        for SFsource in SFsysts: # loop over sources of SF uncertainties
          if 'EL' in SFsource: continue # FIXME Temporary (only MU channel implemented)
          hist = inFile.Get(obs+'_'+SFsource)
          if not hist:
            print 'ERROR: '+obs+'_'+SFsource+' not found, exiting'
            sys.exit(0)
          hist.SetLineColor(Colors[counter])
          Hists[SFsource] = hist
          counter += 1

    # Calculate total uncertainty
    Htotal = Hists['stat'].Clone('total')
    for ibin in range(1,Htotal.GetNbinsX()+1): # loop over bins
      totalErr = 0
      for key,hist in Hists.iteritems(): # loop over uncertainties
        totalErr += hist.GetBinContent(ibin) * hist.GetBinContent(ibin)
      totalErr = math.sqrt(totalErr)
      Htotal.SetBinContent(ibin,totalErr)
    Htotal.SetLineColor(ROOT.kBlack)
    Hists['total'] = Htotal

    # TCanvas
    if Debug: print "DEBUG: Create TCanvas"
    Canvas  = ROOT.TCanvas()
    outName = "Plots/Systematics/Systematics_{0}_{1}_{2}.pdf".format(DataPeriod,case,obs)
    Canvas.Print(outName+"[")

    # Add histograms to a THStack
    if Debug: print "DEBUG: Add histograms to a THStack"
    Stack = ROOT.THStack()
    for key,hist in Hists.iteritems():
      Stack.Add(hist,"HIST")

    # Create TLegend
    if Debug: print "DEBUG: Create a TLegend"
    #Legends = ROOT.TLegend(0.2,0.6,0.4,0.9)
    Legends = ROOT.TLegend(0.7,0.2,0.9,0.4)
    Legends.SetTextFont(42)
    for key,hist in Hists.iteritems():
      if key == 'fit':         legend = 'Non-closure fit'
      elif key == 'unfolding': legend = 'Unfolding'
      elif key == 'lumi':      legend = 'Luminosity'
      elif key == 'stat':      legend = 'Statistical'
      else: legend = key
      Legends.AddEntry(hist,legend,"l")

    if Debug: print "DEBUG: Draw THStack"
    Stack.Draw("nostack")

    obsbase = obs.replace('_Ti1','')

    # Set x-axis title
    if Debug: print "DEBUG: Set X-axis title"
    Stack.GetXaxis().SetTitleSize(20)
    Stack.GetXaxis().SetTitleFont(43)
    Stack.GetXaxis().SetLabelFont(43)
    Stack.GetXaxis().SetLabelSize(19)
    Stack.GetXaxis().SetTitle(XaxisTitles[obsbase])
    Stack.GetXaxis().SetNdivisions(510)
    
    Stack.GetYaxis().SetTitleSize(20)
    Stack.GetYaxis().SetTitleFont(43)
    Stack.GetYaxis().SetLabelFont(43)
    Stack.GetYaxis().SetLabelSize(19)
    Stack.GetYaxis().SetTitleOffset(1.3)
    Stack.GetYaxis().SetTitle("Relative uncertainty")

    if obsbase in XaxisRange:
      Stack.GetXaxis().SetRangeUser(XaxisRange[obsbase][0],XaxisRange[obsbase][1])


    if Debug: print "DEBUG: Draw legends"
    Legends.Draw("same")
    
    ROOT.gPad.RedrawAxis()
    
    # Save PDF
    if Debug: print "DEBUG: Save/print PDF"
    Canvas.Print(outName)
    Canvas.Print(outName+"]")

print ">>> ALL DONE <<<"
