#!/usr/bin/env python

##############################################################################
#                                                                            #
# Purpose: Compare unfolded data vs data to obtain the unfolding factor      #
# Authors: Luis Pinto (lpintoca@cern.ch ) and Jona Bossio (jbossios@cern.ch) #
#                                                                            #
##############################################################################

import ROOT,os,sys,resource,psutil,argparse,math
from  InputFiles             import *
from  PlotterHelperFunctions import *
from  Style                  import *

# List of data samples
Samples = [
  'data15',
  'data16',
]

FinalStates = ['Zc']

# Date of unfolding inputs
Date = '03072020'

# List of observables
Observables = [
  'HFjet0_pt_Ti1',
  'HFjet0_absy_Ti1',
  'Zpt_Ti1',
  'Zabsy_Ti1',
  'deltaYZjHF_Ti1',
  'deltaPhiZjHF_Ti1',
  'deltaRZjHF_Ti1',
]

ATLASlegend = 'NONE' # Options: Internal, NONE, Preliminary, ATLAS
Debug = False

################################################################################
# Do not modify
################################################################################

os.system('mkdir Plots/UnfoldingCorrections')

DataPeriod         = 'data'
DataYears          = []
for sample in Samples:
  if "data15" in sample:
    DataPeriod += '15'
    DataYears.append('15')
  elif "data16" in sample:
    DataPeriod += '16'
    DataYears.append('16')
  elif "data17" in sample:
    DataPeriod += '17'
    DataYears.append('17')
  elif "data18" in sample:
    DataPeriod += '18'
    DataYears.append('18')

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

# Open input file
FileName = '../Unfolding/Outputs/unfoldingOutputs_'+Date+'_'+DataPeriod+'.root'
inFile   = ROOT.TFile.Open(FileName)
if not inFile:
  print 'ERROR: input file not found, exiting'
  sys.exit(0)

# Loop over histograms
Cases = [(x,y) for x in FinalStates for y in Observables]
for case in Cases:
  finalState = case[0] 
  obs        = case[1]
  if Debug: print "###########################################################"
  if Debug: print "DEBUG: Producing PDF for '"+obs+"' observable"

  ############################
  # Get Histograms
  ############################

  # Unfolded data histogram
  if Debug: print "DEBUG: Get Unfolded Data histogram"
  Hist_Unfold_Data = inFile.Get('UnfoldedData_'+finalState+'_'+obs+'_Bayes_iterations_4')
  if not Hist_Unfold_Data:
    print "ERROR: "+"UnfoldedData_"+finalState+"_"+obs+"_Bayes_iterations_4 not found, exiting"
    sys.exit(0)
  if Debug: print "DEBUG: Unfolded Data histogram retrieved"
  if Debug: print "DEBUG: Set line and fill color for data histogram"
  # Hist_Unfold_Data.SetLineColor(ROOT.kBlack)
  # Hist_Unfold_Data.SetFillColor(ROOT.kBlack)

  # Data histogram
  if Debug: print "DEBUG: Get Data histogram"
  Hist_Data = inFile.Get('Data_'+finalState+'_'+obs)
  if not Hist_Data:
    print "ERROR: Data_"+finalState+"_"+obs+" not found, exiting"
    sys.exit(0)
  # Hist_Data.SetLineColor(ROOT.kRed+1)
  # Hist_Data.SetFillColor(ROOT.kRed+1)

  ########################
  # Make Plot
  ########################

  # TCanvas
  if Debug: print "DEBUG: Create TCanvas"
  Canvas  = ROOT.TCanvas()
  outName = "Plots/UnfoldingCorrections/unfold_factor_{0}_{1}_{2}.pdf".format(DataPeriod,finalState,obs)
  Canvas.Print(outName+"[")

  # Set log scales (if requested)
  if Debug: print "DEBUG: Set log scales if requested"
  histnameBase = obs.replace('_Te1','')
  histnameBase = histnameBase.replace('_Ti2','')
  histnameBase = histnameBase.replace('_Ti1','')
  if histnameBase in Logx:
    Canvas.SetLogx()
  #if histnameBase in Logy:
  #  Canvas.SetLogy()

  # Add histograms to THStack and draw legends
  #Legends = ROOT.TLegend(0.7,0.43,0.92,0.9)
  Legends = ROOT.TLegend(0.8,0.7,0.92,0.9)
  Legends.SetTextFont(42)

  # Create histogram
  Hist = Hist_Data.Clone("UnfoldedDataVsData")

  # Loop over bins and fill
  nbins = Hist.GetNbinsX()
  for ibin in range(1, nbins+1):
    data = Hist_Data.GetBinContent(ibin)
    unfold_data = Hist_Unfold_Data.GetBinContent(ibin)
    if data > 0 and unfold_data > 0:
      Hist.SetBinContent(ibin, unfold_data/data)
    else:
      Hist.SetBinContent(ibin, 0)
    Hist.SetBinError(ibin, 0)

  # Draw histogram
  Hist.Draw("pe0")
  
  if XaxisRange.has_key(histnameBase):
    Hist.GetXaxis().SetRangeUser(XaxisRange[histnameBase][0],XaxisRange[histnameBase][1])
  maxY = Hist.GetMaximum()
  minY = Hist.GetMinimum()
  Hist.GetYaxis().SetRangeUser(math.floor(minY),math.ceil(maxY))

  #Hist.GetXaxis().SetLabelSize(0.)
  #Hist.GetXaxis().SetTitleSize(0.)
  Hist.GetYaxis().SetTitleSize(20)
  Hist.GetYaxis().SetTitleFont(43)
  Hist.GetYaxis().SetLabelFont(43)
  Hist.GetYaxis().SetLabelSize(19)
  Hist.GetYaxis().SetTitleOffset(1.3)
  Hist.GetYaxis().SetTitle("#it{C}")
  
  if XaxisTitles.has_key(histnameBase):
    Hist.GetXaxis().SetTitleSize(20)
    Hist.GetXaxis().SetTitleFont(43)
    Hist.GetXaxis().SetLabelFont(43)
    Hist.GetXaxis().SetLabelSize(19)
    Hist.GetXaxis().SetTitleOffset(1.3)
    Hist.GetXaxis().SetTitle(XaxisTitles[histnameBase])
    Hist.GetXaxis().SetNdivisions(510)


  if Debug: print "DEBUG: Draw legends"
  Legends.Draw("same")

  ROOT.gPad.RedrawAxis()

  # Show ATLAS legend
  if Debug: print "DEBUG: Show ATLAS legend"
  if ATLASlegend != 'NONE':
    if   ATLASlegend == "Internal":    atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Internal}}}";
    elif ATLASlegend == "Preliminary": atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
    elif ATLASlegend == "ATLAS":       atlas = "#scale[1.3]{#scale[1.4]{#font[72]{ATLAS}}}";
    else:
      print "ERROR: ATLASlegend not recognized, exiting"
      sys.exit(0)
    ATLASBlock = ROOT.TLatex(0.2,0.8,atlas)
    ATLASBlock.SetNDC()
    ATLASBlock.Draw("same")

  # Show CME and luminosity
  if Debug: print "DEBUG: Show data years"
  if len(DataYears) == 1:
    years = "#scale[1.5]{Data "+DataYears[0]+"}"
  else:
    years = "#scale[1.5]{Data "+DataYears[0]
    for x in range(1,len(DataYears)):
      years+= '+'+DataYears[x]
    years+="}"
  topY = 0.7 if ATLASlegend != 'NONE' else 0.8
  yearsBlock = ROOT.TLatex(0.2,topY,years)
  yearsBlock.SetNDC()
  yearsBlock.Draw("same")

  # Save PDF
  if Debug: print "DEBUG: Save/print PDF"
  Canvas.Print(outName)
  Canvas.Print(outName+"]")

  Hist.Delete()

print '>>> DONE <<<'
