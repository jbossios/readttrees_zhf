#!/usr/bin/env python

NominalSignalGenerator = 'SH2211' # options: SH2211 and MG

####################################################################
#                                                                  #
# Purpose: Compare distributions between data and MC (signal+bkgs) #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

import ROOT,os,sys,resource,psutil,argparse

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--channel',   action='store',      dest="channel")
parser.add_argument('--selection', action='store',      dest="selection")
parser.add_argument('--tagger',    action='store',      dest="tagger")
parser.add_argument('--samples',   action='store',      dest="samples")
parser.add_argument('--debug',     action='store_true', dest="debug", default=False)
args = parser.parse_args()
# Protections
if args.channel is None:
  print 'ERROR: channel not provided, exiting'
  sys.exit(0)
if args.selection is None:
  print 'ERROR: selection not provided, exiting'
  sys.exit(0)
if args.tagger is None:
  print 'ERROR: tagger not provided, exiting'
  sys.exit(0)
if args.samples is None:
  print 'ERROR: You have selected no sample, exiting'
  sys.exit(0)
Debug       = args.debug
Channel     = args.channel
Selection   = args.selection
Tagger      = args.tagger
Samples     = args.samples.split(',')

from InputFiles             import *
from PlotterHelperFunctions import *
from Style                  import *

# Protections
if "Signal_data15" not in Samples and "Signal_data16" not in Samples and "Signal_data17" not in Samples and "Signal_data18" not in Samples:
  print "ERROR: At least one signal_data sample needs to be provided, exiting"
  sys.exit(0)
NominalSignalGenerator_found = False
for sample in Samples:
  if NominalSignalGenerator in sample: NominalSignalGenerator_found = True
if not NominalSignalGenerator_found:
  FATAL('{} is missing in the list of input MC signal samples, exiting'.format(NominalSignalGenerator))
#if "Signal_MC" not in Samples:
#  print "ERROR: Signal_MC is missing in Samples, exiting"
#  sys.exit(0)

# Luminosity
from Luminosities import *

# Total luminosity and needed campaigns
Luminosity         = dict() # collect luminosity for each MC campaign
Luminosity["MC16a"]= 0
Luminosity["MC16d"]= 0
Luminosity["MC16e"]= 0
Campaigns          = []
DataSamples        = [] # List of data samples
MCSignalSamples    = [] # List of MC signal samples
BackgroundSamples  = dict() # List of MC Background samples for each background type
for sample in Samples:
  if "data15" in sample:
    Luminosity["MC16a"] += Luminosity_2015
    Campaigns.append("MC16a")
    MCSignalSamples.append("Signal"+NominalSignalGenerator+"_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data16" in sample:
    Luminosity["MC16a"] += Luminosity_2016
    if "MC16a" not in Campaigns:
      Campaigns.append("MC16a")
      MCSignalSamples.append("Signal"+NominalSignalGenerator+"_MC16a_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data17" in sample:
    Luminosity["MC16d"] += Luminosity_2017
    Campaigns.append("MC16d")
    MCSignalSamples.append("Signal"+NominalSignalGenerator+"_MC16d_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
  elif "data18" in sample:
    Luminosity["MC16e"] += Luminosity_2018
    Campaigns.append("MC16e")
    MCSignalSamples.append("Signal"+NominalSignalGenerator+"_MC16e_"+Channel+"_"+Selection+"_"+Tagger)
    DataSamples.append(sample+"_"+Channel+"_"+Selection+"_"+Tagger)
for sample in Samples:
  if "Signal" not in sample:
    BackgroundSamples[sample] = []
    for campaign in Campaigns:
      BackgroundSamples[sample].append(sample+"_"+campaign+"_"+Channel+"_"+Selection+"_"+Tagger)

TotalLumi = 0
for key,lumi in Luminosity.iteritems():
  TotalLumi += lumi

if Debug: print "DEBUG: total luminosity = "+str(TotalLumi)

Campaign   = "MC16"
for campaign in Campaigns:
  if Campaign != "MC16": Campaign += "+"
  if campaign == "MC16a": Campaign += "a"
  elif campaign == "MC16d": Campaign += "d"
  elif campaign == "MC16e": Campaign += "e"

# AtlasStyle
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

###################################################
# Produce b-tagging discriminant plot in MC Z+jets
###################################################
if Debug: print "DEBUG: Produce BTagWeight MC Z+jet only plot"

# Get Z + at least one b
if Debug: print "DEBUG: Get Z+b plot"
Hist_BTagWeight_Zb,msg = GetTotalHist(MCSignalSamples,"FlavA1B_"+Tagger+"Weight",Debug,Luminosity)
if msg != 'OK':
  print msg
  print "FlavA1B_"+Tagger+"Weight not found, exiting"
  sys.exit(0)
Hist_BTagWeight_Zb.SetLineColor(ROOT.kRed+1)

# Get Z + at least one c
if Debug: print "DEBUG: Get Z+c plot"
Hist_BTagWeight_Zc,msg = GetTotalHist(MCSignalSamples,"FlavA1C_"+Tagger+"Weight",Debug,Luminosity)
if msg != 'OK':
  print msg
  print "FlavA1C_"+Tagger+"Weight not found, exiting"
  sys.exit(0)
Hist_BTagWeight_Zc.SetLineColor(ROOT.kBlue)

# Get Z + light
if Debug: print "DEBUG: Get Z+L plot"
Hist_BTagWeight_ZL,msg = GetTotalHist(MCSignalSamples,"FlavL_"+Tagger+"Weight",Debug,Luminosity)
if msg != 'OK':
  print msg
  print "FlavL_"+Tagger+"Weight not found, exiting"
  sys.exit(0)
Hist_BTagWeight_ZL.SetLineColor(ROOT.kBlack)

# TCanvas
if Debug: print "DEBUG: Create TCanvas"
Canvas  = ROOT.TCanvas()
outName = "Plots/{0}/{1}/{2}/{3}_{4}_{5}_{6}.pdf".format(Selection,Tagger,Campaign,Channel,Selection,Tagger,"BTagWeight_MCZjetOnly")
Canvas.Print(outName+"[")

# Add histograms to a THStack
if Debug: print "DEBUG: Add histograms to a THStack"
Stack = ROOT.THStack()
Stack.Add(Hist_BTagWeight_Zb,"HIST")
Stack.Add(Hist_BTagWeight_Zc,"HIST")
Stack.Add(Hist_BTagWeight_ZL,"HIST")

# Create TLegend
if Debug: print "DEBUG: Create a TLegend"
Legends = ROOT.TLegend(0.2,0.6,0.4,0.9)
Legends.SetTextFont(42)
Legends.AddEntry(Hist_BTagWeight_Zb,"Z+b "+Campaign,"l")
Legends.AddEntry(Hist_BTagWeight_Zc,"Z+c "+Campaign,"l")
Legends.AddEntry(Hist_BTagWeight_ZL,"Z+L "+Campaign,"l")

if Debug: print "DEBUG: Draw THStack"
Stack.Draw("nostack")

# Set x-axis title
if Debug: print "DEBUG: Set X-axis title"
Stack.GetXaxis().SetTitleSize(20)
Stack.GetXaxis().SetTitleFont(43)
Stack.GetXaxis().SetLabelFont(43)
Stack.GetXaxis().SetLabelSize(19)
Stack.GetXaxis().SetTitle("b-tagging discriminant")
Stack.GetXaxis().SetNdivisions(510)

Stack.GetYaxis().SetTitleSize(20)
Stack.GetYaxis().SetTitleFont(43)
Stack.GetYaxis().SetLabelFont(43)
Stack.GetYaxis().SetLabelSize(19)
Stack.GetYaxis().SetTitleOffset(1.3)
Stack.GetYaxis().SetTitle("Events / bin-width")

if Debug: print "DEBUG: Draw legends"
Legends.Draw("same")

ROOT.gPad.RedrawAxis()

# Show channel
if Debug: print "DEBUG: Show channel type"
channel = "#mu" if Channel == "MU" else "e"
channel += " channel"
TextBlock = ROOT.TLatex(0.2,0.55,channel)
TextBlock.SetNDC()
TextBlock.Draw("same")

# Save PDF
if Debug: print "DEBUG: Save/print PDF"
Canvas.Print(outName)
Canvas.Print(outName+"]")

#################################################################################################
# Compare fractions of mixed (b+c) flavour cases on the b-tagging discriminant plot in MC Z+jets
#################################################################################################

#DenominatorFlavour = 'FlavA1B_'

#MixedFlavours = [
#  'FlavMixedE1B1C_',
#  'FlavMixedE1B2C_',
#  'FlavMixedE2B1C_',
#  'FlavMixedE2B2C_',
#  'FlavMixedE1BM2C_',
#  'FlavMixedE2BM2C_',
#  'FlavMixedM2BE1C_',
#  'FlavMixedM2BE2C_',
#]

#Colors.append(ROOT.kBlack)
#Colors.append(ROOT.kBlue)
#Colors.append(ROOT.kRed+1)

#if Debug: print "DEBUG: Compare mixed flavour fractions in MC Z+jets"

# Get total number of events
#if Debug: print "DEBUG: Get hist for "+DenominatorFlavour+" flavour"
#TotalHist,msg = GetTotalHist(MCSignalSamples,DenominatorFlavour+Tagger+"Weight",Debug,Luminosity)
#if msg != 'OK':
#  print msg
#  sys.exit(0)

# TCanvas
#if Debug: print "DEBUG: Create TCanvas"
#Canvas  = ROOT.TCanvas()
#outName = "Plots/{0}/{1}/{2}_{3}_{4}_{5}.pdf".format(Selection,Campaign,Channel,Selection,Tagger,"BTagWeight_MixedFlavourFractions_MCZjetOnly")
#Canvas.Print(outName+"[")

# Add histograms to a THStack
#if Debug: print "DEBUG: Add histograms to a THStack"
#Stack = ROOT.THStack()

# Create TLegend
#if Debug: print "DEBUG: Create a TLegend"
#Legends = ROOT.TLegend(0.2,0.6,0.4,0.9)
#Legends.SetTextFont(42)

## Loop over mixed flavours
#counter = 0
#for flav in MixedFlavours:
#  if Debug: print "DEBUG: Get hist for "+flav+" flavour"
#  Hist,msg = GetTotalHist(MCSignalSamples,flav+Tagger+"Weight",Debug,Luminosity)
#  if msg != 'OK':
#    print msg
#    sys.exit(0)
#  Hist.Divide(TotalHist)
#  Hist.SetLineColor(Colors[counter])
#  Hist.GetXaxis().SetTitle("b-tagging discriminant")
#  Stack.Add(Hist,"HIST")
#  Legends.AddEntry(Hist,flav[:-1],"l")
#  counter += 1

#if Debug: print "DEBUG: Draw THStack"
#Stack.Draw("nostack")

# Set x-axis title
#if Debug: print "DEBUG: Set X-axis title"
#Stack.GetXaxis().SetTitleSize(20)
#Stack.GetXaxis().SetTitleFont(43)
#Stack.GetXaxis().SetLabelFont(43)
#Stack.GetXaxis().SetLabelSize(19)
#Stack.GetXaxis().SetTitle("b-tagging discriminant")
#Stack.GetXaxis().SetNdivisions(510)

#Stack.GetYaxis().SetTitleSize(20)
#Stack.GetYaxis().SetTitleFont(43)
#Stack.GetYaxis().SetLabelFont(43)
#Stack.GetYaxis().SetLabelSize(19)
#Stack.GetYaxis().SetTitleOffset(1.3)
#Stack.GetYaxis().SetTitle("Fraction")

#if Debug: print "DEBUG: Draw legends"
#Legends.Draw("same")

#ROOT.gPad.RedrawAxis()

# Show channel
#if Debug: print "DEBUG: Show channel type"
#channel = "#mu" if Channel == "MU" else "e"
#channel += " channel"
#TextBlock = ROOT.TLatex(0.2,0.55,channel)
#TextBlock.SetNDC()
#TextBlock.Draw("same")

# Save PDF
#if Debug: print "DEBUG: Save/print PDF"
#Canvas.Print(outName)
#Canvas.Print(outName+"]")

#################################################################################################
# Truth and reco cutflow
#################################################################################################

if Debug: print "DEBUG: Produce truth and reco cutflow plots"

Types = ["reco","truth"]

for Type in Types:

  histname = "cutflow_" + Type

  # Get total number of events
  if Debug: print "DEBUG: Get "+histname+" hist"
  Hist,msg = GetTotalHist(MCSignalSamples,histname,Debug,Luminosity)
  if msg != 'OK':
    print msg
    print histname+" not found, exiting"
    sys.exit(0)
  
  # TCanvas
  if Debug: print "DEBUG: Create TCanvas"
  Canvas  = ROOT.TCanvas()
  outName = "Plots/{0}/{1}/{2}/{3}_{4}_{5}_{6}.pdf".format(Selection,Tagger,Campaign,Channel,Selection,Tagger,"Cutflow_"+Type+"_MCZjetOnly")
  Canvas.Print(outName+"[")

  # Compute acceptance for each bin
  nbins = Hist.GetNbinsX()
  for ibin in range(2,nbins+1):
    BinContent = Hist.GetBinContent(ibin)
    if BinContent !=0: 
      acceptance = BinContent/Hist.GetBinContent(1)
      Hist.SetBinContent(ibin,acceptance)
      Hist.SetBinError(ibin,0)
  Hist.SetBinContent(1,1)
  Hist.SetBinError(1,0)
  
  Hist.SetLineColor(ROOT.kBlue)
  Hist.Draw("")
  
  Hist.GetYaxis().SetTitleSize(20)
  Hist.GetYaxis().SetTitleFont(43)
  Hist.GetYaxis().SetLabelFont(43)
  Hist.GetYaxis().SetLabelSize(19)
  Hist.GetYaxis().SetTitleOffset(1.3)
  Hist.GetYaxis().SetTitle("Acceptance")
  
  ROOT.gPad.RedrawAxis()
  
  # Show channel
  if Debug: print "DEBUG: Show channel type"
  channel = "#mu" if Channel == "MU" else "e"
  channel += " channel"
  TextBlock = ROOT.TLatex(0.7,0.8,channel)
  TextBlock.SetNDC()
  TextBlock.Draw("same")

  # Show final acceptance
  if Debug: print "DEBUG: Show acceptance"
  Str = "Acceptance ("+Type+"): "+str(round(acceptance,2))
  TextBlock2 = ROOT.TLatex(0.55,0.85,Str)
  TextBlock2.SetNDC()
  TextBlock2.Draw("same")
  
  # Save PDF
  if Debug: print "DEBUG: Save/print PDF"
  Canvas.Print(outName)
  Canvas.Print(outName+"]")

print ">>> ALL DONE <<<"
