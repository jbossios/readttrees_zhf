
####################################################################
#                                                                  #
# Purpose: Derive systematics from non-SF-based sources            #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

Campaign = 'MC16a' # options: ['MC16a','MC16d','MC16e','MC16all']

Samples = [
  'ZmumuSH2211',
#  'ZeeSH2211',
#  'Top',
#  'TTbarNonAllHad',
#  'Diboson',
#  'Ztautau',
#  'Wjets',
#  'VH',
#  'Wtaunu',
#  'ZmumuMG',
#  'ZeeMG',
]

Channel      = "MU"
Selection    = "SR"
Tagger       = "DL1r"
ATLASlegend  = "Internal" # Options: Internal, Preliminary, ATLAS and NONE
OutputFormat = 'PDF' # Options: PDF, PNG, JPG

# TODO
# Besides comparing quadrature sum of main sources (JET,MUON,EG,MET).
# Also make a plot comparing each component contributing to each source (i.e. 4 plots)

###########################################################################
# DO NOT MODIFY
###########################################################################

import copy
import os
import sys
import math

# Sources of systematics
Sources = ['EG', 'MUON', 'JET', 'MET']

# Get full list of campaigns
Campaigns = [Campaign] if Campaign != 'MC16all' else ['MC16a','MC16d','MC16e']

# Define final list of samples (one set for each campaign)
FullSamples = []
for sample in Samples:
  for campaign in Campaigns:
    if 'Zmumu' in sample or 'Zee' in sample: # signal sample
      Generator = sample.split('Zmumu')[1] if 'Zmumu' in sample else sample.split('Zee')[1]
      FullSamples.append('Signal{}_{}_{}_{}'.format(Generator,campaign,Channel,Selection))
    else: # background sample
      FullSamples.append('{}_{}_{}_{}'.format(sample,campaign,Channel,Selection))

from InputFiles             import *
from PlotterHelperFunctions import *
from ROOT                   import *

# AtlasStyle
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()
gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *

# Get TTree names
from SystematicTTrees import *
TTreeNames = SystTTrees

# Output format
Format = 'pdf' if OutputFormat == 'PDF' else 'png'

# Colors
#Colors = [i for i in range(1,200)]
Colors = [kBlack,kRed+1,kBlue,kGreen,kMagenta,kCyan,kOrange,kOrange+10]

# Loop over Fullsamples
for sample in FullSamples:
  campaign = ''
  if 'MC16a'   in sample: campaign = 'MC16a'
  elif 'MC16d' in sample: campaign = 'MC16d'
  elif 'MC16e' in sample: campaign = 'MC16e'
  # Create output folder
  os.system('mkdir -p Plots/{0}/{1}/{2}/Uncertainties/'.format(Selection, Tagger, campaign))
  # Loop over observables
  for obs in Observables:
    # Loop over cases
    for case in ['Ti1', 'Ti2']:
      if case == 'Ti1' and 'Te1' not in Observables[obs]: continue # Ti1 is not possible
      histname = '{}_{}'.format(obs,case)
      # TCanvas
      Canvas  = TCanvas()
      outName = "Plots/{0}/{1}/{2}/Uncertainties/Uncertainties_{3}_{4}_{5}.{6}".format(Selection, Tagger, campaign, Channel, Selection, histname, Format)
      Canvas.Print(outName+"[")
      # TLegend
      Legends = TLegend(0.7,0.43,0.92,0.9)
      Legends.SetTextFont(42)
      # THStack
      Stack = THStack()
      # Get nominal histogram
      key                  = sample + '_' + Tagger
      NominalInputFileName = InputFiles[key]
      NominalFile          = TFile.Open(NominalInputFileName)
      CHECKObj(NominalFile,'{} not found, exiting'.format(NominalInputFileName))
      NominalHist          = NominalFile.Get('{}'.format(histname))
      CHECKObj(NominalHist,'{} not found in {}, exiting'.format(histname,NominalInputFileName))
      NominalHist.SetDirectory(0)
      NominalFile.Close()
      # Collect quadrature sum of sources
      Hists = dict()
      for source in Sources:
        Hists[source] = dict()
        for sign in ['UP', 'DOWN']:
          Hists[source][sign] = NominalHist.Clone('Total_{}_{}'.format(source,sign))
      # Loop over TTrees
      #counter = 0
      sumUP = { source : {xbin : 0 for xbin in range(1, NominalHist.GetNbinsX()+1)} for source in Sources}
      sumDOWN = { source : {xbin : 0 for xbin in range(1, NominalHist.GetNbinsX()+1)} for source in Sources}
      for TTreeName in TTreeNames:
        # Get variated histogram
        #InputFileName = InputFiles[key].replace('_nominalOnly','_{}'.format(TTreeName))
        InputFileName = InputFiles[key+'_FullSysts']
        File          = TFile.Open(InputFileName)
        CHECKObj(File,'{} not found, exiting'.format(InputFileName))
        Hist          = File.Get('{}'.format(TTreeName+'/'+histname))
        CHECKObj(Hist,'{} not found in {}, exiting'.format(TTreeName+'/'+histname,InputFileName))
        Hist.SetDirectory(0)
        File.Close()
        # Derive uncertainty
        SystHist = Hist.Clone('{}_{}'.format(histname,TTreeName))
        SystHist.Add(NominalHist,-1)
        SystHist.Divide(NominalHist)
	#SystHist.SetLineColor(Colors[counter])
	# Add to appropriate sum
        for source in Sources:
          if source in TTreeName:
            ID = source
            break
        for xbin in range(1, SystHist.GetNbinsX() + 1): # loop over bins
          uncert = SystHist.GetBinContent(xbin)
          if uncert >= 0:
            sumUP[ID][xbin] += uncert * uncert
          else:
            sumDOWN[ID][xbin] += uncert * uncert
	#Stack.Add(SystHist,'HIST')
        #counter += 1
      # Fill histograms with quadrature sums
      counter = 0
      for source in Sources:
        for xbin in range(1,Hists[source]['UP'].GetNbinsX()+1):
          Hists[source]['UP'].SetBinContent(xbin,math.sqrt(sumUP[source][xbin]))
          Hists[source]['DOWN'].SetBinContent(xbin,math.sqrt(sumDOWN[source][xbin]))
        Hists[source]['UP'].SetLineColor(Colors[counter])
        Stack.Add(Hists[source]['UP'],'HIST')
        Legends.AddEntry(Hists[source]['UP'],'{} UP'.format(source),'l')
        counter += 1
        Hists[source]['DOWN'].SetLineColor(Colors[counter])
        Stack.Add(Hists[source]['DOWN'],'HIST')
        Legends.AddEntry(Hists[source]['DOWN'],'{} DOWN'.format(source),'l')
        counter += 1
      Stack.Draw('nostack')
      Stack.GetYaxis().SetTitle('Systematic uncertainty')
      Stack.SetMinimum(0) # Temporary
      Stack.SetMaximum(0.5) # Temporary
      if XaxisTitles.has_key(obs):
        if Channel == 'EL' and obs == 'Zpt':
          Stack.GetXaxis().SetTitle("#it{p}_{T} (e^{+}e^{-}) [GeV]")
        else:
          Stack.GetXaxis().SetTitle(XaxisTitles[obs])
      Legends.Draw('same')
      Canvas.Print(outName)
      Canvas.Print(outName+']')

print('>>> ALL DONE <<<')
