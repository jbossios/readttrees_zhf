
####################################################################
#                                                                  #
# Purpose: Run Data_vs_MC.py and MCSignalplots.py                  #
# Author : Jona Bossio (jbossios@cern.ch)                          #
#                                                                  #
####################################################################

DataSamples = [
  'Signal_data15',
  'Signal_data16',
  'Signal_data17',
  'Signal_data18',
]

AlternativeSignalMCs = [  # Sherpa 2.2.11 (SH2211) will be considered nominal and is always added
#  'MG',     # MadGraph (only for SR)
  'FxFx',   # MadGraph FxFx (only for SR)
#  'SH221',  # Sherpa 2.2.1 (only for SR)
#  'SH2210', # Sherpa 2.2.10 (only for SR)
]

Channel      = "MU" # options: EL, MU
Selection    = "SR" # options: SR, SRa1jet
Tagger       = "DL1r"
ATLASlegend  = "Internal" # Options: Internal, Preliminary, ATLAS and NONE
OutputFormat = 'PDF' # Options: PDF, PNG, JPG

###########################################################################
# DO NOT MODIFY
###########################################################################

# Imports
from InputFiles             import *
from ROOT                   import *
from PlotterHelperFunctions import *
import os,sys

# Protections
if Channel != 'EL' and Channel != 'MU':
  FATAL('Channel ({}) not recognize, exiting'.format(Channel))
if Selection != 'SR' and Selection != 'SRa1jet':
  FATAL('Selection ({}) not recognized, exiting'.format(Selection))

Backgrounds= {
  'EL' : [
    'Top',
    'Diboson',
    'Ztautau',
    'VH',
  ],
  'MU' : [
    'Top',
    'Diboson',
    'Ztautau',
    'VH',
  ],
}[Channel]

#######################################
# Add Ti1 distributions to input files
#######################################

# Make full list of samples
import copy
ExtendedSamples = copy.deepcopy(DataSamples)
# MC samples
for sample in DataSamples: # loop over data-taking years
  if '15' in sample or '16' in sample:
    # Add Signal MCs
    signalSample = 'SignalSH2211_MC16a'
    if signalSample not in ExtendedSamples:
      ExtendedSamples.append(signalSample)
    for alt in AlternativeSignalMCs:
      altSample = 'Signal{}_MC16a'.format(alt)
      if altSample not in ExtendedSamples:
        ExtendedSamples.append(altSample)
    # Add backgrounds
    for bkg in Backgrounds:
      bkgSample = bkg+'_MC16a'
      if bkgSample not in ExtendedSamples:
        ExtendedSamples.append(bkgSample)
  elif '17' in sample:
    # Add Signal MCs
    signalSample = 'SignalSH2211_MC16d'
    if signalSample not in ExtendedSamples:
      ExtendedSamples.append(signalSample)
    for alt in AlternativeSignalMCs:
      ExtendedSamples.append('Signal{}_MC16d'.format(alt))
    # Add backgrounds
    for bkg in Backgrounds:
      ExtendedSamples.append(bkg+'_MC16d')
  elif '18' in sample:
    # Add Signal MCs
    signalSample = 'SignalSH2211_MC16e'
    if signalSample not in ExtendedSamples:
      ExtendedSamples.append(signalSample)
    for alt in AlternativeSignalMCs:
      ExtendedSamples.append('Signal{}_MC16e'.format(alt))
    # Add backgrounds
    for bkg in Backgrounds:
      ExtendedSamples.append(bkg+'_MC16e')

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *

Flavours = ['', 'FlavA1B', 'FlavA1C', 'FlavE1B', 'FlavE1C', 'FlavL']

# Loop over samples
for sample in ExtendedSamples:
  key = sample + '_' + Channel + '_' + Selection + '_' + Tagger
  # Check if expanded file exists
  checkFile = TFile.Open(InputFiles[key])
  if checkFile: continue # skip sample (already has an expanded root file)
  # Open input file
  InputFileName = InputFiles[key].replace('_expanded','')
  File = TFile.Open(InputFileName)
  CHECKObj(File,InputFileName+' not found, exiting')
  Hists = [] # new histograms
  # Loop over flavours
  for flavour in Flavours:
    if flavour != '':
      if not ('Signal' in sample and 'MC' in sample): continue # skip non-signal MC samples
    # Loop over variables
    for var in AllVariables:
      if 'Te1' not in AllVariables[var]: continue # skip two-b-tagged-jet variables
      
      # Sum Te1 and Ti2 1D histograms

      # Open input hists
      Ti2_histname = flavour + '_' + var + '_Ti2' if flavour != '' else var + '_Ti2'
      Ti2_hist = File.Get('nominal/'+Ti2_histname)
      CHECKObj(Ti2_hist,Ti2_histname+' not found in '+InputFileName+', exiting')
      Ti2_hist.SetDirectory(0)
      Te1_histname = flavour + '_' + var + '_Te1' if flavour != '' else var + '_Te1'
      Te1_hist = File.Get('nominal/'+Te1_histname)
      CHECKObj(Te1_hist,Te1_histname+' not found in '+InputFileName+', exiting')
      Te1_hist.SetDirectory(0)
      Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
      Hist.Add(Te1_hist)
      Hists.append(Hist)

      if var not in Observables: continue # 2D histograms only for observables

      # Sum TM histograms
      if 'Signal' in sample and 'MC' in sample and 'FlavA' in flavour:
        Ti2_histname = 'TM_' + flavour + '_' + var + '_Ti2'
        Ti2_hist = File.Get('nominal/'+Ti2_histname)
        CHECKObj(Ti2_hist,Ti2_histname+' not found in '+InputFileName+', exiting')
        Ti2_hist.SetDirectory(0)
        Te1_histname = 'TM_' + flavour + '_' + var + '_Te1'
        Te1_hist = File.Get('nominal/'+Te1_histname)
        CHECKObj(Te1_hist,Te1_histname+' not found in '+InputFileName+', exiting')
        Te1_hist.SetDirectory(0)
        Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
        Hist.Add(Te1_hist)
        Hists.append(Hist)
      
      # Sum Te1 and Ti2 for BTagWeight 2D histograms
      HistNameBase = flavour + '_' + Tagger + 'Weight' if flavour != '' else Tagger+'Weight'
      Te1_histname = HistNameBase + '_vs_' + var + '_Te1'
      Te1_hist = File.Get('nominal/'+Te1_histname)
      CHECKObj(Te1_hist,Te1_histname+' not found in '+InputFileName+', exiting')
      Te1_hist.SetDirectory(0)
      Ti2_histname = HistNameBase + '_vs_' + var + '_Ti2'
      Ti2_hist = File.Get('nominal/'+Ti2_histname)
      CHECKObj(Ti2_hist,Ti2_histname+' not found in '+InputFileName+', exiting')
      Ti2_hist.SetDirectory(0)
      Hist = Ti2_hist.Clone(Ti2_histname.replace('Ti2','Ti1'))
      Hist.Add(Te1_hist)
      Hists.append(Hist)

  # Open output file
  Dir  = File.Get('nominal')
  keys = Dir.GetListOfKeys()
  outFile = TFile(InputFiles[key],'RECREATE')
  for key in keys:
    Hist = key.ReadObj()
    outFile.cd()
    Hist.Write()
  for hist in Hists:
    outFile.cd()
    hist.Write()
  outFile.Close()
  File.Close()

###############
# Run plotters
###############

Samples = ["SignalSH2211_MC"]
for sample in DataSamples:          Samples.append(sample)
for sample in Backgrounds:          Samples.append(sample)
for sample in AlternativeSignalMCs: Samples.append('Signal'+sample+'_MC')

# Create output folder
os.system('mkdir Plots')
os.system('mkdir Plots/'+Selection)
os.system('mkdir Plots/'+Selection+'/'+Tagger)
os.system('mkdir RatioHists')
os.system('mkdir RatioHists/'+Selection)
os.system('mkdir RatioHists/'+Selection+'/'+Tagger)
DataPeriod = 'Data'
for sample in Samples:
  if 'data15' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data15')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data15')
    DataPeriod += '15'
  elif 'data16' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data16')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data16')
    DataPeriod += '16'
  elif 'data17' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data17')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data17')
    DataPeriod += '17'
  elif 'data18' in sample:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data18')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data18')
    DataPeriod += '18'
if 'Signal_data15' in DataSamples and 'Signal_data15' in DataSamples:
    os.system('mkdir Plots/'+Selection+'/'+Tagger+'/Data1516')
    os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/Data1516')
os.system('mkdir Plots/'+Selection+'/'+Tagger+'/'+DataPeriod)
os.system('mkdir RatioHists/'+Selection+'/'+Tagger+'/'+DataPeriod)

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Dicts import *

# Get list of histogram names from ReadTTrees_ZHF
HistNames = []
for var,cases in AllVariables.iteritems():
  for case in cases:
    if case != '': HistNames.append(var+'_'+case)
    else:          HistNames.append(var)
  if '' in cases or 'Te1' in cases:
    HistNames.append(var+'_Ti1')

# Add mu histogram
HistNames.append('correctedAndScaledAverageMu')
HistNames.append('correctedAndScaledActualMu')

# Construct strings with selected samples (each period alone and all together)
samples = []
# Add Full Run 2
if len(DataSamples) > 1:
  Full    = ''
  for sample in Samples: Full += sample+','
  Full = Full[:-1]
  samples.append(Full)
# Add data15+16 if both available
if 'Signal_data15' in DataSamples and 'Signal_data16' in DataSamples:
  String = 'Signal_data15,Signal_data16'
  for sample in Samples:
    if 'data' not in sample: String += ','+sample
  samples.append(String)
import copy
# Get list of MC samples
MCsamples   = copy.deepcopy(Samples)
for sample in Samples:
  if 'data' in sample: MCsamples.remove(sample)
# Get list of data samples
Datasamples = copy.deepcopy(Samples)
for sample in Samples:
  if 'data' not in sample: Datasamples.remove(sample)
for sample in Datasamples:
  string = sample+','
  for MCsample in MCsamples: string += MCsample+','
  string = string[:-1]
  samples.append(string)

Campaign = "MC16"
for sample in Samples:
  if '15' in sample or '16' in sample:
    if 'a' not in Campaign: Campaign += "a"
  elif '17' in sample:
    if Campaign != 'MC16': Campaign += "+"
    Campaign += "d"
  elif '18' in sample:
    if Campaign != 'MC16': Campaign += "+"
    Campaign += "e"
os.system('mkdir Plots/'+Selection+'/'+Tagger+'/'+Campaign)

# Loop over set of samples
counter = 0
for sample in samples:
  command = ''
  # Run Data_vs_MC.py for each histogram
  for name in HistNames: # loop over histograms
    command += 'python Data_vs_MC.py --histname '+name
    command += ' --channel '+Channel
    command += ' --selection '+Selection
    command += ' --tagger '+Tagger
    command += ' --atlas '+ATLASlegend
    command += ' --samples '+sample
    command += ' --format '+OutputFormat
    command += ' && '
  # Run MCSignalplots.py only for the sum of campaigns
  if counter > 0 :
    command += 'python MCSignalplots.py'
    command += ' --channel '+Channel
    command += ' --selection '+Selection
    command += ' --tagger '+Tagger
    command += ' --samples '+sample
    command += ' && '
  command = command[:-2]
  os.system(command)
  counter += 1

