
import sys
import ROOT
import copy
import argparse

data_hists = [
    'cutflow_reco',
    'Zpt_Te1',
    'Zpt_Ti2',
    'HFjet0_pt_Te1',
    'mjjHF_Ti2',
    'deltaPhijjHF_Ti2',
    ##'jet_pt',
    ##'lep_pt',
    ##'Zpt',
    ##'ht',
    ##'Zmass',
]


def compare_hists(key, case, href, htest):
    hists = []
    for ibin in range(1, href.GetNbinsX() + 1):
        passed = True
        if href.GetBinContent(ibin) == 0 and htest.GetBinContent(ibin) != 0:
            passed = False
        if href.GetBinContent(ibin) != 0 and ((href.GetBinContent(ibin) - htest.GetBinContent(ibin)) / href.GetBinContent(ibin)) > 0.001:  # difference is larger than 0.1%
            passed = False
        if not passed:
            print('FATAL: Test differs w.r.t the reference for {} for {} for {}'.format(key, case, href.GetName()))
            print('    href.Integral() = {}'.format(href.Integral()))
            print('    htest.Integral() = {}'.format(htest.Integral()))
            for ibin in range(1, href.GetNbinsX() + 1):
                print('    href.GetBinContent({}) = {}'.format(ibin, href.GetBinContent(ibin)))
                print('    htest.GetBinContent({}) = {}'.format(ibin, htest.GetBinContent(ibin)))
            hists.append(href.GetName())
            break
    return hists


def compare_to_ref(case):
    print('INFO: will compare histograms for {}'.format(case))
    # Prepare settings
    options = case.split('_')
    sample = options[0]
    channel = options[1]
    selection = options[2]
    from run_reader import samples
    sample_key = '{}_{}'.format(sample, options[3]) if 'Data' not in sample else sample
    input_path = samples[sample_key]
    nominal = True
    if 'Data' not in sample and options[3] == 'FullSysts':
        nominal = False
    ttrees = 'nominal' if nominal else 'FullSysts'
    mc = True
    if 'Data' in sample:
        mc = False
    tagger = 'DL1r'
    # open ref file
    if not mc:
        period = {
            'Data15': 'periodJ',
    	'Data16': 'periodA',
            'Data17': 'periodB',
            'Data18': 'periodB',
        }[sample]
        ref_file_name = '{}_{}_{}_{}_{}Tagger_{}_13TeV_Ref.root'.format(sample, period, channel, selection, tagger, sample.replace('D', 'd'))
    else:
        dsid = {
            'ZeeMC16aMG': '363149',
            'ZmumuMC16aSherpa2211': '700323',
    	'MC16eTTbarNonAllHad': '410470',
        }[sample]
        ref_file_name = '{}_{}_{}_{}_{}Tagger_useSFs_useSampleWeights_usePRW_{}_mc16_13TeV_Ref.root'.format(sample, dsid, channel, selection, tagger, 'nominalOnly' if nominal else 'fullSysts')
    ref_file = ROOT.TFile.Open('root://eosuser.cern.ch//eos/user/j/jbossios/SM/WZgroup/ZHF/ReaderRefs/' + ref_file_name)
    hist_names = copy.deepcopy(data_hists)
    mc_nominal_hists = [
        'HFjet0_pt_Te1_JVT_1',
        'HFjet0_pt_Te1_BTag_170',
        'HFjet0_pt_Te1_weight_pileup_1',
    ]
    if 'EL' in channel:
         mc_nominal_hists += [
            'HFjet0_pt_Te1_ELTrig_1',
            'HFjet0_pt_Te1_ELIso_1',
            'HFjet0_pt_Te1_ELPID_1',
            'HFjet0_pt_Te1_ELReco_1',
        ]
    if 'MU' in channel:
         mc_nominal_hists += [
            'HFjet0_pt_Te1_MUTrig_1',
            'HFjet0_pt_Te1_MUIso_1',
            'HFjet0_pt_Te1_MUPID_1',
            'HFjet0_pt_Te1_MUReco_1',
        ]
    if not sample.startswith('MC16') and 'SR' in sample:
         mc_nominal_hists += [
            'TM_FlavA1B_Zpt_Te1',
            'TM_FlavA1C_Zpt_Te1',
            'Zpt_truth',
            'HFjet0_pt_truth',
        ]
    if mc and nominal:
        hist_names += copy.deepcopy(mc_nominal_hists)
    tdirs = {
        True: ['nominal'],
        False: ['JET_EffectiveNP_Modelling4__1up', 'MET_SoftTrk_Scale__1up'],
    }[nominal]
    ref_hists = {tdir: [ref_file.Get('{}/{}'.format(tdir, hname)) for hname in hist_names] for tdir in tdirs}
    for key, hists in ref_hists.items():
        for hist in hists:
            hist.SetDirectory(0)
    ref_file.Close()
    test_file_name = ref_file_name.replace('Ref', 'Test')
    test_file = ROOT.TFile.Open('tests/' + test_file_name)
    if not test_file:
        print('test/{} not found, exiting'.format(test_file_name))
        sys.exit(1)
    test_hists = {tdir: [test_file.Get('{}/{}'.format(tdir, hname)) for hname in hist_names] for tdir in tdirs}
    for key, hists in test_hists.items():
        for hist in hists:
            hist.SetDirectory(0)
    test_file.Close()
    # compare histograms
    diffs = {}
    for key in ref_hists:
        for i in range(len(ref_hists[key])):
            hists = compare_hists(key, case, ref_hists[key][i], test_hists[key][i])
            if len(hists) > 0:
                if key not in diffs:
                    diffs[key] = hists
                else:
                    diffs[key] += hists
    if len(list(diffs.keys())) > 0:
        raise RuntimeError("Test differs w.r.t the reference for {} for {}".format(case, diffs))
    print('INFO: test passed successfully for {}'.format(case))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--case', action='store')
    args = parser.parse_args()
    case = args.case
    compare_to_ref(case)
