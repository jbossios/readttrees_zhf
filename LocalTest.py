# Inputs
InputFiles = [ # path to ROOT files
#  "/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_MU_EMPFlow_nominalOnly/data-tree/" # Test Zmumu
#  "/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_MU_EMPFlow/data-tree/" # Test Zmumu MC16a w/ systs
#  "/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodH_MU_EMPFlow/data-tree/" # Test data15 MU
#  "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/STDM3_EMPFlow_MV2c10_MU/Debugging_MC16d_Zmumu_364112/" # Test MC16d Zmumu
#  "/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_Testing_MC16a_364104_Zmumu/data-tree/",
#  "/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364108_Comparison/data-tree/", # for June coding days
#  "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/Debugging/MC16d_Zmumu_364110/",
# File to improve efficiency of code
#  "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTree_testFile_MC16a_Zmumu_364113/",
#  "/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTree_testFile_MC16a_Zmumu_364108/",
#  '/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/STDM3_EMPFlow_DL1_MU/MC16d_Diboson/user.jbossios.363360.MC16d_Diboson_Sherpa_STDM3_p3975_p4097_363360_030720_tree.root/',
# v4 TTree file
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v4/MC16a_Zmumu_364106/', # CVetoBVeto
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v4/MC16a_Zmumu_364108/',  # BFilter
# New test files
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_ZHFJets/data-tree/',
# Files to do event comparisons
# CxAOD comparisons (noMCtoMCSF)
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364118_ZHFJets_noMCtoMCSF_03Feb2021/data-tree/', # Zee
# Jake comparisons
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/TTrees_JakeValidation/submitDir_data2015_periodH_ZHFJets_Jan2021/data-tree/', # Data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/TTrees_JakeValidation/submitDir_MC16a_Zmumu_364104_eventComparison_ZHFJets_Jan2021/data-tree/', # Zmumu
#NEW
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_eventComparison_ZHFJets_Jan2021/data-tree/', # MC16a Zmumu
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodH_ZHFJets_Jan2021/data-tree/', # data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364118_ZHFJets_Jan2021/data-tree/' # MC16a Zee
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364118_ZHFJets_Jan2021_Testing/data-tree/' # MC16a Zee
#OLD
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodH_eventComparison_fixed_muonsCalibrated/data-tree/', # data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_eventComparison_fixed/data-tree/', # MC16a Zmumu
##  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodH_eventComparison/data-tree/', # data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_eventComparison/data-tree/', # MC16a Zmumu
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_ttbar_410470_eventComparison/data-tree/', # MC16a ttbar
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Diboson_363356_eventComparison/data-tree/', # MC16a Diboson
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_SingleTop_410648_eventComparison/data-tree/', # MC16a SingleTop
# PHYS Ref1
#  '/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/DAOD_PHYS_EMPFlow_DL1_MU/Ref1/user.jbossios.364109.MC16a_Zmumu_DAOD_PHYS_Ref1_p4166_364109_v4_070920_tree.root/',
#  '/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/DAOD_PHYS_EMPFlow_DL1_MU/Ref1/user.jbossios.364111.MC16a_Zmumu_DAOD_PHYS_Ref1_p4166_364111_v4_070920_tree.root/',
# PHYS Ref2
#  '/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/DAOD_PHYS_EMPFlow_DL1_MU/Ref2/user.jbossios.364109.MC16a_Zmumu_DAOD_PHYS_Ref2_p4166_364109_v4_070920_tree.root/',
  #'/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/DAOD_PHYS_EMPFlow_DL1_MU/Ref2/user.jbossios.364110.MC16a_Zmumu_DAOD_PHYS_Ref2_p4166_364110_v4_070920_tree.root/',
#  '/eos/atlas/user/j/jbossios/SM/ZHFRun2/TTrees/DAOD_PHYS_EMPFlow_DL1_MU/Ref2/user.jbossios.364111.MC16a_Zmumu_DAOD_PHYS_Ref2_p4166_364111_v4_070920_tree.root/',
# New files from Feb 2021 (new iso WPs and using MCtoMC SF)
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_ZHFJets_MC2MCSF_24022021/data-tree/', # Zmumu
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364118_ZHFJets_MC2MCSF_24022021/data-tree/', # Zee
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364118_ZHFJets_MC2MCSF_26022021/data-tree/', # Zee
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodH_ZHFJets_24022021/data-tree/', # data15
# Using p4250/1 inputs
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodB_ZHFJets_p4251_r21_2_153_10032021/data-tree/', # data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364119_p4252_r21_2_153_10032021/data-tree/',   # Zee
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_p4252_r21_2_153_10032021/data-tree/', # Zmumu 364104
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364105_p4252_r21_2_153_10032021/data-tree/', # Zmumu 364105
# Updating muon isolation WP to match what it is being used in CxAOD
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodB_ZHFJets_p4251_r21_2_153_24032021/data-tree/', # data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364119_p4252_r21_2_153_24032021/data-tree/',   # Zee
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_p4252_r21_2_153_24032021/data-tree/', # Zmumu 364104
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364105_p4252_r21_2_153_24032021/data-tree/', # Zmumu 364105
# Fix to MET (muon channel)
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_3641004_p4252_r21_2_162_19042021/data-tree/', # Zmumu 364104
# April CDI
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_data2015_periodB_ZHFJets_AprilCDI_21042021/data-tree/', # data15
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_AprilCDI_21042021/data-tree/',       # Zmumu 364104
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364105_AprilCDI_21042021/data-tree/',       # Zmumu 364105
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_364119_AprilCDI_21042021/data-tree/',         # Zee 364119
# v6 single file for comparisons with Alec
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v6/MC16a_Zmumu/user.jbossios.364113.MC16a_Zmumu_Sherpa_STDM3_p4434_364113_v6_280421_tree.root/' , # MC16a Zmumu
# Samples used for event-by-event comparisons at truth level
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v6/MC16a_Zmumu_MG_363125/data-tree/',
# ttbar
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_410470_MC16a_ttbar_ZHFJets_Aug2021/data-tree/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v7/MC16a_Zmumu/user.jbossios.364112.MC16a_Zmumu_Sherpa_STDM3_p4434_364112_v7_030921_tree.root/',
# Test
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_v8_wZHFSelector/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_v8_woZHFSelector/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_364104_v9/data-tree/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v9/MC16a_Zee_MG/user.jbossios.363170.MC16a_Zee_MG_STDM3_p4434_363170_v9_200921_tree.root/',
# Example of TFile with syst TTrees
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v9/MC16a_Zmumu_Sh_2211_systs/user.jbossios.700323.MC16a_Zmumu_Sherpa_2211_STDM4_p4514_700323_v9_121021_tree.root/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/Test_TTrees/MC16a_700323_Zmumu_Sh_2211_systs/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_700323_v10_08032022/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH_v10/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_MG_363125_v9_09032022_Systematics/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH_v10/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_MG_363125_v9_09032022_Nominal/data-tree/',
# Example MC16a Zee SH2211 nominal v10
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v10/MC16a_Zee_Sh_2211/user.jbossios.700321.MC16a_Zee_Sherpa_2211_STDM4_p4514_700321_v10_251021_tree.root/'
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TTrees/xAH/STDM3_PFlow_DL1r_v10/MC16a_Zee_MG/user.jbossios.363170.MC16a_Zee_MG_STDM3_p4434_363170_v10_251021_tree.root/'
# Data15 periodJ pre-v11 to compare with CxAOD (using AB21.2.195)
#  '/eos/user/j/jbossios/SM/WZgroup/ZHF/TTrees/v11_Data15_periodJ_test/Data2015_periodJ_merged_tree/',
# Data18 periodC pre-v11 to compare with CxAOD (using AB21.2.198)
#  '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TTrees/CxAOD_comparisons/Data2018_periodC_merged_tree/',
#  '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TTrees/CxAOD_comparisons/v1/Data2018_periodC_merged_tree/',
# Full Data15 pre-v11 to compare with CxAOD (using AB21.2.198)
#  '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TTrees/CxAOD_comparisons/v11_data2015_full/',
#  '/eos/user/j/jbossios/SM/WZgroup/ZHF/TTrees/v11_Data15_test/v11_Data2015_test_full/',
# MC16a Zmumu/Zee MG for CxAOD comparisons (using AB21.2.198)
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_363125_ZmumuMC16aMG_CxAOD/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_363149_ZeeMC16aMG_CxAOD/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_363149_MC16a_Zee_MG_07032022/data-tree/',
# v11 investigations
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH_AB195/source/MakeTTrees_ZHF/scripts/submitDir_v11_363149_ZeeMC16aMG_CxAOD/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH_AB195/source/MakeTTrees_ZHF/scripts/submitDir_v11_363125_ZmumuMC16aMG_CxAOD/data-tree/',
# v11 final
  #'/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_AB198_final_363125_ZmumuMC16aMG/data-tree/',
  #'/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_AB198_final_363149_ZeeMC16aMG/data-tree/',
# v10 test
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH_v10/source/MakeTTrees_ZHF/scripts/submitDir_v11_363125_ZmumuMC16aMG_CxAOD/data-tree/',
# v11 test files
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_ZmumuMC16aSherpa2211_700323_NominalOnly_15122021/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_v11_ZmumuMC16aSherpa2211_700323_Systematics_19012022/data-tree/',
# Test file made with 21.2.198
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_SH2211_700323_NominalOnly_17Feb2022/data-tree/',
#  '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TestTTrees/Data2018_periodC_v11_test/',
# Final v11 Zee/Zmumu MG
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zmumu_MG_363125_NominalOnly_09032022/data-tree/',
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/source/MakeTTrees_ZHF/scripts/submitDir_MC16a_Zee_MG_363149_NominalOnly_09032022/data-tree/',
   '/eos/atlas/atlaslocalgroupdisk/sm/ZHF_FullRun2/TestTTrees/user.jbossios.410470.MC16a_ttbar_nonallhad_PPy8_STDM3_p4432_410470_v11_bkg_130522_tree.root/',
# Samples to create references
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_data15_periodJ_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_data16_periodA_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_data17_periodB_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_data18_periodB_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_MC16a_Zee_MG_363149_nominalOnly_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_MC16a_Zee_MG_363149_systematics_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_MC16a_Zmumu_SH2211_700323_nominalOnly_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_MC16a_Zmumu_SH2211_700323_systematics_14July2022/',
#  '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestTTrees/v11/submitDir_v11_MC16e_ttbar_410470_14July2022/',
## DeltaR(Z,b-jet) studies
#  '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/Sendv11GridJobs/source/MakeTTrees_ZHF/scripts/submitDir_Data2015_periodJ/data-tree/'
]
Channel   = "EL" # options: MU or EL
Dataset   = "MC16aTTbarNonAllHad" # options: ZmumuMC16xSherpa, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16xZtautau (where x = "a", "d" or "e")
#Dataset   = "ZmumuMC16aSherpa2211" # options: ZmumuMC16xSherpa, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16xZtautau (where x = "a", "d" or "e")
#Dataset   = "ZeeMC16aSherpa2211" # options: ZmumuMC16xSherpa, ZeeMC16xSherpa, ZmumuMC16xMG, ZeeMC16xMG, Data15, Data16, Data17, Data18, MC16xTop, MC16xWenu, MC16xWmunu, MC16xDiboson, MC16xZtautau (where x = "a", "d" or "e")
useTestMetadata = False # should pretty much be always False

# MC Weights (only meaningful for MC, not used at all when running on data)
useSFs           = True  # reco, ID, isolation and trigger lepton SFs
useSampleWeights = True  # eventWeight * eff * xs / totalevents (sumWeights for Sherpa)
usePRW           = True  # pileup reweighting

# Flavour Jet Tagger
Tagger = "DL1r"

# Selection
#Selection = "SR" # options: SR, SRa1jet, SR20, Study, Medium1 and Medium2
Selection = "SR2" # options: SR, SRa1jet, SR20, Study, Medium1 and Medium2
#Selection = "VR" # options: SR, SRa1jet, SR20, Study, Medium1 and Medium2
#Selection = "SRa1jet" # options: SR, SRa1jet, SR20, Study, Medium1 and Medium2

# Distributions
fillReco  = True # fill reco distributions
fillTruth = False # fill truth distributions # Temporary

# Debugging
Debug     = False
Perf      = False

# Jet collection
JetColl   = 'PFlow'

# Systematics
TTrees = 'nominal' # options: nominal, Full(nominal+systs), FullSysts, subset of ttrees separated by comma
#TTrees = 'FullSysts' # options: nominal, Full(nominal+systs), FullSysts, subset of ttrees separated by comma

# Print event level info
PrintRecoInfo    = False
PrintTruthInfo   = False
PrintMatchedInfo = False
cxaod_validation = False

ApplySFsysts = False
GetWgtSysts  = True

###################################################
## DO NOT MODIFY
###################################################

readAMI = True

from ROOT import *
import os,sys
from Arrays import *

# Protections
if "EL" not in Channel and "MU" not in Channel:
  print("ERROR: Channel not recognised, exiting")
  sys.exit(0)
if Dataset not in DatasetOptions:
  print("ERROR: Dataset not recognised, exiting")
  sys.exit(0)

# MC?
MC = False
if "MC" in Dataset:
  MC = True

if not MC: readAMI = False # data

# Protections
if not MC: # data
  if useSFs:           useSFs           = False
  if useSampleWeights: useSampleWeights = False
  if usePRW:           usePRW           = False
  if fillTruth:        fillTruth        = False

# Create output folder
if not os.path.exists('Testing'):
  os.makedirs('Testing')

# Run over a single file from the chosen dataset
counter = 0
command = ""
for File in os.listdir(InputFiles[0]): # Loop over files
  if ".root" not in File:
    continue
  if counter > 0:
    break

  # Run test job
  command += "python3 Reader.py --path "+InputFiles[0]+" --outPATH Testing/ --file "+File+" --jetcoll "+JetColl+" --channel "+Channel+" --dataset "+Dataset+" --selection "+Selection+" --tagger "+Tagger
  command += " --ttrees "+TTrees
  if useTestMetadata: command += " --localTest"
  if Debug:
    command += " --debug"
  if Perf:
    command += " --perf"
  if not fillTruth:
    command += " --noTruth"
  if readAMI: command += " --readAMI"
  if not fillReco:
    command += " --noReco"
  if not useSFs:
    command += " --noSFs"
  if not useSampleWeights:
    command += " --noSampleWeights"
  if not usePRW:
    command += " --noPRW"
  if ApplySFsysts:
    command += " --applySFsysts"
  if GetWgtSysts:
    command += " --getWeightSysts"
  if PrintRecoInfo:
    command += " --printRecoInfo"
  if PrintTruthInfo:
    command += " --printTruthInfo"
  if PrintMatchedInfo:
    command += " --printMatchedInfo"
  if cxaod_validation:
    command += ' --cxaodval'
  command += " && "

  counter += 1

command = command[:-2]
os.system(command)
