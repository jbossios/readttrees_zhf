import sys
from Arrays import DatasetOptions, Taggers

def check_jetcoll(jetcoll):
  if jetcoll is None:
    print("ERROR: Jet collection not provided, exiting...")
    sys.exit(0)
  if jetcoll != 'PFlow' and jetcoll != 'EMTopo':
    print("ERROR: Jet collection ({}) not recognized, exiting...".format(jetcoll))
    sys.exit(0)


def check_channel(channel):
  if channel is None:
    print("ERROR: Channel not provided, exiting...")
    sys.exit(0)
  if channel != "MU" and channel != "EL" and channel != "ELMU":
    print("ERROR: Channel ({}) not recognised, exiting...".format(channel))
    sys.exit(0)


def check_ttree_case(case):
  if case == 'Full':
    print('ERROR: TTrees==Full is not supported anymore (since inputs are not the same), exiting...')
    sys.exit(1)


def check_dataset(dataset):
  if dataset not in DatasetOptions:
    print("ERROR: Dataset ({}) not recognised, exiting...".format(dataset))
    sys.exit(0)


def check_input_path(path):
  if path is None:
    print("ERROR: path to input file not provided, exiting...")
    sys.exit(0)


def check_output_path(output_path):
  if output_path is None:
    print("ERROR: path to output files (outPATH) not provided, exiting...")
    sys.exit(0)

def check_input_file(input_file):
  if input_file is None:
    print("ERROR: input file not provided, exiting...")
    sys.exit(0)


def check_tagger(tagger):
  if tagger not in Taggers:
    print("ERROR: Flavour Jet Tagger ({}) not recognized, exiting...".format(tagger))
    sys.exit(0)
 

def apply_protections(args):
  check_jetcoll(args.jetcoll)
  check_channel(args.channel)
  check_ttree_case(args.ttrees)
  check_dataset(args.dataset)
  check_input_path(args.path)
  check_output_path(args.outPATH)
  check_input_file(args.inputFile)
  check_tagger(args.tagger)
