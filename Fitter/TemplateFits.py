########################################################################################
## AUTHOR:  Jona Bossio (jbossios@cern.ch)                                              #
## PURPOSE: Make template fits to estimate flavour fractions                            #
## DATE:    06 January 2020                                                             #
##                                                                                      #
## PURPOSE:                                                                             #
##          Estimate flavour fractions with an extended maximum likelihood fit to data  #
##                                                                                      #
## PROCEDURE:                                                                           #
##          Get PDFs for Z+Light, Z+b, Z+c, Top, diboson and Ztautau (from MC in SR~)   #
##          SR~: Same as SR but w/o b-tagging                                           #
##          Fit fb*Zb + fc*Zc + (1-fb-fc)*ZL + other bkgs to data in SR~ & obtain fb,fc #
##          Top, diboson and Ztautau have fixed normalization (xs*lumi*SFs)             #
##          Obtain fractions so the fit matches with the data (tested with a chi2)      #
##          Use fb and fc to scale data distributions to get Z+b and Z+c estimations    #
########################################################################################

# Tagger
Tagger = "DL1r"

# Data-taking periods
#DataYears = ["15", "16", "17", "18"]
DataYears = ["15", "16"]

Backgrounds = [  # FIXME ADD OTHER BACKGROUNDS
  'Top',
  'Diboson',
  'Ztautau',
  'VH',
  #'Wmunu',
  #'Wtaunu',
  #'VH',
]

# NOTE: Inclusive flavour fractions are obtained always
ClosureTest = False  # inject known flavour fractions and extract them  # FIXME
vsAllObservables = True  # Obtain flavour fractions as a function of each observable

# Model
model_type = 'LBC'  # options: LBC, LHF

# TODO: allow to use multiple MC16 campaigns
# TODO: is chi2 calculated correctly?

Debug = False

#####################################################################
## DO NOT MODIFY
#####################################################################

# Import ROOT
from ROOT import *
import os
import sys

if not os.path.exists('Plots'):
    os.makedirs(Plots)

# Import helper classes and functions
from HelperClasses import *
from HelperFunctions import *

# Parameters for closure test
# FIXME
ZbFracs4ClosureTest = [0.1, 0.2, 0.3, 0.4]
ZcFracs4ClosureTest = [0.1, 0.2, 0.3, 0.4]
maxZcFracNonClosureUncertainty = 0

# Colors for backgrounds
MyColors = [kOrange+1, kCyan, kPink-6, kOrange-6]  # FIXME add more colours

# Set AtlasStyle
gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()
gROOT.SetBatch(True)

# Import things from Reader
sys.path.insert(1, '../') # insert at 1, 0 is the script path
from Arrays import *
from Dicts import *

# Perform flavour fitting separately for data15+16, data17 and data18

DataDict = dict()
if '15' in DataYears and '16' in DataYears:
    DataDict['1516'] = ['15','16']
elif '15' in DataYears:
    DataDict['15'] = ['15']
elif '16' in DataYears:
    DataDict['16'] = ['16']
if '17' in DataYears:
    DataDict['17'] = ['17']
if '18' in DataYears:
    DataDict['18'] = ['18']

for Set, Datasets in DataDict.items():

    # Import input files from Plotter
    PATH = "../Plotter/"
    sys.path.insert(1, PATH) # insert at 1, 0 is the script path
    from InputFiles import InputFiles
    from Luminosities import *
    from Style import *
    Files = dict()
    Campaigns = []
    for dataset in Datasets:
        Files["Data"+dataset] = InputFiles["Signal_data"+dataset+"_MU_SR_"+Tagger+'_truth']
        if dataset == "15" and "a" not in Campaigns:
            Campaigns.append("a")
        elif dataset == "16" and "a" not in Campaigns:
            Campaigns.append("a")
        elif dataset == "17" and "d" not in Campaigns:
            Campaigns.append("d")
        elif dataset == "18" and "e" not in Campaigns:
            Campaigns.append("e")
    for campaign in Campaigns:
        Files["MC16"+campaign+"_Signal"] = InputFiles["SignalSH2211_MC16"+campaign+"_MU_SR_"+Tagger+'_truth']  # Temporary
        for bkg in Backgrounds:
            Files["MC16"+campaign+"_"+bkg] = InputFiles[bkg+"_MC16"+campaign+"_MU_SR_"+Tagger+'_truth']  # Temporary

    # Name of histograms for each sample
    HistNames = dict()
    for dataset in Datasets:
        HistNames["Data"+dataset] = [Tagger+"LeadQuantile"]
    for campaign in Campaigns:
        HistNames["MC16"+campaign+"_Signal"]  = ["FlavA1B_"+Tagger+"LeadQuantile", "FlavA1C_"+Tagger+"LeadQuantile", "FlavL_"+Tagger+"LeadQuantile"]
        for bkg in Backgrounds:
            HistNames["MC16"+campaign+"_"+bkg] = [Tagger+"LeadQuantile"]

    # Fitting range
    xMin = 1.5
    xMax = 5.5

    ####################
    # Choose luminosity
    ####################
    Luminosity = dict()
    Luminosity["a"] = 0
    for FileKey, FileName in Files.items():
        if "Data15" in FileKey:
            Luminosity["a"] += Luminosity_2015
        if "Data16" in FileKey:
            Luminosity["a"] += Luminosity_2016
        if "Data17" in FileKey:
            Luminosity["d"] = Luminosity_2017
        if "Data18" in FileKey:
            Luminosity["e"] = Luminosity_2018

    # Commented since I don't have full DL1rLeadQuantile distribution anymore (I could recover that by merging over observable bins though)

    ########################
    ## Get input histograms
    ########################
    #Histograms = dict()
    #Nevents = dict()
    #for FileKey, FileName in Files.items():
    #    # open file
    #    File = TFile.Open(FileName)
    #    if not File:
    #        print FileName+" not found, exiting"
    #        sys.exit(0)
    #    # get histograms
    #    for HistName in HistNames[FileKey]:
    #        hist = File.Get(HistName)
    #        if not hist:
    #            print HistName+" not found in "+FileName+", exiting"
    #            sys.exit(0)
    #        hist.SetDirectory(0)
    #        # scale MC histograms by luminosity
    #        if "MC16a" in FileKey:
    #            hist.Scale(Luminosity["a"])
    #        elif "MC16d" in FileKey:
    #            hist.Scale(Luminosity["d"])
    #        elif "MC16e" in FileKey:
    #            hist.Scale(Luminosity["e"])
    #        Nevents[FileKey+"_"+HistName] = hist.Integral()
    #        Histograms[FileKey+"_"+HistName] = hist
    #Histograms["Data_"+Tagger+"LeadQuantile"] = 0  # temporary (total data histogram is prepared below)
    #Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"] = 0  # temporary (total MC FlavA1B histogram is prepared below)
    #Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"] = 0  # temporary (total MC FlavA1C histogram is prepared below)
    #Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"]   = 0  # temporary (total MC FlavL histogram is prepared below)
    #for bkg in Backgrounds:
    #    Histograms["MC_"+bkg+"_"+Tagger+"LeadQuantile"] = 0  # temporary (total MC histogram for this background is prepared below)

    ################################
    ## Prepare final data histogram
    ################################
    #counter = 0
    #for key,histogram in Histograms.iteritems():
    #    if "Data" in key and key!="Data_"+Tagger+"LeadQuantile":
    #        if counter == 0:
    #            Histograms["Data_"+Tagger+"LeadQuantile"] = histogram
    #        else:
    #            Histograms["Data_"+Tagger+"LeadQuantile"].Add(histogram)
    #        counter += 1
    #Histograms["Data_"+Tagger+"LeadQuantile"].SetName("Data_"+Set+"_"+Tagger+"LeadQuantile")
    ## Get total number of events in data
    #Nevents["Data_"+Tagger+"LeadQuantile"] = Histograms["Data_"+Tagger+"LeadQuantile"].Integral()

    ################################
    ## Prepare final MC histograms
    ################################
    ## MC signal
    #for counter, campaign in enumerate(Campaigns):
    #    if not counter:
    #        Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"] = Histograms["MC16"+campaign+"_Signal_FlavA1B_"+Tagger+"LeadQuantile"]
    #        Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"] = Histograms["MC16"+campaign+"_Signal_FlavA1C_"+Tagger+"LeadQuantile"]
    #        Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"] = Histograms["MC16"+campaign+"_Signal_FlavL_"+Tagger+"LeadQuantile"]
    #    else:
    #        Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"].Add(Histograms["MC16"+campaign+"_Signal_FlavA1B_"+Tagger+"LeadQuantile"])
    #        Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"].Add(Histograms["MC16"+campaign+"_Signal_FlavA1C_"+Tagger+"LeadQuantile"])
    #        Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"].Add(Histograms["MC16"+campaign+"_Signal_FlavL_"+Tagger+"LeadQuantile"])
    #if model_type == 'LHF':
    #    # sum FlavA1B and FlavA1C
    #    Histograms["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"] = Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"].Clone("MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile")
    #    Histograms["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"].Add(Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"])
    ## Get total number of events in each MC flavour sample
    #Nevents["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"] = Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"].Integral()
    #Nevents["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"] = Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"].Integral()
    #Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"] = Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"].Integral()
    #if model_type == 'LHF':
    #    Nevents["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"] = Histograms["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"].Integral()
    ## Backgrounds
    #for bkg in Backgrounds:
    #    for counter, campaign in enumerate(Campaigns):
    #        if not counter:
    #            Histograms["MC_"+bkg+"_"+Tagger+"LeadQuantile"] = Histograms["MC16"+campaign+"_"+bkg+"_"+Tagger+"LeadQuantile"]
    #        else:
    #            Histograms["MC_"+bkg+"_"+Tagger+"LeadQuantile"].Add(Histograms["MC16"+campaign+"_"+bkg+"_"+Tagger+"LeadQuantile"])
    #    # Get total number of events in each MC flavour sample
    #    Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"] = Histograms["MC_"+bkg+"_"+Tagger+"LeadQuantile"].Integral()

    ##########################
    ## Prepare PDFs and Data
    ##########################
    #x = RooRealVar("x", "b-tagging discriminant", xMin, xMax)
    #hData = RooDataHist("hData", "", RooArgList(x), Histograms["Data_"+Tagger+"LeadQuantile"])
    #if model_type == 'LBC':
    #    hZb = RooDataHist("hZb", "", RooArgList(x), Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"])
    #    hZc = RooDataHist("hZc", "", RooArgList(x), Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"])
    #else:
    #    hZHF = RooDataHist("hZHF", "", RooArgList(x), Histograms["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"])
    #hZL = RooDataHist("hZL", "", RooArgList(x), Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"])
    #hBkgs = dict()
    #for bkg in Backgrounds:
    #    hBkgs[bkg] = RooDataHist("h"+bkg, "", RooArgList(x), Histograms["MC_"+bkg+"_"+Tagger+"LeadQuantile"])
    #if model_type == 'LBC':
    #    PDFZb = RooHistPdf("PDFZb", "", RooArgSet(x), hZb)
    #    PDFZc = RooHistPdf("PDFZc", "", RooArgSet(x), hZc)
    #else:
    #    PDFZHF = RooHistPdf("PDFZHF", "", RooArgSet(x), hZHF)
    #PDFZL = RooHistPdf("PDFZL", "", RooArgSet(x), hZL)
    #PDFBkgs = dict()
    #for bkg in Backgrounds:
    #    PDFBkgs[bkg] = RooHistPdf("PDF"+bkg, "", RooArgSet(x), hBkgs[bkg])

    ################################
    ## Compare histograms with PDFs
    ################################
    #canvas = TCanvas()
    #legend = TLegend(0.7, 0.7, 0.9, 0.9)
    #outputname = "Plots/PDFs_vs_Histograms_data"
    #for dataset in Datasets:
    #    outputname += dataset
    #outputname += "_{}.pdf".format(model_type)
    #canvas.Print(outputname+"[")
    #x1frame = x.frame()
    #x1frame.SetTitle("")
    #if model_type == 'LBC':
    #    hZb_normalized  = hZb.Clone("hZb_normalized")
    #    hZc_normalized  = hZc.Clone("hZc_normalized")
    #else:
    #    hZHF_normalized  = hZHF.Clone("hZHF_normalized")
    #hZL_normalized  = hZL.Clone("hZL_normalized")
    #hBkgs_normalized = dict()
    #for bkg in Backgrounds:
    #    hBkgs_normalized[bkg] = hBkgs[bkg].Clone("h"+bkg+"_normalized")
    #if model_type == 'LBC':
    #    hZb_normalized.plotOn(x1frame, RooFit.Name("hZb"), RooFit.LineColor(kBlue), RooFit.MarkerColor(kBlue))
    #    PDFZb.plotOn(x1frame, RooFit.Name("PDFZb"), RooFit.LineColor(kBlue))
    #    hZc_normalized.plotOn(x1frame, RooFit.Name("hZc"), RooFit.LineColor(kRed), RooFit.MarkerColor(kRed))
    #    PDFZc.plotOn(x1frame, RooFit.Name("PDFZc"), RooFit.LineColor(kRed))
    #else:
    #    hZHF_normalized.plotOn(x1frame, RooFit.Name("hZHF"), RooFit.LineColor(kBlue), RooFit.MarkerColor(kBlue))
    #    PDFZHF.plotOn(x1frame, RooFit.Name("PDFZHF"), RooFit.LineColor(kBlue))
    #hZL_normalized.plotOn(x1frame,RooFit.Name("hZL"),RooFit.LineColor(kBlack),RooFit.MarkerColor(kBlack))
    #PDFZL.plotOn(x1frame,RooFit.Name("PDFZL"),RooFit.LineColor(kBlack))
    #for counter, bkg in enumerate(Backgrounds):
    #  hBkgs_normalized[bkg].plotOn(x1frame, RooFit.Name("h"+bkg), RooFit.LineColor(MyColors[counter]), RooFit.MarkerColor(MyColors[counter]))
    #  PDFBkgs[bkg].plotOn(x1frame, RooFit.Name("PDF"+bkg), RooFit.LineColor(MyColors[counter]))
    #x1frame.Draw()
    #if model_type == 'LBC':
    #    legend.AddEntry("hZb", "Zb hist", "p")
    #    legend.AddEntry("PDFZb", "Zb PDF", "l")
    #    legend.AddEntry("hZc", "Zc hist", "p")
    #    legend.AddEntry("PDFZc", "Zc PDF", "l")
    #else:
    #    legend.AddEntry("hZHF", "ZHF hist", "p")
    #    legend.AddEntry("PDFZHF", "ZHF PDF", "l")
    #legend.AddEntry("hZL", "ZL hist", "p")
    #legend.AddEntry("PDFZL", "ZL PDF", "l")
    #for bkg in Backgrounds:
    #  legend.AddEntry("h"+bkg,bkg+" hist", "p")
    #  legend.AddEntry("PDF"+bkg,bkg+" PDF", "l")
    #legend.Draw("same")
    #canvas.Print(outputname)
    #canvas.Print(outputname+"]")

    ################
    ## Create model
    ################
    #fixedBkgEvents = dict()
    #for bkg in Backgrounds:
    #    fixedBkgEvents[bkg] = Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"]
    #totalFixedBkgEvents = 0
    #for bkg in Backgrounds:
    #    totalFixedBkgEvents += fixedBkgEvents[bkg]
    #nExtractedData = Nevents["Data_"+Tagger+"LeadQuantile"] - totalFixedBkgEvents
    #if nExtractedData <=0:
    #    print("WARNING: nExtractedData <= 0 !")
    #    print("WARNING: nExtractedData = "+str(nExtractedData))
    #    print("WARNING: nData          = "+str(Nevents["Data_"+Tagger+"LeadQuantile"]))
    #    print("WARNING: nBkg           = "+str(totalFixedBkgEvents))
    #if model_type == 'LBC':
    #    initialZbEvents = 0.3 * nExtractedData if nExtractedData > 0 else 1
    #    initialZcEvents = 0.3 * nExtractedData if nExtractedData > 0 else 1
    #    initialZLEvents = Nevents["Data_"+Tagger+"LeadQuantile"] - totalFixedBkgEvents - initialZbEvents - initialZcEvents if nExtractedData > 0 else 1
    #else:
    #    initialZHFEvents = 0.6 * nExtractedData if nExtractedData > 0 else 1
    #    initialZLEvents = Nevents["Data_"+Tagger+"LeadQuantile"] - totalFixedBkgEvents - initialZHFEvents if nExtractedData > 0 else 1
    #if model_type == 'LBC':
    #    nZb = RooRealVar("nZb", "nZb", initialZbEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"])  # initial number of Z+b events
    #    nZc = RooRealVar("nZc", "nZc", initialZcEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"])  # initial number of Z+c events
    #else:
    #    nZHF = RooRealVar("nZHF", "nZHF", initialZHFEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"])  # initial number of Z+HF events
    #nZL = RooRealVar("nZL", "nZL", initialZLEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"])  # initial number of Z+L events
    #nBkgs = dict()
    #for bkg in Backgrounds:
    #    nBkgs[bkg] = RooRealVar("n"+bkg, "n"+bkg, fixedBkgEvents[bkg])  # fixed number of events for this background
    #if model_type == 'LBC':
    #    Model, MSG = makeModel("model", Backgrounds, PDFZb, PDFZc, PDFZL, PDFBkgs, nZb, nZc, nZL, nBkgs)
    #else:
    #    Model, MSG = makeModel("model", Backgrounds, PDFZHF, PDFZL, PDFBkgs, nZHF, nZL, nBkgs)
    #if MSG != 'OK':
    #    print(MSG)

    #####################
    ## Fit model to data
    #####################
    #fitResult = Model.fitTo(hData, RooFit.Extended(True), RooFit.Save(), RooFit.SumW2Error(True))
    ##fitResult = Model.fitTo(hData, RooFit.Save(), RooFit.SumW2Error(True))

    ################
    ## Protections
    ################
    #nTotalBkgs = 0
    #for bkg in Backgrounds:
    #    nTotalBkgs += nBkgs[bkg].getVal()
    #if model_type == 'LBC':
    #    nExtracted = nZb.getVal() + nZc.getVal() + nZL.getVal() + nTotalBkgs
    #else:
    #    nExtracted = nZHF.getVal() + nZL.getVal() + nTotalBkgs
    #if abs( nExtracted - Nevents["Data_"+Tagger+"LeadQuantile"] ) > (0.01*Nevents["Data_"+Tagger+"LeadQuantile"]):  # find differences larger than 1%
    #    print("######################################################################################################")
    #    print("ERROR: the total number of extracted events does not match with the total number of events in data")
    #    if model_type == 'LBC':
    #        print("nZb: "+str(nZb.getVal()))
    #        print("nZc: "+str(nZc.getVal()))
    #    else:
    #        print("nZHF: "+str(nZHF.getVal()))
    #    print("nZL: "+str(nZL.getVal()))
    #    for bkg in Backgrounds:
    #        print("n"+bkg+": "+str(nBkgs[bkg].getVal()))
    #    print("nTotalExtracted: "+str(nExtracted))
    #    print("nData: "+str(Nevents["Data_"+Tagger+"LeadQuantile"]))
    #    print("######################################################################################################")
    #for bkg in Backgrounds:
    #    if nBkgs[bkg].getVal() != Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"]:
    #        print("######################################################################################################")
    #        print("ERROR: the number of "+bkg+" events changed after the fit and it shouldn't!")
    #        print("nExtracted: "+str(nBkgs[bkg].getVal()))
    #        print("nInjected: "+str(Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"]))
    #        print("######################################################################################################")

    ##############################
    ## Show model fit to data
    ##############################
    #canvas = TCanvas()
    #outputname = "Plots/{}FitResult_data".format(model_type)
    #for dataset in Datasets:
    #    outputname += dataset
    #outputname += ".pdf"
    #canvas.Print(outputname+"[")
    #x2frame = x.frame()
    #x2frame.SetTitle("")
    #hData.plotOn(x2frame, RooFit.Name("data"), RooFit.LineColor(kBlack), RooFit.MarkerColor(kBlack))
    #Model.plotOn(x2frame, RooFit.Name("model"), RooFit.LineColor(kGreen+2), RooFit.LineStyle(kDashed))
    #x2frame.Draw()
    #canvas.Print(outputname)
    #canvas.Print(outputname+"]")

    ###############################################
    ## Calculate chi2 and calculate Z+HF fractions
    ###############################################
    ##chi2   = x2frame.chiSquare("model","data",3)
    #chi2 = x2frame.chiSquare("model", "data")
    #if model_type == 'LBC':
    #    Result = LBCFitResult(fitResult.status,nZb,nZc,nZL,Nevents["Data_"+Tagger+"LeadQuantile"],chi2,Nevents["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"],Nevents["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"],Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"]) # calculates Z+HF fractions
    #else:
    #    Result = LZHFFitResult(fitResult.status, nZHF, nZL, Nevents["Data_"+Tagger+"LeadQuantile"], chi2, Nevents["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"], Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"]) # calculates Z+HF fractions
    #
    #####################################
    ## Scale template distributions
    #####################################
    #if model_type == 'LBC':
    #    Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"].Scale(nZb.getVal()/Histograms["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"].Integral())
    #    Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"].Scale(nZc.getVal()/Histograms["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"].Integral())
    #else:
    #    Histograms["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"].Scale(nZHF.getVal()/Histograms["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"].Integral())
    #Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"].Scale(nZL.getVal()/Histograms["MC_Signal_FlavL_"+Tagger+"LeadQuantile"].Integral())

    ###################
    ## Show fit result
    ###################
    #print("#############################################")
    #print(">>>>>>>>>>>>>>> FIT RESULT <<<<<<<<<<<<<<<<<<")
    #print("Status: "+str(Result.status))
    #if model_type == 'LBC':
    #    print("fZb: "+str(Result.fZb))
    #    print("fZberror: "+str(Result.fZbError))
    #    print("fZc: "+str(Result.fZc))
    #    print("fZcerror: "+str(Result.fZcError))
    #    print("fZL: "+str(Result.fZL))
    #    print("fZLError: "+str(Result.fZLError))
    #    print("SF_Zb: "+str(Result.SF_Zb))
    #    print("SF_Zc: "+str(Result.SF_Zc))
    #else:
    #    print("fZHF: "+str(Result.fZb))
    #    print("fZHFerror: "+str(Result.fZHFerror))
    #    print("fZL: "+str(Result.fZL))
    #    print("fZLError: "+str(Result.fZLerror))
    #    print("SF_ZHF: "+str(Result.SF_ZHF))
    #print("SF_ZL: "+str(Result.SF_ZL))
    #print("chi2: "+str(Result.chi2))
    #print("#############################################")

    ###################
    ## Paper-like plot
    ###################
    #canvas = TCanvas()
    #canvas.SetTopMargin(0.06);
    #outputname = "Plots/PaperPlot_data"
    #for dataset in Datasets:
    #    outputname += dataset
    #outputname += "_{}.pdf".format(model_type)
    #canvas.Print(outputname+"[")
    #x3frame = x.frame()
    #hData.plotOn(x3frame,RooFit.Name("DataPlot"),RooFit.LineColor(kBlack),RooFit.MarkerColor(kBlack),RooFit.LineWidth(2))
    #if model_type == 'LBC':
    #    Model.plotOn(x3frame, RooFit.Name('ZbPlot'), RooFit.Components("PDFZb"), RooFit.LineColor(kGreen+2), RooFit.LineStyle(kDashed))
    #    Model.plotOn(x3frame, RooFit.Name('ZcPlot'), RooFit.Components("PDFZc"), RooFit.LineColor(kRed), RooFit.LineStyle(kDashed))
    #else:
    #    Model.plotOn(x3frame, RooFit.Name('ZHFPlot'), RooFit.Components("PDFZHF"), RooFit.LineColor(kGreen+2), RooFit.LineStyle(kDashed))
    #Model.plotOn(x3frame,RooFit.Name('ZLPlot'),RooFit.Components("PDFZL"),RooFit.LineColor(kMagenta), RooFit.LineStyle(kDashed))
    #counter = 0
    #for bkg in Backgrounds:
    #    Model.plotOn(x3frame,RooFit.Name(bkg+'Plot'),RooFit.Components("PDF"+bkg),RooFit.LineColor(MyColors[counter]), RooFit.LineStyle(kDashed))
    #    counter += 1
    #Model.plotOn(x3frame, RooFit.Name("ModelPlot"), RooFit.LineColor(kBlue))
    #x3frame.Draw()
    #Legends = TLegend(0.2,0.6,0.4,0.9)
    #Legends.SetTextFont(42)
    #Legends.AddEntry(x3frame.findObject("DataPlot"), "Data", "p")
    #Legends.AddEntry(x3frame.findObject("ModelPlot"), "Model", "l")
    #if model_type == 'LBC':
    #    Legends.AddEntry(x3frame.findObject("ZbPlot"), "Z+b", "l")
    #    Legends.AddEntry(x3frame.findObject("ZcPlot"), "Z+c", "l")
    #else:
    #    Legends.AddEntry(x3frame.findObject("ZHFPlot"), "Z+HF", "l")
    #Legends.AddEntry(x3frame.findObject("ZLPlot"),"Z+Light","l")
    #for bkg in Backgrounds:
    #    Legends.AddEntry(x3frame.findObject(bkg+"Plot"), bkg+" quark", "l")
    #Legends.Draw("same")
    #TextBlockchi = TLatex(0.4,0.7,"#scale[0.8]{#chi^{2} / ndf = "+str(round(Result.chi2,2))+"}")
    #if model_type == 'LBC':
    #    TextBlockb   = TLatex(0.4,0.75,"#scale[0.8]{Z+b frac = "+str(round(Result.fZb,2))+" #pm "+str(round(Result.fZbError,3))+"}")
    #    TextBlockc   = TLatex(0.4,0.8,"#scale[0.8]{Z+c frac = "+str(round(Result.fZc,2))+" #pm "+str(round(Result.fZcError,3))+"}")
    #    TextBlockL   = TLatex(0.4,0.85,"#scale[0.8]{Z+L frac = "+str(round(Result.fZL,2))+" #pm "+str(round(Result.fZLError,3))+"}")
    #    TextBlockb.SetNDC()
    #    TextBlockc.SetNDC()
    #    TextBlockL.SetNDC()
    #    TextBlockchi.SetNDC()
    #    TextBlockb.Draw("same")
    #    TextBlockc.Draw("same")
    #    TextBlockL.Draw("same")
    #    TextBlockchi.Draw("same")
    #else:
    #    TextBlockHF = TLatex(0.4, 0.75, "#scale[0.8]{Z+HF frac = "+str(round(Result.fZHF, 2))+" #pm "+str(round(Result.fZHFerror, 3))+"}")
    #    TextBlockL = TLatex(0.4, 0.8, "#scale[0.8]{Z+L frac = "+str(round(Result.fZL, 2))+" #pm "+str(round(Result.fZLerror, 3))+"}")
    #    TextBlockHF.SetNDC()
    #    TextBlockL.SetNDC()
    #    TextBlockchi.SetNDC()
    #    TextBlockHF.Draw("same")
    #    TextBlockL.Draw("same")
    #    TextBlockchi.Draw("same")
    #canvas.Print(outputname)
    #canvas.Print(outputname+"]")

    ####################################
    ## Save all distributions
    ####################################
    #OutputFileName = "Testing_"+Set+"_{}.root".format(model_type)
    #OutputFile = TFile(OutputFileName, "RECREATE")
    #for Hist in Histograms.values():
    #    Hist.Write()
    #OutputFile.Close()

    ################
    ## Closure test
    ################
    #if ClosureTest:
    #  print "###############################################################"
    #  print "###############################################################"
    #  print "INFO: Run Closure Test"
    #  print "###############################################################"
    #  print "###############################################################"
    #  Results = []
    #  counter = 1
    #  for ZbFrac in ZbFracs4ClosureTest:
    #    for ZcFrac in ZcFracs4ClosureTest:
    #      ############################################################
    #      # Generate psuedo-data using a model with desired fractions
    #      ############################################################
    #      nExtractedData = Nevents["Data_"+Tagger+"Weight"] - totalFixedBkgEvents
    #      if nExtractedData <=0:
    #        print "WARNING: nExtractedData <= 0 !"
    #        print "WARNING: nExtractedData = "+str(nExtractedData)
    #        print "WARNING: nData          = "+str(Nevents["Data_"+Tagger+"Weight"])
    #        print "WARNING: nBkg           = "+str(totalFixedBkgEvents)
    #      fixedZbEvents  = ZbFrac * nExtractedData if nExtractedData > 0 else 0
    #      fixedZcEvents  = ZcFrac * nExtractedData if nExtractedData > 0 else 0
    #      fixedZLEvents  = (1 - ZbFrac - ZcFrac) * nExtractedData if nExtractedData > 0 else 0
    #      nZb            = RooRealVar("nZb","nZb",fixedZbEvents)                # fixed number of Z+b events
    #      nZc            = RooRealVar("nZc","nZc",fixedZcEvents)                # fixed number of Z+c events
    #      nZL            = RooRealVar("nZL","nZL",fixedZLEvents)                # fixed number of Z+L events
    #      nBkgs = dict()
    #      for bkg in Backgrounds: nBkgs[bkg] = RooRealVar("n"+bkg,"n"+bkg,fixedBkgEvents[bkg])  # fixed number of events for this background
    #      FakeModel, MSG = makeModel("fakeModel",Backgrounds,PDFZb,PDFZc,PDFZL,PDFBkgs,nZb,nZc,nZL,nBkgs)
    #      if MSG != 'OK': print MSG
    #      hData = FakeModel.generateBinned(RooArgSet(x),Nevents["Data_"+Tagger+"Weight"])
    #
    #      ######################
    #      # Create model to fit
    #      ######################
    #      initialZbEvents = 0.3 * nExtractedData
    #      initialZcEvents = 0.3 * nExtractedData
    #      initialZLEvents = Nevents["Data_"+Tagger+"Weight"] - totalFixedBkgEvents - initialZbEvents -initialZcEvents
    #      nZb      = RooRealVar("nZb","nZb",initialZbEvents,0.,Nevents["Data_"+Tagger+"Weight"]) # initial number of Z+b events
    #      nZc      = RooRealVar("nZc","nZc",initialZcEvents,0.,Nevents["Data_"+Tagger+"Weight"]) # initial number of Z+c events
    #      nZL      = RooRealVar("nZL","nZL",initialZLEvents,0.,Nevents["Data_"+Tagger+"Weight"]) # initial number of Z+L events
    #      nBkgs = dict()
    #      for bkg in Backgrounds: nBkgs[bkg] = RooRealVar("n"+bkg,"n"+bkg,fixedBkgEvents[bkg])  # fixed number of events for this background
    #      Model, MSG = makeModel("model",Backgrounds,PDFZb,PDFZc,PDFZL,PDFBkgs,nZb,nZc,nZL,nBkgs)
    #      if MSG != 'OK': print MSG
    #      #Model.getVariables().Print("v")
    #
    #      ####################
    #      # Fit model to data
    #      ####################
    #      fitResult = Model.fitTo(hData, RooFit.Extended(True), RooFit.Save(), RooFit.SumW2Error(True))
    #
    #      #############################
    #      # Show model fit to data
    #      #############################
    #      #canvas = TCanvas()
    #      #outputname = "Plots/FitResult_data"
    #      #for dataset in Datasets: outputname += dataset
    #      #outputname += ".pdf"
    #      #canvas.Print(outputname+"[")
    #      #x2frame = x.frame()
    #      #x2frame.SetTitle("")
    #      #hData.plotOn(x2frame,RooFit.Name("data"),RooFit.LineColor(kBlack),RooFit.MarkerColor(kBlack))
    #      #Model.plotOn(x2frame,RooFit.Name("model"),RooFit.LineColor(kGreen+2),RooFit.LineStyle(kDashed))
    #      #x2frame.Draw()
    #      #canvas.Print(outputname)
    #      #canvas.Print(outputname+"]")
    #
    #      ##############################################
    #      # Calculate chi2 and calculate Z+HF fractions
    #      ##############################################
    #      #chi2   = x2frame.chiSquare("model","data",3)
    #      chi2   = x2frame.chiSquare("model","data")
    #      Result = FitResult(fitResult.status,nZb,nZc,nZL,Nevents["Data_"+Tagger+"Weight"],chi2,fixedZbEvents,fixedZcEvents,fixedZLEvents,Nevents["Data_"+Tagger+"Weight"]-totalFixedBkgEvents) # calculates Z+HF fractions FIXME SF are wrong
    #      Results.append(Result)
    #
    #      ##################
    #      # Paper-like plot
    #      ##################
    #      canvas = TCanvas()
    #      canvas.SetTopMargin(0.06);
    #      if counter < 10: strCounter = "0"+str(counter)
    #      else: strCounter = str(counter)
    #      outputname = "Plots/ClosureTest_"+strCounter+"_data"
    #      for dataset in Datasets: outputname += dataset
    #      outputname += ".pdf"
    #      canvas.Print(outputname+"[")
    #      x3frame = x.frame()
    #      hData.plotOn(x3frame,RooFit.Name("DataPlot"),RooFit.LineColor(kBlack),RooFit.MarkerColor(kBlack),RooFit.LineWidth(2))
    #      Model.plotOn(x3frame,RooFit.Name('ZbPlot'),RooFit.Components("PDFZb"),RooFit.LineColor(kGreen+2),RooFit.LineStyle(kDashed))
    #      Model.plotOn(x3frame,RooFit.Name('ZcPlot'),RooFit.Components("PDFZc"),RooFit.LineColor(kRed), RooFit.LineStyle(kDashed))
    #      Model.plotOn(x3frame,RooFit.Name('ZLPlot'),RooFit.Components("PDFZL"),RooFit.LineColor(kMagenta), RooFit.LineStyle(kDashed))
    #      counterColor = 0
    #      for bkg in Backgrounds:
    #        Model.plotOn(x3frame,RooFit.Name(bkg+'Plot'),RooFit.Components("PDF"+bkg),RooFit.LineColor(MyColors[counterColor]), RooFit.LineStyle(kDashed))
    #        counterColor += 1
    #      Model.plotOn(x3frame,RooFit.Name("ModelPlot"),RooFit.LineColor(kBlue))
    #      x3frame.Draw()
    #      Legends = TLegend(0.2,0.6,0.4,0.9)
    #      Legends.SetTextFont(42)
    #      Legends.AddEntry(x3frame.findObject("DataPlot"),"Data","p")
    #      Legends.AddEntry(x3frame.findObject("ModelPlot"),"Model","l")
    #      Legends.AddEntry(x3frame.findObject("ZbPlot"),"Z+b","l")
    #      Legends.AddEntry(x3frame.findObject("ZcPlot"),"Z+c","l")
    #      Legends.AddEntry(x3frame.findObject("ZLPlot"),"Z+Light","l")
    #      for bkg in Backgrounds: Legends.AddEntry(x3frame.findObject(bkg+"Plot"),bkg+" quark","l")
    #      Legends.Draw("same")
    #      TextBlockchi = TLatex(0.4,0.65,"#scale[0.8]{#chi^{2} / ndf = "+str(round(Result.chi2,2))+"}")
    #      TextBlockib  = TLatex(0.4,0.7,"#scale[0.8]{injected Z+b frac = "+str(round(Result.initialfZb,2))+"}")
    #      TextBlockic  = TLatex(0.4,0.75,"#scale[0.8]{injected Z+c frac = "+str(round(Result.initialfZc,2))+"}")
    #      TextBlockb   = TLatex(0.4,0.8,"#scale[0.8]{extracted Z+b frac = "+str(round(Result.fZb,3))+" #pm "+str(round(Result.fZbError,3))+"}")
    #      TextBlockc   = TLatex(0.4,0.85,"#scale[0.8]{extracted Z+c frac = "+str(round(Result.fZc,3))+" #pm "+str(round(Result.fZcError,3))+"}")
    #      TextBlockib.SetNDC()
    #      TextBlockic.SetNDC()
    #      TextBlockb.SetNDC()
    #      TextBlockc.SetNDC()
    #      TextBlockchi.SetNDC()
    #      TextBlockib.Draw("same")
    #      TextBlockic.Draw("same")
    #      TextBlockb.Draw("same")
    #      TextBlockc.Draw("same")
    #      TextBlockchi.Draw("same")
    #      canvas.Print(outputname)
    #      canvas.Print(outputname+"]")
    #      counter +=1
    #
    #  ############################
    #  # Show closure test results
    #  ############################
    #  print "#######################################################"
    #  print ">>>>>>>>>>>>>>> CLOSURE TEST RESULTS <<<<<<<<<<<<<<<<<<"
    #  print "#######################################################"
    #  counter = 1
    #  for result in Results:
    #    ZcFracNonClosureUncert = abs((result.fZc-result.initialfZc)/result.initialfZc)
    #    if ZcFracNonClosureUncert > maxZcFracNonClosureUncertainty: maxZcFracNonClosureUncertainty = ZcFracNonClosureUncert
    #    print "######################################################"
    #    print ">>>>>>>>>>>>>>> CLOSURE TEST | FIT RESULT # "+str(counter)+" <<<<<<<<<<<<<<<<<<"
    #    print "Status: "+str(result.status)
    #    print "initialZb: "+str(result.initialfZb)
    #    print "fZb: "+str(result.fZb)
    #    print "initialZc: "+str(result.initialfZc)
    #    print "fZc: "+str(result.fZc)
    #    print "chi2: "+str(result.chi2)
    #    print "######################################################"
    #    counter += 1


    ###################################################################################
    ## Flavour fractions vs observables
    ###################################################################################
    if vsAllObservables:
        # Create output file
        OutputFileName = "Fractions_vs_observables_data"
        for dataset in Datasets:
            OutputFileName += dataset
        OutputFileName += "_{}.root".format(model_type)
        OutputFile = TFile(OutputFileName, "RECREATE")

        # Collect fit results for all observables
        Results = dict()

        for var in Observables:

            if 'Te1' not in Observables[var]: continue  # Temporary (skip two-b-tagged-jet observables)
            obs = var + '_Ti1'

            print("###############################################################")
            print("###############################################################")
            print("INFO: Extract flavour fractions for "+obs)
            print("###############################################################")
            print("###############################################################")

            # Collect fit results for each bin
            Results[obs] = dict()

            # Number of bins for this observable
            obsbase = obs.replace('_Ti1','')
            nObsBins = len(Binning[obsbase])-1 if len(Binning[obsbase]) > 3 else Binning[obsbase][0]
            if Debug:
                print("DEBUG: Number of bins for "+obs+": "+str(nObsBins))

            vsObs = '_vs_'+obs

            ############################
            # Get input TH2D histograms
            ############################
            Histograms_2D = dict() # collect all the TH2D histograms for this observable
            for FileKey, FileName in Files.items(): # Loop over input files
                # open file
                File = TFile.Open(FileName)
                File.cd()
                if not File:
                    print(FileName+" not found, exiting")
                    sys.exit(0)
                # get histograms
                for HistName in HistNames[FileKey]:
                    HistName = HistName+vsObs
                    hist = File.Get(HistName)
                    if not hist:
                        print(HistName+" not found in "+FileName+", exiting")
                        sys.exit(0)
                    hist.SetDirectory(0) # decouple from input file
                    # scale MC histograms by luminosity
                    if "MC16a" in FileKey:
                        hist.Scale(Luminosity["a"])
                    elif "MC16d" in FileKey:
                        hist.Scale(Luminosity["d"])
                    elif "MC16e" in FileKey:
                        hist.Scale(Luminosity["e"])
                    Histograms_2D[FileKey+"_"+HistName] = hist # collect histogram
                File.Close()
            # Create key which is needed later
            Histograms_2D["Data_"+Tagger+"LeadQuantile"+vsObs]= 0
            Histograms_2D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs] = 0
            Histograms_2D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs] = 0
            if model_type == 'LHF':
                Histograms_2D["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs] = 0
            Histograms_2D["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs] = 0
            for bkg in Backgrounds:
                Histograms_2D["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs] = 0

            ###############################
            # Prepare final data histogram
            ###############################
            if Debug: print("DEBUG: prepare final data histogram")
            counter = 0
            for key, histogram in Histograms_2D.items():
                if "Data" in key and key != "Data_"+Tagger+"LeadQuantile"+vsObs:
                    if not counter:
                        Histograms_2D["Data_"+Tagger+"LeadQuantile"+vsObs] = histogram.Clone("Data_"+Tagger+"LeadQuantile"+vsObs)
                    else:
                        Histograms_2D["Data_"+Tagger+"LeadQuantile"+vsObs].Add(histogram)
                    counter += 1

            if Debug:  # Show number of data events
                for key, histogram in Histograms_2D.items():
                    if "Data_" in key: print("DEBUG: Nevents("+key+"): "+str(histogram.Integral()))

            ###############################
            # Prepare final MC histograms
            ###############################
            if Debug: print("DEBUG: prepare final MC histograms")
            # MC signal
            for counter, campaign in enumerate(Campaigns):
                if not counter:
                  Histograms_2D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs] = Histograms_2D["MC16"+campaign+"_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs].Clone("MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs)
                  Histograms_2D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs] = Histograms_2D["MC16"+campaign+"_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs].Clone("MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs)
                  Histograms_2D["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs] = Histograms_2D["MC16"+campaign+"_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs].Clone("MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs)
                else:
                  Histograms_2D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs].Add(Histograms_2D["MC16"+campaign+"_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs])
                  Histograms_2D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs].Add(Histograms_2D["MC16"+campaign+"_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs])
                  Histograms_2D["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs].Add(Histograms_2D["MC16"+campaign+"_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs])
            if model_type == 'LHF':
                # sum FlavA1B and FlavA1C
                Histograms_2D["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs] = Histograms_2D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs].Clone("MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs)
                Histograms_2D["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs].Add(Histograms_2D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs])
            # Backgrounds
            for bkg in Backgrounds:
                for counter, campaign in enumerate(Campaigns):
                    if not counter:
                        Histograms_2D["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs] = Histograms_2D["MC16"+campaign+"_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs].Clone("MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs)
                    else:
                        Histograms_2D["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs].Add(Histograms_2D["MC16"+campaign+"_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs])

            #####################################
            # Loop over bins for this observable
            #####################################
            for obsBin in range(0, nObsBins):
                # Several things bin related that are needed
                HistBin = obsBin + 1
                vsBin = '__'+str(HistBin) if HistBin > 9 else '__0'+str(HistBin)
                Bin = str(HistBin)
                if len(Binning[obsbase]) > 3:
                    obsMin = Binning[obsbase][obsBin]
                    obsMax = Binning[obsbase][obsBin+1]
                else:
                    obsMin = Binning[obsbase][1] + ( ( (Binning[obsbase][2]-Binning[obsbase][1])/nObsBins ) * obsBin )
                    obsMax = Binning[obsbase][1] + ( ( (Binning[obsbase][2]-Binning[obsbase][1])/nObsBins ) * (obsBin+1) )

                # Get projections (PBCT bin distribution for this bin)
                Histograms_1D = dict()  # Projection for each sample
                Nevents = dict()  # collect number of events for each sample and for this bin
                for key, hist in Histograms_2D.items():
                    FullHist = hist.Clone(hist.GetName()+'_tmp')
                    Histograms_1D[key] = FullHist.ProjectionY(hist.GetName()+vsBin, HistBin, HistBin)
                    Nevents[key] = Histograms_1D[key].Integral()
                    if Debug:
                        print("#######################################################")
                        print("DEBUG: key: "+key)
                        print("DEBUG: Bin: "+Bin)
                        print("DEBUG: ObsMin: "+str(obsMin))
                        print("DEBUG: ObsMax: "+str(obsMax))
                        print("DEBUG: Nevents: "+str(Nevents[key]))
                        print("#######################################################")

                # Get total number of events in data
                Nevents["Data_"+Tagger+"LeadQuantile"+vsObs] = Histograms_1D["Data_"+Tagger+"LeadQuantile"+vsObs].Integral()
                # Get total number of predicted (MC) events
                if model_type == 'LBC':
                    Nevents["TotalMC"] = Nevents["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs] + Nevents["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs] + Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs]
                else:
                    Nevents["TotalMC"] = Nevents["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs] + Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs]
                for bkg in Backgrounds:
                    Nevents["TotalMC"] += Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs]
                # Protection (low number of predicted number of events by MC)
                if Nevents["TotalMC"] < 100:
                    print("WARNING: Bin ["+str(obsMin)+","+str(obsMax)+"] for "+obs+" is skipped because the number of predicted MC events is less than 100")
                    continue

                if Debug:
                    print("##############################################################")
                    print("DEBUG: Number of events before fitting")
                    print("DEBUG: obs: "+obs)
                    print("DEBUG: Bin = "+Bin)
                    print("DEBUG: obsMin = "+str(obsMin))
                    print("DEBUG: obsMax = "+str(obsMax))
                    print("DEBUG: Data: "+str(Nevents["Data_"+Tagger+"LeadQuantile"+vsObs]))
                    print("DEBUG: TotalMC: "+str(Nevents["TotalMC"]))
                    print("DEBUG: Z+b: "+str(Nevents["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs]))
                    print("DEBUG: Z+c: "+str(Nevents["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs]))
                    print("DEBUG: Z+L: "+str(Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs]))
                    for bkg in Backgrounds:
                        print("DEBUG: "+bkg+": "+str(Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs]))
                        print("##############################################################")

                #########################
                # Prepare PDFs and Data
                #########################
                x = RooRealVar("x", "PBCT quantile distribution", xMin, xMax)
                hData = RooDataHist("hData", "", RooArgList(x), Histograms_1D["Data_"+Tagger+"LeadQuantile"+vsObs])
                if model_type == 'LBC':
                    hZb = RooDataHist("hZb", "", RooArgList(x), Histograms_1D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs])
                    hZc = RooDataHist("hZc", "", RooArgList(x), Histograms_1D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs])
                else:
                    hZHF = RooDataHist("hZHF", "", RooArgList(x), Histograms_1D["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs])
                hZL = RooDataHist("hZL", "", RooArgList(x), Histograms_1D["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs])
                hBkgs = dict()
                for bkg in Backgrounds:
                    hBkgs[bkg] = RooDataHist("h"+bkg, "", RooArgList(x), Histograms_1D["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs])
                if model_type == 'LBC':
                    PDFZb = RooHistPdf("PDFZb", "", RooArgSet(x), hZb)
                    PDFZc = RooHistPdf("PDFZc", "", RooArgSet(x), hZc)
                else:
                    PDFZHF = RooHistPdf("PDFZHF", "", RooArgSet(x), hZHF)
                PDFZL = RooHistPdf("PDFZL", "", RooArgSet(x), hZL)
                PDFBkgs = dict()
                for bkg in Backgrounds:
                    PDFBkgs[bkg] = RooHistPdf("PDF"+bkg, "", RooArgSet(x), hBkgs[bkg])

                ###############
                # Create model
                ###############
                fixedBkgEvents = dict()
                for bkg in Backgrounds:
                    fixedBkgEvents[bkg] = Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs]
                totalFixedBkgEvents = 0
                for bkg in Backgrounds:
                    totalFixedBkgEvents += fixedBkgEvents[bkg]
                if Debug:
                  print("DEBUG: totalFixedBkgEvents = "+str(totalFixedBkgEvents))
                  print("DEBUG: DataEvents          = "+str(Nevents["Data_"+Tagger+"LeadQuantile"+vsObs]))
                  print("DEBUG: DataSubtracted (expected Z+jets) = "+str(Nevents["Data_"+Tagger+"LeadQuantile"+vsObs]-totalFixedBkgEvents))
                nExtractedData =  Nevents["Data_"+Tagger+"LeadQuantile"+vsObs] - totalFixedBkgEvents
                if nExtractedData <=0:
                    print("WARNING: nExtractedData <= 0 !")
                    print("WARNING: Observable: "+obs)
                    print("WARNING: Bin = "+Bin)
                    print("WARNING: obsMin = "+str(obsMin))
                    print("WARNING: obsMax = "+str(obsMax))
                    print("WARNING: nExtractedData = "+str(nExtractedData))
                    print("WARNING: nData          = "+str(Nevents["Data_"+Tagger+"LeadQuantile"+vsObs]))
                    print("WARNING: nBkg           = "+str(totalFixedBkgEvents))
                if model_type == 'LBC':
                    initialZbEvents = 0.3 * nExtractedData
                    initialZcEvents = 0.3 * nExtractedData
                    initialZLEvents = nExtractedData - initialZbEvents - initialZcEvents
                else:
                    initialZHFEvents = 0.6 * nExtractedData
                    initialZLEvents = nExtractedData - initialZHFEvents
                if model_type == 'LBC':
                    nZb = RooRealVar("nZb", "nZb", initialZbEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"+vsObs])  # initial number of Z+b events
                    nZc = RooRealVar("nZc", "nZc", initialZcEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"+vsObs])  # initial number of Z+c events
                else:
                    nZHF = RooRealVar("nZHF", "nZHF", initialZHFEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"+vsObs])  # initial number of Z+HF events
                nZL = RooRealVar("nZL", "nZL", initialZLEvents, 0., Nevents["Data_"+Tagger+"LeadQuantile"+vsObs])  # initial number of Z+L events
                nBkgs = dict()
                for bkg in Backgrounds:
                    nBkgs[bkg] = RooRealVar("n"+bkg, "n"+bkg, fixedBkgEvents[bkg])  # fixed number of events in background
                if model_type == 'LBC':
                    Model, MSG = makeLBCModel("model", Backgrounds, PDFZb, PDFZc, PDFZL, PDFBkgs, nZb, nZc, nZL, nBkgs)
                else:
                    Model, MSG = makeLHFModel("model", Backgrounds, PDFZHF, PDFZL, PDFBkgs, nZHF, nZL, nBkgs)
                if MSG != 'OK':
                    print(MSG)
                #Model.getVariables().Print("v")

                ####################
                # Fit model to data
                ####################
                fitResult = Model.fitTo(hData, RooFit.Extended(True), RooFit.Save(), RooFit.SumW2Error(True))
                # Protection (extracted Ztotal == 0)
                if model_type == 'LBC':
                    if nZb.getVal() + nZc.getVal() + nZL.getVal() == 0: continue
                else:
                    if nZHF.getVal() + nZL.getVal() == 0: continue

                #############################
                # Show model fit to data
                #############################
                canvas = TCanvas()
                outputname = "Plots/FitResult_data"
                for dataset in Datasets:
                    outputname += dataset
                outputname += "_"+vsObs+vsBin+"_{}.pdf".format(model_type)
                canvas.Print(outputname+"[")
                x2frame = x.frame()
                x2frame.SetTitle("")
                hData.plotOn(x2frame,RooFit.Name("data"),RooFit.LineColor(kBlack),RooFit.MarkerColor(kBlack))
                Model.plotOn(x2frame,RooFit.Name("model"),RooFit.LineColor(kGreen+2),RooFit.LineStyle(kDashed))
                x2frame.Draw()
                canvas.Print(outputname)
                canvas.Print(outputname+"]")

                ##############################################
                # Calculate chi2 and calculate Z+HF fractions
                ##############################################
                #chi2   = x2frame.chiSquare("model","data",3)
                chi2 = x2frame.chiSquare("model", "data")
                if model_type == 'LBC':
                    Result = LBCFitResult(fitResult.status, nZb, nZc, nZL, Nevents["Data_"+Tagger+"LeadQuantile"+vsObs], chi2, Nevents["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs], Nevents["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs], Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs])  # calculates Z+HF fractions
                else:
                    Result = LHFFitResult(
                        fitResult.status,
                        nZHF,
                        nZL,
                        Nevents["Data_"+Tagger+"LeadQuantile"+vsObs],
                        chi2,
                        Nevents["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs],
                        Nevents["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs],
                    )  # calculates Z+HF fractions
                if Result.status:
                    Results[obs][HistBin] = Result

                ###############
                # Protections
                ###############
                nTotalBkgs = 0
                for bkg in Backgrounds:
                    nTotalBkgs += nBkgs[bkg].getVal()
                if model_type == 'LBC':
                    nExtracted = nZb.getVal() + nZc.getVal() + nZL.getVal() + nTotalBkgs
                else:
                    nExtracted = nZHF.getVal() + nZL.getVal() + nTotalBkgs
                if abs( nExtracted - Nevents["Data_"+Tagger+"LeadQuantile"+vsObs] ) > (0.01*Nevents["Data_"+Tagger+"LeadQuantile"+vsObs]): # find differences larger than 1%
                    print("######################################################################################################")
                    print("ERROR: the total number of extracted events does not match with the total number of events in data")
                    print("obs: "+obs)
                    print("obs bin: "+Bin)
                    if model_type == 'LBC':
                        print("nZb: "+str(nZb.getVal()))
                        print("nZc: "+str(nZc.getVal()))
                    else:
                        print("nZHF: "+str(nZHF.getVal()))
                    print("nZL: "+str(nZL.getVal()))
                    for bkg in Backgrounds:
                        print("n"+bkg+": "+str(nBkgs[bkg].getVal()))
                    print("nTotalExtracted: "+str(nExtracted))
                    print("nData: "+str(Nevents["Data_"+Tagger+"LeadQuantile"+vsObs]))
                    print("######################################################################################################")
                for bkg in Backgrounds:
                    if nBkgs[bkg].getVal() != Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs]:
                        print("######################################################################################################")
                        print("ERROR: the number of "+bkg+" events changed after the fit and it shouldn't!")
                        print("nExtracted: "+str(nBkgs[bkt].getVal()))
                        print("nInjected: "+str(Nevents["MC_"+bkg+"_"+Tagger+"LeadQuantile"+vsObs]))
                        print("######################################################################################################")

                ####################################
                # Scale template distributions
                ####################################
                if model_type == 'LBC':
                    Histograms_1D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs].Scale(nZb.getVal()/Histograms_1D["MC_Signal_FlavA1B_"+Tagger+"LeadQuantile"+vsObs].Integral())
                    Histograms_1D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs].Scale(nZc.getVal()/Histograms_1D["MC_Signal_FlavA1C_"+Tagger+"LeadQuantile"+vsObs].Integral())
                else:
                    Histograms_1D["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs].Scale(nZHF.getVal()/Histograms_1D["MC_Signal_FlavA1HF_"+Tagger+"LeadQuantile"+vsObs].Integral())
                Histograms_1D["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs].Scale(nZL.getVal()/Histograms_1D["MC_Signal_FlavL_"+Tagger+"LeadQuantile"+vsObs].Integral())

                ##################
                # Paper-like plot
                ##################
                canvas = TCanvas()
                canvas.SetTopMargin(0.06);
                outputname = "Plots/PaperPlot"+vsObs+vsBin+"_data"
                for dataset in Datasets:
                    outputname += dataset
                outputname += "_{}.pdf".format(model_type)
                canvas.Print(outputname+"[")
                x3frame = x.frame()
                hData.plotOn(x3frame, RooFit.Name("DataPlot"), RooFit.LineColor(kBlack), RooFit.MarkerColor(kBlack), RooFit.LineWidth(2))
                if model_type:
                    Model.plotOn(x3frame, RooFit.Name('ZbPlot'), RooFit.Components("PDFZb"), RooFit.LineColor(kGreen+2), RooFit.LineStyle(kDashed))
                    Model.plotOn(x3frame, RooFit.Name('ZcPlot'), RooFit.Components("PDFZc"), RooFit.LineColor(kRed), RooFit.LineStyle(kDashed))
                else:
                    Model.plotOn(x3frame, RooFit.Name('ZHFPlot'), RooFit.Components("PDFZHF"), RooFit.LineColor(kGreen+2), RooFit.LineStyle(kDashed))
                Model.plotOn(x3frame, RooFit.Name('ZLPlot'), RooFit.Components("PDFZL"), RooFit.LineColor(kMagenta), RooFit.LineStyle(kDashed))
                for counter_color, bkg in enumerate(Backgrounds):
                    Model.plotOn(x3frame, RooFit.Name(bkg+'Plot'), RooFit.Components("PDF"+bkg), RooFit.LineColor(MyColors[counter_color]), RooFit.LineStyle(kDashed))
                Model.plotOn(x3frame, RooFit.Name("ModelPlot"), RooFit.LineColor(kBlue))
                x3frame.SetMinimum(0)
                x3frame.SetMaximum(1.9*(Histograms_1D["Data_"+Tagger+"LeadQuantile"+vsObs].GetMaximum()))
                x3frame.Draw()
                Legends = TLegend(0.2,0.6,0.4,0.9)
                Legends.SetTextFont(42)
                Legends.AddEntry(x3frame.findObject("DataPlot"), "Data", "p")
                Legends.AddEntry(x3frame.findObject("ModelPlot"), "Model", "l")
                if model_type == 'LBC':
                    Legends.AddEntry(x3frame.findObject("ZbPlot"), "Z+b", "l")
                    Legends.AddEntry(x3frame.findObject("ZcPlot"), "Z+c", "l")
                else:
                    Legends.AddEntry(x3frame.findObject("ZHFPlot"), "Z+HF", "l")
                Legends.AddEntry(x3frame.findObject("ZLPlot"), "Z+Light", "l")
                for bkg in Backgrounds: Legends.AddEntry(x3frame.findObject(bkg+"Plot"),bkg+" quark","l")
                Legends.Draw("same")
                TextBlockchi = TLatex(0.4, 0.7, "#scale[0.8]{#chi^{2} / ndf = "+str(round(Result.chi2,2))+"}")
                if model_type == 'LBC':
                    TextBlockb = TLatex(0.4, 0.75, "#scale[0.8]{Z+b frac = "+str(round(Result.fZb,2))+" #pm "+str(round(Result.fZbError,3))+"}")
                    TextBlockc = TLatex(0.4, 0.8, "#scale[0.8]{Z+c frac = "+str(round(Result.fZc,2))+" #pm "+str(round(Result.fZcError,3))+"}")
                    TextBlockL = TLatex(0.4, 0.85, "#scale[0.8]{Z+L frac = "+str(round(Result.fZL,2))+" #pm "+str(round(Result.fZLError,3))+"}")
                    TextBlockObsBin = TLatex(0.4, 0.65, "#scale[0.8]{"+obs+" bin: ["+str(obsMin)+","+str(obsMax)+"] GeV}")
                    TextBlockb.SetNDC()
                    TextBlockc.SetNDC()
                    TextBlockL.SetNDC()
                    TextBlockchi.SetNDC()
                    TextBlockObsBin.SetNDC()
                    TextBlockb.Draw("same")
                    TextBlockc.Draw("same")
                    TextBlockL.Draw("same")
                    TextBlockchi.Draw("same")
                    TextBlockObsBin.Draw("same")
                else:
                    TextBlockHF = TLatex(0.4, 0.75, "#scale[0.8]{Z+HF frac = "+str(round(Result.fZHF, 2))+" #pm "+str(round(Result.fZHFerror, 3))+"}")
                    TextBlockL = TLatex(0.4, 0.8, "#scale[0.8]{Z+L frac = "+str(round(Result.fZL, 2))+" #pm "+str(round(Result.fZLerror, 3))+"}")
                    TextBlockObsBin = TLatex(0.4, 0.65, "#scale[0.8]{"+obs+" bin: ["+str(obsMin)+","+str(obsMax)+"] GeV}")
                    TextBlockHF.SetNDC()
                    TextBlockL.SetNDC()
                    TextBlockchi.SetNDC()
                    TextBlockObsBin.SetNDC()
                    TextBlockHF.Draw("same")
                    TextBlockL.Draw("same")
                    TextBlockchi.Draw("same")
                    TextBlockObsBin.Draw("same")
                canvas.Print(outputname)
                canvas.Print(outputname+"]")

        #############################################
        # Show final results and prepare final plots
        #############################################

        for obs in Results: # loop over observables

            ############################
            # Show results
            ############################
            print("#######################################################")
            print(">>>>>>>>>>>>>> FRACTIONS VS "+obs+" <<<<<<<<<<<<<<<<<")
            print("#######################################################")
            for key,result in Results[obs].iteritems(): # Loop over bins
                print("######################################################")
                print(">>>>>>>>>>>>>>> FRACTIONS VS "+obs+" | BIN # "+str(key)+" <<<<<<<<<<<<<<<<<<")
                print("Status: "+str(result.status))
                print("initialZb: "+str(result.initialfZb))
                print("fZb: "+str(result.fZb))
                print("initialZc: "+str(result.initialfZc))
                print("fZc: "+str(result.fZc))
                print("SF_Zb: "+str(result.SF_Zb))
                print("SF_Zc: "+str(result.SF_Zc))
                print("SF_ZL: "+str(result.SF_ZL))
                print("chi2: "+str(result.chi2))
                print("######################################################")

            ###################################
            # Prepare plot of fractions/SFs vs obs
            ###################################
            # Fill histogram
            obsbase = obs.replace('_Ti1','')
            if len(Binning[obsbase]) > 3:
                if model_type == 'LBC':
                    ZbFractions_vs_pT = TH1D("ZbFractions_vs_"+obs,"",len(Binning[obsbase])-1,Binning_array[obsbase])
                    ZcFractions_vs_pT = TH1D("ZcFractions_vs_"+obs,"",len(Binning[obsbase])-1,Binning_array[obsbase])
                    ZbSF_vs_pT        = TH1D("ZbSF_vs_"+obs,"",len(Binning[obsbase])-1,Binning_array[obsbase])
                    ZcSF_vs_pT        = TH1D("ZcSF_vs_"+obs,"",len(Binning[obsbase])-1,Binning_array[obsbase])
                else:
                    ZHFFractions_vs_pT = TH1D("ZHFFractions_vs_"+obs, "", len(Binning[obsbase])-1, Binning_array[obsbase])
                    ZHFSF_vs_pT        = TH1D("ZHFSF_vs_"+obs, "", len(Binning[obsbase])-1, Binning_array[obsbase])
                ZLSF_vs_pT = TH1D("ZLSF_vs_"+obs,"",len(Binning[obsbase])-1,Binning_array[obsbase])
            else:
                if model_type == 'LBC':
                    ZbFractions_vs_pT = TH1D("ZbFractions_vs_"+obs,"",Binning[obsbase][0],Binning[obsbase][1],Binning[obsbase][2])
                    ZcFractions_vs_pT = TH1D("ZcFractions_vs_"+obs,"",Binning[obsbase][0],Binning[obsbase][1],Binning[obsbase][2])
                    ZbSF_vs_pT        = TH1D("ZbSF_vs_"+obs,"",Binning[obsbase][0],Binning[obsbase][1],Binning[obsbase][2])
                    ZcSF_vs_pT        = TH1D("ZcSF_vs_"+obs,"",Binning[obsbase][0],Binning[obsbase][1],Binning[obsbase][2])
                else:
                    ZHFFractions_vs_pT = TH1D("ZHFFractions_vs_"+obs,"", Binning[obsbase][0], Binning[obsbase][1], Binning[obsbase][2])
                    ZHFSF_vs_pT        = TH1D("ZHFSF_vs_"+obs,"", Binning[obsbase][0], Binning[obsbase][1], Binning[obsbase][2])
                ZLSF_vs_pT = TH1D("ZLSF_vs_"+obs,"",Binning[obsbase][0],Binning[obsbase][1],Binning[obsbase][2])
            for key, result in Results[obs].items():
                if model_type == 'LBC':
                    ZbFractions_vs_pT.SetBinContent(key,result.fZb)
                    ZbFractions_vs_pT.SetBinError(key,result.fZbError)
                    ZcFractions_vs_pT.SetBinContent(key,result.fZc)
                    ZcFractions_vs_pT.SetBinError(key,result.fZcError)
                    ZbSF_vs_pT.SetBinContent(key,result.SF_Zb)
                    ZbSF_vs_pT.SetBinError(key,result.SF_ZbError)
                    ZcSF_vs_pT.SetBinContent(key,result.SF_Zc)
                    ZcSF_vs_pT.SetBinError(key,result.SF_ZcError)
                    ZLSF_vs_pT.SetBinContent(key,result.SF_ZL)
                    ZLSF_vs_pT.SetBinError(key,result.SF_ZLError)
                else:
                    ZHFFractions_vs_pT.SetBinContent(key, result.fZHF)
                    ZHFFractions_vs_pT.SetBinError(key, result.fZHFerror)
                    ZHFSF_vs_pT.SetBinContent(key, result.SF_ZHF)
                    ZHFSF_vs_pT.SetBinError(key, result.SF_ZHFerror)
                    ZLSF_vs_pT.SetBinContent(key, result.SF_ZL)
                    ZLSF_vs_pT.SetBinError(key, result.SF_ZLerror)
            # Plot fractions
            canvas = TCanvas()
            #canvas.SetTopMargin(0.06);
            outputname = "Plots/Fractions_vs_" + obs + "_data"
            for dataset in Datasets:
                outputname += dataset
            outputname += "_{}.pdf".format(model_type)
            canvas.Print(outputname+"[")
            if obsbase in Logx:
                canvas.SetLogx()
            if model_type == 'LBC':
                ZbFractions_vs_pT.SetMarkerColor(kRed)
                ZbFractions_vs_pT.SetLineColor(kRed)
                ZcFractions_vs_pT.SetMarkerColor(kBlue)
                ZcFractions_vs_pT.SetLineColor(kBlue)
                ZcFractions_vs_pT.SetMarkerStyle(24)
            else:
                ZHFFractions_vs_pT.SetMarkerColor(kRed)
                ZHFFractions_vs_pT.SetLineColor(kRed)
            Stack = THStack()
            if model_type == 'LBC':
                Stack.Add(ZbFractions_vs_pT, "p")
                Stack.Add(ZcFractions_vs_pT, "p")
            else:
                Stack.Add(ZHFFractions_vs_pT, "p")
            Stack.Draw("nostack")
            Stack.GetXaxis().SetTitleSize(20)
            Stack.GetXaxis().SetTitleFont(43)
            Stack.GetXaxis().SetLabelFont(43)
            Stack.GetXaxis().SetLabelSize(19)
            Stack.GetXaxis().SetTitle(XaxisTitles[obsbase])
            Stack.GetYaxis().SetTitleSize(20)
            Stack.GetYaxis().SetTitleFont(43)
            Stack.GetYaxis().SetLabelFont(43)
            Stack.GetYaxis().SetLabelSize(19)
            Stack.GetYaxis().SetTitleOffset(1.3)
            Stack.GetYaxis().SetTitle("Fractions")
            Legends = TLegend(0.2,0.2,0.4,0.35)
            Legends.SetTextFont(42)
            Legends.AddEntry(ZbFractions_vs_pT,"Z+b","p")
            Legends.AddEntry(ZcFractions_vs_pT,"Z+c","p")
            Legends.Draw("same")
            years = ''
            for year in Datasets:
                if years == '': years += year
                else: years += '+20'+year
            yearsBlock = TLatex(0.43, 0.2, "Data 20"+years)
            yearsBlock.SetNDC()
            yearsBlock.Draw("same")
            canvas.Print(outputname)
            canvas.Print(outputname+"]")
            # Plot SFs
            canvas = TCanvas()
            #canvas.SetTopMargin(0.06);
            outputname = "Plots/SFs_vs_"+obs+"_data"
            for dataset in Datasets:
                outputname += dataset
            outputname += "_{}.pdf".format(model_type)
            canvas.Print(outputname+"[")
            if obsbase in Logx:
                canvas.SetLogx()
            if model_type == 'LBC':
                ZbSF_vs_pT.SetMarkerColor(kRed)
                ZbSF_vs_pT.SetLineColor(kRed)
                ZcSF_vs_pT.SetMarkerColor(kBlue)
                ZcSF_vs_pT.SetLineColor(kBlue)
                ZcSF_vs_pT.SetMarkerStyle(24)
            else:
                ZHFSF_vs_pT.SetMarkerColor(kRed)
                ZHFSF_vs_pT.SetLineColor(kRed)
            ZLSF_vs_pT.SetMarkerColor(kMagenta)
            ZLSF_vs_pT.SetLineColor(kMagenta)
            ZLSF_vs_pT.SetMarkerStyle(25)
            Stack = THStack()
            Stack.Add(ZbSF_vs_pT,"p")
            Stack.Add(ZcSF_vs_pT,"p")
            Stack.Add(ZLSF_vs_pT,"p")
            Stack.Draw("nostack")
            Stack.GetXaxis().SetTitleSize(20)
            Stack.GetXaxis().SetTitleFont(43)
            Stack.GetXaxis().SetLabelFont(43)
            Stack.GetXaxis().SetLabelSize(19)
            Stack.GetXaxis().SetTitle(XaxisTitles[obsbase])
            Stack.GetYaxis().SetTitleSize(20)
            Stack.GetYaxis().SetTitleFont(43)
            Stack.GetYaxis().SetLabelFont(43)
            Stack.GetYaxis().SetLabelSize(19)
            Stack.GetYaxis().SetTitleOffset(1.3)
            Stack.GetYaxis().SetTitle("SF")
            Legends = TLegend(0.2,0.2,0.4,0.35)
            Legends.SetTextFont(42)
            Legends.AddEntry(ZbSF_vs_pT,"Z+b","p")
            Legends.AddEntry(ZcSF_vs_pT,"Z+c","p")
            Legends.AddEntry(ZLSF_vs_pT,"Z+Light","p")
            Legends.Draw("same")

            # Show data-taking period
            yearsBlock = TLatex(0.43,0.2,"Data 20"+years)
            yearsBlock.SetNDC()
            yearsBlock.Draw("same")
            canvas.Print(outputname)
            canvas.Print(outputname+"]")

            OutputFile.cd()
            if model_type == 'LBC':
                ZbFractions_vs_pT.Write()
                ZcFractions_vs_pT.Write()
                ZbSF_vs_pT.Write()
                ZcSF_vs_pT.Write()
            else:
                ZHFFractions_vs_pT.Write()
                ZHFSF_vs_pT.Write()
            ZLSF_vs_pT.Write()
        OutputFile.Close()

    #print "#################################################################"
    #if ClosureTest: print "Relative non-closure uncertainty on Zc fraction = "+str(maxZcFracNonClosureUncertainty)
    #print "#################################################################"
    #if ClosureTest: # write Zc fraction non-closure uncertainty to a .txt file
    #  outFileName = 'ZcNonClosureUncertainty_data'
    #  for dataset in Datasets: outFileName += dataset
    #  outFileName += '.txt'
    #  outFile = open(outFileName,'w')
    #  outFile.write(str(maxZcFracNonClosureUncertainty))
    #  outFile.close()

print(">>> ALL DONE <<<")
