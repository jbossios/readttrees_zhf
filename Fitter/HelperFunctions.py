from ROOT import *

def makeLBCModel(name, Bkgs, PDFZb, PDFZc, PDFZL, PDFBkgs, nZb, nZc, nZL, nBkgs):
    """Make L+B+C model"""
    # protection
    if len(PDFBkgs) != len(nBkgs):
        msg = 'ERROR in makeModel(): len(PDFBkgs) != len(nBkgs), exiting'
        return 0, msg
    nbkgs = len(PDFBkgs)
    if nbkgs > 6: return 0, 'ERROR in makeModel(): larger number of background than supported, exiting' 
    # make model
    if nbkgs == 0:
        model = RooAddPdf(name, name, RooArgList(PDFZb, PDFZc, PDFZL), RooArgList(nZb, nZc, nZL))
    elif nbkgs == 1:
        model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nZb, nZc, nZL))
    elif nbkgs == 2:
        model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nZb, nZc, nZL))
    elif nbkgs == 3:
        model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nZb, nZc, nZL))
    elif nbkgs == 4:
        model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFBkgs[Bkgs[3]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nBkgs[Bkgs[3]], nZb, nZc, nZL))
    elif nbkgs == 5:
        model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFBkgs[Bkgs[3]], PDFBkgs[Bkgs[4]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nBkgs[Bkgs[3]], nBkgs[Bkgs[4]], nZb, nZc, nZL))
    elif nbkgs == 6:
        model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFBkgs[Bkgs[3]], PDFBkgs[Bkgs[4]], PDFBkgs[Bkgs[5]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nBkgs[Bkgs[3]], nBkgs[Bkgs[4]], nBkgs[Bkgs[5]], nZb, nZc, nZL))
    return model, 'OK'


def makeLHFModel(name, Bkgs, PDFZHF, PDFZL, PDFBkgs, nZHF, nZL, nBkgs):
    """Make L+HF model"""
    # Protection
    if len(PDFBkgs) != len(nBkgs):
        msg = 'ERROR in makeModel(): len(PDFBkgs) != len(nBkgs), exiting'
        return 0, msg
    nbkgs = len(PDFBkgs)
    rooarglist_pdf = RooArgList([PDFBkgs[Bkgs[i]] for i in range(nbkgs)] + [PDFZHF, PDFZL])
    rooarglist_n = RooArgList([nBkgs[Bkgs[i]] for i in range(nbkgs)] + [nZHF, nZL])
    model = RooAddPdf(name, name, rooarglist_pdf, rooarglist_n)
    #if nbkgs > 6: return 0, 'ERROR in makeModel(): larger number of background than supported, exiting'
    ## make model
    #if nbkgs == 0:
    #    model = RooAddPdf(name, name, RooArgList(PDFZb, PDFZc, PDFZL), RooArgList(nZb, nZc, nZL))
    #elif nbkgs == 1:
    #    model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nZb, nZc, nZL))
    #elif nbkgs == 2:
    #    model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nZb, nZc, nZL))
    #elif nbkgs == 3:
    #    model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nZb, nZc, nZL))
    #elif nbkgs == 4:
    #    model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFBkgs[Bkgs[3]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nBkgs[Bkgs[3]], nZb, nZc, nZL))
    #elif nbkgs == 5:
    #    model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFBkgs[Bkgs[3]], PDFBkgs[Bkgs[4]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nBkgs[Bkgs[3]], nBkgs[Bkgs[4]], nZb, nZc, nZL))
    #elif nbkgs == 6:
    #    model = RooAddPdf(name, name, RooArgList(PDFBkgs[Bkgs[0]], PDFBkgs[Bkgs[1]], PDFBkgs[Bkgs[2]], PDFBkgs[Bkgs[3]], PDFBkgs[Bkgs[4]], PDFBkgs[Bkgs[5]], PDFZb, PDFZc, PDFZL), RooArgList(nBkgs[Bkgs[0]], nBkgs[Bkgs[1]], nBkgs[Bkgs[2]], nBkgs[Bkgs[3]], nBkgs[Bkgs[4]], nBkgs[Bkgs[5]], nZb, nZc, nZL))
    return model, 'OK'


def getPtBinMin(array,i):
    ptrange = array[i].split(":")
    return int(ptrange[0])


def getPtBinMax(array,i):
    ptrange = array[i].split(":")
    return int(ptrange[1])
