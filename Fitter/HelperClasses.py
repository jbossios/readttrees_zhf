class LBCFitResult():
    __slots__ = (
    'status',
    'fZb',
    'fZbError',
    'fZc',
    'fZcError',
    'fZL',
    'fZLError',
    'SF_Zb',
    'SF_Zc',
    'SF_ZL',
    'chi2',
    'initialZb',
    'initialZc',
    'initialZL',
    'initialfZb',
    'initialfZc',
    )
    def __init__(
            self,
	    status,
	    nZb,
	    nZc,
	    nZL,
	    Ndata,
	    chi2,
	    initialZb = 0,
	    initialZc = 0,
	    initialZL = 0,
	    initialZtotal=0
	):
        self.status        = status
        self.nZtotal       = nZb.getVal() + nZc.getVal() + nZL.getVal()   # total extracted number of Z+jets events
        self.fZb           = nZb.getVal()   / self.nZtotal                # fraction of Z+b events in Z+jets events
        self.fZc           = nZc.getVal()   / self.nZtotal                # fraction of Z+c events in Z+jets events
        self.fZL           = nZL.getVal()   / self.nZtotal                # fraction of Z+L events in Z+jets events
        self.fZbError      = nZb.getError() / self.nZtotal                # error on the fraction of Z+b events in Z+jets events
        self.fZcError      = nZc.getError() / self.nZtotal                # error on the fraction of Z+c events in Z+jets events
        self.fZLError      = nZL.getError() / self.nZtotal                # error on the fraction of Z+L events in Z+jets events
        self.SF_Zb         = nZb.getVal() / initialZb if initialZb != 0 else 0 # SF to apply to MC Z+b to match data
        self.SF_Zc         = nZc.getVal() / initialZc if initialZc != 0 else 0 # SF to apply to MC Z+c to match data
        self.SF_ZL         = nZL.getVal() / initialZL if initialZL != 0 else 0 # SF to apply to MC Z+c to match data
        self.SF_ZbError    = nZb.getError() / initialZb if initialZb != 0 else 0 # SF to apply to MC Z+b to match data
        self.SF_ZcError    = nZc.getError() / initialZc if initialZc != 0 else 0 # SF to apply to MC Z+c to match data
        self.SF_ZLError    = nZL.getError() / initialZL if initialZL != 0 else 0 # SF to apply to MC Z+c to match data
        self.chi2          = chi2
        self.initialZb     = initialZb # initial number of Z+b events
        self.initialZc     = initialZc # initial number of Z+c events
        self.initialZL     = initialZL # initial number of Z+c events
        self.initialZtotal = initialZb + initialZc + initialZL
        self.initialfZb    = initialZb / self.initialZtotal if self.initialZtotal != 0 else 0 # initial fraction of Z+b events
        self.initialfZc    = initialZc / self.initialZtotal if self.initialZtotal != 0 else 0 # initial fraction of Z+c events

class LHFFitResult():
    __slots__ = (
    'status',
    'fZHF',
    'fZHFerror',
    'fZL',
    'fZLerror',
    'SF_ZHF',
    'SF_ZL',
    'chi2',
    'initialZHF',
    'initialZL',
    'initialfZHF',
    )
    def __init__(self, status, nZHF, nZL, Ndata, chi2, initialZHF=0, initialZL=0, initialZtotal=0):
        self.status = status
        self.nZtotal = nZHF.getVal() + nZL.getVal()  # total extracted number of Z+jets events
        self.fZHF = nZHF.getVal() / self.nZtotal  # fraction of Z+HF events in Z+jets events
        self.fZL = nZL.getVal() / self.nZtotal  # fraction of Z+L events in Z+jets events
        self.fZHFerror = nZHF.getError() / self.nZtotal  # error on the fraction of Z+HF events in Z+jets events
        self.fZLerror = nZL.getError() / self.nZtotal  # error on the fraction of Z+L events in Z+jets events
        self.SF_ZHF = nZHF.getVal() / initialZHF if initialZHF != 0 else 0  # SF to apply to MC Z+HF to match data
        self.SF_ZL = nZL.getVal() / initialZL if initialZL != 0 else 0  # SF to apply to MC Z+c to match data
        self.SF_ZHFerror = nZHF.getError() / initialZHF if initialZHF != 0 else 0  # statistically error on the SF to apply to MC Z+HF to match data
        self.SF_ZLerror = nZL.getError() / initialZL if initialZL != 0 else 0 # statistically error on the SF to apply to MC Z+c to match data
        self.chi2 = chi2
        self.initialZHF = initialZHF  # initial number of Z+HF events
        self.initialZL = initialZL  # initial number of Z+c events
        self.initialZtotal = initialZHF + initialZL
        self.initialfZHF = initialZHF / self.initialZtotal if self.initialZtotal != 0 else 0  # initial fraction of Z+HF events
