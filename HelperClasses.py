from ROOT import TLorentzVector

class iLepton(TLorentzVector):
  def __init__(self):
    TLorentzVector.__init__(self)
    self.SF            = dict()
    self.RecoSF        = dict()
    self.IsoSF         = dict()
    self.PIDSF         = dict() # this is TTVA SF for muons and PID SF for electrons
    self.TrigSF        = dict()
    self.TrigEff       = dict()
    self.isTrigMatched = False
    self.matchedChains = []
    self.isMuon        = False
    self.charge        = 0

class iJet(TLorentzVector):
  def __init__(self):
    TLorentzVector.__init__(self)
    self.isPileup           = False
    self.SF                 = dict()
    self.JvtSF              = dict()
    self.BTaggingSF         = dict()
    self.hadronTruthLabelID = 0
    self.btagQuantile       = 0
